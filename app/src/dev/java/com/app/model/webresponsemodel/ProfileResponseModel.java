package com.app.model.webresponsemodel;

import com.app.model.UserModel;

public class ProfileResponseModel extends AppBaseResponseModel {

    private UserModel data;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}
