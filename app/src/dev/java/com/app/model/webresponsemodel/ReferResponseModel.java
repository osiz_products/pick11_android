package com.app.model.webresponsemodel;


import com.app.model.UserModel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class ReferResponseModel extends AppBaseResponseModel {

    UserModel data;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}
