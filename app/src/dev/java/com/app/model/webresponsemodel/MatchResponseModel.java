package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.MatchModel;

import java.util.List;

public class MatchResponseModel extends AppBaseResponseModel {

    List<MatchModel> data;
    long serverdate;

    public List<MatchModel> getData() {
        return data;
    }

    public void setData(List<MatchModel> data) {
        this.data = data;
    }

    public long getServerdate() {
        return serverdate;
    }

    public void setServerdate(long serverdate) {
        this.serverdate = serverdate;
    }
}
