package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseModel;
import com.app.appbase.AppBaseResponseModel;
import com.app.model.ContestModel;

public class ContestDetailResponseModel extends AppBaseResponseModel {

    public Details details;
    ContestModel data;

    public ContestModel getData() {
        return data;
    }

    public void setData(ContestModel data) {
        this.data = data;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details extends AppBaseModel {

        int totalteams=0;
        int totaljc=0;

        public int getTotalteams() {
            return totalteams;
        }

        public void setTotalteams(int totalteams) {
            this.totalteams = totalteams;
        }

        public int getTotaljc() {
            return totaljc;
        }

        public void setTotaljc(int totaljc) {
            this.totaljc = totaljc;
        }
    }
}
