package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.JoinedTeamModel;

import java.util.List;

public class JoinedTeamResponseModel extends AppBaseResponseModel {

    List<JoinedTeamModel> data;

    public List<JoinedTeamModel> getData() {
        return data;
    }

    public void setData(List<JoinedTeamModel> data) {
        this.data = data;
    }
}
