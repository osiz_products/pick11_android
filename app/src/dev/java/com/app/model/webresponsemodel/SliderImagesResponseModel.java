package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.SliderImagesModel;

import java.util.List;

public class SliderImagesResponseModel extends AppBaseResponseModel {

    List<SliderImagesModel> data;

    public List<SliderImagesModel> getData() {
        return data;
    }

    public void setData(List<SliderImagesModel> data) {
        this.data = data;
    }
}
