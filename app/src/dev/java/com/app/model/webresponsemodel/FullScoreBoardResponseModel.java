package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.fullScoreBoard.FullScoreBoardModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class FullScoreBoardResponseModel extends AppBaseResponseModel {

    FullScoreBoardModel data;

    public FullScoreBoardModel getData() {
        return data;
    }

    public void setData(FullScoreBoardModel data) {
        this.data = data;
    }
}
