package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;


public class EmptyResponseModel extends AppBaseResponseModel {

    Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
