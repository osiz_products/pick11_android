package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseModel;
import com.app.appbase.AppBaseResponseModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;

import java.util.List;

public class PlayerPreviewKabaddiResponseModel extends AppBaseResponseModel {

    List<PlayerPreviewModel> data;

    public List<PlayerPreviewModel> getData() {
        return data;
    }

    public void setData(List<PlayerPreviewModel> data) {
        this.data = data;
    }

    public static class DataBean extends AppBaseModel {
        private String id;
        private String name;
        private String captainid;
        private String vicecaptainid;
        private PlayersBean players;

        private PlayerModel captionModel;
        private PlayerModel vcCaptionModel;

        private String team1_name;
        private String team2_name;
        private long team1Players = -1;
        private long team2Players = -1;

        public void updateData() {
            if (players == null) return;
            if (captionModel == null) {
                captionModel = players.getPlayerModel(captainid);
            }
            if (vcCaptionModel == null) {
                vcCaptionModel = players.getPlayerModel(vicecaptainid);
            }
            updateTeamInfo();
        }

        public void updateTeamInfo() {
            if (team1Players == -1 || team2Players == -1) {
                team1Players = 0;
                team2Players = 0;
                if (players == null) return;
                if (players.getDefender() != null) {
                    for (PlayerModel playerModel : players.getDefender()) {
                        if (playerModel.getTeamname().equals(getTeam1_name())) {
                            team1Players++;
                        } else if (playerModel.getTeamname().equals(getTeam2_name())) {
                            team2Players++;
                        }
                    }

                }

                if (players.getRaider() != null) {
                    for (PlayerModel playerModel : players.getRaider()) {
                        if (playerModel.getTeamname().equals(getTeam1_name())) {
                            team1Players++;
                        } else if (playerModel.getTeamname().equals(getTeam2_name())) {
                            team2Players++;
                        }
                    }

                }

                if (players.getAllrounder() != null) {
                    for (PlayerModel playerModel : players.getAllrounder()) {
                        if (playerModel.getTeamname().equals(getTeam1_name())) {
                            team1Players++;
                        } else if (playerModel.getTeamname().equals(getTeam2_name())) {
                            team2Players++;
                        }
                    }

                }
            }
        }


        public PlayerModel getCaptionModel() {
            return captionModel;
        }

        public PlayerModel getVcCaptionModel() {
            return vcCaptionModel;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return getValidString(name);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCaptainid() {
            return getValidString(captainid);
        }

        public void setCaptainid(String captainid) {
            this.captainid = captainid;
        }

        public String getVicecaptainid() {
            return getValidString(vicecaptainid);
        }

        public void setVicecaptainid(String vicecaptainid) {
            this.vicecaptainid = vicecaptainid;
        }

        public PlayersBean getPlayers() {
            return players;
        }

        public void setPlayers(PlayersBean players) {
            this.players = players;
        }

        public String getTeam1_name() {
            return getValidString(team1_name);
        }

        public void setTeam1_name(String team1_name) {
            this.team1_name = team1_name;
        }

        public String getTeam2_name() {
            return getValidString(team2_name);
        }

        public void setTeam2_name(String team2_name) {
            this.team2_name = team2_name;
        }

        public String getTeam1ShortName() {
            String name = getTeam1_name();
            if (name.length() > 3) {
                return name.substring(0, 3).toUpperCase();
            }
            return name.toUpperCase();
        }


        public String getTeam2ShortName() {
            String name = getTeam2_name();
            if (name.length() > 3) {
                return name.substring(0, 3).toUpperCase();
            }
            return name.toUpperCase();
        }

        public void setTeam2Name(String team2Name) {
            this.team2_name = team2Name;
        }

        public long getTeam1Players() {
            return team1Players;
        }

        public void setTeam1Players(long team1Players) {
            this.team1Players = team1Players;
        }

        public long getTeam2Players() {
            return team2Players;
        }

        public void setTeam2Players(long team2Players) {
            this.team2Players = team2Players;
        }

        public static class PlayersBean {
            private List<PlayerModel> defender;
            private List<PlayerModel> raider;
            private List<PlayerModel> allrounder;

            public List<PlayerModel> getDefender() {
                return defender;
            }

            public void setDefender(List<PlayerModel> defender) {
                this.defender = defender;
            }

            public List<PlayerModel> getRaider() {
                return raider;
            }

            public void setRaider(List<PlayerModel> raider) {
                this.raider = raider;
            }

            public List<PlayerModel> getAllrounder() {
                return allrounder;
            }

            public void setAllrounder(List<PlayerModel> allrounder) {
                this.allrounder = allrounder;
            }

            public PlayerModel getPlayerModel(String playerId) {
                if (defender != null) {
                    for (PlayerModel playerModel : defender) {
                        if (playerModel.getPid().equals(playerId)) {
                            return playerModel;
                        }
                    }

                }

                if (raider != null) {
                    for (PlayerModel playerModel : raider) {
                        if (playerModel.getPid().equals(playerId)) {
                            return playerModel;
                        }
                    }

                }

                if (allrounder != null) {
                    for (PlayerModel playerModel : allrounder) {
                        if (playerModel.getPid().equals(playerId)) {
                            return playerModel;
                        }
                    }

                }
                return null;
            }


            public long getTotalDef() {
                return defender == null ? 0 : defender.size();
            }

            public long getTotalRaid() {
                return raider == null ? 0 : raider.size();
            }

            public long getTotalAr() {
                return allrounder == null ? 0 : allrounder.size();
            }
        }


        @Override
        public String toString() {
            return "Team "+ getName();
        }
    }
}
