package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.MatchScoreModel;

public class MatchScoreResponseModel extends AppBaseResponseModel {

    MatchScoreModel data;

    public MatchScoreModel getData() {
        return data;
    }

    public void setData(MatchScoreModel data) {
        this.data = data;
    }
}
