package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PlayerTypeModel;

import java.util.List;

public class PlayerTypeResponseModel extends AppBaseResponseModel {

    DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        int credits;
        int maxteam;
        int mxfromteam;
        int tmsize;

        public int getCredits() {
            return credits;
        }

        public void setCredits(int credits) {
            this.credits = credits;
        }

        public int getMaxteam() {
            return maxteam;
        }

        public void setMaxteam(int maxteam) {
            this.maxteam = maxteam;
        }

        public int getMxfromteam() {
            return mxfromteam;
        }

        public void setMxfromteam(int mxfromteam) {
            this.mxfromteam = mxfromteam;
        }

        public int getTmsize() {
            return tmsize;
        }

        public void setTmsize(int tmsize) {
            this.tmsize = tmsize;
        }

        List<PlayerTypeModel> list;

        public List<PlayerTypeModel> getList() {
            return list;
        }

        public void setList(List<PlayerTypeModel> list) {
            this.list = list;
        }
    }
}
