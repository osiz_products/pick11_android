package com.app.model.webresponsemodel;

import com.medy.retrofitwrapper.WebServiceBaseResponseModel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class AppBaseResponseModel extends WebServiceBaseResponseModel {

    public String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
