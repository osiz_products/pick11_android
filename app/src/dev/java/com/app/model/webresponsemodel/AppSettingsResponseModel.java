package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.AppSettingsModel;


/**
 * Created by Sunil yadav on 7/8/19.
 */
public class AppSettingsResponseModel extends AppBaseResponseModel {

    AppSettingsModel data;

    public AppSettingsModel getData() {
        return data;
    }

    public void setData(AppSettingsModel data) {
        this.data = data;
    }
}
