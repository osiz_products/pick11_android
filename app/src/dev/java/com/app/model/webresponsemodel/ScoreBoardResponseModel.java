package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.ScoreBoardModel;

import java.util.List;

public class ScoreBoardResponseModel extends AppBaseResponseModel {

    List<ScoreBoardModel> data;

    public List<ScoreBoardModel> getData() {
        return data;
    }

    public void setData(List<ScoreBoardModel> data) {
        this.data = data;
    }
}
