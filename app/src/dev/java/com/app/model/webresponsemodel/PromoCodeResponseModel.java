package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PromoCodeModel;


public class PromoCodeResponseModel extends AppBaseResponseModel {

    PromoCodeModel data;

    public PromoCodeModel getData() {
        return data;
    }

    public void setData(PromoCodeModel data) {
        this.data = data;
    }
}
