package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.CountryModel;

import java.util.List;

public class CountryResponseModel extends AppBaseResponseModel {

    List<CountryModel> data;

    public List<CountryModel> getData() {
        return data;
    }

    public void setData(List<CountryModel> data) {
        this.data = data;
    }
}
