package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseModel;


public class ResendOtpResponseModel extends AppBaseResponseModel {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean extends AppBaseModel {

        private String otp;

        public String getOtp() {
            return getValidString(otp);
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }
    }
}
