package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PrivateBreakUpModel;

public class PrivateBreakUpResponseModel extends AppBaseResponseModel {

    PrivateBreakUpModel data;

    public PrivateBreakUpModel getData() {
        return data;
    }

    public void setData(PrivateBreakUpModel data) {
        this.data = data;
    }
}
