package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PlayerModel;

import java.util.List;

public class PlayersResponseModel extends AppBaseResponseModel {

    List<PlayerModel> data;

    public List<PlayerModel> getData() {
        return data;
    }

    public void setData(List<PlayerModel> data) {
        this.data = data;
    }
}
