package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.MessageModel;

public class MessageResponseModel extends AppBaseResponseModel {

    MessageModel data;

    public MessageModel getData() {
        return data;
    }

    public void setData(MessageModel data) {
        this.data = data;
    }
}
