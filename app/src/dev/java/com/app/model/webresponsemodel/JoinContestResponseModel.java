package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.ContestModel;

import java.util.List;

public class JoinContestResponseModel extends AppBaseResponseModel {

    List<ContestModel> data;

    public List<ContestModel> getData() {
        return data;
    }

    public void setData(List<ContestModel> data) {
        this.data = data;
    }
}
