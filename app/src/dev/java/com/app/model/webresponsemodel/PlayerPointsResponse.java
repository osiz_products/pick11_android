package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PlayerPointsModel;

import java.util.List;


/**
 * Created by Sunil yadav on 1/7/19.
 */
public class PlayerPointsResponse extends AppBaseResponseModel {

    List<PlayerPointsModel> data;

    public List<PlayerPointsModel> getData() {
        return data;
    }

    public void setData(List<PlayerPointsModel> data) {
        this.data = data;
    }
}
