package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.GameTypeModel;

import java.util.List;

public class GameTypeResponseModel extends AppBaseResponseModel {

    List<GameTypeModel> data;

    public List<GameTypeModel> getData() {
        return data;
    }

    public void setData(List<GameTypeModel> data) {
        this.data = data;
    }
}
