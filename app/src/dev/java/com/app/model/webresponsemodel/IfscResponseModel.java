package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.IfscModel;


public class IfscResponseModel extends AppBaseResponseModel {

    IfscModel data;

    public IfscModel getData() {
        return data;
    }

    public void setData(IfscModel data) {
        this.data = data;
    }
}
