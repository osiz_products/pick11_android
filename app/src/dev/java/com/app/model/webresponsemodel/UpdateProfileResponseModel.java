package com.app.model.webresponsemodel;

import com.app.model.UserModel;

public class UpdateProfileResponseModel extends AppBaseResponseModel {

    private DataBean data;
    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private String token;
        private UserModel userinfo;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public UserModel getData() {
            return userinfo;
        }

        public void setData(UserModel data) {
            this.userinfo = data;
        }
    }
}
