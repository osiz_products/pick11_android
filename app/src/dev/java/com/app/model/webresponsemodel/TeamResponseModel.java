package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.TeamModel;

import java.util.List;

public class TeamResponseModel extends AppBaseResponseModel {


    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        long total;
        List<TeamModel> teams;

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public List<TeamModel> getTeams() {
            return teams;
        }

        public void setTeams(List<TeamModel> teams) {
            this.teams = teams;
        }

    }
}
