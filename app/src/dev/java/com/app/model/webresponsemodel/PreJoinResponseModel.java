package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseModel;
import com.app.appbase.AppBaseResponseModel;
import com.app.model.WalletModel;

public class PreJoinResponseModel extends AppBaseResponseModel {

    PreJoinedModel data;

    public PreJoinedModel getData() {
        return data;
    }

    public void setData(PreJoinedModel data) {
        this.data = data;
    }

    public static class PreJoinedModel extends AppBaseModel {

        WalletModel wallet;
        float paybnswlt;
        float paybalwlt;
        float paywinwlt;
        float fees;
        float totalfees;
        float entryfees;
        float bnsdeduction;
        String poolcontestid;

        public WalletModel getWallet() {
            return wallet;
        }

        public void setWallet(WalletModel wallet) {
            this.wallet = wallet;
        }

        public float getPaybnswlt() {
            return paybnswlt;
        }

        public void setPaybnswlt(float paybnswlt) {
            this.paybnswlt = paybnswlt;
        }

        public float getPaybalwlt() {
            return paybalwlt;
        }

        public void setPaybalwlt(float paybalwlt) {
            this.paybalwlt = paybalwlt;
        }

        public float getPaywinwlt() {
            return paywinwlt;
        }

        public void setPaywinwlt(float paywinwlt) {
            this.paywinwlt = paywinwlt;
        }

        public float getTotalfees() {
            return totalfees;
        }

        public void setTotalfees(float totalfees) {
            this.totalfees = totalfees;
        }


        public float getFees() {
            return fees;
        }

        public void setFees(float fees) {
            this.fees = fees;
        }




        public float getEntry_fees() {
            return entryfees;
        }

        public void setEntry_fees(float entry_fees) {
            this.entryfees = entry_fees;
        }

        public String getFeesText() {
            return getValidDecimalFormat(getFees());
        }

        public String getPayBalanceWalletText() {
            String validDecimalFormat = getValidDecimalFormat(getEntry_fees() - getPaybnswlt());
            return validDecimalFormat.replaceAll("\\.00", "");
        }

        public String getEnteryFeestext() {
            String validDecimalFormat = getValidDecimalFormat(getEntry_fees());
            return validDecimalFormat.replaceAll("\\.00", "");
        }

        public float getBnsdeduction() {
            return bnsdeduction;
        }

        public void setBnsdeduction(float bnsdeduction) {
            this.bnsdeduction = bnsdeduction;
        }

        public String getBonsdeduction() {
            String s = getValidDecimalFormat(getBnsdeduction());
            return s.replaceAll("\\.00", "") + "%";
        }

        public String getPoolcontestid() {
            return getValidString(poolcontestid);
        }

        public void setPoolcontestid(String poolcontestid) {
            this.poolcontestid = poolcontestid;
        }

        public String getRemainBalancetext() {
            WalletModel wallet = getWallet();
            if (wallet != null) {
                String validDecimalFormat = getValidDecimalFormat(wallet.getWalletbalance() + wallet.getWltwin());
                return validDecimalFormat.replaceAll("\\.00", "");
            }
            return "0";
        }

        public String getUsedBonusText() {
            String validDecimalFormat = getValidDecimalFormat(getPaybnswlt());
            return validDecimalFormat.replaceAll("\\.00", "");
        }

        public void setData(PreJoinedModel data) {
            setWallet(data.getWallet());
            setPaybnswlt(data.getPaybnswlt());
            setPaybalwlt(data.getPaybalwlt());
            setPaywinwlt(data.getPaywinwlt());
            setFees(data.getFees());
            setEntry_fees(data.getEntry_fees());
            setBnsdeduction(data.getBnsdeduction());
            setPoolcontestid(data.getPoolcontestid());
        }
    }
}
