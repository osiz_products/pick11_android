package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseModel;
import com.app.model.TransactionModel;

import java.util.List;


public class TransactionResponseModel extends AppBaseResponseModel {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean extends AppBaseModel {
        int total = 0;
        List<TransactionModel> list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<TransactionModel> getList() {
            return list;
        }

        public void setList(List<TransactionModel> list) {
            this.list = list;
        }
    }
}
