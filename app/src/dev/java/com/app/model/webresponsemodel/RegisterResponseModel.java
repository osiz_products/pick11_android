package com.app.model.webresponsemodel;


import com.app.model.RegisterOtpModel;

public class RegisterResponseModel extends AppBaseResponseModel {

    RegisterOtpModel data;

    public RegisterOtpModel getData() {
        return data;
    }

    public void setData(RegisterOtpModel data) {
        this.data = data;
    }
}
