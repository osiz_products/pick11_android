package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.TeamsPointModel;

import java.util.List;

public class TeamsPointResponseModel extends AppBaseResponseModel {


    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        int total;
        List<TeamsPointModel> list;
        List<TeamsPointModel> myteams;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<TeamsPointModel> getList() {
            return list;
        }

        public void setList(List<TeamsPointModel> list) {
            this.list = list;
        }

        public List<TeamsPointModel> getMyteams() {
            return myteams;
        }

        public void setMyteams(List<TeamsPointModel> myteams) {
            this.myteams = myteams;
        }
    }
}
