package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.BankModel;

public class BankResponseModel extends AppBaseResponseModel {

    BankModel data;

    public BankModel getData() {
        return data;
    }

    public void setData(BankModel data) {
        this.data = data;
    }
}
