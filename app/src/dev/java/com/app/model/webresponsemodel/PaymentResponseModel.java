package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PaymentModel;

public class PaymentResponseModel extends AppBaseResponseModel {

    PaymentModel data;

    public PaymentModel getData() {
        return data;
    }

    public void setData(PaymentModel data) {
        this.data = data;
    }
}
