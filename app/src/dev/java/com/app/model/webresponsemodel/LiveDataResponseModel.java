package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.LiveDataModel;

import java.util.List;


/**
 * Created by Sunil yadav on 25/7/19.
 */
public class LiveDataResponseModel extends AppBaseResponseModel {


    /**
     * data : {"list":[{"_id":{"$oid":"5d2dd3bd93daa794cfe320c3"},"matchid":"irezim_2019_t20_02","inningsdetail":{"a":{"1":"134/1 in 10.5"},"b":{"1":"132/8 in 13.0"},"team_a":{"logo":"http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1563795077_india-winner-campaign.jpg","short_name":"IRE","full_name":"Ireland"},"team_b":{"logo":"http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1563795080_index.png","short_name":"ZIM","full_name":"Zimbabwe"}},"matchstarted":{"$date":{"$numberLong":"1562943600000"}},"status":"started","status_overview":"result","type":"t20"},{"_id":{"$oid":"5d2dd3be93daa794cfe320c9"},"matchid":"irezim_2019_t20_03","inningsdetail":{"a":{"1":"171/9 in 20.0"},"b":{"1":"172/2 in 16.4"},"team_a":{"logo":"http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1562764557_india-winner-campaign.jpg","short_name":"IRE","full_name":"Ireland"},"team_b":{"logo":"http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1562764561_index.png","short_name":"ZIM","full_name":"Zimbabwe"}},"matchstarted":{"$date":{"$numberLong":"1563116400000"}},"status":"started","status_overview":"result","type":"t20"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<LiveDataModel> list;

        public List<LiveDataModel> getList() {
            return list;
        }

        public void setList(List<LiveDataModel> list) {
            this.list = list;
        }

    }
}


