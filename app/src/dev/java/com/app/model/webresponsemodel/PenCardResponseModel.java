package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.PenCardModel;

public class PenCardResponseModel extends AppBaseResponseModel {

    PenCardModel data;

    public PenCardModel getData() {
        return data;
    }

    public void setData(PenCardModel data) {
        this.data = data;
    }
}
