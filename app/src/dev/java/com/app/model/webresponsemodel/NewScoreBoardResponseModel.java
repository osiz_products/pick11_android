package com.app.model.webresponsemodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.NewScoreBoardModel;

public class NewScoreBoardResponseModel extends AppBaseResponseModel {

    NewScoreBoardModel data;

    public NewScoreBoardModel getData() {
        return data;
    }

    public void setData(NewScoreBoardModel data) {
        this.data = data;
    }
}
