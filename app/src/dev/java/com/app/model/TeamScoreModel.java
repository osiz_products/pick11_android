package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class TeamScoreModel extends AppBaseModel {


    private String name;
    private String logo;
    private List<ScoreModel> score;

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return getValidString(logo);
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<ScoreModel> getScore() {
        return score;
    }

    public void setScore(List<ScoreModel> score) {
        this.score = score;
    }
}
