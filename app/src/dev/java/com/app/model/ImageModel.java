package com.app.model;

import com.app.appbase.AppBaseModel;

public class ImageModel extends AppBaseModel {



    private long id;
    private String thumb;
    private String large;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getThumb() {
        return getValidString(thumb);
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getLarge() {
        return getValidString(large);
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
