package com.app.model;

import com.app.appbase.AppBaseModel;

public class GameTypeModel extends AppBaseModel {

    private String id;
    private String gname;
    private String icon;

    public String getId() {
        return getValidString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGname() {
        return getValidString(gname);
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getIcon() {
        return getValidString(icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public boolean isCricket(){
        return getGname().equalsIgnoreCase("cricket");
    }

    public boolean isFootball(){
        return getGname().equalsIgnoreCase("football");
    }

    public boolean isKabaddi(){
        return getGname().equalsIgnoreCase("kabaddi");
    }
}
