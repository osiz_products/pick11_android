package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;

import java.util.List;

/**
 * Created by Sunil yadav on 17/8/19.
 */
public class BeanModel extends AppBaseModel {
    /**
     * key : b_1
     * runs : 130
     * run_str : 130/10 in 19.5
     * run_rate : 6.55
     * balls : 119
     * dotballs : 57
     * fours : 8
     * sixes : 7
     * wickets : 10
     * extras : 4
     * wide : 3
     * noball : 1
     * legbye : 0
     * bye : 0
     * penalty : 0
     * overs : 19.5
     * batting_order : ["sar_raj","a_karthik","sh_chandran","rn_subramanian","jag_kousik","ns_kumaran","ab_tanwar","r_mithun","lk_akash","rs_shah","lok_raj"]
     * wicket_order : ["a_karthik","sh_chandran","rn_subramanian","sar_raj","ns_kumaran","jag_kousik","r_mithun","ab_tanwar","lk_akash","lok_raj"]
     * bowling_order : ["ab_tanwar","lk_akash","ns_kumaran","r_mithun","rs_shah","lok_raj"]
     * fall_of_wickets : ["Arun Karthik at 19 runs, in 2.3 over","Shijit Chandran at 44 runs, in 5.1 over","R Nilesh Subramanian at 58 runs, in 7.2 over","Sarath Raj at 58 runs, in 7.3 over","N S Kumaran at 97 runs, in 13.5 over","Jagatheesan Kousik at 114 runs, in 16.2 over","R Mithun at 114 runs, in 16.3 over","AB Tanwar at 130 runs, in 18.5 over","L Kiran Akash at 130 runs, in 19.3 over","Lokesh Raj at 130 runs, in 19.5 over"]
     * partnerships : [{"index":1,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"sar_raj","player_b":"a_karthik","first_ball":"7d884512-8d9e-432c-91dd-4c3c6ecba802","last_ball":"6a9cb419-976d-49c3-8a66-d0421c64ccf9","start_over":"0.1","end_over":"2.3","runs":19,"balls":15,"four":2,"six":1,"run_rate":"7.60","player_a_runs":8,"player_a_balls":7,"player_a_four":0,"player_a_six":1,"player_b_runs":11,"player_b_balls":8,"player_b_four":2,"player_b_six":0,"overs_balls":"2.3","dismissed":true},{"index":2,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"sh_chandran","player_b":"sar_raj","first_ball":"563dbf86-ee4b-40fa-878d-229cf02f0cae","last_ball":"cea871f5-7d44-4d1c-bb2e-e2499b602a4b","start_over":"2.4","end_over":"5.1","runs":25,"balls":16,"four":1,"six":2,"run_rate":"9.38","player_a_runs":3,"player_a_balls":5,"player_a_four":0,"player_a_six":0,"player_b_runs":22,"player_b_balls":11,"player_b_four":1,"player_b_six":2,"overs_balls":"2.4","dismissed":true},{"index":3,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"rn_subramanian","player_b":"sar_raj","first_ball":"4c712ac4-2106-455e-aa5f-cce47c573e2a","last_ball":"4aef5219-ae72-4731-823a-f1f1be59d340","start_over":"5.2","end_over":"7.2","runs":14,"balls":13,"four":2,"six":0,"run_rate":"6.46","player_a_runs":12,"player_a_balls":9,"player_a_four":2,"player_a_six":0,"player_b_runs":2,"player_b_balls":4,"player_b_four":0,"player_b_six":0,"overs_balls":"2.1","dismissed":true},{"index":4,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"sar_raj","player_b":"jag_kousik","first_ball":"9d07bcde-22b3-4809-b829-ba318e3f0434","last_ball":"9d07bcde-22b3-4809-b829-ba318e3f0434","start_over":"7.3","end_over":"7.3","runs":0,"balls":1,"four":0,"six":0,"run_rate":"0.00","player_a_runs":0,"player_a_balls":1,"player_a_four":0,"player_a_six":0,"player_b_runs":0,"player_b_balls":0,"player_b_four":0,"player_b_six":0,"overs_balls":"1.1","dismissed":true},{"index":5,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"ns_kumaran","player_b":"jag_kousik","first_ball":"995091c6-6571-420b-b313-c786e960ba79","last_ball":"fcc99716-5701-498c-b753-54a9ce543a6b","start_over":"7.4","end_over":"13.5","runs":39,"balls":38,"four":1,"six":3,"run_rate":"6.16","player_a_runs":11,"player_a_balls":14,"player_a_four":0,"player_a_six":1,"player_b_runs":26,"player_b_balls":24,"player_b_four":1,"player_b_six":2,"overs_balls":"6.2","dismissed":true},{"index":6,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"jag_kousik","player_b":"ab_tanwar","first_ball":"a16dbc5c-e850-4d9b-8dd1-9bf502932e40","last_ball":"565ba92b-7071-4cad-8e69-fa48f69d7bb2","start_over":"13.6","end_over":"16.2","runs":17,"balls":15,"four":1,"six":1,"run_rate":"6.80","player_a_runs":14,"player_a_balls":8,"player_a_four":1,"player_a_six":1,"player_b_runs":3,"player_b_balls":7,"player_b_four":0,"player_b_six":0,"overs_balls":"2.3","dismissed":true},{"index":7,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"r_mithun","player_b":"ab_tanwar","first_ball":"86d36d79-1092-41ef-93ab-4af91c3eeb20","last_ball":"86d36d79-1092-41ef-93ab-4af91c3eeb20","start_over":"16.3","end_over":"16.3","runs":0,"balls":1,"four":0,"six":0,"run_rate":"0.00","player_a_runs":0,"player_a_balls":1,"player_a_four":0,"player_a_six":0,"player_b_runs":0,"player_b_balls":0,"player_b_four":0,"player_b_six":0,"overs_balls":"1.1","dismissed":true},{"index":8,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"ab_tanwar","player_b":"lk_akash","first_ball":"457f4632-e0f5-434c-95c2-d5b5d8833703","last_ball":"6257744d-f49b-44f1-aa6e-928417f2af36","start_over":"16.4","end_over":"18.5","runs":16,"balls":14,"four":1,"six":0,"run_rate":"6.86","player_a_runs":10,"player_a_balls":11,"player_a_four":1,"player_a_six":0,"player_b_runs":4,"player_b_balls":4,"player_b_four":0,"player_b_six":0,"overs_balls":"2.2","dismissed":true},{"index":9,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"rs_shah","player_b":"lk_akash","first_ball":"7dcc5a54-7e7c-4c4d-8c09-50b08c5945d3","last_ball":"099df8d2-3707-44b6-8330-21e6db87c8d2","start_over":"18.6","end_over":"19.3","runs":0,"balls":4,"four":0,"six":0,"run_rate":"0.00","player_a_runs":0,"player_a_balls":1,"player_a_four":0,"player_a_six":0,"player_b_runs":0,"player_b_balls":3,"player_b_four":0,"player_b_six":0,"overs_balls":"0.4","dismissed":true},{"index":10,"match":"tnplt20_2019_q2","pick":"b","innings":"1","player_a":"lok_raj","player_b":"rs_shah","first_ball":"0277055e-c4f7-406b-a603-04ebf4e263ee","last_ball":"8a802bb9-4242-4422-b512-b5e59fae5572","start_over":"19.4","end_over":"19.5","runs":0,"balls":2,"four":0,"six":0,"run_rate":"0.00","player_a_runs":0,"player_a_balls":2,"player_a_four":0,"player_a_six":0,"player_b_runs":0,"player_b_balls":0,"player_b_four":0,"player_b_six":0,"overs_balls":"1.2","dismissed":true}]
     */

    private String key;
    private int runs;
    private String run_str;
    private String run_rate;
    private int balls;
    private int dotballs;
    private int fours;
    private int sixes;
    private int wickets;
    private int extras;
    private int wide;
    private int noball;
    private int legbye;
    private int bye;
    private int penalty;
    private String overs;
    private List<String> batting_order;
    private List<String> wicket_order;
    private List<String> bowling_order;
    private List<String> fall_of_wickets;
    private List<PartnerShipsModel> partnerships;

    public String getKey() {
        return getValidString(key);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public String getRun_str() {
        return getValidString(run_str);
    }

    public void setRun_str(String run_str) {
        this.run_str = run_str;
    }

    public String getRun_rate() {
        return getValidString(run_rate);
    }

    public void setRun_rate(String run_rate) {
        this.run_rate = run_rate;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public int getDotballs() {
        return dotballs;
    }

    public void setDotballs(int dotballs) {
        this.dotballs = dotballs;
    }

    public int getFours() {
        return fours;
    }

    public void setFours(int fours) {
        this.fours = fours;
    }

    public int getSixes() {
        return sixes;
    }

    public void setSixes(int sixes) {
        this.sixes = sixes;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public int getExtras() {
        return extras;
    }

    public void setExtras(int extras) {
        this.extras = extras;
    }

    public int getWide() {
        return wide;
    }

    public void setWide(int wide) {
        this.wide = wide;
    }

    public int getNoball() {
        return noball;
    }

    public void setNoball(int noball) {
        this.noball = noball;
    }

    public int getLegbye() {
        return legbye;
    }

    public void setLegbye(int legbye) {
        this.legbye = legbye;
    }

    public int getBye() {
        return bye;
    }

    public void setBye(int bye) {
        this.bye = bye;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public String getOvers() {
        return getValidString(overs);
    }

    public void setOvers(String overs) {
        this.overs = overs;
    }

    public List<String> getBatting_order() {
        return batting_order;
    }

    public void setBatting_order(List<String> batting_order) {
        this.batting_order = batting_order;
    }

    public List<String> getWicket_order() {
        return wicket_order;
    }

    public void setWicket_order(List<String> wicket_order) {
        this.wicket_order = wicket_order;
    }

    public List<String> getBowling_order() {
        return bowling_order;
    }

    public void setBowling_order(List<String> bowling_order) {
        this.bowling_order = bowling_order;
    }

    public List<String> getFall_of_wickets() {
        return fall_of_wickets;
    }

    public void setFall_of_wickets(List<String> fall_of_wickets) {
        this.fall_of_wickets = fall_of_wickets;
    }

    public List<PartnerShipsModel> getPartnerships() {
        return partnerships;
    }

    public void setPartnerships(List<PartnerShipsModel> partnerships) {
        this.partnerships = partnerships;
    }

}
