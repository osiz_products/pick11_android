package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class MessageModel extends AppBaseModel {

    private String result;
//    private List<OtherBean> others;
    private String info;
    private String completed;

    public String getResult() {
        return getValidString(result);
    }

    public void setResult(String result) {
        this.result = result;
    }

//    public List<OtherBean> getOthers() {
//        return others;
//    }
//
//    public void setOthers(List<OtherBean> others) {
//        this.others = others;
//    }

    public String getInfo() {
        return getValidString(info);
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCompleted() {
        return getValidString(completed);
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    private static class OtherBean extends AppBaseModel {
    }

}
