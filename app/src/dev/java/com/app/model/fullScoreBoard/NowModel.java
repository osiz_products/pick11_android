package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import java.util.List;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class NowModel extends AppBaseModel {


    /**
     * innings : 1
     * runs : 80
     * balls : 223
     * wicket : 4
     * runs_str : 80/4 in 37.1
     * run_rate : 2.15
     * batting_team : b
     * striker : m_wade
     * nonstriker : s_smith
     * bowling_team : a
     * bowler : b_stokes
     * lead_by_str : null
     * trail_by_str : AUS trail by 178 runs.
     * recent_overs : [[38,["e2cf21c0-d60a-40a6-afab-5816bf8f3835"]],[37,["e45fc75f-13f0-41dc-af44-438965a34080","7c9875cd-9e7d-4240-bd3d-7423091ce0d9","e2937d93-2981-4134-8573-720d4daffdbb","94a1a43b-fe03-44ec-875d-eeadfab14e3d","2c7d958c-52fc-420d-a7c3-0edb8fe6a591","29fde044-89fa-4b57-b87a-5330883a7420"]],[36,["79913f9d-1fda-4097-9d59-6d05cd9aeea5","de6a88cd-4e3c-4185-ac7c-9d4b3034373e","9c6b529c-c651-471d-b225-5365fbab6a0a","84f13b4d-89b3-41eb-a416-c66379fca2b3","46bad923-1bb1-4ec8-870e-3f51915e2049","431e8f5d-da9f-4355-aed1-6c5a3da9df15"]]]
     * recent_overs_str : [[38,["r0"]],[37,["r0","r0","b4","r0","r0","r0"]],[36,["r1","r0","r0","r0","r0","r0"]]]
     * last_ball : {"comment":"BA Stokes to Matthew Wade: No runs.","wicket_type":"","over":38,"ball":"1","highlight_names_keys":[["over_end","m_wade"]],"innings":"1","wagon_position":null,"decision_by":null,"wicket":"","match":"engaus_2019_test_02","status":"played","updated":"2019-08-16T11:57:34.006000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"m_wade"},"striker":"m_wade","batting_team":"b","key":"e2cf21c0-d60a-40a6-afab-5816bf8f3835","other_fielder":null,"nonstriker":"s_smith","runs":0,"bowler":{"key":"b_stokes","runs":0,"extras":0,"ball_count":1,"wicket":0},"over_str":"37.1","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":0}}
     * next_ball : {"striker":"m_wade","nonstriker":"s_smith","bowler":"b_stokes","over":38,"ball":2,"over_str":"37.2"}
     * break : {"duration":"","reason":""}
     */

    private String innings;
    private int runs;
    private int balls;
    private int wicket;
    private String runs_str;
    private String run_rate;
    private String batting_team;
    private String striker;
    private String nonstriker;
    private String bowling_team;
    private String bowler;
    private String lead_by_str;
    private String trail_by_str;
    private LastBallBean last_ball;
    private NextBallBean next_ball;
    @SerializedName("break")
    private BreakBean breakX;
    private LinkedTreeMap<String, Object> recent_overs;
    private LinkedTreeMap<String, Object> recent_overs_str;

    public String getInnings() {
        return getValidString(innings);
    }

    public void setInnings(String innings) {
        this.innings = innings;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public int getWicket() {
        return wicket;
    }

    public void setWicket(int wicket) {
        this.wicket = wicket;
    }

    public String getRuns_str() {
        return getValidString(runs_str);
    }

    public void setRuns_str(String runs_str) {
        this.runs_str = runs_str;
    }

    public String getRun_rate() {
        return getValidString(run_rate);
    }

    public void setRun_rate(String run_rate) {
        this.run_rate = run_rate;
    }

    public String getBatting_team() {
        return getValidString(batting_team);
    }

    public void setBatting_team(String batting_team) {
        this.batting_team = batting_team;
    }

    public String getStriker() {
        return getValidString(striker);
    }

    public void setStriker(String striker) {
        this.striker = striker;
    }

    public String getNonstriker() {
        return getValidString(nonstriker);
    }

    public void setNonstriker(String nonstriker) {
        this.nonstriker = nonstriker;
    }

    public String getBowling_team() {
        return getValidString(bowling_team);
    }

    public void setBowling_team(String bowling_team) {
        this.bowling_team = bowling_team;
    }

    public String getBowler() {
        return getValidString(bowler);
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public String getLead_by_str() {
        return getValidString(lead_by_str);
    }

    public void setLead_by_str(String lead_by_str) {
        this.lead_by_str = lead_by_str;
    }

    public String getTrail_by_str() {
        return getValidString(trail_by_str);
    }

    public void setTrail_by_str(String trail_by_str) {
        this.trail_by_str = trail_by_str;
    }

    public LastBallBean getLast_ball() {
        return last_ball;
    }

    public void setLast_ball(LastBallBean last_ball) {
        this.last_ball = last_ball;
    }

    public NextBallBean getNext_ball() {
        return next_ball;
    }

    public void setNext_ball(NextBallBean next_ball) {
        this.next_ball = next_ball;
    }

    public BreakBean getBreakX() {
        return breakX;
    }

    public void setBreakX(BreakBean breakX) {
        this.breakX = breakX;
    }

    public LinkedTreeMap<String, Object> getRecent_overs() {
        return recent_overs;
    }

    public void setRecent_overs(LinkedTreeMap<String, Object> recent_overs) {
        this.recent_overs = recent_overs;
    }

    public LinkedTreeMap<String, Object> getRecent_overs_str() {
        return recent_overs_str;
    }

    public void setRecent_overs_str(LinkedTreeMap<String, Object> recent_overs_str) {
        this.recent_overs_str = recent_overs_str;
    }

    public static class LastBallBean extends AppBaseModel {
        /**
         * comment : BA Stokes to Matthew Wade: No runs.
         * wicket_type :
         * over : 38
         * ball : 1
         * highlight_names_keys : [["over_end","m_wade"]]
         * innings : 1
         * wagon_position : null
         * decision_by : null
         * wicket :
         * match : engaus_2019_test_02
         * status : played
         * updated : 2019-08-16T11:57:34.006000
         * ball_type : normal
         * batsman : {"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"m_wade"}
         * striker : m_wade
         * batting_team : b
         * key : e2cf21c0-d60a-40a6-afab-5816bf8f3835
         * other_fielder : null
         * nonstriker : s_smith
         * runs : 0
         * bowler : {"key":"b_stokes","runs":0,"extras":0,"ball_count":1,"wicket":0}
         * over_str : 37.1
         * extras : 0
         * pick : {"runs":0,"extras":0,"ball_count":1,"wicket":0}
         */

        private String comment;
        private String wicket_type;
        private int over;
        private String ball;
        private String innings;
        private String wagon_position;
        private String decision_by;
        private String wicket;
        private String match;
        private String status;
        private String updated;
        private String ball_type;
        private BatsmanBean batsman;
        private String striker;
        private String batting_team;
        private String key;
        private String other_fielder;
        private String nonstriker;
        private int runs;
        private BowlerBean bowler;
        private String over_str;
        private String extras;
        private TeamBean team;
        private List<List<String>> highlight_names_keys;

        public String getComment() {
            return getValidString(comment);
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getWicket_type() {
            return getValidString(wicket_type);
        }

        public void setWicket_type(String wicket_type) {
            this.wicket_type = wicket_type;
        }

        public int getOver() {
            return over;
        }

        public void setOver(int over) {
            this.over = over;
        }

        public String getBall() {
            return getValidString(ball);
        }

        public void setBall(String ball) {
            this.ball = ball;
        }

        public String getInnings() {
            return getValidString(innings);
        }

        public void setInnings(String innings) {
            this.innings = innings;
        }

        public String getWagon_position() {
            return getValidString(wagon_position);
        }

        public void setWagon_position(String wagon_position) {
            this.wagon_position = wagon_position;
        }

        public String getDecision_by() {
            return getValidString(decision_by);
        }

        public void setDecision_by(String decision_by) {
            this.decision_by = decision_by;
        }

        public String getWicket() {
            return getValidString(wicket);
        }

        public void setWicket(String wicket) {
            this.wicket = wicket;
        }

        public String getMatch() {
            return getValidString(match);
        }

        public void setMatch(String match) {
            this.match = match;
        }

        public String getStatus() {
            return getValidString(status);
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUpdated() {
            return getValidString(updated);
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getBall_type() {
            return getValidString(ball_type);
        }

        public void setBall_type(String ball_type) {
            this.ball_type = ball_type;
        }

        public BatsmanBean getBatsman() {
            return batsman;
        }

        public void setBatsman(BatsmanBean batsman) {
            this.batsman = batsman;
        }

        public String getStriker() {
            return getValidString(striker);
        }

        public void setStriker(String striker) {
            this.striker = striker;
        }

        public String getBatting_team() {
            return getValidString(batting_team);
        }

        public void setBatting_team(String batting_team) {
            this.batting_team = batting_team;
        }

        public String getKey() {
            return getValidString(key);
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getOther_fielder() {
            return getValidString(other_fielder);
        }

        public void setOther_fielder(String other_fielder) {
            this.other_fielder = other_fielder;
        }

        public String getNonstriker() {
            return getValidString(nonstriker);
        }

        public void setNonstriker(String nonstriker) {
            this.nonstriker = nonstriker;
        }

        public int getRuns() {
            return runs;
        }

        public void setRuns(int runs) {
            this.runs = runs;
        }

        public BowlerBean getBowler() {
            return bowler;
        }

        public void setBowler(BowlerBean bowler) {
            this.bowler = bowler;
        }

        public String getOver_str() {
            return getValidString(over_str);
        }

        public void setOver_str(String over_str) {
            this.over_str = over_str;
        }

        public String getExtras() {
            return getValidString(extras);
        }

        public void setExtras(String extras) {
            this.extras = extras;
        }

        public TeamBean getTeam() {
            return team;
        }

        public void setTeam(TeamBean team) {
            this.team = team;
        }

        public List<List<String>> getHighlight_names_keys() {
            return highlight_names_keys;
        }

        public void setHighlight_names_keys(List<List<String>> highlight_names_keys) {
            this.highlight_names_keys = highlight_names_keys;
        }

        public static class BatsmanBean extends AppBaseModel {
            /**
             * runs : 0
             * dotball : 1
             * six : 0
             * four : 0
             * ball_count : 1
             * key : m_wade
             */

            private int runs;
            private int dotball;
            private int six;
            private int four;
            private int ball_count;
            private String key;

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getDotball() {
                return dotball;
            }

            public void setDotball(int dotball) {
                this.dotball = dotball;
            }

            public int getSix() {
                return six;
            }

            public void setSix(int six) {
                this.six = six;
            }

            public int getFour() {
                return four;
            }

            public void setFour(int four) {
                this.four = four;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public String getKey() {
                return getValidString(key);
            }

            public void setKey(String key) {
                this.key = key;
            }
        }

        public static class BowlerBean extends AppBaseModel {
            /**
             * key : b_stokes
             * runs : 0
             * extras : 0
             * ball_count : 1
             * wicket : 0
             */

            private String key;
            private int runs;
            private int extras;
            private int ball_count;
            private int wicket;

            public String getKey() {
                return getValidString(key);
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getExtras() {
                return extras;
            }

            public void setExtras(int extras) {
                this.extras = extras;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public int getWicket() {
                return wicket;
            }

            public void setWicket(int wicket) {
                this.wicket = wicket;
            }
        }

        public static class TeamBean {
            /**
             * runs : 0
             * extras : 0
             * ball_count : 1
             * wicket : 0
             */

            private int runs;
            private int extras;
            private int ball_count;
            private int wicket;

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getExtras() {
                return extras;
            }

            public void setExtras(int extras) {
                this.extras = extras;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public int getWicket() {
                return wicket;
            }

            public void setWicket(int wicket) {
                this.wicket = wicket;
            }
        }
    }

    public static class NextBallBean extends AppBaseModel {
        /**
         * striker : m_wade
         * nonstriker : s_smith
         * bowler : b_stokes
         * over : 38
         * ball : 2
         * over_str : 37.2
         */

        private String striker;
        private String nonstriker;
        private String bowler;
        private int over;
        private int ball;
        private String over_str;

        public String getStriker() {
            return getValidString(striker);
        }

        public void setStriker(String striker) {
            this.striker = striker;
        }

        public String getNonstriker() {
            return getValidString(nonstriker);
        }

        public void setNonstriker(String nonstriker) {
            this.nonstriker = nonstriker;
        }

        public String getBowler() {
            return getValidString(bowler);
        }

        public void setBowler(String bowler) {
            this.bowler = bowler;
        }

        public int getOver() {
            return over;
        }

        public void setOver(int over) {
            this.over = over;
        }

        public int getBall() {
            return ball;
        }

        public void setBall(int ball) {
            this.ball = ball;
        }

        public String getOver_str() {
            return getValidString(over_str);
        }

        public void setOver_str(String over_str) {
            this.over_str = over_str;
        }
    }

    public static class BreakBean extends AppBaseModel {
        /**
         * duration :
         * reason :
         */

        private String duration;
        private String reason;

        public String getDuration() {
            return getValidString(duration);
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getReason() {
            return getValidString(reason);
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
