package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;
import com.app.model.IdBean;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class FullScoreBoardModel extends AppBaseModel {


    /**
     * _id : {"$oid":"5d552d5c7f622df684c0f488"}
     * matchid : engaus_2019_test_02
     * matchstarted : {"$date":{"$numberLong":"1565776800000"}}
     */

    private IdBean _id;
    private String matchid;
    private MatchstartedBean matchstarted;
    private InningsDetails inningsdetail;
    private String status_overview;
    private String status;
    private ScoreMatchModel match;
    private String gameid;
    private String type;
    private MessageModel msg_info;

    public IdBean get_id() {
        return _id;
    }

    public void set_id(IdBean _id) {
        this._id = _id;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public MatchstartedBean getMatchstarted() {
        return matchstarted;
    }

    public void setMatchstarted(MatchstartedBean matchstarted) {
        this.matchstarted = matchstarted;
    }

    public static class MatchstartedBean {
        /**
         * $date : {"$numberLong":"1565776800000"}
         */

        private $dateBean $date;

        public $dateBean get$date() {
            return $date;
        }

        public void set$date($dateBean $date) {
            this.$date = $date;
        }

        public static class $dateBean {
            /**
             * $numberLong : 1565776800000
             */

            private long $numberLong;

            public long get$numberLong() {
                return $numberLong;
            }

            public void set$numberLong(long $numberLong) {
                this.$numberLong = $numberLong;
            }
        }
    }

    public InningsDetails getInningsdetail() {
        return inningsdetail;
    }

    public void setInningsdetail(InningsDetails inningsdetail) {
        this.inningsdetail = inningsdetail;
    }

    public String getStatus_overview() {
        return getValidString(status_overview);
    }

    public void setStatus_overview(String status_overview) {
        this.status_overview = status_overview;
    }

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ScoreMatchModel getMatch() {
        return match;
    }

    public void setMatch(ScoreMatchModel match) {
        this.match = match;
    }

    public String getGameid() {
        return getValidString(gameid);
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getType() {
        return getValidString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public MessageModel getMsg_info() {
        return msg_info;
    }

    public void setMsg_info(MessageModel msg_info) {
        this.msg_info = msg_info;
    }
}

