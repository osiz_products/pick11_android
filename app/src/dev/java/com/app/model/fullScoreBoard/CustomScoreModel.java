package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;

import java.util.List;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class CustomScoreModel extends AppBaseModel {

    /**
     * logo : http://fancy11.com/fancy11/public/uploads/teamlogo/1565496264_eng.png
     * short_name : ENG
     * full_name : England
     * runWicketOver : 258/10 in 77.1
     * man_of_match :
     * title : England vs Australia  - 2nd Test Match - England vs Australia  - The Ashes 2019
     * date : 14th Aug 2019 10:00 GMT
     * toss : Australia  won the toss and chose to bowl first
     * venue : Lord's, London, England
     * extras : {"key":"b_1","runs":122,"run_str":"122/5 in 53.1","run_rate":"2.29","balls":319,"dotballs":258,"fours":12,"sixes":0,"wickets":5,"extras":14,"wide":1,"noball":0,"legbye":5,"bye":8,"penalty":0,"overs":"53.1"}
     * player : [{"dismissed":true,"seasonal_role":"batsman","key":"d_warner","fullname":"David Warner","name":"David Warner","dots":15,"sixes":0,"runs":3,"balls":17,"fours":0,"strike_rate":17.65,"out_str":"c CT Bancroft b Pat Cummins","bowling":{"dots":104,"runs":61,"balls":126,"maiden_overs":8,"wickets":3,"extras":6,"overs":"21.0","economy":2.9}}]
     */

    private String status;
    private String status_overview;
    private String logo;
    private String short_name;
    private String full_name;
    private String runWicketOver;
    private String man_of_match;
    private String title;
    private String date;
    private String toss;
    private String venue;
    private ExtrasBean extras;
    private List<PlayerBean> player;

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_overview() {
        return getValidString(status_overview);
    }

    public void setStatus_overview(String status_overview) {
        this.status_overview = status_overview;
    }

    public String getLogo() {
        return getValidString(logo);
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShort_name() {
        return getValidString(short_name);
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getFull_name() {
        return getValidString(full_name);
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getRunWicketOver() {
        return getValidString(runWicketOver);
    }

    public void setRunWicketOver(String runWicketOver) {
        this.runWicketOver = runWicketOver;
    }

    public String getMan_of_match() {
        return getValidString(man_of_match);
    }

    public void setMan_of_match(String man_of_match) {
        this.man_of_match = man_of_match;
    }

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return getValidString(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getToss() {
        return getValidString(toss);
    }

    public void setToss(String toss) {
        this.toss = toss;
    }

    public String getVenue() {
        return getValidString(venue);
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public ExtrasBean getExtras() {
        return extras;
    }

    public void setExtras(ExtrasBean extras) {
        this.extras = extras;
    }

    public List<PlayerBean> getPlayer() {
        return player;
    }

    public void setPlayer(List<PlayerBean> player) {
        this.player = player;
    }

    public static class ExtrasBean extends AppBaseModel {
        /**
         * key : b_1
         * runs : 122
         * run_str : 122/5 in 53.1
         * run_rate : 2.29
         * balls : 319
         * dotballs : 258
         * fours : 12
         * sixes : 0
         * wickets : 5
         * extras : 14
         * wide : 1
         * noball : 0
         * legbye : 5
         * bye : 8
         * penalty : 0
         * overs : 53.1
         */

        private String key;
        private String runs;
        private String run_str;
        private String run_rate;
        private String balls;
        private String dotballs;
        private String fours;
        private String sixes;
        private String wickets;
        private String extras;
        private String wide;
        private String noball;
        private String legbye;
        private String bye;
        private String penalty;
        private String overs;

        public String getKey() {
            return getValidString(key);
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getRuns() {
            return runs;
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getRun_str() {
            return getValidString(run_str);
        }

        public void setRun_str(String run_str) {
            this.run_str = run_str;
        }

        public String getRun_rate() {
            return getValidString(run_rate);
        }

        public void setRun_rate(String run_rate) {
            this.run_rate = run_rate;
        }

        public String getBalls() {
            return balls;
        }

        public void setBalls(String balls) {
            this.balls = balls;
        }

        public String getDotballs() {
            return dotballs;
        }

        public void setDotballs(String dotballs) {
            this.dotballs = dotballs;
        }

        public String getFours() {
            return fours;
        }

        public void setFours(String fours) {
            this.fours = fours;
        }

        public String getSixes() {
            return sixes;
        }

        public void setSixes(String sixes) {
            this.sixes = sixes;
        }

        public String getWickets() {
            return wickets;
        }

        public void setWickets(String wickets) {
            this.wickets = wickets;
        }

        public String getExtras() {
            return extras;
        }

        public void setExtras(String extras) {
            this.extras = extras;
        }

        public String getWide() {
            return wide;
        }

        public void setWide(String wide) {
            this.wide = wide;
        }

        public String getNoball() {
            return noball;
        }

        public void setNoball(String noball) {
            this.noball = noball;
        }

        public String getLegbye() {
            return legbye;
        }

        public void setLegbye(String legbye) {
            this.legbye = legbye;
        }

        public String getBye() {
            return bye;
        }

        public void setBye(String bye) {
            this.bye = bye;
        }

        public String getPenalty() {
            return penalty;
        }

        public void setPenalty(String penalty) {
            this.penalty = penalty;
        }

        public String getOvers() {
            return getValidString(overs);
        }

        public void setOvers(String overs) {
            this.overs = overs;
        }
    }

    public static class PlayerBean extends AppBaseModel {
        /**
         * dismissed : true
         * seasonal_role : batsman
         * key : d_warner
         * fullname : David Warner
         * name : David Warner
         * dots : 15
         * sixes : 0
         * runs : 3
         * balls : 17
         * fours : 0
         * strike_rate : 17.65
         * out_str : c CT Bancroft b Pat Cummins
         * bowling : {"dots":104,"runs":61,"balls":126,"maiden_overs":8,"wickets":3,"extras":6,"overs":"21.0","economy":2.9}
         */

        private boolean dismissed;
        private String seasonal_role;
        private String key;
        private String fullname;
        private String name;
        private String dots;
        private String sixes;
        private String runs;
        private String balls;
        private String fours;
        private String strike_rate;
        private String out_str;
        private BowlingModel bowling;

        public boolean isDismissed() {
            return dismissed;
        }

        public void setDismissed(boolean dismissed) {
            this.dismissed = dismissed;
        }

        public String getSeasonal_role() {
            return getValidString(seasonal_role);
        }

        public void setSeasonal_role(String seasonal_role) {
            this.seasonal_role = seasonal_role;
        }

        public String getKey() {
            return getValidString(key);
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getFullname() {
            return getValidString(fullname);
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getName() {
            return getValidString(name);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDots() {
            return getValidString(dots);
        }

        public void setDots(String dots) {
            this.dots = dots;
        }

        public String getSixes() {
            return getValidString(sixes);
        }

        public void setSixes(String sixes) {
            this.sixes = sixes;
        }

        public String getRuns() {
            return getValidString(runs);
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getBalls() {
            return getValidString(balls);
        }

        public void setBalls(String balls) {
            this.balls = balls;
        }

        public String getFours() {
            return getValidString(fours);
        }

        public void setFours(String fours) {
            this.fours = fours;
        }

        public String getStrike_rate() {
            return getValidString(strike_rate);
        }

        public void setStrike_rate(String strike_rate) {
            this.strike_rate = strike_rate;
        }

        public String getOut_str() {
            return out_str;
        }

        public void setOut_str(String out_str) {
            this.out_str = out_str;
        }

        public BowlingModel getBowling() {
            return bowling;
        }

        public void setBowling(BowlingModel bowling) {
            this.bowling = bowling;
        }

        public boolean isBatsMan() {
            return !isBowler();
        }

        public boolean isBowler() {
            return getSeasonal_role().equalsIgnoreCase("bowler");
        }

    }

    public boolean isCompleted() {
        return getStatus().equalsIgnoreCase("completed");
    }
}
