package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class TeamModel extends AppBaseModel {

    /**
     * logo : http://fancy11.com/fancy11/public/uploads/teamlogo/1565496264_eng.png
     * short_name : ENG
     * full_name : England
     */

    private String logo;
    private String short_name;
    private String full_name;

    public String getLogo() {
        return getValidString(logo);
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShort_name() {
        return getValidString(short_name);
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getFull_name() {
        return getValidString(full_name);
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
