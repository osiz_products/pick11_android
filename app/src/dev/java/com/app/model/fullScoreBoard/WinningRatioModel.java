package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class WinningRatioModel extends AppBaseModel {


    /**
     * a : null
     * b : null
     */

    private String a;
    private String b;

    public String getA() {
        return getValidString(a);
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return getValidString(b);
    }

    public void setB(String b) {
        this.b = b;
    }
}
