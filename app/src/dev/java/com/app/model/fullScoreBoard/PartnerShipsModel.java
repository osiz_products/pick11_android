package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class PartnerShipsModel extends AppBaseModel {


    /**
     * index : 1
     * match : engaus_2019_test_02
     * pick : a
     * innings : 1
     * player_a : rory_burns
     * player_b : j_roy
     * first_ball : 37a62aa0-b89a-41b0-ae2e-c841b2b1ce00
     * last_ball : ed345085-2d7a-4db3-b3dd-a1532f84c169
     * start_over : 0.1
     * end_over : 1.3
     * runs : 0
     * balls : 9
     * four : 0
     * six : 0
     * run_rate : 0.00
     * player_a_runs : 0
     * player_a_balls : 6
     * player_a_four : 0
     * player_a_six : 0
     * player_b_runs : 0
     * player_b_balls : 3
     * player_b_four : 0
     * player_b_six : 0
     * overs_balls : 1.3
     * dismissed : true
     */

    private int index;
    private String match;
    private String team;
    private String innings;
    private String player_a;
    private String player_b;
    private String first_ball;
    private String last_ball;
    private String start_over;
    private String end_over;
    private int runs;
    private int balls;
    private int four;
    private int six;
    private String run_rate;
    private int player_a_runs;
    private int player_a_balls;
    private int player_a_four;
    private int player_a_six;
    private int player_b_runs;
    private int player_b_balls;
    private int player_b_four;
    private int player_b_six;
    private String overs_balls;
    private boolean dismissed;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMatch() {
        return getValidString(match);
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public String getTeam() {
        return getValidString(team);
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getInnings() {
        return getValidString(innings);
    }

    public void setInnings(String innings) {
        this.innings = innings;
    }

    public String getPlayer_a() {
        return getValidString(player_a);
    }

    public void setPlayer_a(String player_a) {
        this.player_a = player_a;
    }

    public String getPlayer_b() {
        return getValidString(player_b);
    }

    public void setPlayer_b(String player_b) {
        this.player_b = player_b;
    }

    public String getFirst_ball() {
        return getValidString(first_ball);
    }

    public void setFirst_ball(String first_ball) {
        this.first_ball = first_ball;
    }

    public String getLast_ball() {
        return getValidString(last_ball);
    }

    public void setLast_ball(String last_ball) {
        this.last_ball = last_ball;
    }

    public String getStart_over() {
        return getValidString(start_over);
    }

    public void setStart_over(String start_over) {
        this.start_over = start_over;
    }

    public String getEnd_over() {
        return getValidString(end_over);
    }

    public void setEnd_over(String end_over) {
        this.end_over = end_over;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public int getFour() {
        return four;
    }

    public void setFour(int four) {
        this.four = four;
    }

    public int getSix() {
        return six;
    }

    public void setSix(int six) {
        this.six = six;
    }

    public String getRun_rate() {
        return getValidString(run_rate);
    }

    public void setRun_rate(String run_rate) {
        this.run_rate = run_rate;
    }

    public int getPlayer_a_runs() {
        return player_a_runs;
    }

    public void setPlayer_a_runs(int player_a_runs) {
        this.player_a_runs = player_a_runs;
    }

    public int getPlayer_a_balls() {
        return player_a_balls;
    }

    public void setPlayer_a_balls(int player_a_balls) {
        this.player_a_balls = player_a_balls;
    }

    public int getPlayer_a_four() {
        return player_a_four;
    }

    public void setPlayer_a_four(int player_a_four) {
        this.player_a_four = player_a_four;
    }

    public int getPlayer_a_six() {
        return player_a_six;
    }

    public void setPlayer_a_six(int player_a_six) {
        this.player_a_six = player_a_six;
    }

    public int getPlayer_b_runs() {
        return player_b_runs;
    }

    public void setPlayer_b_runs(int player_b_runs) {
        this.player_b_runs = player_b_runs;
    }

    public int getPlayer_b_balls() {
        return player_b_balls;
    }

    public void setPlayer_b_balls(int player_b_balls) {
        this.player_b_balls = player_b_balls;
    }

    public int getPlayer_b_four() {
        return player_b_four;
    }

    public void setPlayer_b_four(int player_b_four) {
        this.player_b_four = player_b_four;
    }

    public int getPlayer_b_six() {
        return player_b_six;
    }

    public void setPlayer_b_six(int player_b_six) {
        this.player_b_six = player_b_six;
    }

    public String getOvers_balls() {
        return getValidString(overs_balls);
    }

    public void setOvers_balls(String overs_balls) {
        this.overs_balls = overs_balls;
    }

    public boolean isDismissed() {
        return dismissed;
    }

    public void setDismissed(boolean dismissed) {
        this.dismissed = dismissed;
    }
}
