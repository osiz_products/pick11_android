package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class TossModel extends AppBaseModel {


    /**
     * decision : bat
     * won : a
     * str : Dindigul Dragons won the toss and chose to bat first
     */

    private String decision;
    private String won;
    private String str;

    public String getDecision() {
        return getValidString(decision);
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getWon() {
        return getValidString(won);
    }

    public void setWon(String won) {
        this.won = won;
    }

    public String getStr() {
        return getValidString(str);
    }

    public void setStr(String str) {
        this.str = str;
    }
}
