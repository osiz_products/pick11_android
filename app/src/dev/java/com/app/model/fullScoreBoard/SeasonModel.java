package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class SeasonModel extends AppBaseModel {

    private String key;
    private String name;

    public String getKey() {
        return getValidString(key);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }
}
