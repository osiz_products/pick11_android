package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;

import java.util.List;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class TeamsModel extends AppBaseModel {


    /**
     * key : eng
     * name : England
     * short_name : ENG
     * match : {"key":"a","season_team_key":"engaus_2019_eng","keeper":"j_bairstow","captain":"j_root","players":["j_bairstow","j_root","jc_archer","jac_leach","j_roy","j_denly","s_broad","j_buttler","sam_curran","b_stokes","rory_burns","c_woakes","o_stone"],"playing_xi":["rory_burns","j_roy","j_root","j_denly","j_buttler","b_stokes","j_bairstow","c_woakes","jc_archer","s_broad","jac_leach"]}
     */

    private String key;
    private String name;
    private String short_name;
    private MatchBean match;

    public String getKey() {
        return getValidString(key);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return getValidString(short_name);
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public MatchBean getMatch() {
        return match;
    }

    public void setMatch(MatchBean match) {
        this.match = match;
    }

    public static class MatchBean extends AppBaseModel {
        /**
         * key : a
         * season_team_key : engaus_2019_eng
         * keeper : j_bairstow
         * captain : j_root
         * players : ["j_bairstow","j_root","jc_archer","jac_leach","j_roy","j_denly","s_broad","j_buttler","sam_curran","b_stokes","rory_burns","c_woakes","o_stone"]
         * playing_xi : ["rory_burns","j_roy","j_root","j_denly","j_buttler","b_stokes","j_bairstow","c_woakes","jc_archer","s_broad","jac_leach"]
         */

        private String key;
        private String season_team_key;
        private String keeper;
        private String captain;
        private List<String> players;
        private List<String> playing_xi;

        public String getKey() {
            return getValidString(key);
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getSeason_team_key() {
            return getValidString(season_team_key);
        }

        public void setSeason_team_key(String season_team_key) {
            this.season_team_key = season_team_key;
        }

        public String getKeeper() {
            return getValidString(keeper);
        }

        public void setKeeper(String keeper) {
            this.keeper = keeper;
        }

        public String getCaptain() {
            return getValidString(captain);
        }

        public void setCaptain(String captain) {
            this.captain = captain;
        }

        public List<String> getPlayers() {
            return players;
        }

        public void setPlayers(List<String> players) {
            this.players = players;
        }

        public List<String> getPlaying_xi() {
            return playing_xi;
        }

        public void setPlaying_xi(List<String> playing_xi) {
            this.playing_xi = playing_xi;
        }
    }
}
