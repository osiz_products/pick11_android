package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class BowlingModel extends AppBaseModel {

    /**
     * dots : 8
     * runs : 21
     * balls : 18
     * maiden_overs : 0
     * wickets : 2
     * extras : 1
     * overs : 3.0
     * economy : 7
     */

    private String dots;
    private String runs;
    private String balls;
    private String maiden_overs;
    private String wickets;
    private String extras;
    private String overs;
    private String economy;

    public String getDots() {
        return getValidString(dots);
    }

    public void setDots(String dots) {
        this.dots = dots;
    }

    public String getRuns() {
        return getValidString(runs);
    }

    public void setRuns(String runs) {
        this.runs = runs;
    }

    public String getBalls() {
        return getValidString(balls);
    }

    public void setBalls(String balls) {
        this.balls = balls;
    }

    public String getMaiden_overs() {
        return getValidString(maiden_overs);
    }

    public void setMaiden_overs(String maiden_overs) {
        this.maiden_overs = maiden_overs;
    }

    public String getWickets() {
        return getValidString(wickets);
    }

    public void setWickets(String wickets) {
        this.wickets = wickets;
    }

    public String getExtras() {
        return getValidString(extras);
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getOvers() {
        return getValidString(overs);
    }

    public void setOvers(String overs) {
        this.overs = overs;
    }

    public String getEconomy() {
        return getValidString(economy);
    }

    public void setEconomy(String economy) {
        this.economy = economy;
    }
}
