package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;

import java.util.List;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class BattingModel extends AppBaseModel {


    /**
     * dismissed : true
     * dots : 3
     * sixes : 0
     * runs : 2
     * balls : 5
     * fours : 0
     * strike_rate : 40
     * dismissed_at : {"team_runs":124,"over":19,"ball":3,"wicket_index":9,"ball_key":"114fc132-9e33-4074-86ed-4a458bad8af9"}
     * out_str : bowled b V Athisayaraj Davidson
     * ball_of_dismissed : {"comment":"V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!<\/b>. No runs.","wicket_type":"bowled","over":19,"ball":"3","highlight_names_keys":[["bowled","g_periyaswamy"]],"innings":"1","wagon_position":null,"decision_by":"umpire","wicket":"g_periyaswamy","match":"tnplt20_2019_g27","status":"played","updated":"2019-08-09T11:17:27.009000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"},"striker":"g_periyaswamy","batting_team":"b","other_fielder":null,"nonstriker":"m_siddharth","runs":0,"bowler":{"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1},"over_str":"18.3","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":1}}
     */

    private boolean dismissed;
    private int dots;
    private int sixes;
    private int runs;
    private int balls;
    private int fours;
    private int strike_rate;
    private DismissedAtBean dismissed_at;
    private String out_str;
    private BallOfDismissedBean ball_of_dismissed;

    public boolean isDismissed() {
        return dismissed;
    }

    public void setDismissed(boolean dismissed) {
        this.dismissed = dismissed;
    }

    public int getDots() {
        return dots;
    }

    public void setDots(int dots) {
        this.dots = dots;
    }

    public int getSixes() {
        return sixes;
    }

    public void setSixes(int sixes) {
        this.sixes = sixes;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public int getFours() {
        return fours;
    }

    public void setFours(int fours) {
        this.fours = fours;
    }

    public int getStrike_rate() {
        return strike_rate;
    }

    public void setStrike_rate(int strike_rate) {
        this.strike_rate = strike_rate;
    }

    public DismissedAtBean getDismissed_at() {
        return dismissed_at;
    }

    public void setDismissed_at(DismissedAtBean dismissed_at) {
        this.dismissed_at = dismissed_at;
    }

    public String getOut_str() {
        return out_str;
    }

    public void setOut_str(String out_str) {
        this.out_str = out_str;
    }

    public BallOfDismissedBean getBall_of_dismissed() {
        return ball_of_dismissed;
    }

    public void setBall_of_dismissed(BallOfDismissedBean ball_of_dismissed) {
        this.ball_of_dismissed = ball_of_dismissed;
    }

    public static class DismissedAtBean {
        /**
         * team_runs : 124
         * over : 19
         * ball : 3
         * wicket_index : 9
         * ball_key : 114fc132-9e33-4074-86ed-4a458bad8af9
         */

        private int team_runs;
        private int over;
        private int ball;
        private int wicket_index;
        private String ball_key;

        public int getTeam_runs() {
            return team_runs;
        }

        public void setTeam_runs(int team_runs) {
            this.team_runs = team_runs;
        }

        public int getOver() {
            return over;
        }

        public void setOver(int over) {
            this.over = over;
        }

        public int getBall() {
            return ball;
        }

        public void setBall(int ball) {
            this.ball = ball;
        }

        public int getWicket_index() {
            return wicket_index;
        }

        public void setWicket_index(int wicket_index) {
            this.wicket_index = wicket_index;
        }

        public String getBall_key() {
            return ball_key;
        }

        public void setBall_key(String ball_key) {
            this.ball_key = ball_key;
        }
    }

    public static class BallOfDismissedBean {
        /**
         * comment : V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!</b>. No runs.
         * wicket_type : bowled
         * over : 19
         * ball : 3
         * highlight_names_keys : [["bowled","g_periyaswamy"]]
         * innings : 1
         * wagon_position : null
         * decision_by : umpire
         * wicket : g_periyaswamy
         * match : tnplt20_2019_g27
         * status : played
         * updated : 2019-08-09T11:17:27.009000
         * ball_type : normal
         * batsman : {"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"}
         * striker : g_periyaswamy
         * batting_team : b
         * other_fielder : null
         * nonstriker : m_siddharth
         * runs : 0
         * bowler : {"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1}
         * over_str : 18.3
         * extras : 0
         * pick : {"runs":0,"extras":0,"ball_count":1,"wicket":1}
         */

        private String comment;
        private String wicket_type;
        private int over;
        private String ball;
        private String innings;
        private Object wagon_position;
        private String decision_by;
        private String wicket;
        private String match;
        private String status;
        private String updated;
        private String ball_type;
        private BatsmanBean batsman;
        private String striker;
        private String batting_team;
        private Object other_fielder;
        private String nonstriker;
        private int runs;
        private BowlerBean bowler;
        private String over_str;
        private String extras;
        private TeamBean team;
        private List<List<String>> highlight_names_keys;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getWicket_type() {
            return wicket_type;
        }

        public void setWicket_type(String wicket_type) {
            this.wicket_type = wicket_type;
        }

        public int getOver() {
            return over;
        }

        public void setOver(int over) {
            this.over = over;
        }

        public String getBall() {
            return ball;
        }

        public void setBall(String ball) {
            this.ball = ball;
        }

        public String getInnings() {
            return innings;
        }

        public void setInnings(String innings) {
            this.innings = innings;
        }

        public Object getWagon_position() {
            return wagon_position;
        }

        public void setWagon_position(Object wagon_position) {
            this.wagon_position = wagon_position;
        }

        public String getDecision_by() {
            return decision_by;
        }

        public void setDecision_by(String decision_by) {
            this.decision_by = decision_by;
        }

        public String getWicket() {
            return wicket;
        }

        public void setWicket(String wicket) {
            this.wicket = wicket;
        }

        public String getMatch() {
            return match;
        }

        public void setMatch(String match) {
            this.match = match;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getBall_type() {
            return ball_type;
        }

        public void setBall_type(String ball_type) {
            this.ball_type = ball_type;
        }

        public BatsmanBean getBatsman() {
            return batsman;
        }

        public void setBatsman(BatsmanBean batsman) {
            this.batsman = batsman;
        }

        public String getStriker() {
            return striker;
        }

        public void setStriker(String striker) {
            this.striker = striker;
        }

        public String getBatting_team() {
            return batting_team;
        }

        public void setBatting_team(String batting_team) {
            this.batting_team = batting_team;
        }

        public Object getOther_fielder() {
            return other_fielder;
        }

        public void setOther_fielder(Object other_fielder) {
            this.other_fielder = other_fielder;
        }

        public String getNonstriker() {
            return nonstriker;
        }

        public void setNonstriker(String nonstriker) {
            this.nonstriker = nonstriker;
        }

        public int getRuns() {
            return runs;
        }

        public void setRuns(int runs) {
            this.runs = runs;
        }

        public BowlerBean getBowler() {
            return bowler;
        }

        public void setBowler(BowlerBean bowler) {
            this.bowler = bowler;
        }

        public String getOver_str() {
            return over_str;
        }

        public void setOver_str(String over_str) {
            this.over_str = over_str;
        }

        public String getExtras() {
            return extras;
        }

        public void setExtras(String extras) {
            this.extras = extras;
        }

        public TeamBean getTeam() {
            return team;
        }

        public void setTeam(TeamBean team) {
            this.team = team;
        }

        public List<List<String>> getHighlight_names_keys() {
            return highlight_names_keys;
        }

        public void setHighlight_names_keys(List<List<String>> highlight_names_keys) {
            this.highlight_names_keys = highlight_names_keys;
        }

        public static class BatsmanBean {
            /**
             * runs : 0
             * dotball : 1
             * six : 0
             * four : 0
             * ball_count : 1
             * key : g_periyaswamy
             */

            private int runs;
            private int dotball;
            private int six;
            private int four;
            private int ball_count;
            private String key;

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getDotball() {
                return dotball;
            }

            public void setDotball(int dotball) {
                this.dotball = dotball;
            }

            public int getSix() {
                return six;
            }

            public void setSix(int six) {
                this.six = six;
            }

            public int getFour() {
                return four;
            }

            public void setFour(int four) {
                this.four = four;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }
        }

        public static class BowlerBean {
            /**
             * key : va_davidson
             * runs : 0
             * extras : 0
             * ball_count : 1
             * wicket : 1
             */

            private String key;
            private int runs;
            private int extras;
            private int ball_count;
            private int wicket;

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getExtras() {
                return extras;
            }

            public void setExtras(int extras) {
                this.extras = extras;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public int getWicket() {
                return wicket;
            }

            public void setWicket(int wicket) {
                this.wicket = wicket;
            }
        }

        public static class TeamBean {
            /**
             * runs : 0
             * extras : 0
             * ball_count : 1
             * wicket : 1
             */

            private int runs;
            private int extras;
            private int ball_count;
            private int wicket;

            public int getRuns() {
                return runs;
            }

            public void setRuns(int runs) {
                this.runs = runs;
            }

            public int getExtras() {
                return extras;
            }

            public void setExtras(int extras) {
                this.extras = extras;
            }

            public int getBall_count() {
                return ball_count;
            }

            public void setBall_count(int ball_count) {
                this.ball_count = ball_count;
            }

            public int getWicket() {
                return wicket;
            }

            public void setWicket(int wicket) {
                this.wicket = wicket;
            }
        }
    }
}
