package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;
import com.app.model.BowlingModel;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class PlayerModel extends AppBaseModel {


    /**
     * seasonal_role : bowler
     * fullname : G Periyaswamy
     * name : G Periyaswamy
     * identified_roles : {"keeper":false,"bowler":true,"batsman":false}
     * match : {"innings":{"1":{"batting":{"dismissed":true,"dots":3,"sixes":0,"runs":2,"balls":5,"fours":0,"strike_rate":40,"dismissed_at":{"team_runs":124,"over":19,"ball":3,"wicket_index":9,"ball_key":"114fc132-9e33-4074-86ed-4a458bad8af9"},"out_str":"bowled b V Athisayaraj Davidson","ball_of_dismissed":{"comment":"V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!<\/b>. No runs.","wicket_type":"bowled","over":19,"ball":"3","highlight_names_keys":[["bowled","g_periyaswamy"]],"innings":"1","wagon_position":null,"decision_by":"umpire","wicket":"g_periyaswamy","match":"tnplt20_2019_g27","status":"played","updated":"2019-08-09T11:17:27.009000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"},"striker":"g_periyaswamy","batting_team":"b","other_fielder":null,"nonstriker":"m_siddharth","runs":0,"bowler":{"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1},"over_str":"18.3","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":1}}},"bowling":{"dots":8,"runs":21,"balls":18,"maiden_overs":0,"wickets":2,"extras":1,"overs":"3.0","economy":7}}}}
     */

    private String seasonal_role;
    private String fullname;
    private String name;
    private IdentifiedRolesBean identified_roles;
    private MatchBean match;

    public String getSeasonal_role() {
        return getValidString(seasonal_role);
    }

    public void setSeasonal_role(String seasonal_role) {
        this.seasonal_role = seasonal_role;
    }

    public String getFullname() {
        return  getValidString(fullname);
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getName() {
        return getValidString( name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public IdentifiedRolesBean getIdentified_roles() {
        return identified_roles;
    }

    public void setIdentified_roles(IdentifiedRolesBean identified_roles) {
        this.identified_roles = identified_roles;
    }

    public MatchBean getMatch() {
        return match;
    }

    public void setMatch(MatchBean match) {
        this.match = match;
    }

    public static class IdentifiedRolesBean {
        /**
         * keeper : false
         * bowler : true
         * batsman : false
         */

        private boolean keeper;
        private boolean bowler;
        private boolean batsman;

        public boolean isKeeper() {
            return keeper;
        }

        public void setKeeper(boolean keeper) {
            this.keeper = keeper;
        }

        public boolean isBowler() {
            return bowler;
        }

        public void setBowler(boolean bowler) {
            this.bowler = bowler;
        }

        public boolean isBatsman() {
            return batsman;
        }

        public void setBatsman(boolean batsman) {
            this.batsman = batsman;
        }
    }

    public static class MatchBean {
        /**
         * innings : {"1":{"batting":{"dismissed":true,"dots":3,"sixes":0,"runs":2,"balls":5,"fours":0,"strike_rate":40,"dismissed_at":{"team_runs":124,"over":19,"ball":3,"wicket_index":9,"ball_key":"114fc132-9e33-4074-86ed-4a458bad8af9"},"out_str":"bowled b V Athisayaraj Davidson","ball_of_dismissed":{"comment":"V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!<\/b>. No runs.","wicket_type":"bowled","over":19,"ball":"3","highlight_names_keys":[["bowled","g_periyaswamy"]],"innings":"1","wagon_position":null,"decision_by":"umpire","wicket":"g_periyaswamy","match":"tnplt20_2019_g27","status":"played","updated":"2019-08-09T11:17:27.009000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"},"striker":"g_periyaswamy","batting_team":"b","other_fielder":null,"nonstriker":"m_siddharth","runs":0,"bowler":{"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1},"over_str":"18.3","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":1}}},"bowling":{"dots":8,"runs":21,"balls":18,"maiden_overs":0,"wickets":2,"extras":1,"overs":"3.0","economy":7}}}
         */

        private InningsBean innings;

        public InningsBean getInnings() {
            return innings;
        }

        public void setInnings(InningsBean innings) {
            this.innings = innings;
        }

        public static class InningsBean {
            /**
             * 1 : {"batting":{"dismissed":true,"dots":3,"sixes":0,"runs":2,"balls":5,"fours":0,"strike_rate":40,"dismissed_at":{"team_runs":124,"over":19,"ball":3,"wicket_index":9,"ball_key":"114fc132-9e33-4074-86ed-4a458bad8af9"},"out_str":"bowled b V Athisayaraj Davidson","ball_of_dismissed":{"comment":"V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!<\/b>. No runs.","wicket_type":"bowled","over":19,"ball":"3","highlight_names_keys":[["bowled","g_periyaswamy"]],"innings":"1","wagon_position":null,"decision_by":"umpire","wicket":"g_periyaswamy","match":"tnplt20_2019_g27","status":"played","updated":"2019-08-09T11:17:27.009000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"},"striker":"g_periyaswamy","batting_team":"b","other_fielder":null,"nonstriker":"m_siddharth","runs":0,"bowler":{"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1},"over_str":"18.3","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":1}}},"bowling":{"dots":8,"runs":21,"balls":18,"maiden_overs":0,"wickets":2,"extras":1,"overs":"3.0","economy":7}}
             */

            @SerializedName("1")
            private _$1Bean _$1;

            public _$1Bean get_$1() {
                return _$1;
            }

            public void set_$1(_$1Bean _$1) {
                this._$1 = _$1;
            }

            public static class _$1Bean {
                /**
                 * batting : {"dismissed":true,"dots":3,"sixes":0,"runs":2,"balls":5,"fours":0,"strike_rate":40,"dismissed_at":{"team_runs":124,"over":19,"ball":3,"wicket_index":9,"ball_key":"114fc132-9e33-4074-86ed-4a458bad8af9"},"out_str":"bowled b V Athisayaraj Davidson","ball_of_dismissed":{"comment":"V Athisayaraj Davidson to G Periyaswamy: <b>Bowled!<\/b>. No runs.","wicket_type":"bowled","over":19,"ball":"3","highlight_names_keys":[["bowled","g_periyaswamy"]],"innings":"1","wagon_position":null,"decision_by":"umpire","wicket":"g_periyaswamy","match":"tnplt20_2019_g27","status":"played","updated":"2019-08-09T11:17:27.009000","ball_type":"normal","batsman":{"runs":0,"dotball":1,"six":0,"four":0,"ball_count":1,"key":"g_periyaswamy"},"striker":"g_periyaswamy","batting_team":"b","other_fielder":null,"nonstriker":"m_siddharth","runs":0,"bowler":{"key":"va_davidson","runs":0,"extras":0,"ball_count":1,"wicket":1},"over_str":"18.3","extras":"0","pick":{"runs":0,"extras":0,"ball_count":1,"wicket":1}}}
                 * bowling : {"dots":8,"runs":21,"balls":18,"maiden_overs":0,"wickets":2,"extras":1,"overs":"3.0","economy":7}
                 */

                private BattingModel batting;
                private BowlingModel bowling;

                public BattingModel getBatting() {
                    return batting;
                }

                public void setBatting(BattingModel batting) {
                    this.batting = batting;
                }

                public BowlingModel getBowling() {
                    return bowling;
                }

                public void setBowling(BowlingModel bowling) {
                    this.bowling = bowling;
                }
            }
        }
    }
}
