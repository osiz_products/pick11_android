package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;
import com.google.gson.internal.LinkedTreeMap;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class ScoreMatchModel extends AppBaseModel {

    private String key;
    private String name;
    private String short_name;
    private String related_name;
    private String description;
    private String long_description;
    private String title;
    private SeasonModel season;
    private TeamBean teams;
    private MessageModel msgs;
    private StartDateBean start_date;
    private String format;
    private String match_overs;
    private String venue;
    private String status;
    private String status_overview;
    private String winner_team;
    private String ref;
    private String expires;
    private long approx_completed_ts;
    private TossModel toss;
    //batting_order
    private InningsModel innings;
    private WinningRatioModel winning_ratio;
    private boolean dl_applied;
    private String man_of_match;
    //    inactive_balls
    private NowModel now;
    private String day;
    //    balls
    LinkedTreeMap<String, Object> players;
    public String data_review_checkpoint;
    public String first_batting;
    public String result_by;
    public String cache_key;


    /**
     * a : {"key":"eng","name":"England","short_name":"ENG","match":{"key":"a","season_team_key":"engaus_2019_eng","keeper":"j_bairstow","captain":"j_root","players":["j_bairstow","j_root","jc_archer","jac_leach","j_roy","j_denly","s_broad","j_buttler","sam_curran","b_stokes","rory_burns","c_woakes","o_stone"],"playing_xi":["rory_burns","j_roy","j_root","j_denly","j_buttler","b_stokes","j_bairstow","c_woakes","jc_archer","s_broad","jac_leach"]}}
     * b : {"key":"aus","name":"Australia ","short_name":"AUS","match":{"key":"b","season_team_key":"engaus_2019_aus","keeper":"t_paine","captain":"t_paine","players":["m_neser","t_paine","j_pattinson","m_harris","p_cummins","j_hazlewood","m_starc","n_lyon","p_siddle","m_wade","u_khawaja","c_bancroft","s_smith","d_warner","t_head","m_marsh","mar_labuschagne"],"playing_xi":["c_bancroft","d_warner","u_khawaja","s_smith","t_head","m_wade","t_paine","p_cummins","p_siddle","n_lyon","j_hazlewood"]}}
     */


    public static class TeamBean extends AppBaseModel {

        private TeamsModel a;
        private TeamsModel b;

        public TeamsModel getA() {
            return a;
        }

        public void setA(TeamsModel a) {
            this.a = a;
        }

        public TeamsModel getB() {
            return b;
        }

        public void setB(TeamsModel b) {
            this.b = b;
        }
    }

    public static class StartDateBean extends AppBaseModel {

        /**
         * timestamp : 1565703900
         * iso : 2019-08-13T13:45+00:00
         * str : 13th Aug 2019 13:45 GMT
         */

        private long timestamp;
        private String iso;
        private String str;

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public String getIso() {
            return getValidString(iso);
        }

        public void setIso(String iso) {
            this.iso = iso;
        }

        public String getStr() {
            return getValidString(str);
        }

        public void setStr(String str) {
            this.str = str;
        }
    }

    public String getKey() {
        return getValidString(key);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return getValidString(short_name);
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getRelated_name() {
        return getValidString(related_name);
    }

    public void setRelated_name(String related_name) {
        this.related_name = related_name;
    }

    public String getDescription() {
        return getValidString(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLong_description() {
        return getValidString(long_description);
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SeasonModel getSeason() {
        return season;
    }

    public void setSeason(SeasonModel season) {
        this.season = season;
    }

    public TeamBean getTeams() {
        return teams;
    }

    public void setTeams(TeamBean teams) {
        this.teams = teams;
    }

    public MessageModel getMsgs() {
        return msgs;
    }

    public void setMsgs(MessageModel msgs) {
        this.msgs = msgs;
    }

    public StartDateBean getStart_date() {
        return start_date;
    }

    public void setStart_date(StartDateBean start_date) {
        this.start_date = start_date;
    }

    public String getFormat() {
        return getValidString(format);
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getMatch_overs() {
        return getValidString(match_overs);
    }

    public void setMatch_overs(String match_overs) {
        this.match_overs = match_overs;
    }

    public String getVenue() {
        return getValidString(venue);
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_overview() {
        return getValidString(status_overview);
    }

    public void setStatus_overview(String status_overview) {
        this.status_overview = status_overview;
    }

    public String getWinner_team() {
        return getValidString(winner_team);
    }

    public void setWinner_team(String winner_team) {
        this.winner_team = winner_team;
    }

    public String getRef() {
        return getValidString(ref);
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getExpires() {
        return getValidString(expires);
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public long getApprox_completed_ts() {
        return approx_completed_ts;
    }

    public void setApprox_completed_ts(long approx_completed_ts) {
        this.approx_completed_ts = approx_completed_ts;
    }

    public TossModel getToss() {
        return toss;
    }

    public void setToss(TossModel toss) {
        this.toss = toss;
    }

    public InningsModel getInnings() {
        return innings;
    }

    public void setInnings(InningsModel innings) {
        this.innings = innings;
    }

    public WinningRatioModel getWinning_ratio() {
        return winning_ratio;
    }

    public void setWinning_ratio(WinningRatioModel winning_ratio) {
        this.winning_ratio = winning_ratio;
    }

    public boolean isDl_applied() {
        return dl_applied;
    }

    public void setDl_applied(boolean dl_applied) {
        this.dl_applied = dl_applied;
    }

    public String getMan_of_match() {
        return getValidString(man_of_match);
    }

    public void setMan_of_match(String man_of_match) {
        this.man_of_match = man_of_match;
    }

    public NowModel getNow() {
        return now;
    }

    public void setNow(NowModel now) {
        this.now = now;
    }

    public String getDay() {
        return getValidString(day);
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LinkedTreeMap<String, Object> getPlayers() {
        return players;
    }

    public void setPlayers(LinkedTreeMap<String, Object> players) {
        this.players = players;
    }

    public String getData_review_checkpoint() {
        return getValidString(data_review_checkpoint);
    }

    public void setData_review_checkpoint(String data_review_checkpoint) {
        this.data_review_checkpoint = data_review_checkpoint;
    }

    public String getFirst_batting() {
        return getValidString(first_batting);
    }

    public void setFirst_batting(String first_batting) {
        this.first_batting = first_batting;
    }

    public String getResult_by() {
        return getValidString(result_by);
    }

    public void setResult_by(String result_by) {
        this.result_by = result_by;
    }

    public String getCache_key() {
        return getValidString(cache_key);
    }

    public void setCache_key(String cache_key) {
        this.cache_key = cache_key;
    }
}
