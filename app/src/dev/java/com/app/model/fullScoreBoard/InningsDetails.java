package com.app.model.fullScoreBoard;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Sunil yadav on 17/8/19.
 */
public class InningsDetails extends AppBaseModel {

    /**
     * a : {"1":"258/10 in 77.1"}
     * b : {"1":"80/4 in 37.1"}
     * team_a : {"logo":"http://fancy11.com/fancy11/public/uploads/teamlogo/1565496264_eng.png","short_name":"ENG","full_name":"England"}
     * team_b : {"logo":"http://fancy11.com/fancy11/public/uploads/teamlogo/1565496277_aus.png","short_name":"AUS","full_name":"Australia "}
     */

    private ABean a;
    private BBean b;
    private TeamModel team_a;
    private TeamModel team_b;

    public ABean getA() {
        return a;
    }

    public void setA(ABean a) {
        this.a = a;
    }

    public BBean getB() {
        return b;
    }

    public void setB(BBean b) {
        this.b = b;
    }

    public TeamModel getTeam_a() {
        return team_a;
    }

    public void setTeam_a(TeamModel team_a) {
        this.team_a = team_a;
    }

    public TeamModel getTeam_b() {
        return team_b;
    }

    public void setTeam_b(TeamModel team_b) {
        this.team_b = team_b;
    }

    public static class ABean {
        /**
         * 1 : 258/10 in 77.1
         */

        @SerializedName("1")
        private String _$1;

        public String get_$1() {
            return _$1;
        }

        public void set_$1(String _$1) {
            this._$1 = _$1;
        }
    }

    public static class BBean {
        /**
         * 1 : 80/4 in 37.1
         */

        @SerializedName("1")
        private String _$1;

        public String get_$1() {
            return _$1;
        }

        public void set_$1(String _$1) {
            this._$1 = _$1;
        }
    }




}
