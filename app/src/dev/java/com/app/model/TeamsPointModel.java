package com.app.model;

import com.app.appbase.AppBaseModel;

public class TeamsPointModel extends AppBaseModel {

    private long uteamid;
    private String teamname;
    private float ptotal;
    private long rank;
    private float winbal;
    private Integer playerPercentage;

    private float captainPercentage;
    private float viceCaptainPercentage;
    private float winamount;
    private String won_msg;


    public long getUteamid() {
        return uteamid;
    }

    public void setUteamid(long uteamid) {
        this.uteamid = uteamid;
    }

    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public float getPtotal() {
        return (ptotal);
    }

    public void setPtotal(float ptotal) {
        this.ptotal = ptotal;
    }

    public long getRank() {
        return (rank);
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    public float getWinbal() {
        return winbal;
    }

    public void setWinbal(float winbal) {
        this.winbal = winbal;
    }

    public String getTotalPointstext() {
        String s = getValidDecimalFormat(getPtotal());
        return s.replaceAll("\\.00", "");
    }

    public String getWinAmountText() {
        String s = getValidDecimalFormat(getWinbal());
        return s.replaceAll("\\.00", "");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TeamsPointModel)
            return ((TeamsPointModel) obj).getUteamid() == getUteamid();
        return false;
    }

    public Integer getPlayerPercentage() {
        return playerPercentage;
    }

    public void setPlayerPercentage(Integer playerPercentage) {
        this.playerPercentage = playerPercentage;
    }

    public float getCaptainPercentage() {
        return captainPercentage;
    }

    public void setCaptainPercentage(float captainPercentage) {
        this.captainPercentage = captainPercentage;
    }

    public float getViceCaptainPercentage() {
        return viceCaptainPercentage;
    }

    public void setViceCaptainPercentage(float viceCaptainPercentage) {
        this.viceCaptainPercentage = viceCaptainPercentage;
    }

    public float getWinamount() {
        return winamount;
    }

    public void setWinamount(float winamount) {
        this.winamount = winamount;
    }

    public String getWon_msg() {
        return won_msg;
    }

    public void setWon_msg(String won_msg) {
        this.won_msg = won_msg;
    }
}
