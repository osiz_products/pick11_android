package com.app.model;

import com.app.appbase.AppBaseModel;

public class TeamModelNew extends AppBaseModel {

    public long getIdnew() {
        return Idnew;
    }

    public void setIdnew(long idnew) {
        Idnew = idnew;
    }

    private long Idnew;
    private long userid;
    private String matchid;
    private String teamname;
    private String cap;
    private String vcap;

    private float playerPercentage;
    private float captainPercentage;
    private float viceCaptainPercentage;
    //public boolean isclicked=false;
    //public int index;


  /*  public long getId() {
        return Idnew;
    }

    public void setId(long id) {
        this.Idnew = id;
    }
*/
    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getCap() {
        return getValidString(cap);
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getVcap() {
        return getValidString(vcap);
    }

    public void setVcap(String vcap) {
        this.vcap = vcap;
    }

    public float getPlayerPercentage() {
        return playerPercentage;
    }

    public void setPlayerPercentage(float playerPercentage) {
        this.playerPercentage = playerPercentage;
    }

    public float getCaptainPercentage() {
        return captainPercentage;
    }

    public void setCaptainPercentage(float captainPercentage) {
        this.captainPercentage = captainPercentage;
    }

    public float getViceCaptainPercentage() {
        return viceCaptainPercentage;
    }

    public void setViceCaptainPercentage(float viceCaptainPercentage) {
        this.viceCaptainPercentage = viceCaptainPercentage;
    }

    /*public TeamModel(boolean isclicked,int index*//*,String fanId,String strAmount*//*)
    {
        this.index=index;
        this.isclicked=isclicked;
        *//*this.fanId=fanId;
        this.strAmount=strAmount;*//*
    }*/
}

