package com.app.model;

import com.app.appbase.AppBaseModel;

public class ScratchModel extends AppBaseModel {

    private String scratchcardamount;
    private String userid;
    private String matchid;
    private String contestid;
    private String poolcontestid;
    private String teamname;
    private String is_active;
    private String scratchcardimage;
    private String scratchcardfrontimage;

    private String type;
    private String couponcardimage;
    private String validtill;
    private String couponcode;


    public String getScratchcardamount() {
        return scratchcardamount;
    }

    public void setScratchcardamount(String scratchcardamount) {
        this.scratchcardamount = scratchcardamount;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getContestid() {
        return contestid;
    }

    public void setContestid(String contestid) {
        this.contestid = contestid;
    }

    public String getPoolcontestid() {
        return poolcontestid;
    }

    public void setPoolcontestid(String poolcontestid) {
        this.poolcontestid = poolcontestid;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getScratchcardimage() {
        return scratchcardimage;
    }

    public void setScratchcardimage(String scratchcardimage) {
        this.scratchcardimage = scratchcardimage;
    }

    public String getScratchcardfrontimage() {
        return scratchcardfrontimage;
    }

    public void setScratchcardfrontimage(String scratchcardfrontimage) {
        this.scratchcardfrontimage = scratchcardfrontimage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCouponcardimage() {
        return couponcardimage;
    }

    public void setCouponcardimage(String couponcardimage) {
        this.couponcardimage = couponcardimage;
    }

    public String getValidtill() {
        return validtill;
    }

    public void setValidtill(String validtill) {
        this.validtill = validtill;
    }

    public String getCouponcode() {
        return couponcode;
    }

    public void setCouponcode(String couponcode) {
        this.couponcode = couponcode;
    }
}
