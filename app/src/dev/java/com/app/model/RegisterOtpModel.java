package com.app.model;

import com.app.appbase.AppBaseModel;

public class RegisterOtpModel extends AppBaseModel {


    /**
     * mobile_code : +91
     * mobile : 9602991945
     * otp : 9530
     */

    private String mobile_code;
    private String mobile;
    private String otp;

    public String getMobile_code() {
        return getValidString(mobile_code);
    }

    public void setMobile_code(String mobile_code) {
        this.mobile_code = mobile_code;
    }

    public String getMobile() {
        return getValidString(mobile);
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return getValidString(otp);
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
