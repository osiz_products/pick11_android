package com.app.model;

import com.app.appbase.AppBaseModel;

public class PaymentModel extends AppBaseModel {

    private String TXNID;
    private String BANKTXNID;
    private String ORDERID;
    private String TXNAMOUNT;
    private String STATUS;
    private String TXNTYPE;
    private String GATEWAYNAME;
    private String RESPCODE;
    private String RESPMSG;
    private String BANKNAME;
    private String MID;
    private String PAYMENTMODE;
    private String REFUNDAMT;
    private String TXNDATE;

    private WalletModel wallet;

    public String getTXNID() {
        return getValidString(TXNID);
    }

    public void setTXNID(String TXNID) {
        this.TXNID = TXNID;
    }

    public String getBANKTXNID() {
        return getValidString(BANKTXNID);
    }

    public void setBANKTXNID(String BANKTXNID) {
        this.BANKTXNID = BANKTXNID;
    }

    public String getORDERID() {
        return getValidString(ORDERID);
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    public String getTXNAMOUNT() {
        return getValidString(TXNAMOUNT);
    }

    public void setTXNAMOUNT(String TXNAMOUNT) {
        this.TXNAMOUNT = TXNAMOUNT;
    }

    public String getSTATUS() {
        return getValidString(STATUS);
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getTXNTYPE() {
        return getValidString(TXNTYPE);
    }

    public void setTXNTYPE(String TXNTYPE) {
        this.TXNTYPE = TXNTYPE;
    }

    public String getGATEWAYNAME() {
        return getValidString(GATEWAYNAME);
    }

    public void setGATEWAYNAME(String GATEWAYNAME) {
        this.GATEWAYNAME = GATEWAYNAME;
    }

    public String getRESPCODE() {
        return getValidString(RESPCODE);
    }

    public void setRESPCODE(String RESPCODE) {
        this.RESPCODE = RESPCODE;
    }

    public String getRESPMSG() {
        return getValidString(RESPMSG);
    }

    public void setRESPMSG(String RESPMSG) {
        this.RESPMSG = RESPMSG;
    }

    public String getBANKNAME() {
        return getValidString(BANKNAME);
    }

    public void setBANKNAME(String BANKNAME) {
        this.BANKNAME = BANKNAME;
    }

    public String getMID() {
        return getValidString(MID);
    }

    public void setMID(String MID) {
        this.MID = MID;
    }

    public String getPAYMENTMODE() {
        return getValidString(PAYMENTMODE);
    }

    public void setPAYMENTMODE(String PAYMENTMODE) {
        this.PAYMENTMODE = PAYMENTMODE;
    }

    public String getREFUNDAMT() {
        return getValidString(REFUNDAMT);
    }

    public void setREFUNDAMT(String REFUNDAMT) {
        this.REFUNDAMT = REFUNDAMT;
    }

    public String getTXNDATE() {
        return getValidString(TXNDATE);
    }

    public void setTXNDATE(String TXNDATE) {
        this.TXNDATE = TXNDATE;
    }

    public WalletModel getWallet() {
        return wallet;
    }

    public void setWallet(WalletModel wallet) {
        this.wallet = wallet;
    }

    public String getFormattedDate(int tag) {
        String time = "";
        if (!isValidString(getTXNDATE())) return time;
        if (Long.parseLong(getTXNDATE()) > 0) {
            if (tag == TAG_THREE) {
                time = getFormattedCalendar(DATE_THREE, Long.parseLong(getTXNDATE()));
            }
        }
        return time;
    }
}
