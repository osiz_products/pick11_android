package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class PlayerInfoModel extends AppBaseModel {

    private String name;
    private List<PlayersBean> players;

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PlayersBean> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayersBean> players) {
        this.players = players;
    }

    public static class PlayersBean extends AppBaseModel {

        private String name;
        private String pid;

        public String getName() {
            return getValidString(name);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPid() {
            return getValidString(pid);
        }

        public void setPid(String pid) {
            this.pid = pid;
        }
    }
}
