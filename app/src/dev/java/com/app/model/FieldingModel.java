package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FieldingModel extends AppBaseModel {

    private String title;
    private List<ScoresBean> scores;

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ScoresBean> getScores() {
        return scores;
    }

    public void setScores(List<ScoresBean> scores) {
        this.scores = scores;
    }

    public static class ScoresBean extends AppBaseModel {
        /**
         * name : AB de Villiers
         * runout : 0
         * stumped : 0
         * bowled : 0
         * lbw : 0
         * catch : 1
         * pid : 44936
         */

        private String name;
        private String runout;
        private String stumped;
        private String bowled;
        private String lbw;
        @SerializedName("catch")
        private String catchX;
        private String pid;

        public String getName() {
            return getValidString(name);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRunout() {
            return getValidString(runout);
        }

        public void setRunout(String runout) {
            this.runout = runout;
        }

        public String getStumped() {
            return getValidString(stumped);
        }

        public void setStumped(String stumped) {
            this.stumped = stumped;
        }

        public String getBowled() {
            return getValidString(bowled);
        }

        public void setBowled(String bowled) {
            this.bowled = bowled;
        }

        public String getLbw() {
            return getValidString(lbw);
        }

        public void setLbw(String lbw) {
            this.lbw = lbw;
        }

        public String getCatchX() {
            return getValidString(catchX);
        }

        public void setCatchX(String catchX) {
            this.catchX = catchX;
        }

        public String getPid() {
            return getValidString(pid);
        }

        public void setPid(String pid) {
            this.pid = pid;
        }
    }
}
