package com.app.model;

import com.app.appbase.AppBaseModel;

public class TransactionModel extends AppBaseModel {

    private String txid;
    private String amount;
    private String status;
    private String ttype;
    private String atype;
    private long txdate;
    private String des;

    public String getTxid() {
        return getValidString(txid);
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getAmount() {
        return getValidString(amount);
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTtype() {
        return getValidString(ttype);
    }

    public void setTtype(String ttype) {
        this.ttype = ttype;
    }

    public String getAtype() {
        return getValidString(atype);
    }

    public void setAtype(String atype) {
        this.atype = atype;
    }

    public long getTxdate() {
        return txdate;
    }

    public void setTxdate(long txdate) {
        this.txdate = txdate;
    }

    public String getDes() {
        return getValidString(des);
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCreditAmount() {
        return getTtype().equalsIgnoreCase("cr") ? getAmount() : "0.00";
    }

    public String getDeditAmount() {
        return getTtype().equalsIgnoreCase("dr") ? getAmount() : "0.00";
    }

    public boolean isCredit() {
        return getTtype().equalsIgnoreCase("cr");
    }

    public String getCreditAmountText() {
        return getCreditAmount().replaceAll("\\.00", "");
    }

    public String getDeditAmountText() {
        return getDeditAmount().replaceAll("\\.00", "");
    }


    public String getFormattedDate(int tag) {
        String time = "0";
        if (getTxdate() > 0) {
            if (tag == TAG_THREE) {
                time = getFormattedCalendar(DATE_THREE, getTxdate());
            }
        }
        return time;
    }


    public String getTrasnctionType(String atype) {
        String string = "";
        if (atype.equals("ntflpool")) {
            string = "Cancel pool refund | Balance Wallet";
        } else if (atype.equals("cjoin")) {
            string = "Contest join | Balance Wallet";
        } else if (atype.equals("win")) {
            string = "Win amount | Balance Wallet";
        } else if (atype.equals("addbal")) {
            string = "Add balance";
            if (getStatus().equals("TXN_PENDING")) {
                string += " | Pending";
            } else {
                string += " | Success";
            }
        } else if (atype.equals("withdr")) {
            string = "Withdrawal";
            if (getStatus().equals("TXN_PENDING")) {
                string += " | Pending";
            } else {
                string += " | Success";
            }
        }

        return string;
    }
}
