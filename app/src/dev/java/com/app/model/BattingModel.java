package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BattingModel extends AppBaseModel {

    private String title;
    private List<ScoresBean> scores;

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ScoresBean> getScores() {
        return scores;
    }

    public void setScores(List<ScoresBean> scores) {
        this.scores = scores;
    }

    public static class ScoresBean extends AppBaseModel {
        /**
         * dismissal-by : {"name":"S Lamichhane","pid":"960361"}
         * dismissal : lbw
         * SR : 100
         * 6s : 0
         * 4s : 1
         * B : 7
         * R : 7
         * dismissal-info : lbw b Lamichhane
         * batsman : SR Watson
         * pid : 8180
         * detail : 8 (w 8)
         */

        @SerializedName("dismissal-by")
        private Object dismissalby;
        private String dismissal;
        private String SR;
        @SerializedName("6s")
        private String _$6s;
        @SerializedName("4s")
        private String _$4s;
        private String B;
        private String R;
        @SerializedName("dismissal-info")
        private String dismissalinfo;
        private String batsman;
        private String pid;
        private String detail;

        public Object getDismissalby() {
            return dismissalby;
        }

        public void setDismissalby(Object dismissalby) {
            this.dismissalby = dismissalby;
        }

        public String getDismissal() {
            return getValidString(dismissal);
        }

        public void setDismissal(String dismissal) {
            this.dismissal = dismissal;
        }

        public String getSR() {
            return getValidString(SR);
        }

        public void setSR(String SR) {
            this.SR = SR;
        }

        public String get_$6s() {
            return getValidString(_$6s);
        }

        public void set_$6s(String _$6s) {
            this._$6s = _$6s;
        }

        public String get_$4s() {
            return getValidString(_$4s);
        }

        public void set_$4s(String _$4s) {
            this._$4s = _$4s;
        }

        public String getB() {
            return getValidString(B);
        }

        public void setB(String B) {
            this.B = B;
        }

        public String getR() {
            return getValidString(R);
        }

        public void setR(String R) {
            this.R = R;
        }

        public String getDismissalinfo() {
            return getValidString(dismissalinfo);
        }

        public void setDismissalinfo(String dismissalinfo) {
            this.dismissalinfo = dismissalinfo;
        }

        public String getBatsman() {
            return getValidString(batsman);
        }

        public void setBatsman(String batsman) {
            this.batsman = batsman;
        }

        public String getPid() {
            return getValidString(pid);
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getDetail() {
            return getValidString(detail);
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public static class DismissalbyBean extends AppBaseModel {
            /**
             * name : S Lamichhane
             * pid : 960361
             */

            private String name;
            private String pid;

            public String getName() {
                return getValidString(name);
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPid() {
                return getValidString(pid);
            }

            public void setPid(String pid) {
                this.pid = pid;
            }
        }
    }
}
