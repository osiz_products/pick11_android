package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class ContestDetailModel extends AppBaseModel {

    double joinfee;
    double totalwining;
    String winners;
    int maxteams;
    int joinleft;
    String c;
    String m;
    String s;

    List<PriceBreakUpModel> brkprize;

    public double getJoinfee() {
        return joinfee;
    }

    public void setJoinfee(double joinfee) {
        this.joinfee = joinfee;
    }

    public double getTotalwining() {
        return totalwining;
    }

    public void setTotalwining(double totalwining) {
        this.totalwining = totalwining;
    }

    public String getWinners() {
        return getValidString(winners);
    }

    public void setWinners(String winners) {
        this.winners = winners;
    }

    public int getMaxteams() {
        return maxteams;
    }

    public void setMaxteams(int maxteams) {
        this.maxteams = maxteams;
    }

    public int getJoinleft() {
        return joinleft;
    }

    public void setJoinleft(int joinleft) {
        this.joinleft = joinleft;
    }

    public String getPricePool() {
        return String.valueOf(getTotalwining());
    }

    public String get_entry_fees() {
        return String.valueOf(getJoinfee());
    }

    public String getC() {
        return getValidString(c);
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getM() {
        return getValidString(m);
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getS() {
        return getValidString(s);
    }

    public void setS(String s) {
        this.s = s;
    }

    public List<PriceBreakUpModel> getBrkprize() {
        return brkprize;
    }

    public void setBrkprize(List<PriceBreakUpModel> brkprize) {
        this.brkprize = brkprize;
    }

    public String getTotalTeam() {
        return String.valueOf(getMaxteams()) + " Teams";
    }

    public String getTeamLeft() {
        return String.valueOf(getJoinleft()) + " Teams Left";
    }

    public String getPercent() {
        if (getMaxteams() - getJoinleft() > 1)
            return String.valueOf(((getMaxteams() - getJoinleft()) * 100) / getMaxteams()) + "%";
        return "0%";
    }

    public boolean isConfirmWinning() {
        return getC().equalsIgnoreCase("0");
    }

    public boolean isMultiJoin() {
        return getM().equalsIgnoreCase("1");
    }
}
