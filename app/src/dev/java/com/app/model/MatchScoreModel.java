package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class MatchScoreModel extends AppBaseModel {

    String _id;
    String matchid;
    String type;
    String ismatchcomplete;
    MatchScoreDetailModel match;
    TeamDetails teamdetails;
    ScoreBoard teamScore;
    String matchstatus;

    public String get_id() {
        return getValidString(_id);
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMatchid() {
        return getValidString(matchid);
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getType() {
        return getValidString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsmatchcomplete() {
        return getValidString(ismatchcomplete);
    }

    public void setIsmatchcomplete(String ismatchcomplete) {
        this.ismatchcomplete = ismatchcomplete;
    }

    public MatchScoreDetailModel getMatch() {
        return match;
    }

    public void setMatch(MatchScoreDetailModel match) {
        this.match = match;
    }

    public TeamDetails getTeamdetails() {
        return teamdetails;
    }

    public void setTeamdetails(TeamDetails teamdetails) {
        this.teamdetails = teamdetails;
    }

    public ScoreBoard getTeamScore() {
        return teamScore;
    }

    public void setTeamScore(ScoreBoard teamScore) {
        this.teamScore = teamScore;
    }

    public String getMatchstatus() {
        return getValidString(matchstatus);
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus;
    }

    public class TeamDetails {

        private TeamBean team1;
        private TeamBean team2;

        public TeamBean getTeam1() {
            return team1;
        }

        public void setTeam1(TeamBean team1) {
            this.team1 = team1;
        }

        public TeamBean getTeam2() {
            return team2;
        }

        public void setTeam2(TeamBean team2) {
            this.team2 = team2;
        }
    }

    public class ScoreBoard {
        List<ScoreModel> team1;
        List<ScoreModel> team2;

        public List<ScoreModel> getTeam1() {
            return team1;
        }

        public void setTeam1(List<ScoreModel> team1) {
            this.team1 = team1;
        }

        public List<ScoreModel> getTeam2() {
            return team2;
        }

        public void setTeam2(List<ScoreModel> team2) {
            this.team2 = team2;
        }
    }

    public class TeamBean extends AppBaseModel {


        private String teamName;
        private String teamlogo;

        public String getTeamName() {
            return getValidString(teamName);
        }

        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }

        public String getTeamlogo() {
            return getValidString(teamlogo);
        }

        public void setTeamlogo(String teamlogo) {
            this.teamlogo = teamlogo;
        }
    }
}
