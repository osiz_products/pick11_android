package com.app.model;

import com.app.appbase.AppBaseModel;

public class JoinedTeamModel extends AppBaseModel {

    long uteamid;

    public long getUteamid() {
        return uteamid;
    }

    public void setUteamid(long uteamid) {
        this.uteamid = uteamid;
    }
}
