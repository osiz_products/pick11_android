package com.app.model;

import com.app.appbase.AppBaseModel;
import com.pickeleven.R;

import java.util.concurrent.TimeUnit;

public class MatchModel extends AppBaseModel {

    private String matchid;
    private String seriesid;
    private String seriesname;
    private String matchname;
    private String team1;
    private String team2;
    private String team1logo;
    private String team2logo;
    private String gametype;
    private String gameid;
    private double totalpoints;
    private String mtype;
    private String match_progress;
    private long mdate;
    private long mdategmt;
    private String mstatus;
    private TeamScoreModel team1Score;
    private TeamScoreModel team2Score;
    private String lineups_out;
    private String status;

    public String getMatchid() {
        return getValidString(matchid);
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getSeriesname() {
        return getValidString(seriesname);
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public String getMatchname() {
        return getValidString(matchname);
    }

    public void setMatchname(String matchname) {
        this.matchname = matchname;
    }

    public String getSeriesid() {
        return getValidString(seriesid);
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getTeam1() {
        return getValidString(team1);
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return getValidString(team2);
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getTeam1logo() {
        return getValidString(team1logo);
    }

    public void setTeam1logo(String team1logo) {
        this.team1logo = team1logo;
    }

    public String getTeam2logo() {
        return getValidString(team2logo);
    }

    public void setTeam2logo(String team2logo) {
        this.team2logo = team2logo;
    }

    public String getGametype() {
        return getValidString(gametype);
    }

    public void setGametype(String gametype) {
        this.gametype = gametype;
    }

    public String getGameid() {
        return getValidString(gameid);
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public double getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(double totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getMatch_progress() {
        return getValidString(match_progress);
    }

    public void setMatch_progress(String match_progress) {
        this.match_progress = match_progress;
    }

    public String getMtype() {
        return getValidString(mtype);
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public long getMdate() {
        return mdate;
    }

    public void setMdate(long mdate) {
        this.mdate = mdate;
    }

    public long getMdategmt() {
        return mdategmt;
    }

    public void getMdategmt(long mdategmt) {
        this.mdategmt = mdategmt;
    }

    public String getMstatus() {
        return getValidString(mstatus);
    }

    public void setMstatus(String mstatus) {
        this.mstatus = mstatus;
    }

    public TeamScoreModel getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(TeamScoreModel team1Score) {
        this.team1Score = team1Score;
    }

    public TeamScoreModel getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(TeamScoreModel team2Score) {
        this.team2Score = team2Score;
    }

    public String getFormattedDate(int tag) {
        String time = "0";
        if (getMdate() > 0) {
            if (tag == TAG_ONE) {
                time = getFormattedCalendar(DATE_ONE, getMdate());
            }
        }
        return time;
    }

    public boolean isFixtureMatch() {
        return getMstatus().endsWith("uc");
    }

    public boolean isLiveMatch() {
        return getMstatus().endsWith("li");
    }

    public boolean isPastMatch() {
        return getMstatus().equals("dc");
    }

    public boolean isUnderReview() {
        return getMstatus().equals("cm");
    }

    public boolean isCanceled() {
        return getMstatus().equals("cl");
    }

    public String getRemainTimeText(long serverdate) {

        if (isFixtureMatch()) {
//            System.out.println("Start");
            long matchRemainTime = getMdategmt() - serverdate;
            if (matchRemainTime < 0) {
                return "";
            }
            long days = TimeUnit.SECONDS.toDays(matchRemainTime);
            StringBuilder builder = new StringBuilder();
            if (days > 0) {
                builder.append(days);
                builder.append((days > 1) ? " days" : " day");
                builder.append(" ");
            }
            builder.append(getValidTimeText(matchRemainTime - TimeUnit.DAYS.toSeconds(days)));
//            System.out.println("End");
            return builder.toString();
        } else if (isLiveMatch()) {
           // return "STARTED";
            return "In progress";
        } else if (isUnderReview()) {
            return "Under Review";
        } else if (isCanceled()) {
            return "Canceled";
        } else {
            return "COMPLETED";
        }
    }


    public int getTimerColor() {
        if (isFixtureMatch()) {
            return R.color.color_timer;
        } else if (isLiveMatch()) {
            return R.color.colorOrange;
        } else if (isUnderReview()) {
            return R.color.colorOrange;
        } else if (isPastMatch()) {
            return R.color.colorGreen;
        } else if (isCanceled()) {
            return R.color.color_black;
        } else {
            return R.color.color_white;
        }
    }

    public String getLineups_out() {
        return lineups_out;
    }

    public void setLineups_out(String lineups_out) {
        this.lineups_out = lineups_out;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
