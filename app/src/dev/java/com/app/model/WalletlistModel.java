package com.app.model;

import com.app.appbase.AppBaseModel;

public class WalletlistModel extends AppBaseModel {

    private String email;
    private String team1;
    private String team2;
    private Double comissionfee;
    private long mdate;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public long getMdate() {
        return mdate;
    }

    public void setMdate(long mdate) {
        this.mdate = mdate;
    }


    public String getFormattedDate(int tag) {
        String time = "0";
        if (getMdate() > 0) {
            if (tag == TAG_THREE) {
                time = getFormattedCalendar(TIME_FIVE, getMdate());
            }
        }
        return time;
    }


    public Double getComissionfee() {
        return comissionfee;
    }

    public void setComissionfee(Double comissionfee) {
        this.comissionfee = comissionfee;
    }
}


