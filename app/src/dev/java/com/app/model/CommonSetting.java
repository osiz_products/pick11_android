package com.app.model;

import com.app.appbase.AppBaseModel;

public class CommonSetting extends AppBaseModel {
    private String supportemail;
    private String supportphone;
    private String supportteam;
    private String joinbonusmessage;
    private String panimgsize;

    public String getSupportemail() {
        return getValidString(supportemail);
    }

    public void setSupportemail(String supportemail) {
        this.supportemail = supportemail;
    }

    public String getSupportphone() {
        return getValidString(supportphone);
    }

    public void setSupportphone(String supportphone) {
        this.supportphone = supportphone;
    }

    public String getSupportteam() {
        return getValidString(supportteam);
    }

    public void setSupportteam(String supportteam) {
        this.supportteam = supportteam;
    }

    public String getJoinbonusmessage() {
        return getValidString(joinbonusmessage);
    }

    public void setJoinbonusmessage(String joinbonusmessage) {
        this.joinbonusmessage = joinbonusmessage;
    }

    public String getPanimgsize() {
        return getValidString(panimgsize);
    }

    public void setPanimgsize(String panimgsize) {
        this.panimgsize = panimgsize;
    }
}
