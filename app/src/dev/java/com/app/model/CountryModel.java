package com.app.model;

import com.app.appbase.AppBaseModel;

public class CountryModel extends AppBaseModel {

    private String id;
    private String sortname;
    private String name;
    private String phonecode;

    public String getId() {
        return getValidString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSortname() {
        return getValidString(sortname);
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhonecode() {
        return getValidString(phonecode);
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }
}
