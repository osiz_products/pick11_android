package com.app.model;

import com.app.appbase.AppBaseModel;

public class TransactionInfModel extends AppBaseModel {

    private String team1logo;
    private String team2logo;
    private String team1;
    private String team2;
    private String mtype;
    private String seriesname;
    private String expectedEarnings;
    private Integer joinedUserCount;

    public String getTeam1logo() {
        return team1logo;
    }

    public void setTeam1logo(String team1logo) {
        this.team1logo = team1logo;
    }

    public String getTeam2logo() {
        return team2logo;
    }

    public void setTeam2logo(String team2logo) {
        this.team2logo = team2logo;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public String getSeriesname() {
        return seriesname;
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public String getExpectedEarnings() {
        return expectedEarnings;
    }

    public void setExpectedEarnings(String expectedEarnings) {
        this.expectedEarnings = expectedEarnings;
    }

    public Integer getJoinedUserCount() {
        return joinedUserCount;
    }

    public void setJoinedUserCount(Integer joinedUserCount) {
        this.joinedUserCount = joinedUserCount;
    }

}


