package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewScoreBoardModel extends AppBaseModel {

    /**
     * inningsdetail : {"a":{"1":"182/6 in 20.0"},"b":{"1":"152/9 in 20.0"},"team_a":{"logo":"http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771499_DD.jpeg","short_name":"DID","full_name":"Dindigul Dragons"},"team_b":{"logo":"http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771501_MP.jpeg","short_name":"MPS","full_name":"Madurai Panthers"}}
     * matchstarted : {"$date":{"$numberLong":"1563803100000"}}
     * status : completed
     * status_overview : result
     * type : t20
     */

    private InningsdetailBean inningsdetail;
    private MatchstartedBean matchstarted;
    private String status;
    private String status_overview;
    private String type;
    private MsgInfoBean msg_info;

    public InningsdetailBean getInningsdetail() {
        return inningsdetail;
    }

    public void setInningsdetail(InningsdetailBean inningsdetail) {
        this.inningsdetail = inningsdetail;
    }

    public MatchstartedBean getMatchstarted() {
        return matchstarted;
    }

    public void setMatchstarted(MatchstartedBean matchstarted) {
        this.matchstarted = matchstarted;
    }

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_overview() {
        return getValidString(status_overview);
    }

    public void setStatus_overview(String status_overview) {
        this.status_overview = status_overview;
    }

    public String getType() {
        return getValidString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public MsgInfoBean getMsg_info() {
        return msg_info;
    }

    public void setMsg_info(MsgInfoBean msg_info) {
        this.msg_info = msg_info;
    }

    public boolean isMatchStarted() {
        return getStatus().equalsIgnoreCase("started");
    }

    public boolean isMatchNotStarted() {
        return getStatus().equalsIgnoreCase("not_started");
    }

    public boolean isMatchCompleted() {
        return getStatus().equalsIgnoreCase("completed");
    }

//    result, scheduled, abandoned

    public static class InningsdetailBean {
        /**
         * a : {"1":"182/6 in 20.0"}
         * b : {"1":"152/9 in 20.0"}
         * team_a : {"logo":"http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771499_DD.jpeg","short_name":"DID","full_name":"Dindigul Dragons"}
         * team_b : {"logo":"http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771501_MP.jpeg","short_name":"MPS","full_name":"Madurai Panthers"}
         */

        private ABean a;
        private BBean b;
        private TeamABean team_a;
        private TeamBBean team_b;

        private TeamABeanf team_home;
        private TeamBBeanf team_away;

        public ABean getA() {
            return a;
        }

        public void setA(ABean a) {
            this.a = a;
        }

        public BBean getB() {
            return b;
        }

        public void setB(BBean b) {
            this.b = b;
        }

        public TeamABean getTeam_a() {
            return team_a;
        }

        public void setTeam_a(TeamABean team_a) {
            this.team_a = team_a;
        }

        public TeamBBean getTeam_b() {
            return team_b;
        }

        public void setTeam_b(TeamBBean team_b) {
            this.team_b = team_b;
        }

        public TeamABeanf getTeam_home() {
            return team_home;
        }

        public void setTeam_home(TeamABeanf team_home) {
            this.team_home = team_home;
        }

        public TeamBBeanf getTeam_away() {
            return team_away;
        }

        public void setTeam_away(TeamBBeanf team_away) {
            this.team_away = team_away;
        }

        public static class ABean extends AppBaseModel {
            /**
             * 1 : 182/6 in 20.0
             */

            @SerializedName("1")
            private String _$1;

            public String get_$1() {
                return getValidString(_$1);
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }
        }

        public static class BBean extends AppBaseModel {
            /**
             * 1 : 152/9 in 20.0
             */

            @SerializedName("1")
            private String _$1;

            public String get_$1() {
                return getValidString(_$1);
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }
        }

        public static class TeamABean extends AppBaseModel {
            /**
             * logo : http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771499_DD.jpeg
             * short_name : DID
             * full_name : Dindigul Dragons
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }

        public static class TeamABeanf extends AppBaseModel {
            /**
             * logo : http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771499_DD.jpeg
             * short_name : DID
             * full_name : Dindigul Dragons
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }

        public static class TeamBBean extends AppBaseModel {
            /**
             * logo : http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771501_MP.jpeg
             * short_name : MPS
             * full_name : Madurai Panthers
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }

        public static class TeamBBeanf extends AppBaseModel {
            /**
             * logo : http://172.104.180.148/brfcricketapi/public/uploads/teamlogo/1563771501_MP.jpeg
             * short_name : MPS
             * full_name : Madurai Panthers
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }
    }

    public static class MatchstartedBean extends AppBaseModel {
        /**
         * $date : {"$numberLong":"1563803100000"}
         */

        private $dateBean $date;

        public $dateBean get$date() {
            return $date;
        }

        public void set$date($dateBean $date) {
            this.$date = $date;
        }

        public static class $dateBean extends AppBaseModel {
            /**
             * $numberLong : 1563803100000
             */

            private long $numberLong;

            public long get$numberLong() {
                return $numberLong;
            }

            public void set$numberLong(long $numberLong) {
                this.$numberLong = $numberLong;
            }
        }
    }

    public static class MsgInfoBean extends AppBaseModel {
        private String result;
        private List<OthersBean> others;
        private String info;
        private String completed;

        public String getResult() {
            return getValidString(result);
        }

        public void setResult(String result) {
            this.result = result;
        }

        public List<OthersBean> getOthers() {
            return others;
        }

        public void setOthers(List<OthersBean> others) {
            this.others = others;
        }

        public String getInfo() {
            return getValidString(info);
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getCompleted() {
            return getValidString(completed);
        }

        public void setCompleted(String completed) {
            this.completed = completed;
        }
    }

    private class OthersBean {
    }
}
