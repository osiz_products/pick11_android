package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

public class PlayerPointsDetailModel extends AppBaseModel {


    private float totalpoints;
    private String matchId;
    private String pid;
    private float run;
    private float four;
    private float six;
    private float wicket;
    private float mdnover;
    @SerializedName("catch")
    private float catchX;
    private float stumped;
    private String playername;
    private float runout;
    private float fiftyBonus;
    private float hundredBonus;
    private float fourwhb;
    private float fivewhb;
    private float duck;
    private float playeingPoints;
    private float srone;
    private float srtwo;
    private float srthree;
    private float erone;
    private float ertwo;
    private float erthree;
    private float erfour;
    private float erfive;
    private float ersix;
    private float thrower;
    private float catcher;

    public float getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(float totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getMatchId() {
        return getValidString(matchId);
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getPid() {
        return getValidString(pid);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public float getRun() {
        return run;
    }

    public void setRun(float run) {
        this.run = run;
    }

    public float getFour() {
        return four;
    }

    public void setFour(float four) {
        this.four = four;
    }

    public float getSix() {
        return six;
    }

    public void setSix(float six) {
        this.six = six;
    }

    public float getWicket() {
        return wicket;
    }

    public void setWicket(float wicket) {
        this.wicket = wicket;
    }

    public float getMdnover() {
        return mdnover;
    }

    public void setMdnover(float mdnover) {
        this.mdnover = mdnover;
    }

    public float getCatchX() {
        return catchX;
    }

    public void setCatchX(float catchX) {
        this.catchX = catchX;
    }

    public float getStumped() {
        return stumped;
    }

    public void setStumped(float stumped) {
        this.stumped = stumped;
    }

    public String getPlayername() {
        return getValidString(playername);
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }

    public float getRunout() {
        return runout;
    }

    public void setRunout(float runout) {
        this.runout = runout;
    }

    public float getFiftyBonus() {
        return fiftyBonus;
    }

    public void setFiftyBonus(float fiftyBonus) {
        this.fiftyBonus = fiftyBonus;
    }

    public float getHundredBonus() {
        return hundredBonus;
    }

    public void setHundredBonus(float hundredBonus) {
        this.hundredBonus = hundredBonus;
    }

    public float getFourwhb() {
        return fourwhb;
    }

    public void setFourwhb(float fourwhb) {
        this.fourwhb = fourwhb;
    }

    public float getFivewhb() {
        return fivewhb;
    }

    public void setFivewhb(float fivewhb) {
        this.fivewhb = fivewhb;
    }

    public float getDuck() {
        return duck;
    }

    public void setDuck(float duck) {
        this.duck = duck;
    }

    public float getPlayeingPoints() {
        return playeingPoints;
    }

    public void setPlayeingPoints(float playeingPoints) {
        this.playeingPoints = playeingPoints;
    }

    public float getSrone() {
        return srone;
    }

    public void setSrone(float srone) {
        this.srone = srone;
    }

    public float getSrtwo() {
        return srtwo;
    }

    public void setSrtwo(float srtwo) {
        this.srtwo = srtwo;
    }

    public float getSrthree() {
        return srthree;
    }

    public void setSrthree(float srthree) {
        this.srthree = srthree;
    }

    public float getErone() {
        return erone;
    }

    public void setErone(float erone) {
        this.erone = erone;
    }

    public float getErtwo() {
        return ertwo;
    }

    public void setErtwo(float ertwo) {
        this.ertwo = ertwo;
    }

    public float getErthree() {
        return erthree;
    }

    public void setErthree(float erthree) {
        this.erthree = erthree;
    }

    public float getErfour() {
        return erfour;
    }

    public void setErfour(float erfour) {
        this.erfour = erfour;
    }

    public float getErfive() {
        return erfive;
    }

    public void setErfive(float erfive) {
        this.erfive = erfive;
    }

    public float getErsix() {
        return ersix;
    }

    public void setErsix(float ersix) {
        this.ersix = ersix;
    }

    public float getThrower() {
        return thrower;
    }

    public void setThrower(float thrower) {
        this.thrower = thrower;
    }

    public float getCatcher() {
        return catcher;
    }

    public void setCatcher(float catcher) {
        this.catcher = catcher;
    }

    public float getTotalEr() {
        return (getErone() + getErtwo() + getErthree() + getErfour() + getErfive() + getErsix());
    }

    public float getTotalSr() {
        return (getSrone() + getSrtwo() + getSrthree());
    }

    public String getValidFormatText(float value) {
        String s = getValidDecimalFormat(value);
        return s.replaceAll("\\.00", "");
//        return s.replaceAll("\\.00", "");
    }
}