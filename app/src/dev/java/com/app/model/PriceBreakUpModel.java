package com.app.model;

import com.app.appbase.AppBaseModel;

public class PriceBreakUpModel extends AppBaseModel {

    /**
     * pmin : 1
     * pmax : 1
     * pamount : 100.00
     */

    private long pmin;
    private long pmax;
    private double pamount;

    public long getPmin() {
        return pmin;
    }

    public void setPmin(long pmin) {
        this.pmin = pmin;
    }

    public long getPmax() {
        return pmax;
    }

    public void setPmax(long pmax) {
        this.pmax = pmax;
    }

    public double getPamount() {
        return pamount;
    }

    public void setPamount(double pamount) {
        this.pamount = pamount;
    }

    public String getAmountText() {
        String s = getValidDecimalFormat(getPamount());
        return s.replaceAll("\\.00", "");
    }

    public String getRank() {
        String data = "";
        if (getPmax() - getPmin() <= 1)
            data = "Rank " + getPmax();
        else
            data = "#" + getPmin() + "-" + getPmax();

        return data;
    }
}
