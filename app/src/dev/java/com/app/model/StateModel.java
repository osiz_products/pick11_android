package com.app.model;

import com.app.appbase.AppBaseModel;

public class StateModel extends AppBaseModel {

    private String id;
    private String name;

    public String getId() {
        return getValidString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }
}
