package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.Locale;

public class ScoreModel extends AppBaseModel {

    /**
     * inningName : Mumbai Indians Innings
     * inningRun : 113
     * inningWicket : 3
     * inningOver : 15.3
     */

    private String inningName;
    private int inningRun;
    private int inningWicket;
    private double inningOver;

    public String getInningName() {
        return getValidString(inningName);
    }

    public void setInningName(String inningName) {
        this.inningName = inningName;
    }

    public int getInningRun() {
        return inningRun;
    }

    public void setInningRun(int inningRun) {
        this.inningRun = inningRun;
    }

    public int getInningWicket() {
        return inningWicket;
    }

    public void setInningWicket(int inningWicket) {
        this.inningWicket = inningWicket;
    }

    public double getInningOver() {
        return inningOver;
    }

    public void setInningOver(double inningOver) {
        this.inningOver = inningOver;
    }

    public String getInningOverText() {
        String value = String.format(Locale.ENGLISH, "%.1f", getInningOver());
        return value.replaceAll("\\.00", "");
    }
}
