package com.app.model;

import com.app.appbase.AppBaseModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;

public class WinnerBreakUpRankModel extends AppBaseModel {


    /**
     * pmin : 1
     * pmax : 1
     * percent : 50
     */

    private int pmin;
    private int pmax;
    private float percent;

    public int getPmin() {
        return pmin;
    }

    public void setPmin(int pmin) {
        this.pmin = pmin;
    }

    public int getPmax() {
        return pmax;
    }

    public void setPmax(int pmax) {
        this.pmax = pmax;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public String getRank() {
        if ((getPmax() - getPmin()) > 0) {
            return "Rank " + getPmin() + " - " + getPmax();
        }
        return "Rank " + getPmin();
    }

    public String getPercentText() {
        return getValidDecimalFormat(getPercent()).replaceAll("\\.00", "") + "%";
    }

    public String getPercentValue(PrizePoolRequestModel prizePoolRequestModel) {
        if (prizePoolRequestModel == null) return "";
        int tolwinprize = Integer.valueOf(prizePoolRequestModel.tolwinprize);
        float v = tolwinprize * getPercent() / 100;
        return getValidDecimalFormat(v).replaceAll("\\.00", "");
    }
}
