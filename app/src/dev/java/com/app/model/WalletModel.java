package com.app.model;

import com.app.appbase.AppBaseModel;

public class WalletModel extends AppBaseModel {

    private double walletbalance = 0.00;
    private double wltwin = 0.00;
    private double wltbns = 0.00;
    private double exposure = 0.00;
    private double totalcash = 0.00;
    private double addedcash = 0.00;


    public double getWalletbalance() {
        return walletbalance;
    }

    public void setWalletbalance(double walletbalance) {
        this.walletbalance = walletbalance;
    }

    public double getWltwin() {
        return wltwin;
    }

    public void setWltwin(double wltwin) {
        this.wltwin = wltwin;
    }

    public double getWltbns() {
        return wltbns;
    }

    public void setWltbns(double wltbns) {
        this.wltbns = wltbns;
    }

    public double getExposure() {
        return exposure;
    }

    public void setExposure(double exposure) {
        this.exposure = exposure;
    }

    public String getTotalBalance() {
        return getValidDecimalFormat(getWalletbalance() + getWltbns() + getWltwin());
    }

    public double getTotalcash() {
        return totalcash;
    }

    public void setTotalcash(double totalcash) {
        this.totalcash = totalcash;
    }

    public double getAddedcash() {
        return addedcash;
    }

    public void setAddedcash(double addedcash) {
        this.addedcash = addedcash;
    }
}
