package com.app.model;

import com.app.appbase.AppBaseModel;



public class PromoCodeModel extends AppBaseModel {

    String id;
    String bonus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
