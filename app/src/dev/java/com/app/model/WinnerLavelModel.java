package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class WinnerLavelModel extends AppBaseModel {


    /**
     * _id : {"$oid":"5d7f794df0303d33b0965260"}
     * winner : 10
     * ranks : [{"pmin":1,"pmax":1,"percent":25},{"pmin":2,"pmax":2,"percent":20},{"pmin":3,"pmax":3,"percent":15},{"pmin":4,"pmax":4,"percent":10},{"pmin":5,"pmax":10,"percent":5}]
     */

    private IdBean _id;
    private int winner;
    private boolean isSelected = false;
    private List<WinnerBreakUpRankModel> ranks;

    public IdBean get_id() {
        return _id;
    }

    public void set_id(IdBean _id) {
        this._id = _id;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public List<WinnerBreakUpRankModel> getRanks() {
        return ranks;
    }

    public void setRanks(List<WinnerBreakUpRankModel> ranks) {
        this.ranks = ranks;
    }

}
