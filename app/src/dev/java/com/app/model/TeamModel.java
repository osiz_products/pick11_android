package com.app.model;

import com.app.appbase.AppBaseModel;

public class TeamModel extends AppBaseModel {

    private long id;
    private long userid;
    private String matchid;
    private String teamname;
    private String cap;
    private String vcap;

    private float playerPercentage;
    private float captainPercentage;
    private float viceCaptainPercentage;
    //public boolean isclicked=false;
    //public int index;
    private boolean isChecked;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getCap() {
        return getValidString(cap);
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getVcap() {
        return getValidString(vcap);
    }

    public void setVcap(String vcap) {
        this.vcap = vcap;
    }

    public float getPlayerPercentage() {
        return playerPercentage;
    }

    public void setPlayerPercentage(float playerPercentage) {
        this.playerPercentage = playerPercentage;
    }

    public float getCaptainPercentage() {
        return captainPercentage;
    }

    public void setCaptainPercentage(float captainPercentage) {
        this.captainPercentage = captainPercentage;
    }

    public float getViceCaptainPercentage() {
        return viceCaptainPercentage;
    }

    public void setViceCaptainPercentage(float viceCaptainPercentage) {
        this.viceCaptainPercentage = viceCaptainPercentage;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    /*public TeamModel(boolean isclicked,int index*//*,String fanId,String strAmount*//*)
    {
        this.index=index;
        this.isclicked=isclicked;
        *//*this.fanId=fanId;
        this.strAmount=strAmount;*//*
    }*/
}
