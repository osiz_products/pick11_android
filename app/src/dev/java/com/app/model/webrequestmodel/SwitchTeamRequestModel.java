package com.app.model.webrequestmodel;

public class SwitchTeamRequestModel extends AppBaseRequestModel {

    public String matchid;
    public String poolcontestid;
    public String uteamid;
    public String switchteamid;

}
