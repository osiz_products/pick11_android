package com.app.model.webrequestmodel;

import java.io.File;


public class RegisterRequestModel extends AppBaseRequestModel {
    public String phone;
    public String email;
    public String password;
    public String device_id;
    public String refercode;
    public String devicetype;
    public String devicetoken;
    public String countryId;
    public String stateId;
    public File profilepic;

}
