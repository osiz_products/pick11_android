package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseRequestModel;

import java.io.File;

public class UpdatePenCardRequestModel extends AppBaseRequestModel {

    public String panname;
    public String pannumber;
    public String dob;
    public String state;
    public File panimage;
}
