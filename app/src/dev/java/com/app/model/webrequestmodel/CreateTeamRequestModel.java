package com.app.model.webrequestmodel;

import java.util.List;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class CreateTeamRequestModel extends AppBaseRequestModel {

    public String gameid;
    public String matchid;
    public String userteamid;
    public List<String> userteamplayers;
    public String iscap;
    public String isvcap;

}
