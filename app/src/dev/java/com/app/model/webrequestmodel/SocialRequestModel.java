package com.app.model.webrequestmodel;


public class SocialRequestModel extends AppBaseRequestModel {

    public String phone;
    public String email;
    public String socialid;
    public String logintype;
    public String devicetype;
    public String devicetoken;
}
