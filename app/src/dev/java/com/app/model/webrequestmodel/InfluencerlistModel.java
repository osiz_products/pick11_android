package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseModel;
import com.app.model.TransactionInfModel;
import com.app.model.webresponsemodel.AppBaseResponseModel;

import java.util.List;

public class InfluencerlistModel extends AppBaseResponseModel {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean extends AppBaseModel {
        int total = 0;
        List<TransactionInfModel> list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<TransactionInfModel> getList() {
            return list;
        }

        public void setList(List<TransactionInfModel> list) {
            this.list = list;
        }
    }
}
