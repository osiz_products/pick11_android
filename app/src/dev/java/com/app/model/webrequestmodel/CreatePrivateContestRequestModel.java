package com.app.model.webrequestmodel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class CreatePrivateContestRequestModel extends AppBaseRequestModel {

    public String atype;
    public String contestsize;
    public String ismultiple;
    public String joinfees;
    public String matchid;
    public String name;
    public String prizebreakid;
    public int winners;
    public String winningprize;

}
