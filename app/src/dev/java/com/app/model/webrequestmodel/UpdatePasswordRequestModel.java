package com.app.model.webrequestmodel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class UpdatePasswordRequestModel extends AppBaseRequestModel {

    public String oldpassword;
    public String password;

}
