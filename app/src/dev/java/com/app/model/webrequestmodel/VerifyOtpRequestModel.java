package com.app.model.webrequestmodel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class VerifyOtpRequestModel extends AppBaseRequestModel {
    public String phone;
    public String otp;

}
