package com.app.model.webrequestmodel;


import com.app.appbase.AppBaseResponseModel;
import com.app.model.InfluencerModel;

public class InfluencerRequestModel extends AppBaseResponseModel {

    InfluencerModel data;

    public InfluencerModel getData() {
        return data;
    }

    public void setData(InfluencerModel data) {
        this.data = data;
    }



}
