package com.app.model.webrequestmodel;


public class LoginRequestModel extends AppBaseRequestModel {

    public String username;
    public String password;
    public String otp;
    public String device_id;
    public String devicetype;
    public String devicetoken;
    public String isInfluencer;
}
