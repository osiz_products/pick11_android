package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseRequestModel;

public class TransactionRequestModel extends AppBaseRequestModel {

    public int page;
    public int limit;
    public String gameid;
    public String atype;

}
