package com.app.model.webrequestmodel;

public class Matchhistory extends AppBaseRequestModel {

    private Integer totalJoinedContest;
    private Integer totalJoinedMatches;
    private Integer totalJoinedSeries;
    private Integer totalWins;

    public Integer getTotalJoinedContest() {
        return totalJoinedContest;
    }

    public void setTotalJoinedContest(Integer totalJoinedContest) {
        this.totalJoinedContest = totalJoinedContest;
    }

    public Integer getTotalJoinedMatches() {
        return totalJoinedMatches;
    }

    public void setTotalJoinedMatches(Integer totalJoinedMatches) {
        this.totalJoinedMatches = totalJoinedMatches;
    }

    public Integer getTotalJoinedSeries() {
        return totalJoinedSeries;
    }

    public void setTotalJoinedSeries(Integer totalJoinedSeries) {
        this.totalJoinedSeries = totalJoinedSeries;
    }

    public Integer getTotalWins() {
        return totalWins;
    }

    public void setTotalWins(Integer totalWins) {
        this.totalWins = totalWins;
    }
}
