package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.WalletModel;

public class WithdrawRequestResponseModel extends AppBaseResponseModel {

    WalletModel data;

    public WalletModel getData() {
        return data;
    }

    public void setData(WalletModel data) {
        this.data = data;
    }
}
