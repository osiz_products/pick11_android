package com.app.model.webrequestmodel;


public class ForgotPasswordVerifyRequestModel extends AppBaseRequestModel {

    public String username;
    public String otp;
    public String password;
    public String device_id;
    public String devicetype;
    public String devicetoken;

}
