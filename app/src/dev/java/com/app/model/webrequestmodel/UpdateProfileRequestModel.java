package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseRequestModel;

import java.io.File;

public class UpdateProfileRequestModel extends AppBaseRequestModel {

    public String name;
    public String teamname;
    public String gender;
    public String dob;
    public String address;
    public String state;
    public String city;
    public String device_id;
    public String devicetype;
    public String devicetoken;
    public File profilepic;
}
