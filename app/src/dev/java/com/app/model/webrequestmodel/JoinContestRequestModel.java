package com.app.model.webrequestmodel;


public class JoinContestRequestModel extends AppBaseRequestModel {

    //    normal join
    public String atype;
    public String matchid;
    public String poolcontestid;
    public String uteamid;
    public String fees;

    //  Create private contest
    public String contestsize;
    public String ismultiple;
    public String joinfees;
    public String name;
    public String prizebreakid;
    public int winners;
    public String winningprize;

    //Join private contest
    public boolean isJoin;
    public String ccode;

}
