package com.app.model.webrequestmodel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class NotiStatusRequestModel extends AppBaseRequestModel {

    public String sendAll;
    public String atype;
    public String id;

}
