package com.app.model.webrequestmodel;

import java.io.File;



public class BankRequestModel extends AppBaseRequestModel {

    public String acholdername;
    public String acno;
    public String ifsccode;
    public String bankname;
    public String state;
    public File image;

}
