package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseModel;
import com.app.appbase.AppBaseRequestModel;
import com.app.model.TransactionInfModel;
import java.util.List;

public class InfluencerMatchlistModel extends AppBaseRequestModel {

    private InfluencerMatchlistModel.DataBean data;

    public InfluencerMatchlistModel.DataBean getData() {
        return data;
    }

    public void setData(InfluencerMatchlistModel.DataBean data) {
        this.data = data;
    }

    public boolean isError() {
        return false;
    }

    public static class DataBean extends AppBaseModel {
        int total = 0;

        List<TransactionInfModel> matchDetails;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

//        public List<TransactionInfModel> getList() {
//            return list;
//        }

//        public void setList(List<TransactionInfModel> list) {
//            this.list = list;
//        }

        public List<TransactionInfModel> getMatchDetails() {
            return matchDetails;
        }

        public void setMatchDetails(List<TransactionInfModel> matchDetails) {
            this.matchDetails = matchDetails;
        }
    }
}

