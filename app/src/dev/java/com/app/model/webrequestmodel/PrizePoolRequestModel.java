package com.app.model.webrequestmodel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class PrizePoolRequestModel extends AppBaseRequestModel {

    public String name;
    public String contestsize;
    public String tolwinprize;
    public String entry_fees;
    public String type;

}
