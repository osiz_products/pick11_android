package com.app.model.webrequestmodel;

import com.app.appbase.AppBaseResponseModel;
import com.app.model.WalletlistModel;

import java.util.List;

public class InfluencerWalletModel extends AppBaseResponseModel {

    List<WalletlistModel> data;
    long serverdate;

    public List<WalletlistModel> getData() {
        return data;
    }

    public void setData(List<WalletlistModel> data) {
        this.data = data;
    }

    public long getServerdate() {
        return serverdate;
    }

    public void setServerdate(long serverdate) {
        this.serverdate = serverdate;
    }
}
