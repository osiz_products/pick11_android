package com.app.model.webrequestmodel;

public class ForgotPasswordRequestModel extends AppBaseRequestModel {

    public String username;
    public String atype;
    public String devicetoken;
    public String device_id;
    public String devicetype;

}
