package com.app.model;

import com.app.appbase.AppBaseModel;

/**
 * Created by Sunil yadav on 17/7/19.
 */
public class IdBean extends AppBaseModel {
    /**
     * $oid : 5d2f04334c72051ca967fe43
     */

    private String $oid;

    public String get$oid() {
        return getValidString($oid);
    }

    public void set$oid(String $oid) {
        this.$oid = $oid;
    }
}
