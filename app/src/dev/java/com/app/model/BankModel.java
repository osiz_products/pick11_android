package com.app.model;

import com.app.appbase.AppBaseModel;

public class BankModel extends AppBaseModel {

    private String id;
    private String acno;
    private String ifsccode;
    private String bankname;
    private String acholdername;
    private String image;
    private String isverified;
    private String state;

    public String getId() {
        return getValidString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAcno() {
        return getValidString(acno);
    }

    public void setAcno(String acno) {
        this.acno = acno;
    }

    public String getIfsccode() {
        return getValidString(ifsccode);
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }

    public String getBankname() {
        return getValidString(bankname);
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAcholdername() {
        return getValidString(acholdername);
    }

    public void setAcholdername(String acholdername) {
        this.acholdername = acholdername;
    }

    public String getImage() {
        return getValidString(image);
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsverified() {
        return getValidString(isverified);
    }

    public void setIsverified(String isverified) {
        this.isverified = isverified;
    }

    public boolean isBankVerified(){
        return getIsverified().equalsIgnoreCase("1");
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
