package com.app.model;

import com.app.appbase.AppBaseModel;

public class PenCardModel extends AppBaseModel {

    private String panname;
    private String pannumber;
    private long dob;
    private String panimage;
    private String isverified;

    public String getPanname() {
        return getValidString(panname);
    }

    public void setPanname(String panname) {
        this.panname = panname;
    }

    public String getPannumber() {
        return getValidString(pannumber);
    }

    public void setPannumber(String pannumber) {
        this.pannumber = pannumber;
    }

    public long getDob() {
        return dob;
    }

    public void setDob(long dob) {
        this.dob = dob;
    }

    public String getPanimage() {
        return getValidString(panimage);
    }

    public void setPanimage(String panimage) {
        this.panimage = panimage;
    }

    public String getIsverified() {
        return getValidString(isverified);
    }

    public void setIsverified(String isverified) {
        this.isverified = isverified;
    }

    public boolean isPenVerified(){
        return getIsverified().equalsIgnoreCase("1");
    }

    public String getFormattedDate(int tag) {
        String time = "0";
        if (getDob() > 0) {
            if (tag == TAG_THREE) {
                time = getFormattedCalendar(TIME_FIVE, getDob());
            }
        }
        return time;
    }
}
