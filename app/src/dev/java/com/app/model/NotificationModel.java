package com.app.model;

import com.app.appbase.AppBaseModel;

/**
 * @author Sunil kumar yadav
 * @since 08/01/19
 */

public class NotificationModel extends AppBaseModel {

    public String id;
    public String title;
    public String discription;
    public String image;
    public String created;
    public String updated_at;
    public String banned;
    public String img_path;

    public String getTitle () {
        return getValidString(title);
    }

    public String getDiscription () {
        return getValidString(discription);
    }

    public String getCreated () {
        return getValidString(created);
    }

    public String getImageUrl () {
        if (isValidString(image)) {
            return img_path + "/" + image;
        }
        return null;
    }


}
