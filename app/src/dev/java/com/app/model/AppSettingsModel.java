package com.app.model;

import com.app.appbase.AppBaseModel;


/**
 * Created by Sunil yadav on 7/8/19.
 */
public class AppSettingsModel extends AppBaseModel {

    private int unread_count;
    private PrivateContestModel pvtcontest;
    private ConfigModel config;

    public int getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(int unread_count) {
        this.unread_count = unread_count;
    }

    public PrivateContestModel getPvtcontest() {
        return pvtcontest;
    }

    public void setPvtcontest(PrivateContestModel pvtcontest) {
        this.pvtcontest = pvtcontest;
    }

    public ConfigModel getConfig() {
        return config;
    }

    public void setConfig(ConfigModel config) {
        this.config = config;
    }

    public class ConfigModel {
        private CommonSetting common;

        public CommonSetting getCommon() {
            return common;
        }

        public void setCommon(CommonSetting common) {
            this.common = common;
        }
    }
}
