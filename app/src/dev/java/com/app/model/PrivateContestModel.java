package com.app.model;

import com.app.appbase.AppBaseModel;

public class PrivateContestModel extends AppBaseModel {


    /**
     * winprize : {"min":0,"max":500}
     * cnstsize : {"min":2,"max":100}
     * adminchrg : 20
     * min_entry_fees : 5
     */

    private WinprizeBean winprize;
    private CnstsizeBean cnstsize;
    private float adminchrg;
    private int min_entry_fees;

    public WinprizeBean getWinprize() {
        return winprize;
    }

    public void setWinprize(WinprizeBean winprize) {
        this.winprize = winprize;
    }

    public CnstsizeBean getCnstsize() {
        return cnstsize;
    }

    public void setCnstsize(CnstsizeBean cnstsize) {
        this.cnstsize = cnstsize;
    }

    public float getAdminchrg() {
        return adminchrg;
    }

    public void setAdminchrg(float adminchrg) {
        this.adminchrg = adminchrg;
    }

    public int getMin_entry_fees() {
        return min_entry_fees;
    }

    public void setMin_entry_fees(int min_entry_fees) {
        this.min_entry_fees = min_entry_fees;
    }

    public static class WinprizeBean {
        /**
         * min : 0
         * max : 500
         */

        private int min;
        private int max;

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }

    public static class CnstsizeBean {
        /**
         * min : 2
         * max : 100
         */

        private int min;
        private int max;

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }
}
