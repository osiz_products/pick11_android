package com.app.model;

import com.app.appbase.AppBaseModel;

public class PlayerModel extends AppBaseModel {

    private String matchid;
    private String pid;
    private String teamname;
    private double pts;
    private float credit;
    private int iscap = 0;
    private int isvcap = 0;
    private long playertype;
    private String pimg;
    private String isplaying;
    private String fullname;
    private String ptypename;
    private String pname;
    private String ptype;
    private String team_id;
    private String team_name;
    private boolean isSelected;
    private String points;

    private float playerPercentage;
    private float captainPercentage;
    private float viceCaptainPercentage;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getPid() {
        return getValidString(pid);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public double getPts() {
        return pts;
    }

    public void setPts(double pts) {
        this.pts = pts;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public int getIscap() {
        return iscap;
    }

    public void setIscap(int iscap) {
        this.iscap = iscap;
    }

    public int getIsvcap() {
        return isvcap;
    }

    public void setIsvcap(int isvcap) {
        this.isvcap = isvcap;
    }

    public long getPlayertype() {
        return playertype;
    }

    public void setPlayertype(long playertype) {
        this.playertype = playertype;
    }

    public String getPimg() {
        return getValidString(pimg);
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public String getIsplaying() {
        return getValidString(isplaying);
    }

    public void setIsplaying(String isplaying) {
        this.isplaying = isplaying;
    }

    public String getFullname() {
        return getValidString(fullname);
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPtypename() {
        return getValidString(ptypename);
    }

    public void setPtypename(String ptypename) {
        this.ptypename = ptypename;
    }

    public String getPname() {
        return getValidString(pname);
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPtype() {
        return getValidString(ptype);
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getTeam_id() {
        return getValidString(team_id);
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getTeam_name() {
        return getValidString(team_name);
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getPoints() {
        return getValidString(points);
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getTeam_type() {
        return getTeamname() + "-" + getPtype();
    }

    public boolean isWicketKeeper() {
        return getPlayertype() == 1;
    }

    public boolean isBatsmen() {
        return getPlayertype() == 2;
    }

    public boolean isAllRounder() {
        return getPlayertype() == 3;
    }

    public boolean isBowler() {
        return getPlayertype() == 4;
    }

    public boolean isPlaying() {
        return getIsplaying().equalsIgnoreCase("1");
    }

    public boolean isPlayingVisible(){
        return (!getIsplaying().equalsIgnoreCase("-1"));
    }

    public boolean isDefender() {
        return getPlayertype() == 5;
    }

    public boolean isRaider() {
        return getPlayertype() == 6;
    }

    public boolean isAllRounderKabaddi() {
        return getPlayertype() == 7;
    }

    public boolean isGoalKeeper() {
        return getPlayertype() == 8;
    }

    public boolean isDefenderFootball() {
        return getPlayertype() == 9;
    }

    public boolean isMidFielder() {
        return getPlayertype() == 10;
    }

    public boolean isForward() {
        return getPlayertype() == 11;
    }

    public String getCreditText() {
        String s = getValidDecimalFormat(getCredit());
        return s.replaceAll("\\.00", "");
    }

    public String getPointsText() {
        if (isValidString(getPoints())) {
            String s = getValidDecimalFormat(Float.valueOf(getPoints()));
            return s.replaceAll("\\.00", "");
        }
        return getPoints();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PlayerModel)
            return ((PlayerModel) obj).getPid().equals(getPid());
        return false;
    }

    public float getPlayerPercentage() {
        return playerPercentage;
    }

    public void setPlayerPercentage(float playerPercentage) {
        this.playerPercentage = playerPercentage;
    }

    public float getCaptainPercentage() {
        return captainPercentage;
    }

    public void setCaptainPercentage(float captainPercentage) {
        this.captainPercentage = captainPercentage;
    }

    public float getViceCaptainPercentage() {
        return viceCaptainPercentage;
    }

    public void setViceCaptainPercentage(float viceCaptainPercentage) {
        this.viceCaptainPercentage = viceCaptainPercentage;
    }
}
