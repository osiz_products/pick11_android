package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BowlingModel extends AppBaseModel {

    private String title;
    private List<ScoresBean> scores;

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ScoresBean> getScores() {
        return scores;
    }

    public void setScores(List<ScoresBean> scores) {
        this.scores = scores;
    }

    public static class ScoresBean extends AppBaseModel {
        /**
         * 6s : 2
         * 4s : 3
         * 0s : 12
         * Econ : 8.00
         * W : 1
         * R : 32
         * M : 0
         * O : 4
         * bowler : Yasir Shah
         * pid : 43685
         */

        @SerializedName("6s")
        private String _$6s;
        @SerializedName("4s")
        private String _$4s;
        @SerializedName("0s")
        private String _$0s;
        private String Econ;
        private String W;
        private String R;
        private String M;
        private String O;
        private String bowler;
        private String pid;

        public String get_$6s() {
            return getValidString(_$6s);
        }

        public void set_$6s(String _$6s) {
            this._$6s = _$6s;
        }

        public String get_$4s() {
            return getValidString(_$4s);
        }

        public void set_$4s(String _$4s) {
            this._$4s = _$4s;
        }

        public String get_$0s() {
            return getValidString(_$0s);
        }

        public void set_$0s(String _$0s) {
            this._$0s = _$0s;
        }

        public String getEcon() {
            return getValidString(Econ);
        }

        public void setEcon(String Econ) {
            this.Econ = Econ;
        }

        public String getW() {
            return getValidString(W);
        }

        public void setW(String W) {
            this.W = W;
        }

        public String getR() {
            return getValidString(R);
        }

        public void setR(String R) {
            this.R = R;
        }

        public String getM() {
            return getValidString(M);
        }

        public void setM(String M) {
            this.M = M;
        }

        public String getO() {
            return getValidString(O);
        }

        public void setO(String O) {
            this.O = O;
        }

        public String getBowler() {
            return getValidString(bowler);
        }

        public void setBowler(String bowler) {
            this.bowler = bowler;
        }

        public String getPid() {
            return getValidString(pid);
        }

        public void setPid(String pid) {
            this.pid = pid;
        }
    }
}
