package com.app.model;

import com.app.appbase.AppBaseModel;



public class PlayerPointsModel extends AppBaseModel {


    private String matchId;
    private String pid;
    private String seriesid;
    private String selectedplayer;
    private String selectplyrper;
    private String team1;
    private String team2;
    private String totalplayer;
    private String totalpoints;
    private String pts;
    private int selectedPercentage;

    public String getMatchId() {
        return getValidString(matchId);
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getPid() {
        return getValidString(pid);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSeriesid() {
        return getValidString(seriesid);
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getSelectedplayer() {
        return getValidString(selectedplayer);
    }

    public void setSelectedplayer(String selectedplayer) {
        this.selectedplayer = selectedplayer;
    }

    public String getSelectplyrper() {
        return getValidString(selectplyrper);
    }

    public void setSelectplyrper(String selectplyrper) {
        this.selectplyrper = selectplyrper;
    }

    public String getTeam1() {
        return getValidString(team1);
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return getValidString(team2);
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getTotalplayer() {
        return getValidString(totalplayer);
    }

    public void setTotalplayer(String totalplayer) {
        this.totalplayer = totalplayer;
    }

    public String getTotalpoints() {
        return getValidString(totalpoints);
    }

    public void setTotalpoints(String totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getPts() {
        return pts;
    }

    public void setPts(String pts) {
        this.pts = pts;
    }

    public int getSelectedPercentage() {
        return selectedPercentage;
    }

    public void setSelectedPercentage(int selectedPercentage) {
        this.selectedPercentage = selectedPercentage;
    }
}
