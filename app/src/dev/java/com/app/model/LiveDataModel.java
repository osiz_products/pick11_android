package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;



public class LiveDataModel extends AppBaseModel {


    private IdBean _id;
    private String matchid;
    private InningsdetailBean inningsdetail;
    private MatchstartedBean matchstarted;
    private String status;
    private String status_overview;
    private String type;

    public IdBean get_id() {
        return _id;
    }

    public void set_id(IdBean _id) {
        this._id = _id;
    }

    public String getMatchid() {
        return getValidString(matchid);
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public InningsdetailBean getInningsdetail() {
        return inningsdetail;
    }

    public void setInningsdetail(InningsdetailBean inningsdetail) {
        this.inningsdetail = inningsdetail;
    }

    public MatchstartedBean getMatchstarted() {
        return matchstarted;
    }

    public void setMatchstarted(MatchstartedBean matchstarted) {
        this.matchstarted = matchstarted;
    }

    public String getStatus() {
        return getValidString(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_overview() {
        return getValidString(status_overview);
    }

    public void setStatus_overview(String status_overview) {
        this.status_overview = status_overview;
    }

    public String getType() {
        return getValidString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class IdBean extends AppBaseModel {
        /**
         * $oid : 5d2dd3bd93daa794cfe320c3
         */

        private String $oid;

        public String get$oid() {
            return getValidString($oid);
        }

        public void set$oid(String $oid) {
            this.$oid = $oid;
        }
    }

    public static class InningsdetailBean extends AppBaseModel {


        private ABean a;
        private BBean b;
        private TeamABean team_a;
        private TeamBBean team_b;

        public ABean getA() {
            return a;
        }

        public void setA(ABean a) {
            this.a = a;
        }

        public BBean getB() {
            return b;
        }

        public void setB(BBean b) {
            this.b = b;
        }

        public TeamABean getTeam_a() {
            return team_a;
        }

        public void setTeam_a(TeamABean team_a) {
            this.team_a = team_a;
        }

        public TeamBBean getTeam_b() {
            return team_b;
        }

        public void setTeam_b(TeamBBean team_b) {
            this.team_b = team_b;
        }

        public static class ABean extends AppBaseModel {
            /**
             * 1 : 134/1 in 10.5
             */

            @SerializedName("1")
            private String _$1;

            public String get_$1() {
                return getValidString(_$1);
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }
        }

        public static class BBean extends AppBaseModel {
            /**
             * 1 : 132/8 in 13.0
             */

            @SerializedName("1")
            private String _$1;

            public String get_$1() {
                return getValidString(_$1);
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }
        }

        public static class TeamABean extends AppBaseModel {
            /**
             * logo : http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1563795077_india-winner-campaign.jpg
             * short_name : IRE
             * full_name : Ireland
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }

        public static class TeamBBean extends AppBaseModel {
            /**
             * logo : http://192.168.1.17/brfantasyfull/public/uploads/teamlogo/1563795080_index.png
             * short_name : ZIM
             * full_name : Zimbabwe
             */

            private String logo;
            private String short_name;
            private String full_name;

            public String getLogo() {
                return getValidString(logo);
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getShort_name() {
                return getValidString(short_name);
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getFull_name() {
                return getValidString(full_name);
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }
    }

    public static class MatchstartedBean extends AppBaseModel {
        /**
         * $date : {"$numberLong":"1562943600000"}
         */

        private $dateBean $date;

        public $dateBean get$date() {
            return $date;
        }

        public void set$date($dateBean $date) {
            this.$date = $date;
        }

        public static class $dateBean extends AppBaseModel {
            /**
             * $numberLong : 1562943600000
             */

            private String $numberLong;

            public String get$numberLong() {
                return getValidString($numberLong);
            }

            public void set$numberLong(String $numberLong) {
                this.$numberLong = $numberLong;
            }
        }
    }
}

