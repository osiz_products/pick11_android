package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class ContestCategoryModel extends AppBaseModel {

    long matchcontestid;
    long contestid;
    String title;
    String subtitle;
    String contestlogo;
    List<ContestModel> contestPools;

    public long getMatchcontestid() {
        return matchcontestid;
    }

    public void setMatchcontestid(long matchcontestid) {
        this.matchcontestid = matchcontestid;
    }

    public long getContestid() {
        return contestid;
    }

    public void setContestid(long contestid) {
        this.contestid = contestid;
    }

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return getValidString(subtitle);
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getContestlogo() {
        return getValidString(contestlogo);
    }

    public void setContestlogo(String contestlogo) {
        this.contestlogo = contestlogo;
    }

    public List<ContestModel> getContestPools() {
        return contestPools;
    }

    public void setContestPools(List<ContestModel> contestPools) {
        this.contestPools = contestPools;
    }
}
