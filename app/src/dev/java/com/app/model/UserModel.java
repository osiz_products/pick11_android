package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class UserModel extends AppBaseModel {

    private long id;
    private String username;
    private String phone;
    private String email;
    private String name;
    private String address;
    private String city;
    private String state;
    private String refercode;
    private String refbns;
    private String refbnsfrnd;
    private String message;
    private String teamname;
    private String gender;
    private String secondaryemail;
    private String dob;
    private String profilepic;
    private String token;
    private String istnameedit;
    private String isphoneverify;
    private String isemailverify;
    private String ispanverify;
    private String isbankdverify;
    private String isInfluencer;

    private WalletModel walletModel;
    private CommonSetting setting;

    private List<GameTypeModel> gameType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return getValidString(username);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return getValidString(phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return getValidString(email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return getValidString(address);
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return getValidString(city);
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return getValidString(state);
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRefercode() {
        return getValidString(refercode);
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getRefbns() {
        return getValidString(refbns);
    }

    public void setRefbns(String refbns) {
        this.refbns = refbns;
    }

    public String getRefbnsfrnd() {
        return getValidString(refbnsfrnd);
    }

    public void setRefbnsfrnd(String refbnsfrnd) {
        this.refbnsfrnd = refbnsfrnd;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getGender() {
        return getValidString(gender);
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSecondaryemail() {
        return getValidString(secondaryemail);
    }

    public void setSecondaryemail(String secondaryemail) {
        this.secondaryemail = secondaryemail;
    }

    public String getDob() {
        return getValidString(dob);
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfilepic() {
        return getValidString(profilepic);
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getToken() {
        return getValidString(token);
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIstnameedit() {
        return getValidString(istnameedit);
    }

    public void setIstnameedit(String istnameedit) {
        this.istnameedit = istnameedit;
    }

    public String getIsphoneverify() {
        return getValidString(isphoneverify);
    }

    public void setIsphoneverify(String isphoneverify) {
        this.isphoneverify = isphoneverify;
    }

    public String getIsemailverify() {
        return getValidString(isemailverify);
    }

    public void setIsemailverify(String isemailverify) {
        this.isemailverify = isemailverify;
    }

    public String getIspanverify() {
        return getValidString(ispanverify);
    }

    public void setIspanverify(String ispanverify) {
        this.ispanverify = ispanverify;
    }

    public String getIsbankdverify() {
        return getValidString(isbankdverify);
    }

    public void setIsbankdverify(String isbankdverify) {
        this.isbankdverify = isbankdverify;
    }

    public boolean isTeamNameEditable() {
        return getIstnameedit().equals("1");
    }

    public boolean isPhoneVerify() {
        return getIsphoneverify().equals("1");
    }

    public boolean isEmailVerify() {
        return getIsemailverify().equals("1");
    }

    public boolean isPanVerify() {
        return getIspanverify().equals("1");
    }

    public boolean isBankVerify() {
        return getIsbankdverify().equals("1");
    }

    public boolean isKycCompleted() {
        return isPhoneVerify() && isEmailVerify() && isPanVerify() && isBankVerify();
    }

    public List<GameTypeModel> getGameType() {
        return gameType;
    }

    public void setGameType(List<GameTypeModel> gameType) {
        this.gameType = gameType;
    }

    public WalletModel getWalletModel() {
        return walletModel;
    }

    public void setWalletModel(WalletModel walletModel) {
        this.walletModel = walletModel;
    }

    public CommonSetting getSetting() {
        return setting;
    }

    public void setSetting(CommonSetting setting) {
        this.setting = setting;
    }

    public String getReferalBonusText() {
        String s = getValidDecimalFormat(getRefbns());
        return s.replaceAll("\\.00", "");
    }

    public String getReferalBonusFriendText() {
        String s = getValidDecimalFormat(getRefbnsfrnd());
        return s.replaceAll("\\.00", "");
    }

    public String getFormattedDate(int tag) {
        String time = "";
        if (!isValidString(getDob())) return time;
        if (Long.parseLong(getDob()) > 0) {
            if (tag == TAG_THREE) {
                time = getFormattedCalendar(TIME_FIVE, Long.parseLong(getDob()));
            }
        }
        return time;
    }

    public boolean isWithdrawAvailable() {
        if (isPhoneVerify() && isEmailVerify() && isPanVerify() && isBankVerify())
            return true;
        return false;
    }

    public void updateUserModel(UserModel userModel) {
        setSetting(userModel.getSetting());
    }

    public String getIsInfluencer() {
        return isInfluencer;
    }

    public void setIsInfluencer(String isInfluencer) {
        this.isInfluencer = isInfluencer;
    }


}
