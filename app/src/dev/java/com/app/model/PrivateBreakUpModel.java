package com.app.model;

import com.app.appbase.AppBaseModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;

import java.util.List;

public class PrivateBreakUpModel extends AppBaseModel {

    private String _id;
    private PrizePoolRequestModel prizePoolRequestModel;
    private List<WinnerLavelModel> winnerslabs;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }


    public PrizePoolRequestModel getPrizePoolRequestModel() {
        return prizePoolRequestModel;
    }

    public void setPrizePoolRequestModel(PrizePoolRequestModel prizePoolRequestModel) {
        this.prizePoolRequestModel = prizePoolRequestModel;
    }

    public List<WinnerLavelModel> getWinnerslabs() {
        return winnerslabs;
    }

    public void setWinnerslabs(List<WinnerLavelModel> winnerslabs) {
        this.winnerslabs = winnerslabs;
    }
}
