package com.app.model;

import com.app.appbase.AppBaseModel;

public class PlayerPreviewModel extends AppBaseModel {

    private String pid;
    private String teamname;
    private double pts;
    private float credit;
    private int iscap;
    private int isvcap;
    private String pname;
    private String fullname;
    private String ptypename;
    private String ptype;
    private long playertype;
    private String isplaying;
    private String pimg;
    private String points;

    private float playerPercentage;
    private float captainPercentage;
    private float viceCaptainPercentage;

    public String getPid() {
        return getValidString(pid);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTeamname() {
        return getValidString(teamname);
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public double getPts() {
        return pts;
    }

    public void setPts(double pts) {
        this.pts = pts;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public int getIscap() {
        return iscap;
    }

    public void setIscap(int iscap) {
        this.iscap = iscap;
    }

    public int getIsvcap() {
        return isvcap;
    }

    public void setIsvcap(int isvcap) {
        this.isvcap = isvcap;
    }

    public String getPname() {
        return getValidString(pname);
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getFullname() {
        return getValidString(fullname);
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPtypename() {
        return getValidString(ptypename);
    }

    public void setPtypename(String ptypename) {
        this.ptypename = ptypename;
    }

    public String getPtype() {
        return getValidString(ptype);
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public long getPlayertype() {
        return (playertype);
    }

    public void setPlayertype(long playertype) {
        this.playertype = playertype;
    }

    public String getIsplaying() {
        return getValidString(isplaying);
    }

    public void setIsplaying(String isplaying) {
        this.isplaying = isplaying;
    }

    public String getPimg() {
        return getValidString(pimg);
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public String getPoints() {
        return getValidString(points);
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public boolean isWicketKeeper() {
        return getPlayertype() == 1;
    }

    public boolean isBatsmen() {
        return getPlayertype() == 2;
    }

    public boolean isAllRounder() {
        return getPlayertype() == 3;
    }

    public boolean isBowler() {
        return getPlayertype() == 4;
    }

    public boolean isDefender() {
        return getPlayertype() == 5;
    }

    public boolean isRaider() {
        return getPlayertype() == 6;
    }

    public boolean isAllRounderK() {
        return getPlayertype() == 7;
    }

    public boolean isGoalKeeper() {
        return getPlayertype() == 8;
    }

    public boolean isDefenderFootball() {
        return getPlayertype() == 9;
    }

    public boolean isMidFielder() {
        return getPlayertype() == 10;
    }

    public boolean isForward() {
        return getPlayertype() == 11;
    }

    public float getPlayerPercentage() {
        return playerPercentage;
    }

    public void setPlayerPercentage(float playerPercentage) {
        this.playerPercentage = playerPercentage;
    }

    public float getCaptainPercentage() {
        return captainPercentage;
    }

    public void setCaptainPercentage(float captainPercentage) {
        this.captainPercentage = captainPercentage;
    }

    public float getViceCaptainPercentage() {
        return viceCaptainPercentage;
    }

    public void setViceCaptainPercentage(float viceCaptainPercentage) {
        this.viceCaptainPercentage = viceCaptainPercentage;
    }
}
