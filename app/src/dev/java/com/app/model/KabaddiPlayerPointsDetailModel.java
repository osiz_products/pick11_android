package com.app.model;

import com.app.appbase.AppBaseRequestModel;

public class KabaddiPlayerPointsDetailModel extends AppBaseRequestModel {


    /**
     * totalpoints : 11
     * matchId : pkl_2019_g74
     * pid : Amit Kumar_1114
     * playername : Amit Kumar
     * playeingPoints : 2
     * greencard : 0
     * yellowcard : 0
     * redcard : 0
     * touch : 6
     * raidbonus : 3
     * unsuccessraid : 0
     * successtackle : 0
     * supertackle : 0
     * pushallout : 0
     * getallout : 0
     */

    private float totalpoints;
    private String matchId;
    private String pid;
    private String playername;
    private float playeingPoints;
    private float greencard;
    private float yellowcard;
    private float redcard;
    private float touch;
    private float raidbonus;
    private float unsuccessraid;
    private float successtackle;
    private float supertackle;
    private float pushallout;
    private float getallout;

    public float getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(float totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getMatchId() {
        return getValidString(matchId);
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getPid() {
        return getValidString(pid);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPlayername() {
        return getValidString(playername);
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }

    public float getPlayeingPoints() {
        return playeingPoints;
    }

    public void setPlayeingPoints(float playeingPoints) {
        this.playeingPoints = playeingPoints;
    }

    public float getGreencard() {
        return greencard;
    }

    public void setGreencard(float greencard) {
        this.greencard = greencard;
    }

    public float getYellowcard() {
        return yellowcard;
    }

    public void setYellowcard(float yellowcard) {
        this.yellowcard = yellowcard;
    }

    public float getRedcard() {
        return redcard;
    }

    public void setRedcard(float redcard) {
        this.redcard = redcard;
    }

    public float getTouch() {
        return touch;
    }

    public void setTouch(float touch) {
        this.touch = touch;
    }

    public float getRaidbonus() {
        return raidbonus;
    }

    public void setRaidbonus(float raidbonus) {
        this.raidbonus = raidbonus;
    }

    public float getUnsuccessraid() {
        return unsuccessraid;
    }

    public void setUnsuccessraid(float unsuccessraid) {
        this.unsuccessraid = unsuccessraid;
    }

    public float getSuccesstackle() {
        return successtackle;
    }

    public void setSuccesstackle(float successtackle) {
        this.successtackle = successtackle;
    }

    public float getSupertackle() {
        return supertackle;
    }

    public void setSupertackle(float supertackle) {
        this.supertackle = supertackle;
    }

    public float getPushallout() {
        return pushallout;
    }

    public void setPushallout(float pushallout) {
        this.pushallout = pushallout;
    }

    public float getGetallout() {
        return getallout;
    }

    public void setGetallout(float getallout) {
        this.getallout = getallout;
    }

    public String getValidFormatText(float value) {
        String s = getValidDecimalFormat(value);
        return s.replaceAll("\\.00", "");
//        return s.replaceAll("\\.00", "");
    }
}
