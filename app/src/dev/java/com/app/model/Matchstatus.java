package com.app.model;

public class Matchstatus {

    private String match_status;

    public String getMatch_status() {
        return match_status;
    }

    public void setMatch_status(String match_status) {
        this.match_status = match_status;
    }
}
