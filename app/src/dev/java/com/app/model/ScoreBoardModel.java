package com.app.model;

import com.app.appbase.AppBaseModel;
import com.google.gson.annotations.SerializedName;

public class ScoreBoardModel extends AppBaseModel {



    private String _id;
    private String matchid;
    private String type;
    private DetailsBean details;
    @SerializedName("man-of-the-match")
    private String manofthematch;
    private String toss_winner_team;
    private String winner_team;
    private boolean matchStarted;

    public String get_id() {
        return getValidString(_id);
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMatchid() {
        return getValidString(matchid);
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public String getType() {
        return getValidString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public DetailsBean getDetails() {
        return details;
    }

    public void setDetails(DetailsBean details) {
        this.details = details;
    }

    public String getManofthematch() {
        return manofthematch;
    }

    public void setManofthematch(String manofthematch) {
        this.manofthematch = manofthematch;
    }

    public String getToss_winner_team() {
        return getValidString(toss_winner_team);
    }

    public void setToss_winner_team(String toss_winner_team) {
        this.toss_winner_team = toss_winner_team;
    }

    public String getWinner_team() {
        return getValidString(winner_team);
    }

    public void setWinner_team(String winner_team) {
        this.winner_team = winner_team;
    }

    public boolean isMatchStarted() {
        return matchStarted;
    }

    public void setMatchStarted(boolean matchStarted) {
        this.matchStarted = matchStarted;
    }

    public static class DetailsBean extends AppBaseModel {


        private TeamScoreModel team1;
        private TeamScoreModel team2;

        public TeamScoreModel getTeam1() {
            return team1;
        }

        public void setTeam1(TeamScoreModel team1) {
            this.team1 = team1;
        }

        public TeamScoreModel getTeam2() {
            return team2;
        }

        public void setTeam2(TeamScoreModel team2) {
            this.team2 = team2;
        }

    }
}
