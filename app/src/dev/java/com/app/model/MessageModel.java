package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class MessageModel extends AppBaseModel {


    /**
     * total : 2
     * list : [{"id":"2","title":"two","message":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu fermentum libero, commodo molestie arcu. Morbi finibus maximus consequat. Aliquam fermentum, tellus vel semper malesuada, felis neque porttitor dolor, eu convallis tortor ex non ligula. Donec sed ultricies sapien. Curabitur dictum sed mauris ac fringilla. Aliquam commodo vel leo non laoreet. Etiam non sollicitudin elit.","img":"","created":"1552020479"},{"id":"1","title":"one","message":"fantasy cricket start...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu fermentum libero, commodo molestie arcu. Morbi finibus maximus consequat. Aliquam fermentum, tellus vel semper malesuada, felis neque porttitor dolor, eu convallis tortor ex non ligula. Donec sed ultricies sapien. Curabitur dictum sed mauris ac fringilla. Aliquam commodo vel leo non laoreet. Etiam non sollicitudin elit.","img":"","created":"1552020479"}]
     */

    private String total;
    private String imgUrl;
    private List<ListBean> list;

    public String getTotal() {
        return getValidString(total);
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getImgUrl() {
        return getValidString(imgUrl);
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean extends AppBaseModel {
        /**
         * id : 2
         * title : two
         * message : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu fermentum libero, commodo molestie arcu. Morbi finibus maximus consequat. Aliquam fermentum, tellus vel semper malesuada, felis neque porttitor dolor, eu convallis tortor ex non ligula. Donec sed ultricies sapien. Curabitur dictum sed mauris ac fringilla. Aliquam commodo vel leo non laoreet. Etiam non sollicitudin elit.
         * img :
         * created : 1552020479
         */

        private String id;
        private String title;
        private String message;
        private String img;
        private String created;
        private String userid;
        private String sendAll;
        private IdBean _id;

        public String matchid;
        public String poolcontestid;
        public String gameid;
        public String mstatus;


        public String getMstatus() {
            return getValidString(mstatus);
        }

        public void setMstatus(String mstatus) {
            this.mstatus = mstatus;
        }



        public String getGameid() {
            return getValidString(gameid);
        }

        public void setGameid(String gameid) {
            this.gameid = gameid;
        }

        public String getMatchid() {
            return getValidString(matchid);
        }

        public void setMatchid(String matchid) {
            this.matchid = matchid;
        }

        public String getPoolcontestid() {
            return getValidString(poolcontestid);
        }

        public void setPoolcontestid(String poolcontestid) {
            this.poolcontestid = poolcontestid;
        }


        public String getId() {
            return getValidString(id);
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return getValidString(title);
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return getValidString(message);
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getImg() {
            return getValidString(img);
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUserid() {
            return getValidString(userid);
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getSendAll() {
            return getValidString(sendAll);
        }

        public void setSendAll(String sendAll) {
            this.sendAll = sendAll;
        }

        public boolean isRead() {
            return getUserid().equalsIgnoreCase("1");
        }

        public String getFormattedDate(int tag) {
            String time = "";
            if (!isValidString(getCreated())) return time;
            if (Long.parseLong(getCreated()) > 0) {
                if (tag == TAG_TWO) {
                    time = getFormattedCalendar(DATE_TIME_TWO, Long.parseLong(getCreated()));
                }
            }
            return time;
        }


        public IdBean get_id() {
            return _id;
        }

        public void set_id(IdBean _id) {
            this._id = _id;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj != null && obj instanceof ListBean) {
                return (((ListBean) obj).get_id().get$oid()).equals(get_id().get$oid());
            }
            return false;
        }

    }
}
