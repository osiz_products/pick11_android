package com.app.model;

import com.app.appbase.AppBaseResponseModel;

public class UserPaymentstatusResponseModel extends AppBaseResponseModel {

    UserPaymentstatusModel data;

    public UserPaymentstatusModel getData() {
        return data;
    }

    public void setData(UserPaymentstatusModel data) {
        this.data = data;
    }

}
