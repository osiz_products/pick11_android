package com.app.model;

import com.app.appbase.AppBaseModel;

public class SliderImagesModel extends AppBaseModel {


    private String id;
    private String title;
    private String img;

    public String getId() {
        return getValidString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return getValidString(img);
    }

    public void setImg(String img) {
        this.img = img;
    }
}


