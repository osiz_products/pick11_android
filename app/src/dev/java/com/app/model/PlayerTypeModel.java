package com.app.model;

import com.app.appbase.AppBaseModel;

public class PlayerTypeModel extends AppBaseModel {

    private long id;
    private String name;
    private String fullname;
    private int min;
    private int max;
    private String icon;
    private String tab_name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullname() {
        return getValidString(fullname);
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int
                               min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getIcon() {
        return getValidString(icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTab_name() {
        return tab_name;
    }

    public void setTab_name(String tab_name) {
        this.tab_name = tab_name;
    }

    public String getTitle() {
        int i = getMax() - getMin();
        if (i > 0)
            return "Pick " + getMin() + "-" + getMax() + " " + getFullname();
        else
            return "Pick " + getMin() + " " + getFullname();
    }

    public String getMinPlayerMessage(int min, String playerType) {
        return String.format("Every pick needs at least %s %s", min, playerType);
    }
}
