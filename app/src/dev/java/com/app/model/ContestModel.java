package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class ContestModel extends AppBaseModel {

    long poolcontestid;
    long contestmetaid;
    long contestid;
    long jointmwith;
    int id;
    double joinfee;
    double totalwining;
    int winners;
    int maxteams;
    int joinleft;
    String mstatus;
    String c;
    String m;
    String s;
    String iscancel;
    String isprivate;
    String ccode;
    String name;
    String title;

    List<PriceBreakUpModel> brkprize;

    public long getPoolcontestid() {
        return poolcontestid;
    }

    public void setPoolcontestid(long poolcontestid) {
        this.poolcontestid = poolcontestid;
    }

    public long getContestmetaid() {
        return contestmetaid;
    }

    public void setContestmetaid(long contestmetaid) {
        this.contestmetaid = contestmetaid;
    }

    public long getContestid() {
        return contestid;
    }

    public void setContestid(long contestid) {
        this.contestid = contestid;
    }

    public long getJointmwith() {
        return jointmwith;
    }

    public void setJointmwith(long jointmwith) {
        this.jointmwith = jointmwith;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getJoinfee() {
        return joinfee;
    }

    public void setJoinfee(double joinfee) {
        this.joinfee = joinfee;
    }

    public double getTotalwining() {
        return totalwining;
    }

    public void setTotalwining(double totalwining) {
        this.totalwining = totalwining;
    }

    public int getWinners() {
        return (winners);
    }

    public void setWinners(int winners) {
        this.winners = winners;
    }

    public String getMstatus() {
        return (mstatus);
    }

    public void setMstatus(String mstatus) {
        this.mstatus = mstatus;
    }

    public int getMaxteams() {
        return maxteams;
    }

    public void setMaxteams(int maxteams) {
        this.maxteams = maxteams;
    }

    public int getJoinleft() {
        return joinleft;
    }

    public void setJoinleft(int joinleft) {
        this.joinleft = joinleft;
    }

    public String getPricePool() {
        return String.valueOf(getTotalwining());
    }

    public String get_entry_fees() {
        return String.valueOf(getJoinfee());
    }

    public String getC() {
        return getValidString(c);
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getM() {
        return getValidString(m);
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getS() {
        return getValidString(s);
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getIscancel() {
        return getValidString(iscancel);
    }

    public void setIscancel(String iscancel) {
        this.iscancel = iscancel;
    }

    public String getIsprivate() {
        return getValidString(isprivate);
    }

    public void setIsprivate(String isprivate) {
        this.isprivate = isprivate;
    }

    public String getCcode() {
        return getValidString(ccode);
    }

    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    public String getName() {
        return getValidString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return getValidString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<PriceBreakUpModel> getBrkprize() {
        return brkprize;
    }

    public void setBrkprize(List<PriceBreakUpModel> brkprize) {
        this.brkprize = brkprize;
    }

    public String getTotalTeam() {
        return String.valueOf(getMaxteams()) + " Spots";
    }

    public String getTeamLeft() {
        return String.valueOf(getJoinleft()) + " Spots Left";
    }

    public String getPercent() {
        float winners = (float) getWinners();
        float maxteams = (float) getMaxteams();
        float percentage = (winners * 100) / maxteams;
        String format = String.format(Locale.US, "%.1f", percentage);
        return format + "%";
    }

    private float getPercentFload() {
        float winners = (float) getWinners();
        float maxteams = (float) getMaxteams();
        float percentage = (winners * 100) / maxteams;
        return percentage;
    }

    public String getPricePoolText() {
        String s = getValidDecimalFormat(getPricePool());
        return s.replaceAll("\\.00", "");
    }

    public String get_entry_feesText() {
        String s = getValidDecimalFormat(get_entry_fees());
        return s.replaceAll("\\.00", "");
    }

    public boolean isContestFull() {
        return getJoinleft() <= 0;
    }

    public boolean isConfirmWinning() {
        return getC().equalsIgnoreCase("1");
    }

    public boolean isMultiJoin() {
        return getM().equalsIgnoreCase("1");
    }

    public boolean isCancel() {
        return getIscancel().equalsIgnoreCase("1");
    }

    public boolean isPrivateContest() {
        return getIsprivate().equalsIgnoreCase("1");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof ContestModel) {
            return (((ContestModel) obj).getPoolcontestid() == getContestmetaid());
        }
        return false;
    }

    /*
     * Comparator implementation to Sort Order object based on OrderTotalwining
     */
    public static class OrderTotalwining implements Comparator<ContestModel> {
        boolean asseding;

        public OrderTotalwining(boolean asseding) {
            this.asseding = asseding;
        }

        @Override
        public int compare(ContestModel o1, ContestModel o2) {
            if (asseding)
                return Double.compare(o1.getTotalwining(), o2.getTotalwining());
            return Double.compare(o2.getTotalwining(), o1.getTotalwining());
        }
    }

    /*
     * Comparator implementation to Sort Order object based on OrderMaxteams
     */
    public static class OrderMaxteams implements Comparator<ContestModel> {
        boolean asseding;

        public OrderMaxteams(boolean asseding) {
            this.asseding = asseding;
        }

        @Override
        public int compare(ContestModel o1, ContestModel o2) {
            if (asseding)
                return Double.compare(o1.getMaxteams(), o2.getMaxteams());
            return Double.compare(o2.getMaxteams(), o1.getMaxteams());
        }
    }

    /*
     * Comparator implementation to Sort Order object based on OrderByMaxWinners
     */
    public static class OrderByMaxWinners implements Comparator<ContestModel> {
        boolean asseding;

        public OrderByMaxWinners(boolean asseding) {
            this.asseding = asseding;
        }

        @Override
        public int compare(ContestModel o1, ContestModel o2) {
            if (asseding)
                return Float.compare(o1.getPercentFload(), o2.getPercentFload());
            return Float.compare(o2.getPercentFload(), o1.getPercentFload());
        }
    }

    /*
     * Comparator implementation to Sort Order object based on joinfees
     */
    public static class OrderByFee implements Comparator<ContestModel> {
        boolean asseding;

        public OrderByFee(boolean asseding) {
            this.asseding = asseding;
        }

        @Override
        public int compare(ContestModel o1, ContestModel o2) {
            if (asseding)
                return Double.compare(o1.getJoinfee(), o2.getJoinfee());
            return Double.compare(o2.getJoinfee(), o1.getJoinfee());
        }
    }

    public String digitToText() {
        NumberToWordsConverter ntw = new NumberToWordsConverter(); // directly implement this
        return ntw.convert(Integer.valueOf(getPricePoolText()));
//        return ntw.convert(Integer.valueOf("500"));
    }

    private class NumberToWordsConverter {

        public String convert(final int n) {
            if (n < 100000) {
                return "" + n;
            }
            if (n < 10000000) {
                String s = String.valueOf(n / 100000);
                String s2 = (n % 100000 == 0) ? "" : String.valueOf(((n % 100000) / 10000));
                String s3 = (n % 10000 == 0) ? "" : String.valueOf(((n % 10000) / 1000));

                String s1 = s + (isValidString(s2) ? "." : "")
                        + (isValidString(s2) ? s2 : "")
                        + (isValidString(s3) ? s3 : "") + " Lakh ";

                return s1;
            }
            if (n < 1000000000) {
                String s = String.valueOf(n / 10000000);
                String s2 = (n % 10000000 == 0) ? "" : String.valueOf((n % 10000000) / 1000000);
                String s3 = (n % 100000 == 0) ? "" : String.valueOf(((n % 10000000) / 1000000));
                String s1 = s + (isValidString(s2) ? "." : "")
                        + (isValidString(s2) ? s2 : "")
                        + (isValidString(s3) ? s3 : "") + " Crore";

                return s1;
            }

            return String.valueOf(n);
        }
    }
}
