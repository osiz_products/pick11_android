package com.app.model;

import com.app.appbase.AppBaseModel;

import java.util.List;

public class MatchScoreDetailModel extends AppBaseModel {

//    @SerializedName("man-of-the-match")
//    private ManofTheMatch manofthematch;
    private String toss_winner_team;
    private String winner_team;
    private boolean matchStarted;

    List<FieldingModel> fielding;
    List<BowlingModel> bowling;
    List<BattingModel> batting;
    List<PlayerInfoModel> team;

   /* public ManofTheMatch getManofthematch() {
        return manofthematch;
    }

    public void setManofthematch(ManofTheMatch manofthematch) {
        this.manofthematch = manofthematch;
    }*/

    public String getToss_winner_team() {
        return getValidString(toss_winner_team);
    }

    public void setToss_winner_team(String toss_winner_team) {
        this.toss_winner_team = toss_winner_team;
    }

    public String getWinner_team() {
        return getValidString(winner_team);
    }

    public void setWinner_team(String winner_team) {
        this.winner_team = winner_team;
    }

    public boolean isMatchStarted() {
        return matchStarted;
    }

    public void setMatchStarted(boolean matchStarted) {
        this.matchStarted = matchStarted;
    }

    public List<FieldingModel> getFielding() {
        return fielding;
    }

    public void setFielding(List<FieldingModel> fielding) {
        this.fielding = fielding;
    }

    public List<BowlingModel> getBowling() {
        return bowling;
    }

    public void setBowling(List<BowlingModel> bowling) {
        this.bowling = bowling;
    }

    public List<BattingModel> getBatting() {
        return batting;
    }

    public void setBatting(List<BattingModel> batting) {
        this.batting = batting;
    }

    public List<PlayerInfoModel> getTeam() {
        return team;
    }

    public void setTeam(List<PlayerInfoModel> team) {
        this.team = team;
    }

    public class ManofTheMatch extends AppBaseModel {
        /**
         * name : SR Watson
         * pid : 8180
         */

        private String name;
        private String pid;

        public String getName() {
            return getValidString(name);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPid() {
            return getValidString(pid);
        }

        public void setPid(String pid) {
            this.pid = pid;
        }
    }
}
