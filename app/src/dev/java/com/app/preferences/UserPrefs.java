package com.app.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.app.model.CountryModel;
import com.app.model.NotificationModel;
import com.app.model.PlayerModel;
import com.app.model.StateModel;
import com.app.model.UserModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.preferences.BasePrefs;

import java.util.ArrayList;
import java.util.List;


public class UserPrefs extends BasePrefs {


    static final String Prefsname = "prefs_user";
    static String KEY_LOGGEDIN_USER = "logged_in_user";
    static String KEY_SOUND_SETTING = "sound_setting";
    static String KEY_NOTIFICATION_DATA = "notification_data";
    static String KEY_COUNTRY_LIST = "countryList";
    static String KEY_STATE_LIST = "stateList";
    static String KEY_JACKPOT = "jackpot";
    static String KEY_PERCENTAGE = "playerPercentage";
    public static String KEY_APPMODE = "modeSelect";
    public static String KEY_SELECT_ALL_TEAM = "key";
    Context context;


    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    List<UserPrefsListener> userPrefsListenerList = new ArrayList<>();

    public UserPrefs(Context context) {
        this.context = context;
        mSharedPreferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();
    }

    public void appMode(String modeSelect) {
        Log.e("get_GET",""+modeSelect);
        setStringKeyValuePrefs(KEY_APPMODE,modeSelect);
    }

     /*public void appMode(String modeSelect) {
        Log.e("Get_get",""+modeSelect);
        if(modeSelect!=null) {
            SharedPreferences prefs = getContext().getSharedPreferences("", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(KEY_APPMODE, modeSelect);
            editor.commit();
        }
    }*/

    public void setselectall(int selectall) {
        mSharedPreferencesEditor.putInt(KEY_SELECT_ALL_TEAM, selectall).commit();
    }

    public int getselectall() {
        return mSharedPreferences.getInt(KEY_SELECT_ALL_TEAM, 0);
    }


    public void addListener (UserPrefsListener userPrefsListener) {
        userPrefsListenerList.add(userPrefsListener);
    }

    public void removeListener (UserPrefsListener userPrefsListener) {
        userPrefsListenerList.remove(userPrefsListener);
    }

    public void clearListeners () {
        userPrefsListenerList.clear();
    }

    @Override
    public String getPrefsName () {
        return Prefsname;
    }

    @Override
    public Context getContext () {
        return context;
    }

    public void updateSoundSetting (boolean soundEnabled) {
        setBooleanKeyValuePrefs(KEY_SOUND_SETTING, soundEnabled);
        triggerSoundSettingUpdate(soundEnabled);

    }

    public boolean getSoundSetting () {
        return getBooleanKeyValuePrefs(KEY_SOUND_SETTING);
    }

    private void triggerSoundSettingUpdate (boolean soundEnabled) {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.soundSettingUpdate(soundEnabled);
        }
    }

    public void updateJackpotSetting (boolean jackpot) {
        setBooleanKeyValuePrefs(KEY_JACKPOT, jackpot);
        triggerJackpotSettingUpdate(jackpot);

    }

    public boolean getJackpotSetting () {
        return getBooleanKeyValuePrefs(KEY_JACKPOT);
    }

    private void triggerJackpotSettingUpdate (boolean jackpot) {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.jackpotSettingUpdate(jackpot);
        }
    }

    public void updateNotificationList (List<NotificationModel> data) {
        if (data == null) return;
        String toJson = new Gson().toJson(data);
        if (setStringKeyValuePrefs(KEY_NOTIFICATION_DATA, toJson)) {
            triggerNotificationListUpdate(data);
        }
    }

    public List<NotificationModel> getNotificationList () {
        List<NotificationModel> data = null;
        try {
            String toJson = getStringKeyValuePrefs(KEY_NOTIFICATION_DATA);
            if (isValidString(toJson)) {
                data = new Gson().fromJson(toJson, new TypeToken<List<NotificationModel>>() {
                }.getType());
            }
        } catch (JsonSyntaxException e) {

        }
        return data;
    }

    private void triggerNotificationListUpdate (List<NotificationModel> data) {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.notificationListUpdate(data);
        }
    }

    public UserModel getLoggedInUser () {
        UserModel userModel = null;
        try {
            String userDetail = getStringKeyValuePrefs(KEY_LOGGEDIN_USER);
            if (isValidString(userDetail)) {
                userModel = new Gson().fromJson(userDetail, UserModel.class);
            }
        } catch (JsonSyntaxException e) {

        }
        return userModel;
    }


    public PlayerModel PlayerModel () {
        PlayerModel playermodel = null;
        try {
            String userDetail = getStringKeyValuePrefs(KEY_PERCENTAGE);
            if (isValidString(userDetail)) {
                playermodel = new Gson().fromJson(userDetail, PlayerModel.class);
            }
        } catch (JsonSyntaxException e) {

        }
        return playermodel;
    }

    public void saveLoggedInUser (UserModel userModel) {
        if (userModel == null) return;
        String userDetail = new Gson().toJson(userModel);
        if (setStringKeyValuePrefs(KEY_LOGGEDIN_USER, userDetail)) {
            triggerUserLoggedIn(userModel);
        }
    }

    private void triggerUserLoggedIn (UserModel userModel) {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.userLoggedIn(userModel);
        }
    }

    public void updateLoggedInUser (UserModel userModel) {
        if (userModel == null) return;
        String userdetail = new Gson().toJson(userModel);
        if (setStringKeyValuePrefs(KEY_LOGGEDIN_USER, userdetail)) {
            triggerLoggedInUserUpdate(userModel);
        }
    }

    private void triggerLoggedInUserUpdate (UserModel userModel) {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.loggedInUserUpdate(userModel);
        }
    }


    public void clearLoggedInUser () {
        if (setStringKeyValuePrefs(KEY_LOGGEDIN_USER, "")) {
            triggerLoggedInUserClear();
        }
        setStringKeyValuePrefs(KEY_NOTIFICATION_DATA, "");
    }

    private void triggerLoggedInUserClear () {
        for (UserPrefsListener userPrefsListener : userPrefsListenerList) {
            userPrefsListener.loggedInUserClear();
        }
    }

    public void updateCountryList (List<CountryModel> data) {
        if (data == null) return;
        String toJson = new Gson().toJson(data);
        if (setStringKeyValuePrefs(KEY_COUNTRY_LIST, toJson)) {
//            triggerNotificationListUpdate(data);
        }
    }

    public List<CountryModel> getCountryList () {
        List<CountryModel> data = null;
        try {
            String toJson = getStringKeyValuePrefs(KEY_COUNTRY_LIST);
            if (isValidString(toJson)) {
                data = new Gson().fromJson(toJson, new TypeToken<List<CountryModel>>() {
                }.getType());
            }
        } catch (JsonSyntaxException e) {

        }
        return data;
    }
    public void updateStateList (List<StateModel> data) {
        if (data == null) return;
        String toJson = new Gson().toJson(data);
        if (setStringKeyValuePrefs(KEY_STATE_LIST, toJson)) {
        }
    }

    public List<StateModel> getStateList () {
        List<StateModel> data = null;
        try {
            String toJson = getStringKeyValuePrefs(KEY_STATE_LIST);
            if (isValidString(toJson)) {
                data = new Gson().fromJson(toJson, new TypeToken<List<StateModel>>() {
                }.getType());
            }
        } catch (JsonSyntaxException e) {

        }
        return data;
    }


    public interface UserPrefsListener {

        void notificationListUpdate(List<NotificationModel> data);

        void soundSettingUpdate(boolean soundEnabled);

        void jackpotSettingUpdate(boolean jackpot);

        void userLoggedIn(UserModel userModel);

        void loggedInUserUpdate(UserModel userModel);

        void loggedInUserClear();
    }
}
