package com.app.appbase;

import com.medy.retrofitwrapper.WebServiceBaseResponseModel;


public class AppBaseResponseModel extends WebServiceBaseResponseModel {

    String msg;

    public String getMsg() {
        return getValidString(msg);
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
