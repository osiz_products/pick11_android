package com.app.appbase;

import android.os.Bundle;

import com.app.fcm.AppNotificationMessagingService;
import com.app.model.NotificationModel;
import com.app.model.UserModel;
import com.app.preferences.UserPrefs;
import com.base.BaseApplication;
import com.pickeleven.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebRequestInterface;
import com.medy.retrofitwrapper.WebServiceResponseListener;
import com.rest.WebRequestHelper;

import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public abstract class AppBaseApplication extends BaseApplication implements
        UserPrefs.UserPrefsListener, WebServiceResponseListener {

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1100;

    public String currency_symbol = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    private UserPrefs userPrefs;
    private UserModel userModel;
    private WebRequestInterface webRequestInterface;
    private WebRequestHelper webRequestHelper;
    private boolean soundEnabled;
    private boolean jackpot;
    private List<NotificationModel> notificationModelList;

    @Override
    public void onCreate() {
        super.onCreate();
        AppNotificationMessagingService.createNotificationChannel(this);
        currency_symbol = getResources().getString(R.string.currency_symbol) + " ";
        webRequestHelper = new WebRequestHelper(this);
        userPrefs = new UserPrefs(this);
        userPrefs.addListener(this);
        userModel = userPrefs.getLoggedInUser();
        soundEnabled = userPrefs.getSoundSetting();
        jackpot = userPrefs.getJackpotSetting();
        notificationModelList = userPrefs.getNotificationList();

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (userModel!=null) {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(userModel.getId()));
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, userModel.getName());
//            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }

    }


    @Override
    public void userLoggedIn(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public void loggedInUserClear() {
        this.userModel = null;
    }

    @Override
    public void soundSettingUpdate(boolean soundEnabled) {
        this.soundEnabled = soundEnabled;
    }

    @Override
    public void jackpotSettingUpdate(boolean jackpot) {
        this.jackpot = jackpot;
    }

    @Override
    public void notificationListUpdate(List<NotificationModel> data) {
        if (this.notificationModelList != null) {
            this.notificationModelList.clear();
            this.notificationModelList.addAll(data);
        } else {
            this.notificationModelList = data;
        }

    }


    public UserPrefs getUserPrefs() {
        return userPrefs;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public boolean isSoundEnabled() {
        return soundEnabled;
    }

    public boolean isJackpot() {
        return jackpot;
    }

    public List<NotificationModel> getNotificationModelList() {
        return notificationModelList;
    }

    public void updateUserInPrefs() {
        if (this.userModel != null) {
            userPrefs.updateLoggedInUser(userModel);
        }
    }

    public void setUserModel(UserModel userModel) {
        if (this.userModel == null)
            this.userModel = userModel;
        else
            this.userModel.updateUserModel(userModel);
    }


    public int getGooglePlayServicesAvailableStatus() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        return api.isGooglePlayServicesAvailable(this);
    }

    public boolean isPlayServiceUpdated() {
        int resultCode = getGooglePlayServicesAvailableStatus();
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    public boolean isPlayServiceErrorUserResolvable(AppBaseActivity appBaseActivity) {
        int resultCode = getGooglePlayServicesAvailableStatus();
        GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        if (instance.isUserResolvableError(resultCode)) {
            instance.getErrorDialog(appBaseActivity,
                    resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            return true;
        }
        return false;
    }

    public WebRequestHelper getWebRequestHelper() {
        return webRequestHelper;
    }

    public WebRequestInterface getWebRequestInterfaceDefault() {
        if (webRequestInterface == null) {
            OkHttpClient okHttpClient = getOkHttpClient();
            webRequestInterface = new Retrofit.Builder()
                    .baseUrl("http://www.test.com/")
                   // .baseUrl("http://pick11.agiesnetworking.com/")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(WebRequestInterface.class);
        }
        return webRequestInterface;
    }

    public OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        okClientBuilder.connectTimeout(1 * 60 * 1000, TimeUnit.MILLISECONDS);
        okClientBuilder.readTimeout(30 * 1000, TimeUnit.MILLISECONDS);
        okClientBuilder.writeTimeout(15 * 1000, TimeUnit.MILLISECONDS);
        okClientBuilder.addInterceptor(logging);

        return okClientBuilder.build();

    }


    @Override
    public void onWebRequestCall(WebRequest webRequest) {
        if (getUserModel() != null) {
            webRequest.addHeader("Authorization", "Bearer " + getUserModel().getToken());
        }
        webRequest.setWebRequestInterface(getWebRequestInterfaceDefault());
    }

    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {

    }
}
