package com.app.appbase;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import com.app.model.NotificationModel;
import com.app.model.UserModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.myteam.createTeam.CreateTeamActivity;
import com.app.ui.main.dashboard.ScoreWebViewActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.uitilites.Validate;
import com.base.BaseActivity;
import com.pickeleven.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebRequestErrorDialog;
import com.medy.retrofitwrapper.WebServiceResponseListener;
import com.permissions.PermissionHelperNew;
import com.rest.WebRequestConstants;
import com.rest.WebRequestHelper;
import com.smsreceiver.SMSReceiver;

import java.util.List;

import static com.app.ui.main.cricket.contests.ContestsActivity.REQUEST_CREATE_TEAM;


public abstract class AppBaseActivity extends BaseActivity
        implements WebServiceResponseListener, WebRequestConstants, SMSReceiver.OTPReceiveListener,
        UserPrefs.UserPrefsListener {

    public static final int REQUEST_OTP_VERIFY = 102;
    public static final int REQUEST_CASHOUT = 104;
    public static final int REQUEST_CREATE_PRIVATE_CONTEST = 105;
    public static final int REQUEST_JOIN_PRIVATE_CONTEST = 106;
    public static final int REQUEST_JOIN_CREATE_TEAM = 107;

    private Dialog alertDialogProgressBar;
    private WebRequestErrorDialog errorMessageDialog;
    private Validate validate;
    private SMSReceiver smsReceiver;
    UserPrefs userPrefs;

    public static final int OTP_COUNTER = 1 * 60 * 1000;
    private static final int OVERLAY_PERMISSION_REQ_CODE = 1234;
    public String currency_symbol = "₹ ";

    ToolBarFragment toolBarFragment;

    public ToolBarFragment getToolBarFragment() {
        return toolBarFragment;
    }

    public Validate getValidate() {
        return validate;
    }

    @Override
    public void beforeSetContentView() {
        super.beforeSetContentView();
        setupActionBar();
    }

    @Override
    public void initializeComponent() {
        userPrefs = new UserPrefs(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary, this.getTheme()));
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorBlack, this.getTheme()));
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorBlack));
            }
        }

        Fragment fragmentById = getFm().findFragmentById(R.id.fragment_toolbar);
        if (fragmentById != null && fragmentById instanceof AppBaseFragment) {
            toolBarFragment = (ToolBarFragment) fragmentById;
            toolBarFragment.initializeComponent();
        }
        smsReceiver = new SMSReceiver();
        validate = new Validate(this);
        currency_symbol = getResources().getString(R.string.currency_symbol) + " ";
        UserModel userModel = getUserModel();
        if (userModel == null) {

            Crashlytics.setUserIdentifier("");
            Crashlytics.setUserName("");
            Crashlytics.setUserEmail("");

        } else {
            Crashlytics.setUserIdentifier(String.valueOf(userModel.getId()));
            Crashlytics.setUserName(userModel.getName());
            Crashlytics.setUserEmail(userModel.getEmail());
        }
    }

    public void setHeaderTitle(String headerName) {
        if (toolBarFragment != null)
            toolBarFragment.setHeader_name(headerName);
    }

    public void setNotificationCount(int notificationCount) {
        if (toolBarFragment != null)
            toolBarFragment.isCountVisible(notificationCount);
    }

    public void updateDeleteAllVisibility(int deleteAll) {
        if (toolBarFragment != null)
            toolBarFragment.isDeleteAllVisible(deleteAll);
    }

    public void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getColorFromStyle(R.attr.colorPrimaryDark));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.color_black));
        }
        setLightStatusBar(false);
    }

    public void setLightStatusBar() {
        setLightStatusBar(false);
        setWindowUnderStatusBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View view = getWindow().getDecorView();
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            setStatusBarColor(getResources().getColor(R.color.transparent_color));
            setNavigationBarColor(getColorFromStyle(R.attr.bottom_wave_color));
        }
    }

    @Override
    public void onWebRequestCall(WebRequest webRequest) {
        ((AppBaseApplication) getApplication()).onWebRequestCall(webRequest);
    }

    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {
        ((AppBaseApplication) getApplication()).onWebRequestPreResponse(webRequest);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        ((AppBaseApplication) getApplication()).onWebRequestResponse(webRequest);
    }

    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
            getWindow().setNavigationBarColor(color);
        }
    }

    public void setNavigationBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(color);
        }
    }

    public void setLightStatusBar(boolean isLight) {
        if (isLight) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR |
                        View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }
    }




    public void setWindowUnderStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }


    public Bundle generateBundleForTransitions(Pair... pairArray) {
        return ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairArray).toBundle();
    }

    public void displayProgressBar(boolean isCancellable) {
        displayProgressBar(isCancellable, "");
    }

    public void displayProgressBar(boolean isCancellable, String loaderMsg) {
        dismissProgressBar();
        if (isFinishing()) return;
        alertDialogProgressBar = new Dialog(this,
                R.style.DialogWait);
        alertDialogProgressBar.setCancelable(isCancellable);
        alertDialogProgressBar
                .requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.dialog_wait);
        TextView tv_loader_msg = alertDialogProgressBar.findViewById(R.id.tv_loader_msg);
        if (loaderMsg != null && !loaderMsg.trim().isEmpty()) {
            tv_loader_msg.setText(loaderMsg);
        } else {
            tv_loader_msg.setVisibility(View.GONE);
        }

        try {
            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            alertDialogProgressBar.show();

        } catch (Exception ignore) {

        }
    }

    public void dismissProgressBar() {
        try {
            if (!isFinishing() && alertDialogProgressBar != null) {
                alertDialogProgressBar.dismiss();
            }
        } catch (Exception ignore) {

        }
    }

    public void showErrorMsg(String msg) {
        showErrorMsg(msg, Gravity.BOTTOM);
    }

    public void showErrorMsg(String msg, final int GravityTop) {
        if (!isFinishing()) {
            if (errorMessageDialog == null) {
                errorMessageDialog = new WebRequestErrorDialog(this, msg) {
                    @Override
                    public int getLayoutResourceId() {
                        return R.layout.dialog_error;
                    }

                    @Override
                    public int getGravity() {
                        return GravityTop;
                    }

                    @Override
                    public int getDialogAnimationStyleId() {
                        return super.getDialogAnimationStyleId();
                    }

                    @Override
                    public int getMessageTextViewId() {
                        return R.id.tv_message;
                    }

                    @Override
                    public int getDismissBtnTextViewId() {
                        return R.id.tv_ok;
                    }
                };
            } else if (errorMessageDialog.isShowing()) {
                errorMessageDialog.dismiss();
            }
            errorMessageDialog.setMsg(msg);
            errorMessageDialog.show();
        }
    }

    public WebRequestHelper getWebRequestHelper() {

        return ((MyApplication) getApplication()).getWebRequestHelper();
    }

    public SMSReceiver getSmsReceiver() {
        return smsReceiver;
    }

    public void startSMSListener() {
        try {
            getSmsReceiver().setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        MyApplication.getInstance().printLog("OTP Received: ", otp);
    }

    @Override
    public void onOTPTimeOut() {
        MyApplication.getInstance().printLog("OTP Reviver: ", "OTP time Out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        MyApplication.getInstance().printLog("OTP Reviver: ", error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (PermissionHelperNew.areExplicitPermissionsRequired()) {
                if (!Settings.canDrawOverlays(this)) {
                    showErrorMsg("Please allow required permission.");
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void userLoggedIn(UserModel userModel) {

    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {

    }

    @Override
    public void loggedInUserClear() {

    }

    @Override
    public void soundSettingUpdate(boolean soundEnabled) {

    }

    @Override
    public void jackpotSettingUpdate(boolean jackpot) {

    }

    @Override
    public void notificationListUpdate(List<NotificationModel> data) {

    }


    public void updateUserInPrefs() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            ((AppBaseApplication) getApplication()).updateUserInPrefs();
        }
    }

    public UserPrefs getUserPrefs() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getApplication()).getUserPrefs();
        }
        return null;
    }

    public UserModel getUserModel() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getApplication()).getUserModel();
        }
        return null;
    }


    public List<NotificationModel> getNotificationList() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getApplication()).getNotificationModelList();
        }
        return null;
    }


    public boolean isPlayServiceUpdated() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getApplication()).isPlayServiceUpdated();
        }
        return false;
    }

    public boolean isPlayServiceErrorUserResolvable() {
        if (getApplication() != null && getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getApplication()).isPlayServiceErrorUserResolvable(this);
        }
        return false;
    }

    public void callLogout() {
        ((MyApplication) getApplication()).unAuthorizedResponse(null);
    }

    public void loadImage(Object mContext, ImageView imageView,
                          final ProgressBar pb_image, String imageUrl, int placeHolder) {
        this.loadImage(mContext, imageView, pb_image, imageUrl, placeHolder, placeHolder, placeHolder, 500);
    }

    public void loadImage(Object mContext, ImageView imageView,
                          final ProgressBar pb_image, String imageUrl, int placeHolder, int requireWidth) {
        this.loadImage(mContext, imageView, pb_image, imageUrl, placeHolder, placeHolder, placeHolder, requireWidth);
    }

    public void loadImage(Object mContext, ImageView imageView,
                          final ProgressBar pb_image, String imageUrl,
                          int placeHolder, int error, int fallBack, int requireWidth) {
        if (imageUrl == null || imageUrl.trim().isEmpty()) {
            if (pb_image != null && pb_image.getVisibility() != View.INVISIBLE) {
                pb_image.setVisibility(View.INVISIBLE);
            }
            imageView.setImageResource(fallBack);
            return;
        }
        if (mContext == null) return;


        RequestManager requestManager = null;
        if (mContext instanceof Activity) {
            requestManager = Glide.with((Activity) mContext);
        } else if (mContext instanceof Fragment) {
            requestManager = Glide.with((Fragment) mContext);
        } else {
            requestManager = Glide.with((Context) mContext);
        }
        if (requestManager == null) return;
        RequestOptions options = new RequestOptions()
                .placeholder(placeHolder)
                .fallback(error);
        if (requireWidth > 0) {
            options.override(requireWidth);
        }
        options.error(error)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);

        if (pb_image != null && pb_image.getVisibility() != View.VISIBLE) {
            pb_image.setVisibility(View.VISIBLE);
        }
        requestManager.asBitmap().load(imageUrl).
                apply(options).
                listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        if (pb_image != null && pb_image.getVisibility() != View.INVISIBLE) {
                            pb_image.setVisibility(View.INVISIBLE);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        if (pb_image != null && pb_image.getVisibility() != View.INVISIBLE) {
                            pb_image.setVisibility(View.INVISIBLE);
                        }
                        return false;
                    }
                }).into(imageView);

    }


    public boolean copyToClipboard(String text) {
        try {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData
                    .newPlainText(getResources().getString(R.string.app_name), text);
            clipboard.setPrimaryClip(clip);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void shareWithFriends(String referCode) {
        try {
//            if (getUserModel() == null) return;
            String appName = getResources().getString(R.string.app_name);
            String referMessage = getResources().getString(R.string.refer_message);
//            referMessage = String.format(referMessage, referCode,
//                    "30", getPackageName(),
//                    appName,
//                    "100");

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, referMessage);
            startActivity(Intent.createChooser(i, "Share Via :"));
        } catch (Exception ignore) {

        }
    }

    public void makeDirectCall(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            showCustomToast("No app found for call.");
        }
    }

    public void goToWebViewActivity(Bundle bundle) {
        Intent intent = new Intent(this, ScoreWebViewActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToCreateTeamActivity(Bundle bundle) {
        String gemeType = MyApplication.getInstance().getGemeType();
        Intent intent = null;
        if (gemeType.equalsIgnoreCase("1")) {
            intent = new Intent(this, CreateTeamActivity.class);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            startActivityForResult(intent, REQUEST_CREATE_TEAM);
        } else if (gemeType.equalsIgnoreCase("2")) {
            intent = new Intent(this, com.app.ui.main.football.myteam.createTeam.CreateTeamActivity.class);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            startActivityForResult(intent, com.app.ui.main.football.contests.ContestsActivity.REQUEST_CREATE_TEAM);
        }else {
            intent = new Intent(this, com.app.ui.main.kabaddi.myteam.createTeam.CreateTeamActivity.class);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            startActivityForResult(intent, com.app.ui.main.kabaddi.contests.ContestsActivity.REQUEST_CREATE_TEAM);
        }
//        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void updateViewVisibitity (View view, int visibility) {
        if (getApplication() == null) return;
        if (this.getApplicationContext() instanceof BaseActivity) {
            ((BaseActivity) getApplicationContext()).updateViewVisibility(view, visibility);
        }
    }
}
