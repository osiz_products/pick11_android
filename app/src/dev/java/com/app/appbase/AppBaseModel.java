package com.app.appbase;


import com.model.BaseModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AppBaseModel extends BaseModel {


    public static final String DATE_MMMDDYYYY = "MMM dd,YYYY";

    public String getFormatedDateString(String format, long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp * 1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        return simpleDateFormat.format(calendar.getTime());
    }

    public String getValidTimeText(long value) {

        long hours = TimeUnit.SECONDS.toHours(value);
        long minutes = TimeUnit.SECONDS.toMinutes(value - TimeUnit.HOURS.toSeconds(hours));
        long second = value - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes);
        StringBuilder builder = new StringBuilder();
        if (hours < 0)
            builder.append("-");
        if (hours != 0)
            builder.append(String.format(Locale.US, "%02dh", Math.abs(hours)));

        if (minutes < 0) {
            builder.append(" -");
        } else {
            builder.append(" ");
        }
        if (minutes != 0)
            builder.append(String.format(Locale.US, "%02dm", Math.abs(minutes)));

        if (second < 0) {
            builder.append(" -");
        } else {
            builder.append(" ");
        }
        if (second != 0)
            builder.append(String.format(Locale.US, "%02ds", Math.abs(second)));

        return builder.toString();
    }

    public String capitalize(String capString){
        if (!capString.isEmpty()) {
            StringBuffer capBuffer = new StringBuffer();
            Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
            while (capMatcher.find()) {
                capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
            }

            return capMatcher.appendTail(capBuffer).toString();
        }
        return "";
    }

}
