package com.app.appbase;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.model.NotificationModel;
import com.app.model.UserModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebRequestErrorDialog;
import com.medy.retrofitwrapper.WebServiceResponseListener;
import com.rest.WebRequestConstants;
import com.rest.WebRequestHelper;

import java.util.List;


public abstract class AppBaseFragment extends BaseFragment
        implements WebServiceResponseListener, WebRequestConstants, UserPrefs.UserPrefsListener {

    private Dialog alertDialogProgressBar;
    private WebRequestErrorDialog errorMessageDialog;

    public String currency_symbol = "";

    @Override
    public void initializeComponent() {
        currency_symbol = getResources().getString(R.string.currency_symbol) + " ";
    }

    public void onPageSelected(){

    }

    @Override
    public void onWebRequestCall(WebRequest webRequest) {
        if (getActivity() != null) {
            ((AppBaseActivity) getActivity()).onWebRequestCall(webRequest);
        }
    }
    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {
        if (getActivity() != null) {
            ((AppBaseActivity) getActivity()).onWebRequestPreResponse(webRequest);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        if (getActivity() != null) {
            ((AppBaseActivity) getActivity()).onWebRequestResponse(webRequest);
        }
    }

    public void displayProgressBar(boolean isCancellable) {
        displayProgressBar(isCancellable, "");
    }

    public void displayProgressBar(boolean isCancellable, String loaderMsg) {
        dismissProgressBar();
        if (getActivity() == null) return;
        alertDialogProgressBar = new Dialog(getActivity(),
                R.style.DialogWait);
        alertDialogProgressBar.setCancelable(isCancellable);
        alertDialogProgressBar
                .requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.dialog_wait);
        TextView tv_loader_msg = alertDialogProgressBar.findViewById(R.id.tv_loader_msg);
        if (loaderMsg != null && !loaderMsg.trim().isEmpty()) {
            tv_loader_msg.setText(loaderMsg);
        } else {
            tv_loader_msg.setVisibility(View.GONE);
        }
        try {
            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            alertDialogProgressBar.show();
        } catch (Exception ignore) {

        }
    }

    public void dismissProgressBar() {
        try {
            if (getActivity() != null && alertDialogProgressBar != null && alertDialogProgressBar.isShowing()) {
                alertDialogProgressBar.dismiss();
            }
        } catch (Exception ignore) {

        }
    }

    public void showErrorMsg(String msg) {

        if (isValidActivity() && isVisible()) {
            if (errorMessageDialog == null) {
                errorMessageDialog = new WebRequestErrorDialog(getActivity(), msg) {
                    @Override
                    public int getLayoutResourceId() {
                        return R.layout.dialog_error;
                    }

                    @Override
                    public int getMessageTextViewId() {
                        return R.id.tv_message;
                    }

                    @Override
                    public int getDismissBtnTextViewId() {
                        return R.id.tv_ok;
                    }
                };
            } else if (errorMessageDialog.isShowing()) {
                errorMessageDialog.dismiss();
            }
            try {
                errorMessageDialog.setMsg(msg);
                errorMessageDialog.show();
            } catch (Exception ignore) {

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dismissProgressBar();
    }


    public WebRequestHelper getWebRequestHelper() {
        if (getActivity() == null) return null;
        return ((AppBaseActivity) getActivity()).getWebRequestHelper();
    }


    @Override
    public void userLoggedIn(UserModel userModel) {

    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {

    }

    @Override
    public void loggedInUserClear() {

    }

    @Override
    public void soundSettingUpdate(boolean soundEnabled) {

    }

    @Override
    public void jackpotSettingUpdate(boolean jackpot) {

    }

    @Override
    public void notificationListUpdate(List<NotificationModel> data) {

    }

    public UserPrefs getUserPrefs() {
        if (getActivity() != null && getActivity().getApplication() != null && getActivity().getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getActivity().getApplication()).getUserPrefs();
        }
        return null;
    }

    public UserModel getUserModel() {
        if (getActivity() != null && getActivity().getApplication() != null && getActivity().getApplication() instanceof AppBaseApplication) {
            return ((AppBaseApplication) getActivity().getApplication()).getUserModel();
        }
        return null;
    }

    public void shareWithFriends() {
        try {
            if (getUserModel() == null) return;
            String referMessage = "Doodhbhandaar shares a passion for tested quality milk at your doorsteps." +
                    "\nhttps://play.google.com/store/apps/details?id=com.doodhbhandaar&hl=en";

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, referMessage);
            startActivity(Intent.createChooser(i, "Share Via :"));
        } catch (Exception ignore) {

        }
    }

    public MyApplication getMyApplication() throws IllegalAccessException {
        if (getActivity() == null)
            throw new IllegalAccessException("Activity is not found");
        return ((MyApplication) getActivity().getApplication());
    }


    public void updateViewVisibility(View view, int visibility) {
        if (view != null && view.getVisibility() != visibility)
            view.setVisibility(visibility);
    }

}
