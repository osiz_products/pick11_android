package com.app.ui.main.navmenu.livescore.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.LiveDataModel;
import com.app.model.MatchModel;
import com.pickeleven.R;

import java.util.List;

public class LiveScoreAdapter extends AppBaseRecycleAdapter {

    private List<LiveDataModel> list;
    private MatchModel matchModel;

    public LiveScoreAdapter(List<LiveDataModel> list, MatchModel matchModel) {
        this.list = list;
        this.matchModel = matchModel;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_live_score));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public LiveDataModel getItem(int position) {
        return list.get(position);
    }

    private class ViewHolder extends BaseViewHolder {
        private TextView tv_live_text;
        private ImageView iv_image_first;
        private ProgressBar pb_image_first;
        private TextView tv_team_one;
        private TextView tv_team_one_score;
        private ImageView iv_image_second;
        private ProgressBar pb_image_second;
        private TextView tv_team_two;
        private TextView tv_team_two_score;
        private TextView tv_match_type;
        private TextView tv_match_start_time;
        private TextView tv_full_score_card;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_live_text = itemView.findViewById(R.id.tv_live_text);
            iv_image_first = itemView.findViewById(R.id.iv_image_first);
            pb_image_first = itemView.findViewById(R.id.pb_image_first);
            tv_team_one = itemView.findViewById(R.id.tv_team_one);
            tv_team_one_score = itemView.findViewById(R.id.tv_team_one_score);
            iv_image_second = itemView.findViewById(R.id.iv_image_second);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            tv_team_two = itemView.findViewById(R.id.tv_team_two);
            tv_team_two_score = itemView.findViewById(R.id.tv_team_two_score);
            tv_match_type = itemView.findViewById(R.id.tv_match_type);
            tv_match_start_time = itemView.findViewById(R.id.tv_match_start_time);
            tv_full_score_card = itemView.findViewById(R.id.tv_full_score_card);

            updateViewVisibitity(tv_match_start_time, View.GONE);
            updateViewVisibitity(tv_live_text, View.GONE);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            LiveDataModel liveDataModel = list.get(position);
            if (liveDataModel == null) return null;

            tv_full_score_card.setTag(position);
            tv_full_score_card.setOnClickListener(this);


            LiveDataModel.InningsdetailBean inningsdetail = liveDataModel.getInningsdetail();
            if (inningsdetail != null) {
                LiveDataModel.InningsdetailBean.ABean a = inningsdetail.getA();
                LiveDataModel.InningsdetailBean.BBean b = inningsdetail.getB();
                LiveDataModel.InningsdetailBean.TeamABean team_a = inningsdetail.getTeam_a();
                if (team_a != null) {
                    ((AppBaseActivity) getContext()).loadImage(getContext(),
                            iv_image_first, pb_image_first, team_a.getLogo(), R.drawable.dummy_logo);
                    tv_team_one.setText(team_a.getShort_name());
                } else {
                    tv_team_one.setText("");
                }

                if (a != null) {
                    String string = a.get_$1();
                    tv_team_one_score.setText(string);
                } else {
                    tv_team_one_score.setText("");
                }

                LiveDataModel.InningsdetailBean.TeamBBean team_b = inningsdetail.getTeam_b();
                if (team_b != null) {
                    ((AppBaseActivity) getContext()).loadImage(getContext(),
                            iv_image_second, pb_image_second, team_b.getLogo(), R.drawable.dummy_logo);
                    tv_team_two.setText(team_b.getShort_name());
                } else {
                    tv_team_two.setText("");
                }

                if (b != null) {
                    String string = b.get_$1();
                    tv_team_two_score.setText(string);
                } else {
                    tv_team_two_score.setText("");
                }
            }

            tv_match_type.setText(liveDataModel.getType());
            tv_match_start_time.setText(liveDataModel.getStatus());
            tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_black));


        /*ScoreBoardModel.DetailsBean details = scoreBoardModel.getDetails();
            if(details ==null)return;
        TeamScoreModel team1 = details.getTeam1();
        TeamScoreModel team2 = details.getTeam2();

            if(team1 !=null)

        {
            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_first, pb_image_first, team1.getLogo(), R.drawable.dummy_logo);
            tv_team_one.setText(team1.getName());
            List<ScoreModel> score = team1.getScore();
            if (score != null && score.size() > 0) {
                ScoreModel scoreModel = score.get(0);
                String string = scoreModel.getInningRun() + " / " + scoreModel.getInningWicket()
                        + "<font color=" + getContext().getResources().getColor(R.color.colorGray)
                        + "> (" + scoreModel.getInningOverText() + ")</font>";
                tv_team_one_score.setText(Html.fromHtml(string), TextView.BufferType.SPANNABLE);
            } else {
                tv_team_one_score.setText("");
            }
        } else

        {
            tv_team_one.setText("");
        }

            if(team2 !=null)

        {
            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_second, pb_image_second, team2.getLogo(), R.drawable.dummy_logo);
            tv_team_two.setText(team2.getName());
            List<ScoreModel> score = team2.getScore();
            if (score != null && score.size() > 0) {
                ScoreModel scoreModel = score.get(0);
                String string = scoreModel.getInningRun() + " / " + scoreModel.getInningWicket()
                        + "<font color=" + getContext().getResources().getColor(R.color.colorGray)
                        + "> (" + scoreModel.getInningOverText() + ")</font>";
                tv_team_two_score.setText(Html.fromHtml(string), TextView.BufferType.SPANNABLE);
            } else {
                tv_team_two_score.setText("");
            }
        } else

        {
            tv_team_two.setText("");
        }
            tv_match_type.setText(scoreBoardModel.getType());*/
//            tv_match_start_time.setText("Play in Progress");
//            tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));

            /*if (matchModel != null) {
                if (matchModel.isFixtureMatch()) {
                    tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
                    tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_black));
                } else if (matchModel.isLiveMatch()) {
                    tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
                    tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_black));
                } else {
                    tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
                    tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_black));
                }
            } else {
                tv_match_start_time.setText("Play in Progress");
                tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_black));
            }*/
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
