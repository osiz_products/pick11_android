package com.app.ui.main.navmenu;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseFragment;
import com.app.ui.forgotpassword.ForgotPasswordActivity;
import com.app.ui.forgotpassword.ForgotPasswordVerifyActivity;
import com.app.ui.login.EmailLoginActivity;
import com.app.ui.login.MobileLoginActivity;
import com.app.ui.main.allTransaction.AllTransactionsActivity;
import com.app.ui.main.cricket.allContest.AllContestsActivity;
import com.app.ui.main.cricket.contestDetail.ContestsDetailActivity;
import com.app.ui.main.cricket.contests.ContestsActivity;
import com.app.ui.main.cricket.joinedContests.JoinedContestsActivity;
import com.app.ui.main.cricket.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.cricket.myteam.createTeam.CreateTeamActivity;
import com.app.ui.main.cricket.myteam.myTeams.MyTeamsActivity;
import com.app.ui.main.cricket.myteam.playerDetail.PlayerDetailActivity;
import com.app.ui.main.cricket.myteam.playerDetail.PlayerPointsDetailActivity;
import com.app.ui.main.cricket.myteam.selectcaption.ChooseCaptionActivity;
import com.app.ui.main.cricket.privateContests.InviteCodeActivity;
import com.app.ui.main.cricket.privateContests.PrivateContestActivity;
import com.app.ui.main.cricket.privateContests.ShareActivity;
import com.app.ui.main.cricket.privateContests.prizebreakup.PricePoolBreakUpActivity;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.main.dashboard.ScoreWebViewActivity;
import com.app.ui.main.dashboard.myMatches.MyMatchesActivity;
import com.app.ui.main.navmenu.faq.FaqActivity;
import com.app.ui.main.navmenu.livescore.NewLiveScoreActivity;
import com.app.ui.main.navmenu.livescore.details.LiveScoreDetailActivity;
import com.app.ui.main.navmenu.myaccount.AddCashActivity;
import com.app.ui.main.navmenu.myaccount.LowBalanceActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.ui.main.navmenu.myaccount.WithdrawActivity;
import com.app.ui.main.navmenu.profile.ChangePasswordActivity;
import com.app.ui.main.navmenu.profile.EditProfileActivity;
import com.app.ui.main.navmenu.profile.ProfileActivity;
import com.app.ui.main.navmenu.referFriend.ReferFriendActivity;
import com.app.ui.main.navmenu.support.SupportActivity;
import com.app.ui.main.navmenu.verification.VerificationActivity;
import com.app.ui.main.notification.NotificationActivity;
import com.app.ui.register.RegisterActivity;
import com.app.ui.register.RegisterVerifyActivity;
import com.pickeleven.R;

public class ToolBarFragment extends AppBaseFragment {

    private TextView tv_header_name;
    private ImageView iv_menu;
    private ImageView iv_back;
    private ImageView iv_wallet;
    private RelativeLayout rl_notification;
    private ImageView iv_notification;
    private ImageView iv_notification_budge;
    private ImageView iv_edit_profile;
    private ImageView iv_delete_all;
    public TextView tv_score;

    @Override
    public int getLayoutResourceId() {
        return R.layout.include_toolbar;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        if (getActivity() == null || getView() == null) return;
        tv_header_name = getView().findViewById(R.id.tv_header_name);
        iv_menu = getView().findViewById(R.id.iv_menu);
        iv_back = getView().findViewById(R.id.iv_back);
        iv_wallet = getView().findViewById(R.id.iv_wallet);
        rl_notification = getView().findViewById(R.id.rl_notification);
        iv_notification = getView().findViewById(R.id.iv_notification);
        iv_notification_budge = getView().findViewById(R.id.iv_notification_budge);
        iv_edit_profile = getView().findViewById(R.id.iv_edit_profile);
        iv_delete_all = getView().findViewById(R.id.iv_delete_all);
        tv_score = getView().findViewById(R.id.tv_score);

        updateViewVisibility(iv_notification_budge, View.GONE);

        iv_menu.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        iv_wallet.setOnClickListener(this);
        rl_notification.setOnClickListener(this);
        iv_edit_profile.setOnClickListener(this);
        iv_delete_all.setOnClickListener(this);
        tv_score.setOnClickListener(this);

        onCreateView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if (getActivity() == null) return;
                getActivity().onBackPressed();
                break;

            default:
                if (getActivity() == null) return;
                ((ToolbarFragmentInterFace) getActivity()).onToolbarItemClick(v);
                break;
        }
    }

    public void setHeader_name(String header_name) {
        this.tv_header_name.setText(header_name);
    }

    public void isCountVisible(int count) {
        if (rl_notification.getVisibility() == View.VISIBLE) {
            if (count > 0) {
                updateViewVisibility(iv_notification_budge, View.VISIBLE);
            } else {
                updateViewVisibility(iv_notification_budge, View.GONE);
            }
        }
    }

    public void isDeleteAllVisible(int count) {
       /* if (rl_delete_all.getVisibility() == View.VISIBLE) {
            if (count > 0) {
                updateViewVisibility(iv_delete_all, View.VISIBLE);
            } else {
                updateViewVisibility(iv_delete_all, View.GONE);
            }
        }*/
    }

    public interface ToolbarFragmentInterFace {
        void onToolbarItemClick(View view);
    }

    private void onCreateView() {
        if (getActivity() == null) return;

        if (getActivity() instanceof RegisterVerifyActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Sign Up");

        } else if (getActivity() instanceof EmailLoginActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Enter Password");

        } else if (getActivity() instanceof MobileLoginActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Enter OTP");

        } else if (getActivity() instanceof RegisterActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Sign Up");

        } else if (getActivity() instanceof DashboardActivity) {
           // updateViewVisibility(iv_menu, View.VISIBLE);
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.GONE);
            updateViewVisibility(rl_notification, View.VISIBLE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Home");

        } else if (getActivity() instanceof NotificationActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.VISIBLE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Notifications");

        } else if (getActivity() instanceof MyAccountActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("My Account");

        } else if (getActivity() instanceof AddCashActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Add Cash");

        } else if (getActivity() instanceof SupportActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Support");

        } else if (getActivity() instanceof FaqActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("More");

        } else if (getActivity() instanceof VerificationActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Verification");

        } else if (getActivity() instanceof NewLiveScoreActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
         //   tv_header_name.setText("Live Score");
            tv_header_name.setText("Live scores & Stats");

        } else if (getActivity() instanceof LiveScoreDetailActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
           // tv_header_name.setText("Live Score");
            tv_header_name.setText("Live scores & Stats");

        } else if (getActivity() instanceof AllTransactionsActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Transaction History");

        } else if (getActivity() instanceof ProfileActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.VISIBLE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Profile");

        } else if (getActivity() instanceof EditProfileActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Edit Profile");

        } else if (getActivity() instanceof ChangePasswordActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Change Password");

        } else if (getActivity() instanceof ForgotPasswordActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Forgot Password");

        } else if (getActivity() instanceof ForgotPasswordVerifyActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Reset Password");

        } else if (getActivity() instanceof ReferFriendActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Refer Friend");

        } else if (getActivity() instanceof LowBalanceActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Low Balance");

        } else if (getActivity() instanceof ScoreWebViewActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("");

        } else if (getActivity() instanceof WithdrawActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Withdraw Ammount");

        } else if (getActivity() instanceof ContestsActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.contests.ContestsActivity
                || getActivity() instanceof com.app.ui.main.football.contests.ContestsActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.VISIBLE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Contests");

        } else if (getActivity() instanceof ContestsDetailActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.contestDetail.ContestsDetailActivity
                || getActivity() instanceof com.app.ui.main.football.contestDetail.ContestsDetailActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.VISIBLE);
            tv_header_name.setText("Contest Details");

        } else if (getActivity() instanceof AllContestsActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.allContest.AllContestsActivity
                || getActivity() instanceof com.app.ui.main.football.allContest.AllContestsActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.VISIBLE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Contests");

        } else if (getActivity() instanceof JoinedContestsActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.joinedContests.JoinedContestsActivity
                || getActivity() instanceof com.app.ui.main.football.joinedContests.JoinedContestsActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.VISIBLE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Joined Contests");

        } else if (getActivity() instanceof MyMatchesActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.VISIBLE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("My Matches");

        } else if (getActivity() instanceof ShareActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Invite Your Friends");

        } else if (getActivity() instanceof InviteCodeActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Invite Code");

        } else if (getActivity() instanceof ChooseTeamActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.chooseTeam.ChooseTeamActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.chooseTeam.ChooseTeamActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Choose Team");

        } else if (getActivity() instanceof CreateTeamActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.createTeam.CreateTeamActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.createTeam.CreateTeamActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Create Team");

        } else if (getActivity() instanceof ChooseCaptionActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.selectcaption.ChooseCaptionActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.selectcaption.ChooseCaptionActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("");

        } else if (getActivity() instanceof MyTeamsActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.myTeams.MyTeamsActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.myTeams.MyTeamsActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("My Teams");

        } else if (getActivity() instanceof PlayerDetailActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.playerDetail.PlayerDetailActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.playerDetail.PlayerDetailActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
//            tv_header_name.setText("Withdraw Ammount");

        } else if (getActivity() instanceof PlayerPointsDetailActivity
                || getActivity() instanceof com.app.ui.main.kabaddi.myteam.playerDetail.PlayerPointsDetailActivity
                || getActivity() instanceof com.app.ui.main.football.myteam.playerDetail.PlayerPointsDetailActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
//            tv_header_name.setText("Withdraw Ammount");

        } else if (getActivity() instanceof PrivateContestActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Create Private Contest");

        } else if (getActivity() instanceof PricePoolBreakUpActivity) {
            updateViewVisibility(iv_menu, View.GONE);
            updateViewVisibility(iv_back, View.VISIBLE);
            updateViewVisibility(rl_notification, View.GONE);
            updateViewVisibility(iv_wallet, View.GONE);
            updateViewVisibility(iv_edit_profile, View.GONE);
            updateViewVisibility(iv_delete_all, View.GONE);
            updateViewVisibility(tv_score, View.GONE);
            tv_header_name.setText("Create Private Contest");

        }

    }
}
