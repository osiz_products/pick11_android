package com.app.ui.main.cricket.myteam.chooseTeam;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestModel;
import com.app.model.JoinedTeamModel;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.TeamModel;
import com.app.model.TeamsPointModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webrequestmodel.SwitchTeamRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.JoinedTeamResponseModel;
import com.app.model.webresponsemodel.PlayerPreviewResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.model.webresponsemodel.TeamResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.SelectTeamDialog;
import com.app.ui.dialogs.joinconfirmdialog.ConfirmationCreatePrivateContestDialog;
import com.app.ui.dialogs.joinconfirmdialog.ConfirmationJoinContestDialog;
import com.app.ui.dialogs.selection.DataDialog;
import com.app.ui.main.cricket.contests.ContestsActivity;
import com.app.ui.main.cricket.myteam.chooseTeam.adapter.ChooseTeamAdapter;
import com.app.ui.main.cricket.myteam.playerpreview.TeamPreviewDialog;
import com.app.ui.main.navmenu.myaccount.LowBalanceActivity;

import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ChooseTeamActivity extends AppBaseActivity {

    LinearLayout ll_heading;
    RecyclerView recycler_view;
    ChooseTeamAdapter adapter;
    TextView tv_no_record_found;
    LinearLayout ll_join_contest;
    TextView tv_join_contest;
    LinearLayout ll_create_team;
    TextView tv_my_teams_total;
    LinearLayout ll_switch_team;
    TextView tv_switch_team;
    LinearLayout ll_option_team_name;
    TextView tv_switch_team_name;
    CheckBox tv_Select_all_btn;
    ImageView iv_switch_team_name;
    //    ImageView hv_back;
    CheckBox  cb_team_name;
    List<TeamModel> myAllTeams = new ArrayList<>();
    String old_team_id = "";
    String alreadyJoinedTeams = "";
    ConfirmationJoinContestDialog confirmationJoinContestDialog;

    boolean isselected = false;

    TeamsPointModel teamsPointModel;

    UserPrefs userPrefs;
    int key = 0;
    String ids ="";

    private PlayerTypeResponseModel.DataBean getPlayerTypeModel() {
        return MyApplication.getInstance().getPlayerTypeModels();
    }


    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    private JoinContestRequestModel getJoinPrivateContestRequestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA4);
            if (isValidString(string))
                return new Gson().fromJson(string, JoinContestRequestModel.class);
        }
        return null;
    }


    private JoinContestRequestModel getCreatePrivateContestRequestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA3);
            if (isValidString(string))
                return new Gson().fromJson(string, JoinContestRequestModel.class);
        }
        return null;
    }

    private ContestModel getContestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA2);
            if (isValidString(string))
                return new Gson().fromJson(string, ContestModel.class);
        }
        return null;
    }

    private String getMatchContestId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return getIntent().getExtras().getString(DATA1);
        return "";
    }

    private boolean isJoin() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getBoolean(DATA, false);
        }
        return false;
    }

    private boolean isJoinPrivateContest() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getBoolean(DATA5, false);
        }
        return false;
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_choose_team;
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.
     */


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        recycler_view = findViewById(R.id.recycler_view);
        ll_heading = findViewById(R.id.ll_heading);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        ll_option_team_name = findViewById(R.id.ll_option_team_name);
        tv_switch_team_name = findViewById(R.id.tv_switch_team_name);
        iv_switch_team_name = findViewById(R.id.iv_switch_team_name);
        ll_join_contest = findViewById(R.id.ll_join_contest);
        tv_join_contest = findViewById(R.id.tv_join_contest);
        ll_create_team = findViewById(R.id.ll_create_team);
        updateViewVisibility(ll_create_team, View.GONE);
        tv_my_teams_total = findViewById(R.id.tv_my_teams_total);
        ll_switch_team = findViewById(R.id.ll_switch_team);
        tv_switch_team = findViewById(R.id.tv_switch_team);
        cb_team_name = findViewById(R.id.cb_team_name);
        tv_Select_all_btn = findViewById(R.id.txtSelectall);
//        hv_back = findViewById(R.id.hv_back);


        ll_option_team_name.setOnClickListener(this);
        tv_join_contest.setOnClickListener(this);
        ll_create_team.setOnClickListener(this);
        ll_switch_team.setOnClickListener(this);
//        hv_back.setOnClickListener(this);
        tv_Select_all_btn.setOnClickListener(this);
        this.getWindow().setNavigationBarColor(getResources().getColor(R.color.colorGray));
        this.getWindow().setStatusBarColor(getResources().getColor(R.color.colorGray));

        initializeRecyclerView();

        userPrefs = new UserPrefs(this);
        userPrefs.setselectall(key);

        getJoinedTeam();

        if (isJoin()) {
            updateViewVisibility(ll_join_contest, View.VISIBLE);
            updateViewVisibility(ll_heading, View.GONE);
            updateViewVisibility(ll_switch_team, View.GONE);
        } else {
            updateViewVisibility(ll_join_contest, View.GONE);
            updateViewVisibility(ll_heading, View.VISIBLE);
            updateViewVisibility(ll_switch_team, View.VISIBLE);
        }
        if (isJoinPrivateContest()){
            updateViewVisibility(ll_create_team, View.VISIBLE);
        }else {
            updateViewVisibility(ll_create_team, View.GONE);
        }


    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new ChooseTeamAdapter(this,myAllTeams) {
            @Override
            public boolean isTeamAlreadyJoined(String teamId) {
                return alreadyJoinedTeams.contains(teamId);
            }
        };
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TeamModel teamModel = myAllTeams.get(position);
                switch (v.getId()) {
                    case R.id.ll_preview:
                        getTeamPlayers(teamModel);
                        break;
                    case R.id.cb_team_name:
                        boolean isChecked=((CheckBox) v).isChecked();
                        myAllTeams.get(position).setChecked(isChecked);
                        adapter.notifyTest();

                      selectedIds();
                        break;
                    default:
                        if (!adapter.isTeamAlreadyJoined(String.valueOf(teamModel.getId()))) {
                            adapter.setSelectedTeam(position);
                            setupUi();
                        }
                        break;
                }
            }
        });
    }

    private void selectedIds() {
        try {
            ArrayList<String> seleted=new ArrayList<>();
            for(TeamModel s:myAllTeams){
                if(s.isChecked()){
                    seleted.add(""+s.getId());
                }
            }
            Log.e("SelectedIds",seleted.toString());
            //Convert JSON Array
            JSONArray jArr=new JSONArray();
            for(String s:seleted){
                JSONObject jObj=new JSONObject();
                jObj.put("uteamid",s);
                jArr.put(jObj);
                Log.e("SelectedIds",jArr.toString());

                ids = jArr.toString();
                ids = String.valueOf(jArr);

                Log.e("SelectTem111111",""+ids);
                adapter.setDataTest(ids);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setupUi() {
        if (adapter != null) {
            if (isValidString(adapter.getSelectedTeam())) {
                tv_switch_team.setActivated(true);
            } else {
                tv_switch_team.setActivated(false);
            }
        } else {
            tv_switch_team.setActivated(false);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.hv_back:
//                onBackPressed();
//                break;

            case R.id.ll_option_team_name:
                showTeamsDialog();
                break;

            case R.id.tv_join_contest:
                 ids = adapter.getSelectedTeam();

                Log.i("ids__",ids);
                if (TextUtils.isEmpty(ids)) {
                    showErrorMsg("Please select Team for join");
                    return;
                }
                if (getCreatePrivateContestRequestModel() != null) {
                    preJoinCreatePrivateContest();
                    return;
                }
                if (getJoinPrivateContestRequestModel() != null) {
                    preJoinPrivateContest();
                    return;
                }
                preJoinContest();
                break;

            case R.id.ll_create_team:
                goToCreateTeamActivity(null);
                break;

            case R.id.ll_switch_team:
                String id = adapter.getSelectedTeam();
                 //ids = adapter.getSelectedTeam();
                Log.i("ids",id);
                if (TextUtils.isEmpty(id)) {
                    showErrorMsg("Please select Team for switch");
                    return;
                }
                callSwitchTeam();
                break;

            case R.id.txtSelectall:
                Log.i("click","click");
                if(!isselected){
                    isselected = true;
                    key = 1;
                    userPrefs.setselectall(key);
                    Log.i("clickkey","" + userPrefs.getselectall());

                    for(int i=0;i<myAllTeams.size();i++){
                        myAllTeams.get(i).setChecked(true);
                    }
                    adapter.notifyDataSetChanged();
//                    initializeRecyclerView();
                }else{
                    isselected = false;
                    key = 0;
                    userPrefs.setselectall(key);
                    Log.i("clickkey","" + userPrefs.getselectall());
//                    adapter.notifyDataSetChanged();
                   // initializeRecyclerView();
                    for(int i=0;i<myAllTeams.size();i++){
                        myAllTeams.get(i).setChecked(false);
                    }
                    adapter.notifyDataSetChanged();
                }
                selectedIds();
                break;

        }
    }


    private void showTeamsDialog() {
        if (alreadyJoinedTeams.split(",").length > 1) {
            hideKeyboard();
            final List<TeamModel> alreadySelectedTeam = new ArrayList<>();
            for (int i = 0; i < myAllTeams.size(); i++) {

                if (adapter.isTeamAlreadyJoined(String.valueOf(myAllTeams.get(i).getId()))) {
                    alreadySelectedTeam.add(myAllTeams.get(i));
                }

            }
            final SelectTeamDialog selectStateDialog = new SelectTeamDialog();
            selectStateDialog.setDataList(alreadySelectedTeam);
            selectStateDialog.setOnItemSelectedListeners(new DataDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectStateDialog.dismiss();
                    old_team_id = String.valueOf(alreadySelectedTeam.get(position).getId());
                    setHeading();
                }
            });
            selectStateDialog.show(getFm(), selectStateDialog.getClass().getSimpleName());
        }

    }


    private void setHeading() {
        if (alreadyJoinedTeams.split(",").length > 1) {
            updateViewVisibility(iv_switch_team_name, View.VISIBLE);
        } else {
            updateViewVisibility(iv_switch_team_name, View.GONE);
        }

        TeamModel teamModel = new TeamModel();
        if (myAllTeams != null || myAllTeams.size() > 0) {
            for (int i = 0; i < myAllTeams.size(); i++) {
                if (String.valueOf(myAllTeams.get(i).getId()).equalsIgnoreCase(old_team_id)) {
                    teamModel = myAllTeams.get(i);
                }
            }

            if (teamModel != null) {
                tv_switch_team_name.setText(teamModel.getTeamname());

                TeamsPointModel teamsPointModel = new TeamsPointModel();

                teamsPointModel.setTeamname(tv_switch_team_name.getText().toString());

                Log.i("teamname1",tv_switch_team_name.getText().toString());
            }

        }


    }

    private void preJoinContest() {
        try {
            String matchContestId = getMatchContestId();
            if (TextUtils.isEmpty(matchContestId)) return;
            String new_team_id = adapter.getSelectedTeam();
            if (new_team_id == null) return;
            JoinContestRequestModel requestModel = new JoinContestRequestModel();
            requestModel.atype = PRE_JOIN;
            requestModel.matchid = getMatchModel().getMatchid();
            requestModel.poolcontestid = getMatchContestId();
            requestModel.uteamid = new_team_id;

            requestModel.fees = String.valueOf(getContestModel().getJoinfee());

            Log.e("Get_Joinfee",""+String.valueOf(getContestModel().getJoinfee()));

            openConfirmJoinContest(requestModel);

        }catch (Exception e){

        }

    }

    private void JoinContest() {
        String matchContestId = getMatchContestId();
        if (TextUtils.isEmpty(matchContestId)) return;
        String new_team_id = adapter.getSelectedTeam();
        if (new_team_id == null) return;
        displayProgressBar(false);
        JoinContestRequestModel requestModel = new JoinContestRequestModel();
        requestModel.atype = JOIN;
        requestModel.matchid = getMatchModel().getMatchid();
        requestModel.poolcontestid = getMatchContestId();
        requestModel.uteamid = new_team_id;
        requestModel.fees = String.valueOf(getContestModel().getJoinfee());
        getWebRequestHelper().joinContest(requestModel, this);
    }

    private void callSwitchTeam() {

        if (getMatchModel() != null) {
            String matchContestId = getMatchContestId();
            if (TextUtils.isEmpty(matchContestId)) return;
            String new_team_id = adapter.getSelectedTeam();

            Log.e("Selected_SwithTeam",""+new_team_id);


            if (new_team_id == null) return;
            displayProgressBar(false);
            SwitchTeamRequestModel requestModel = new SwitchTeamRequestModel();
            requestModel.matchid = getMatchModel().getMatchid();
            requestModel.poolcontestid = getMatchContestId();
            requestModel.uteamid = old_team_id;
            requestModel.switchteamid = new_team_id;

            getWebRequestHelper().switchTeam(requestModel, this);


        }
    }

    private void preJoinCreatePrivateContest() {
        if (getCreatePrivateContestRequestModel() == null) return;
        JoinContestRequestModel requestModel = getCreatePrivateContestRequestModel();
        openConfirmCreatePrivateContest(requestModel);
    }

    private void preJoinPrivateContest() {
        if (getJoinPrivateContestRequestModel() == null) return;
        JoinContestRequestModel requestModel = getJoinPrivateContestRequestModel();
        openConfirmJoinPrivateContest(requestModel);
    }

    private void joinPrivateContest() {
        String new_team_id = adapter.getSelectedTeam();
        if (new_team_id == null) return;
        displayProgressBar(false);
        JoinContestRequestModel requestModel = getJoinPrivateContestRequestModel();
        if (requestModel == null) return;
        requestModel.atype = JOIN;
        requestModel.uteamid = new_team_id;
        getWebRequestHelper().joinContest(requestModel, this);
    }

    private void createPrivateContest() {
        String new_team_id = adapter.getSelectedTeam();
        if (new_team_id == null) return;
        displayProgressBar(false);
        JoinContestRequestModel requestModel = getCreatePrivateContestRequestModel();
        if (requestModel == null) return;
        requestModel.atype = JOIN;
        requestModel.uteamid = new_team_id;
        getWebRequestHelper().createPrivateContest(requestModel, this);
    }

    private void getJoinedTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getJoinedTeam(matchModel, getMatchContestId(), this);
    }

    private void getAllTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getAllTeam(matchModel, this);
    }

    private void getTeamPlayers(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_JOINED_TEAM:
                handleJoinedTeamResponse(webRequest);
                break;

            case ID_ALL_TEAM:
                handleAllTeamResponse(webRequest);
                break;

            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;

            case ID_SWITCH_TEAM:
                handleSwitchTeamResponse(webRequest);
                break;

            case ID_JOIN_CONTEST:
                handleJoinContestResponse(webRequest);
                break;

            case ID_CREATE_PRIVATE_CONTEST:
                handleJoinPrivateContestResponse(webRequest);
                break;
        }
    }


    private void handleJoinedTeamResponse(WebRequest webRequest) {
        StringBuilder builder = new StringBuilder();
        builder.append("");
        JoinedTeamResponseModel responseModel = webRequest.getResponsePojo(JoinedTeamResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                List<JoinedTeamModel> data = responseModel.getData();
                if (data == null) return;
                for (JoinedTeamModel datum : data) {
                    if (builder.length() > 0)
                        builder.append(",").append(datum.getUteamid());
                    else
                        builder.append(datum.getUteamid());
                }
            }
        }
        alreadyJoinedTeams = builder.toString();
        old_team_id = alreadyJoinedTeams.split(",")[0];
        getAllTeam();
    }

    private void handleSwitchTeamResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            onBackPressed();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }


    private void handleAllTeamResponse(WebRequest webRequest) {
        TeamResponseModel responseModel = webRequest.getResponsePojo(TeamResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            TeamResponseModel.DataBean data = responseModel.getData();
            if (data == null) return;
            List<TeamModel> teams = data.getTeams();
            myAllTeams.clear();
            if (teams != null && teams.size() > 0) {
                myAllTeams.addAll(teams);
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
            updateButton();
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

        setHeading();
        updateButton();
    }

    private void updateButton() {
        if (getPlayerTypeModel() != null && myAllTeams.size() < getPlayerTypeModel().getMaxteam()) {
            updateViewVisibility(ll_create_team, View.VISIBLE);
        } else {
            updateViewVisibility(ll_create_team, View.GONE);
        }
        if (myAllTeams.size() > 0) {
            updateViewVisibility(ll_join_contest, View.VISIBLE);
        } else {
            updateViewVisibility(ll_join_contest, View.GONE);
        }
    }


    private void handleTeamPlayersResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> wicketkeeper = new ArrayList<>();
            List<PlayerModel> bowler = new ArrayList<>();
            List<PlayerModel> batsman = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();


            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPname(datum.getPname());
                playerModel.setFullname(datum.getFullname());
                playerModel.setPtypename(datum.getPtypename());
                playerModel.setPtype(datum.getPtype());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setIsplaying(datum.getIsplaying());
                playerModel.setPimg(datum.getPimg());
                playerModel.setPoints(datum.getPoints());
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isWicketKeeper()) {
                    playerModel.setPtype("WK");
                    wicketkeeper.add(playerModel);
                } else if (datum.isBatsmen()) {
                    playerModel.setPtype("BAT");
                    batsman.add(playerModel);
                } else if (datum.isAllRounder()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                } else if (datum.isBowler()) {
                    playerModel.setPtype("BOWL");
                    bowler.add(playerModel);
                }

            }

            PlayerPreviewResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewResponseModel.DataBean.PlayersBean();
            playersBean.setWicketkeeper(wicketkeeper);
            playersBean.setBatsman(batsman);
            playersBean.setAllrounder(allrounder);
            playersBean.setBowler(bowler);

            PlayerPreviewResponseModel.DataBean team = new PlayerPreviewResponseModel.DataBean();
            team.setId(String.valueOf(teamModel.getId()));
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);
            team.setName(teamModel.getTeamname());
            for (PlayerPreviewModel datum : data) {
                if (datum.getIscap() == 1) {
                    team.setCaptainid(datum.getPid());
                }
                if (datum.getIsvcap() == 1) {
                    team.setVicecaptainid(datum.getPid());
                }
            }

            TeamPreviewDialog instance = TeamPreviewDialog.getInstance(null);
            instance.setTeam(team);
            instance.show(getFm(), instance.getClass().getSimpleName());


        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }


    private void goToLowBalanceActivity(Bundle bundle) {
        Intent intent = new Intent(this, LowBalanceActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (isFinishing()) return;
        startActivityForResult(intent, ContestsActivity.REQUEST_LOW_BALANCE);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    private void openConfirmJoinContest(JoinContestRequestModel model) {
        String matchContestId = getMatchContestId();
        if (!isValidString(matchContestId)) return;
        Bundle bundle = new Bundle();
        bundle.putString(DATA, new Gson().toJson(model));
        confirmationJoinContestDialog = ConfirmationJoinContestDialog.getInstance(bundle);
        confirmationJoinContestDialog.setConfirmationJoinContestListener(new ConfirmationJoinContestDialog.ConfirmationJoinContestListener() {
            @Override
            public void onProceed() {
                JoinContest();
            }

            @Override
            public void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                confirmationJoinContestDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(preJoinedModel));
                goToLowBalanceActivity(bundle);
            }
        });
        confirmationJoinContestDialog.show(getFm(), confirmationJoinContestDialog.getClass().getSimpleName());
    }

    private void openConfirmJoinPrivateContest(JoinContestRequestModel model) {
        Bundle bundle = new Bundle();
        bundle.putString(DATA, new Gson().toJson(model));
        confirmationJoinContestDialog = ConfirmationJoinContestDialog.getInstance(bundle);
        confirmationJoinContestDialog.setConfirmationJoinContestListener(new ConfirmationJoinContestDialog.ConfirmationJoinContestListener() {
            @Override
            public void onProceed() {
                printLog("1212121", "Join private contest");
                joinPrivateContest();
            }

            @Override
            public void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                confirmationJoinContestDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(preJoinedModel));
                goToLowBalanceActivity(bundle);
            }
        });
        confirmationJoinContestDialog.show(getFm(), confirmationJoinContestDialog.getClass().getSimpleName());
    }

    private void openConfirmCreatePrivateContest(JoinContestRequestModel model) {
        Bundle bundle = new Bundle();
        bundle.putString(DATA, new Gson().toJson(model));
        final ConfirmationCreatePrivateContestDialog confirmationJoinPrivateContestDialog = ConfirmationCreatePrivateContestDialog.getInstance(bundle);
        confirmationJoinPrivateContestDialog.setConfirmationJoinContestListener(new ConfirmationCreatePrivateContestDialog.ConfirmationJoinContestListener() {
            @Override
            public void onProceed() {
                createPrivateContest();
            }

            @Override
            public void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                confirmationJoinPrivateContestDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(preJoinedModel));
                goToLowBalanceActivity(bundle);
            }
        });
        confirmationJoinPrivateContestDialog.show(getFm(), confirmationJoinPrivateContestDialog.getClass().getSimpleName());
    }

    private void handleJoinContestResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            Intent intent = new Intent();
            intent.putExtra(DATA, "REQUEST_CREATE_PRIVATE_CONTEST");
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void handleJoinPrivateContestResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);

            Intent intent = new Intent();
            intent.putExtra(DATA, "REQUEST_CREATE_PRIVATE_CONTEST");
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ContestsActivity.REQUEST_CREATE_TEAM) {
                getJoinedTeam();
            } else if (requestCode == ContestsActivity.REQUEST_LOW_BALANCE) {
                preJoinContest();
            } else if (requestCode == REQUEST_CREATE_PRIVATE_CONTEST) {
                preJoinContest();
            }
        }
    }
}

