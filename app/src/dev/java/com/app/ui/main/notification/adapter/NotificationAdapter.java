package com.app.ui.main.notification.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.MessageModel;
import com.app.ui.main.cricket.contestDetail.ContestsDetailActivity;
import com.pickeleven.R;
import com.utilities.DeviceScreenUtil;

import java.util.List;

public class NotificationAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private boolean loadMore;
    List<MessageModel.ListBean> list;

    public NotificationAdapter(Context context) {
        this.context = context;
    }

    public void updateData (List<MessageModel.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public boolean setLoadMore (boolean loadMore) {
        if (list == null) return false;
        this.loadMore = loadMore;
        if (loadMore) {
            notifyItemInserted(list.size());
        } else {
            notifyItemRemoved(list.size());
        }
        return true;
    }
    @Override
    public int getViewType (int position) {
        if (list == null) {
            return VIEW_TYPE_DATA;
        } else if (loadMore && position == list.size()) {
            return VIEW_TYPE_LOAD_MORE;
        }
        return VIEW_TYPE_DATA;
    }

    @Override
    public BaseViewHolder getViewHolder (ViewGroup parent, int viewType) {
        if (VIEW_TYPE_DATA == viewType) {
            return new ViewHolder(inflateLayout(R.layout.item_notification));
        }
        return new LoadMoreViewHolder(inflateLayout(R.layout.adapter_item_load_more));
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return null;
    }

    @Override
    public int getDataCount () {
        return list == null ? 0 : (loadMore ? list.size() + 1 : list.size());
    }

    public MessageModel.ListBean getItem(int position) {
        return list.get(position);
    }

    public List<MessageModel.ListBean> getList() {
        return list;
    }

    private class ViewHolder extends BaseViewHolder {
        private RelativeLayout ll_header;
        private TextView tv_title;
        private TextView tv_date;
        private ImageView iv_delete;
        private RelativeLayout rl_message;
        private TextView tv_message;
        private ImageView iv_image;
        private CardView slectView;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_header = itemView.findViewById(R.id.ll_header);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_date = itemView.findViewById(R.id.tv_date);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            rl_message = itemView.findViewById(R.id.rl_message);
            tv_message = itemView.findViewById(R.id.tv_message);
            iv_image = itemView.findViewById(R.id.iv_image);
            slectView = itemView.findViewById(R.id.slectView);
            updateViewVisibitity(rl_message, View.GONE);
            updateViewVisibitity(iv_image, View.GONE);
//            updateViewVisibility(iv_delete, View.GONE);
        }

        @Override
        public String setData(int position) {

            final MessageModel.ListBean listBean = list.get(position);
            ll_header.setTag(position);
            ll_header.setOnClickListener(this);
            iv_delete.setTag(position);
            iv_delete.setOnClickListener(this);
            tv_title.setText(listBean.getTitle());
            tv_date.setText(listBean.getFormattedDate(2));
            tv_message.setText(listBean.getMessage());

            slectView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Log.e("GET_matchid",""+listBean.getMatchid());
                    Log.e("GET_poolcontestid",""+listBean.getPoolcontestid());
                    Log.e("GET_gameid",""+listBean.getGameid());*/
                 /*   Intent intent = new Intent(context, ContestsDetailActivity.class);
                    intent.putExtra("matchid",listBean.getMatchid());
                    intent.putExtra("poolcontestid",listBean.getPoolcontestid());
                    intent.putExtra("gameid",listBean.getGameid());
                    intent.putExtra("mstatus",listBean.getMstatus());
                    context.startActivity(intent);*/
                }
            });

            if (isValidString(listBean.getImg())) {
                updateViewVisibitity(iv_image, View.VISIBLE);
                int width = DeviceScreenUtil.getInstance().getWidth();
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) iv_image.getLayoutParams();
                layoutParams.height = Math.round(width * 0.437f);
                iv_image.setLayoutParams(layoutParams);
                ((AppBaseActivity) context).loadImage(context, iv_image, null, listBean.getImg(), R.drawable.dummy_logo);
            } else {
                updateViewVisibitity(iv_image, View.GONE);
            }

            if (listBean.isRead()) {
                updateViewVisibitity(rl_message, View.VISIBLE);
            } else {
                updateViewVisibitity(rl_message, View.GONE);
            }
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
