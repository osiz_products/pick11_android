package com.app.ui.main.kabaddi.myteam.selectcaption.adapter;

import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.PlayerModel;
import com.pickeleven.R;

import java.util.List;

public class ChooseCaptionAdapter extends AppBaseRecycleAdapter {

    private List<PlayerModel> list;
    String caption;
    String vcCaption;

    public ChooseCaptionAdapter(List<PlayerModel> list) {
        this.list = list;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setVcCaption(String vcCaption) {
        this.vcCaption = vcCaption;
    }

    public String getCaption() {
        return caption;
    }

    public String getVcCaption() {
        return vcCaption;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_choose_caption));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public void updateCaption(int position) {
        PlayerModel playerModel = list.get(position);
        if (vcCaption != null && vcCaption.equals(String.valueOf(playerModel.getPid()))) {
            vcCaption = null;
        }
        this.caption = String.valueOf(playerModel.getPid());
        notifyDataSetChanged();
    }

    public void updateVcCaption(int position) {
        PlayerModel playerModel = list.get(position);
        if (caption != null && caption.equals(String.valueOf(playerModel.getPid()))) {
            caption = null;
        }
        this.vcCaption = String.valueOf(playerModel.getPid());
        notifyDataSetChanged();
    }

    private class ViewHolder extends BaseViewHolder {
        private RelativeLayout rl_layout;
        private ImageView iv_player_image;
        private ProgressBar pb_image;
        private TextView tv_player_name;
        private TextView tv_team_type;
        private TextView tv_player_points;
        private TextView tv_captain;
        private TextView tv_vice_captain;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_layout = itemView.findViewById(R.id.rl_layout);
            iv_player_image = itemView.findViewById(R.id.iv_player_image);
            pb_image = itemView.findViewById(R.id.pb_image);
            tv_player_name = itemView.findViewById(R.id.tv_player_name);
            tv_team_type = itemView.findViewById(R.id.tv_team_type);
            tv_player_points = itemView.findViewById(R.id.tv_player_points);
            tv_captain = itemView.findViewById(R.id.tv_captain);
            tv_vice_captain = itemView.findViewById(R.id.tv_vice_captain);

        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            tv_captain.setTag(position);
            tv_captain.setOnClickListener(this);
            tv_vice_captain.setTag(position);
            tv_vice_captain.setOnClickListener(this);
            PlayerModel playerModel = list.get(position);

            if (isValidString(playerModel.getPimg())) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_player_image, pb_image,
                        playerModel.getPimg(), R.drawable.no_image, 100);
            } else {
                updateViewVisibitity(pb_image, View.INVISIBLE);
                iv_player_image.setImageDrawable(getContext().getResources().getDrawable(R.drawable.no_image));
            }

            tv_player_name.setText(playerModel.getPname());
//            tv_team_type.setText(playerModel.getTeam_type());
            String rank = "<font color='" + getContext().getResources().getColor(R.color.colorGray) +
                    "'>"+playerModel.getTeamname()+"</font>-" + playerModel.getPtype();
            tv_team_type.setText(Html.fromHtml(rank), TextView.BufferType.SPANNABLE);
            tv_player_points.setText(String.valueOf(playerModel.getPts()));

            if (caption == null && playerModel.getIscap() == 1)
                setCaption(playerModel.getPid());
            else playerModel.setIscap(0);
            if (vcCaption == null && playerModel.getIsvcap() == 1)
                setVcCaption(playerModel.getPid());
            else playerModel.setIsvcap(0);

            if (caption == null) {
                tv_captain.setActivated(false);
                tv_captain.setText("C");
            } else {
                if (String.valueOf(playerModel.getPid()).equals(caption)) {
                    tv_captain.setActivated(true);
                    tv_captain.setText("2x");
                } else {
                    tv_captain.setActivated(false);
                    tv_captain.setText("C");

                }
            }

            if (vcCaption == null) {
                tv_vice_captain.setActivated(false);
                tv_vice_captain.setText("VC");
            } else {
                if (String.valueOf(playerModel.getPid()).equals(vcCaption)) {
                    tv_vice_captain.setActivated(true);
                    tv_vice_captain.setText("1.5x");
                } else {
                    tv_vice_captain.setActivated(false);
                    tv_vice_captain.setText("VC");
                }
            }
            return rank;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
