package com.app.ui.main.navmenu.livescore.details.fragment;

import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseFragment;
import com.app.model.fullScoreBoard.CustomScoreModel;
import com.app.ui.main.navmenu.livescore.details.fragment.adapter.BatsManAdapter;
import com.app.ui.main.navmenu.livescore.details.fragment.adapter.BowlingAdapter;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class ScoreFragment extends AppBaseFragment {

    TextView tv_title_batsman;
    TextView tv_runWicketOver;
    RelativeLayout rl_extras;
    TextView tv_extras;
    RelativeLayout rl_total;
    TextView tv_total;
    RelativeLayout rl_not_playing;
    TextView tv_did_not_bat;
    CustomScoreModel scoreModel;

    List<CustomScoreModel.PlayerBean> batsMan = new ArrayList<>();
    RecyclerView recycler_view_batsman;
    BatsManAdapter batsManAdapter;
    List<CustomScoreModel.PlayerBean> bowler = new ArrayList<>();
    RecyclerView recycler_view_bowler;
    BowlingAdapter bowlingAdapter;


    public void setCustomScoreModel(CustomScoreModel scoreModel) {
        this.scoreModel = scoreModel;
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_score;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        tv_title_batsman = getView().findViewById(R.id.tv_title_batsman);
        tv_runWicketOver = getView().findViewById(R.id.tv_runWicketOver);
        rl_extras = getView().findViewById(R.id.rl_extras);
        tv_extras = getView().findViewById(R.id.tv_extras);
        rl_total = getView().findViewById(R.id.rl_total);
        tv_total = getView().findViewById(R.id.tv_total);
        rl_not_playing = getView().findViewById(R.id.rl_not_playing);
        tv_did_not_bat = getView().findViewById(R.id.tv_did_not_bat);

        setUpList();
    }

    private void setUpList() {
        batsMan.clear();
        bowler.clear();
        if (scoreModel == null) return;
        for (CustomScoreModel.PlayerBean playerBean : scoreModel.getPlayer()) {
            if (playerBean.isBowler()) {
                bowler.add(playerBean);
            } else {
                batsMan.add(playerBean);
            }
        }
        recyclerViewBatsMan();
        recyclerViewBowling();

        CustomScoreModel.ExtrasBean extras = scoreModel.getExtras();
        if (extras != null) {
            updateViewVisibility(rl_extras, View.VISIBLE);
            updateViewVisibility(rl_total, View.VISIBLE);
            String extrasData = "( b " + extras.getBye() + ", lb " + extras.getLegbye()
                    + ", w " + extras.getWide() + ", nb " + extras.getNoball() + ", p " + extras.getPenalty() + ")";
            String string = "<b>" + extras.getExtras() + "</b>" + extrasData;
            tv_extras.setText(Html.fromHtml(string), TextView.BufferType.SPANNABLE);

            String total = "(" + extras.getWickets() + " wkts, " + extras.getOvers() + " Ov)";
            String totalString = "<b>" + extras.getRuns() + "</b>" + total;
            tv_total.setText(Html.fromHtml(totalString), TextView.BufferType.SPANNABLE);

        } else {
            updateViewVisibility(rl_extras, View.GONE);
            updateViewVisibility(rl_total, View.GONE);
        }

        if (scoreModel.isCompleted()) {
            updateViewVisibility(rl_not_playing, View.VISIBLE);
        } else {
            updateViewVisibility(rl_not_playing, View.GONE);
        }
    }

    private void recyclerViewBatsMan() {
        tv_title_batsman.setText(scoreModel.getFull_name());
        tv_runWicketOver.setText(scoreModel.getRunWicketOver());
        recycler_view_batsman = getView().findViewById(R.id.recycler_view_batsman);
        batsManAdapter = new BatsManAdapter(getActivity(), batsMan);
        recycler_view_batsman.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view_batsman.setAdapter(batsManAdapter);
        recycler_view_batsman.setNestedScrollingEnabled(false);
        ItemClickSupport.addTo(recycler_view_batsman).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (v.getId()) {

                }
            }
        });
    }

    private void recyclerViewBowling() {
        if (scoreModel == null) return;
        recycler_view_bowler = getView().findViewById(R.id.recycler_view_bowler);
        bowlingAdapter = new BowlingAdapter(getActivity(), bowler);
        recycler_view_bowler.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view_bowler.setAdapter(bowlingAdapter);
        recycler_view_bowler.setNestedScrollingEnabled(false);
        ItemClickSupport.addTo(recycler_view_bowler).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (v.getId()) {

                }
            }
        });
    }
}
