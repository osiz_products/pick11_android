package com.app.ui.main.slider.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.viewpager.widget.PagerAdapter;

import com.app.appbase.AppBaseActivity;
import com.pickeleven.R;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public ViewPagerAdapter(Context context, ArrayList<String> IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.pager_item, view, false);

        assert imageLayout != null;
        ImageView imageView = imageLayout.findViewById(R.id.image);
        ProgressBar pb_image = imageLayout.findViewById(R.id.pb_image);
        String imageUrl = IMAGES.get(position);
        if (!TextUtils.isEmpty(imageUrl)) {
            ((AppBaseActivity) context).loadImage(context, imageView, pb_image, imageUrl,
                    R.drawable.dummy_logo);
        } else {
            imageView.setImageResource(R.drawable.dummy_logo);
            pb_image.setVisibility(View.INVISIBLE);
        }
        view.addView(imageLayout);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}
