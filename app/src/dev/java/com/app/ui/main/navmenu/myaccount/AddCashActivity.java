package com.app.ui.main.navmenu.myaccount;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.PaymentModel;
import com.app.model.StateModel;
import com.app.model.UserPaymentstatusResponseModel;
import com.app.model.webresponsemodel.PaymentResponseModel;
import com.app.model.webresponsemodel.PromoCodeResponseModel;
import com.app.model.webresponsemodel.StateResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.TransactionDetailDialog;
import com.app.ui.dialogs.paytmpayment.PaytmPaymentDialog;
import com.app.ui.dialogs.state.SelectStateDialog;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.tooltip.OnClickListener;
import com.utilities.DatePickerUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class
AddCashActivity extends AppBaseActivity {

    EditText tv_add_balance, et_promoCode;
    TextView tv_100, tv_200, tv_500, tv_continue, tv_have_promoCode, tv_apply, tv_error_promoCode;
    LinearLayout ll_paytm, ll_others, ll_continue;
    RelativeLayout rl_promoCode;
    String pcode = "";

    boolean isPaytm;
    boolean isVisible;
    UserPrefs userPrefs;
    AlertDialog alertDialogCongratulations;
    Context context;
    String getState;
    String getUserDateOfbirth;
    TextView txtState;
    TextView editDateOfbrith;
    String getPaymentStatus;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_add_cash;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);

        tv_add_balance = findViewById(R.id.tv_add_balance);
        tv_100 = findViewById(R.id.tv_100);
        tv_200 = findViewById(R.id.tv_200);
        tv_500 = findViewById(R.id.tv_500);
        ll_paytm = findViewById(R.id.ll_paytm);
        ll_others = findViewById(R.id.ll_others);
        ll_continue = findViewById(R.id.ll_continue);
        tv_continue = findViewById(R.id.tv_continue);
        updateViewVisibility(ll_others, View.GONE);
        tv_have_promoCode = findViewById(R.id.tv_have_promoCode);
      //  rl_promoCode = findViewById(R.id.rl_promoCode);
      //  updateViewVisibility(rl_promoCode, View.GONE);
        et_promoCode = findViewById(R.id.et_promoCode);
        tv_apply = findViewById(R.id.tv_apply);
        tv_error_promoCode = findViewById(R.id.tv_error_promoCode);
        updateViewVisibility(tv_error_promoCode, View.GONE);

        tv_100.setOnClickListener(this);
        tv_200.setOnClickListener(this);
        tv_500.setOnClickListener(this);
        ll_paytm.setOnClickListener(this);
        ll_others.setOnClickListener(this);
        tv_continue.setOnClickListener(this);
        tv_have_promoCode.setOnClickListener(this);
        tv_apply.setOnClickListener(this);

        isPaytm = true;
      //  ll_paytm.setBackgroundColor(getResources().getColor(R.color.color_item_bg2));
        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        getWebRequestHelper().getPayStatus(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_100:
                updateValue(100);
                break;

            case R.id.tv_200:
                updateValue(200);
                break;

            case R.id.tv_500:
                updateValue(500);
                break;

            case R.id.ll_paytm:
                isPaytm = true;
               // ll_paytm.setBackgroundColor(getResources().getColor(R.color.color_item_bg2));
                break;

            case R.id.ll_others:
                break;

            case R.id.tv_have_promoCode:
                updateViewVisibility(tv_error_promoCode, View.GONE);
                et_promoCode.setText("");
                if (!isVisible) {
                    isVisible = true;
                    updateViewVisibility(rl_promoCode, View.VISIBLE);
                } else {
                    isVisible = false;
                    updateViewVisibility(rl_promoCode, View.GONE);
                }
                break;

            case R.id.tv_apply:
                updateViewVisibility(tv_error_promoCode, View.GONE);
                applyPromoCode();
                break;

            case R.id.tv_continue:
               addBalance();
                //popupCheckAge();
                break;
        }
    }

    private void applyPromoCode() {

        String amount = tv_add_balance.getText().toString().trim();
        String promoCode = et_promoCode.getText().toString().trim();
        if (amount.isEmpty()) {
            showErrorMsg("Please enter amount.");
            return;
        }

        long amt = Long.parseLong(amount);
        if (amt > MAX_AMOUNT) {
            showErrorMsg("Amount cannot be greater than 10000");
            return;
        }

        if (promoCode.isEmpty()) {
            showErrorMsg("Please enter promocode.");
            return;
        }

        if (getWebRequestHelper() == null) return;
        displayProgressBar(false);
        getWebRequestHelper().getPromoCodeUrl(amount, promoCode, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_PROMO_CODE:
                handlePromoCodeResponse(webRequest);
                break;

            case ID_GET_STATE:
                handleGetStateResponse(webRequest);
                break;

            case ID_USER_STATE:
                handleGetUserPayRequestResponse(webRequest);
                break;

            case ID_USER_STATUS_PAY:
                handleGetUserPayStatus(webRequest);
                break;
        }
    }

    private void handleGetUserPayRequestResponse(WebRequest webRequest) {

        UserPaymentstatusResponseModel responseModel = webRequest.getResponsePojo(UserPaymentstatusResponseModel.class);
        if (responseModel != null) {
            String message = responseModel.getMsg();
            int getStatusCode = responseModel.getCode();

            if (getStatusCode == 0){

                String amount = tv_add_balance.getText().toString().trim();
                if (amount.isEmpty()    ) {
                    showErrorMsg("Please enter amount.");
                    return;
                }

                long amt = Long.parseLong(amount);
                if (amt > MAX_AMOUNT) {
                    showErrorMsg("Amount cannot be greater than 10000");
                    return;
                }

                final PaytmPaymentDialog paytmPaymentDialog = new PaytmPaymentDialog();
                paytmPaymentDialog.setPaymentSuccessListener(new PaytmPaymentDialog.PaymentSuccessListener() {
                    @Override
                    public void onPaymentSuccess(PaymentResponseModel responseModel) {
                        paytmPaymentDialog.dismiss();
                        if (responseModel != null && getUserModel() != null) {
                            if (responseModel.getData() != null)
                                if (responseModel.getData().getWallet() != null) {
                                    getUserModel().setWalletModel(responseModel.getData().getWallet());
                                    updateUserInPrefs();
                                    addTransactionDetailDialog(responseModel.getData());
                                }
                        }
                    }
                });
                paytmPaymentDialog.setWalletRechargeAmount(amount);
                paytmPaymentDialog.setPromoCode(pcode);
                paytmPaymentDialog.show(getFm(), PaytmPaymentDialog.class.getSimpleName());

            }else {
                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
                Log.e("SelectUserPayStatus",""+message+"  getCode"+getStatusCode);
            }

        }

    }

    private void handleGetUserPayStatus(WebRequest webRequest) {

        UserPaymentstatusResponseModel responseModel = webRequest.getResponsePojo(UserPaymentstatusResponseModel.class);
        if (responseModel != null) {
            getPaymentStatus = responseModel.getData().getIsfirsttime();
            Log.e("SelectUserPayStatus",""+getPaymentStatus);
        }
    }


    private void handleGetStateResponse(WebRequest webRequest) {
        StateResponseModel responseModel = webRequest.getResponsePojo(StateResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<StateModel> data = responseModel.getData();
            getUserPrefs().updateStateList(data);
            selectStateDialog();
        }
    }

    private void selectStateDialog() {
        final List<StateModel> state = getUserPrefs().getStateList();
        if (state == null) {
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().getStatusUrl(this);
        } else {
            final SelectStateDialog selectTeamDialog = new SelectStateDialog();
            selectTeamDialog.setDataList(state);
            selectTeamDialog.setTitle("Select State");
            selectTeamDialog.setOnItemSelectedListeners(new SelectStateDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectTeamDialog.dismiss();
                    txtState.setText(String.valueOf(state.get(position).getName()));
                    getState = String.valueOf(state.get(position).getName());
                    Log.e("SelectState",""+getState);

                    if (getState != null ){

                        if (getState.equals("Andhra Pradesh")){
                            checkUserSelectState();
                        }else if(getState.equals("Assam")){
                            checkUserSelectState();
                        }else if(getState.equals("Odisha")){
                            checkUserSelectState();
                        }else if(getState.equals("Telangana")){
                            checkUserSelectState();
                        }else if(getState.equals("Nagaland")){
                            checkUserSelectState();
                        }else if(getState.equals("Sikkim")){
                            checkUserSelectState();
                        }
                    }

                }
            });
            selectTeamDialog.show(getFm(), selectTeamDialog.getClass().getSimpleName());
        }
    }

    private void handlePromoCodeResponse(WebRequest webRequest) {
        PromoCodeResponseModel responseModel = webRequest.getResponsePojo(PromoCodeResponseModel.class);
        if (responseModel != null) {
            String message = responseModel.getMsg();
            if (isValidString(message)) {
                updateViewVisibility(tv_error_promoCode, View.VISIBLE);
                tv_error_promoCode.setText(message);
            } else {
                updateViewVisibility(tv_error_promoCode, View.GONE);
            }

            if (responseModel.isError()){
                tv_error_promoCode.setActivated(false);
            }else {
                pcode = webRequest.getExtraData(DATA);
                tv_error_promoCode.setActivated(true);
            }
        }
    }

    private void updateValue(int value) {
        int totalValue = 0;
        String trim = tv_add_balance.getText().toString().trim();
        if (isValidString(trim)) {
            totalValue = Integer.parseInt(trim);
            tv_add_balance.setText(String.valueOf(totalValue + value));
        } else {
            tv_add_balance.setText(String.valueOf(totalValue + value));
        }
    }

    private void addBalance() {

        String amount = tv_add_balance.getText().toString().trim();
        if (amount.isEmpty()) {
            showErrorMsg("Please enter amount.");
            return;
        }

        long amt = Long.parseLong(amount);
        if (amt > MAX_AMOUNT) {
            showErrorMsg("Amount cannot be greater than 10000");
            return;
        }

        if (getPaymentStatus.equals("1")){
            popupCheckAge();

        }else {
            final PaytmPaymentDialog paytmPaymentDialog = new PaytmPaymentDialog();
            paytmPaymentDialog.setPaymentSuccessListener(new PaytmPaymentDialog.PaymentSuccessListener() {
                @Override
                public void onPaymentSuccess(PaymentResponseModel responseModel) {
                    paytmPaymentDialog.dismiss();
                    if (responseModel != null && getUserModel() != null) {
                        if (responseModel.getData() != null)
                            if (responseModel.getData().getWallet() != null) {

                      /*          Log.e("ADDCASH",""+responseModel.getData().getWallet());
                                Log.e("GETWALLET",""+responseModel.getData());*/

                                getUserModel().setWalletModel(responseModel.getData().getWallet());
                                updateUserInPrefs();
                                addTransactionDetailDialog(responseModel.getData());
                            }
                    }
                    else
                    {
//                        Log.e("GETWALLET",""+responseModel.getData());
                    }
                }
            });
            paytmPaymentDialog.setWalletRechargeAmount(amount);
            paytmPaymentDialog.setPromoCode(pcode);
            paytmPaymentDialog.show(getFm(), PaytmPaymentDialog.class.getSimpleName());
        }



       /* if (!isPaytm) {
            showErrorMsg("Please select payment type.");
            return;
        }*/
//        displayProgressBar(false, "Wait...");
//        getWebRequestHelper().getFantasyPtsSystem(this);

    }

    private void popupCheckAge() {

        LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.check_first_timepay_dialog, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(view);

        TextView popupCancel = view.findViewById(R.id.popupCancel);
        TextView popupOK = view.findViewById(R.id.popupOK);
        txtState = view.findViewById(R.id.txtState);
        editDateOfbrith = view.findViewById(R.id.editDateOfbrith);
        txtState.setText(getState);



            txtState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectStateDialog();
            }
        });

        editDateOfbrith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(editDateOfbrith);
            }
        });

        popupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 7/5/18 your click listener
                alertDialogCongratulations.dismiss();

            }
        });
        popupOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 7/5/18 your click listener
                if (getState != null ) {
                    if (getState.equals("Andhra Pradesh")) {
                        checkUserSelectState();
                    } else if (getState.equals("Assam")) {
                        checkUserSelectState();
                    } else if (getState.equals("Odisha")) {
                        checkUserSelectState();
                    } else if (getState.equals("Telangana")) {
                        checkUserSelectState();
                    } else if (getState.equals("Nagaland")) {
                        checkUserSelectState();
                    } else if (getState.equals("Sikkim")) {
                        checkUserSelectState();
                    } else {

                        if (getUserDateOfbirth != null) {
                              getWebRequestHelper().getUserStatesUrl(getState, getUserDateOfbirth, AddCashActivity.this);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please select date of birth", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(getApplicationContext(), "Please select state", Toast.LENGTH_SHORT).show();
                }


            }
        });

        alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.show();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);//

    }

    private void checkUserSelectState() {

        LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.check_state_alert_dialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(view);

        TextView popupOK = view.findViewById(R.id.info_popupOK);
        popupOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogCongratulations.dismiss();
            }
        });

        alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.show();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);//

    }

    private void showDatePickerDialog(final TextView editDateOfbrith) {

        DatePickerUtil.showDatePicker(this, editDateOfbrith, false, new DatePickerUtil.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Calendar calendar) {
                Date date = calendar.getTime();
               // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd");
                String sel_date = simpleDateFormat.format(date);
                editDateOfbrith.setTag(calendar);
                editDateOfbrith.setText(sel_date);
                Log.e("SelectData",""+sel_date);
                getUserDateOfbirth = sel_date;
            }
        });
    }

    private void addTransactionDetailDialog(PaymentModel paymentModel) {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, "Transaction Detail");
        bundle.putString(POS_BTN, "OK");
        bundle.putString(DATA, new Gson().toJson(paymentModel));
        TransactionDetailDialog dialog = TransactionDetailDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    dialog.dismiss();
                    supportFinishAfterTransition();
                }
            }
        });
        dialog.show(getFm(), TransactionDetailDialog.class.getSimpleName());
    }
}
