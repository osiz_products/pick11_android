package com.app.ui.main.navmenu.livescore;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.LiveDataModel;
import com.app.model.MatchModel;
import com.app.model.webresponsemodel.LiveDataResponseModel;
import com.app.model.webresponsemodel.ScoreBoardResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.navmenu.livescore.adapter.LiveScoreAdapter;
import com.app.ui.main.navmenu.livescore.details.LiveScoreDetailActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.rest.WebServices;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class NewLiveScoreActivity extends AppBaseActivity {

    TextView tv_live_score_card;
    SwipeRefreshLayout swipeRefresh;
    ProgressBar pb_data;
    RecyclerView recycler_view;
    LiveScoreAdapter adapter;
    TextView tv_no_record_found;
    List<LiveDataModel> list = new ArrayList<>();
    UserPrefs userPrefs;

    public MatchModel getMatchModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return new Gson().fromJson(extras.getString(DATA), MatchModel.class);
        return null;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
           getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
           getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_new_live_score;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        setupSwipeLayout();
        tv_live_score_card = findViewById(R.id.tv_live_score_card);
        pb_data = findViewById(R.id.pb_data);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        initializeRecyclerView();
        getUpcomingMatches();
//        getLiveScoreAll();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                getUpcomingMatches();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new LiveScoreAdapter(list, getMatchModel());
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                try {
                    LiveDataModel liveDataModel = adapter.getItem(position);
                    if (liveDataModel==null)return;
                    switch (v.getId()) {
                        case R.id.tv_full_score_card:
//                            Bundle bundle = new Bundle();
//                            bundle.putString(DATA, liveDataModel.getMatchid());
//                            goToLiveScoreDetailActivity(bundle);

                            Bundle bundle = new Bundle();
                            String url = String.format(WebServices.WebScoreUrl(), MyApplication.getInstance().getGemeType(), liveDataModel.getMatchid());
                            bundle.putString(DATA, url);
                            printLog("Url: ", url);
                            bundle.putString(DATA2, "Full Score Card");
                            goToWebViewActivity(bundle);
                            break;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void getUpcomingMatches() {
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getLiveData("", LIVE, this);
    }

    public void getLiveScoreAll() {
//        swipeRefresh.setRefreshing(true);
//        if (getMatchModel() != null)
//            getWebRequestHelper().getLiveScoreAll(getMatchModel().getMatchid(), this);
//        else
            getWebRequestHelper().getLiveScoreAll("live", this);
    }


    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        switch (webRequest.getWebRequestId()) {
            case ID_LIVE_DATA:
                handleLiveDataResponse(webRequest);
                break;

            case ID_LIVE_SCORE_BOARD:
                handleLiveScoreBoardResponse(webRequest);
                break;
        }
    }

    private void handleLiveScoreBoardResponse(WebRequest webRequest) {
        ScoreBoardResponseModel responseModel = webRequest.getResponsePojo(ScoreBoardResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                /*List<ScoreBoardModel> data = responseModel.getData();
                if (data != null) {
                    list.clear();
                    list.addAll(data);
                    adapter.notifyDataSetChanged();
                    updateNoDataView();*/
                    /*for (ScoreBoardModel scoreBoardModel : data) {
                        for (MatchModel matchModel : list) {
                            if (matchModel.getMatchid().equalsIgnoreCase(scoreBoardModel.getMatchid())) {
                                ScoreBoardModel.DetailsBean details = scoreBoardModel.getDetails();
                                if (details != null) {
                                    TeamScoreModel team1 = details.getTeam1();
                                    TeamScoreModel team2 = details.getTeam2();
                                    matchModel.setTeam1Score(team1);
                                    matchModel.setTeam2Score(team2);
                                }
                            }
                            if (adapter != null)
                                adapter.notifyDataSetChanged();
                        }
                    }*/
//                }
            } else {
                if (isFinishing()) return;
                updateNoDataView();
            }
        }
    }

    private void handleLiveDataResponse(WebRequest webRequest) {
        LiveDataResponseModel responsePojo = webRequest.getResponsePojo(LiveDataResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            list.clear();
            LiveDataResponseModel.DataBean data = responsePojo.getData();
            if (data!=null){
                List<LiveDataModel> lists = data.getList();
                if (lists != null && lists.size() > 0) {
                    list.addAll(lists);
                }
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
            updateNoDataView();

        } else {
            if (isFinishing()) return;
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (list.size() > 0) {
            updateViewVisibility(tv_no_record_found, View.GONE);
            updateViewVisibility(tv_live_score_card, View.VISIBLE);
        } else {
            tv_no_record_found.setText("No Live Score available");
            updateViewVisibility(tv_no_record_found, View.VISIBLE);
            updateViewVisibility(tv_live_score_card, View.GONE);
        }
    }

    private void goToLiveScoreDetailActivity(Bundle bundle) {
        Intent intent = new Intent(this, LiveScoreDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
