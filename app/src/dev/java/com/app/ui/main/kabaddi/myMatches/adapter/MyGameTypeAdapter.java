package com.app.ui.main.kabaddi.myMatches.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.GameTypeModel;
import com.pickeleven.R;

import java.util.List;

public class MyGameTypeAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<GameTypeModel> list;
    private int isPosition = -1;

    public MyGameTypeAdapter(Context context, List<GameTypeModel> list) {
        this.context = context;
        this.list = list;
    }

    public void setIsPosition(int isPosition) {
        this.isPosition = isPosition;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_game_type));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        LinearLayout ll_layout_game;
        ImageView iv_image;
        TextView tv_game_type;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout_game = itemView.findViewById(R.id.ll_layout_game);
            iv_image = itemView.findViewById(R.id.iv_image);
            tv_game_type = itemView.findViewById(R.id.tv_game_type);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            GameTypeModel gameTypeModel = list.get(position);
            if (!gameTypeModel.getIcon().isEmpty()) {
                ((AppBaseActivity) context).loadImage(context, iv_image, null,
                        gameTypeModel.getIcon(), R.drawable.dummy_logo, 100);
            } else {
                iv_image.setImageDrawable(getContext().getResources().getDrawable(R.drawable.dummy_logo));
            }
            tv_game_type.setText(gameTypeModel.capitalize(gameTypeModel.getGname()));
            if (position == isPosition) {
                ll_layout_game.setActivated(true);
            } else {
                ll_layout_game.setActivated(false);
            }

            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
