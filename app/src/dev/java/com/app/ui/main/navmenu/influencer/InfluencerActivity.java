package com.app.ui.main.navmenu.influencer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.InfluencerModel;
import com.app.model.TransactionInfModel;
import com.app.model.UserModel;
import com.app.model.WalletlistModel;
import com.app.model.webrequestmodel.InfluencerMatchlistModel;
import com.app.model.webrequestmodel.InfluencerWalletModel;
import com.app.model.webrequestmodel.TransactionRequestModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;

import java.util.ArrayList;
import java.util.List;

public class InfluencerActivity extends AppBaseActivity implements ToolBarFragment.ToolbarFragmentInterFace{

    ImageView backpage;
    ImageView wallet;
    TextView button_up_Matches;
    TextView button_History;
    ImageView iv_profile_pic;
    ProgressBar pb_image;
    RecyclerView recycler_viewInf;
    RecyclerView recycler_viewWallet;
    SwipeRefreshLayout swipeRefresh;
    MyUpcomingInfAdapter adapter;
    MyWalletInfAdapter adapters;
    LinearLayout ll_no_record_foundInf;
    TextView tv_no_record_foundInf,refer_Code;
    List<TransactionInfModel> list = new ArrayList<>();
    List<WalletlistModel> lists = new ArrayList<>();
    String referCode;
    UserPrefs userPrefs;
    private int totalPages = 1000;
    private int currentPage = 0;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_influencer;
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserPrefs().removeListener(this);
        getUserPrefs().addListener(this);
        setupUserDetail();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void initializeComponent() {
        super.initializeComponent();
        initializeRecyclerView();
        userPrefs = new UserPrefs(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            /*bagheaderImg.setVisibility(View.VISIBLE);
            bagheaderImg_black.setVisibility(View.GONE);*/
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            /*bagheaderImg_black.setVisibility(View.VISIBLE);
            bagheaderImg.setVisibility(View.GONE);*/
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initializeRecyclerView() {
        getReferCode();
        getInfWalletlist();
        setupSwipeLayout();
        callNextApi();

        updateViewVisibitity(ll_no_record_foundInf, View.GONE);
        recycler_viewInf = findViewById(R.id.recycler_viewInf);
        recycler_viewWallet = findViewById(R.id.recycler_viewWallet);
        button_up_Matches = findViewById(R.id.button_up_Matches);
        button_History = findViewById(R.id.button_History);
        refer_Code = findViewById(R.id.refer_Code);
        iv_profile_pic = InfluencerActivity.this.findViewById(R.id.iv_profile_pic);

        adapter = new MyUpcomingInfAdapter(this);
        adapter.updateData(list);
        recycler_viewInf.setAdapter(adapter);
        recycler_viewInf.setLayoutManager(getFullHeightLinearLayoutManager());

        adapters = new MyWalletInfAdapter(this);
        adapters.updateData(lists);
        recycler_viewWallet.setAdapter(adapters);
        recycler_viewWallet.setLayoutManager(getFullHeightLinearLayoutManager());

        recycler_viewInf.setVisibility(View.VISIBLE);
        recycler_viewWallet.setVisibility(View.GONE);

        backpage = findViewById(R.id.backpage);
        wallet = findViewById(R.id.wallet);
        ll_no_record_foundInf = findViewById(R.id.ll_no_record_foundInf);
        tv_no_record_foundInf = findViewById(R.id.tv_no_record_foundInf);

        button_up_Matches.setTextColor(getResources().getColor(R.color.colorPrimary, getResources().newTheme()));
        button_History.setTextColor(getResources().getColor(R.color.login_text_color, getResources().newTheme()));

        backpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Intent intent = new Intent(getApplication(), DashboardActivity.class);
               /* if (bundle != null) {
                    intent.putExtras(bundle);
                }*/
                //startActivity(intent);
                overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
                finish();
            }
        });

        button_up_Matches.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                //getInfDetails();
                list.clear();
                callNextApi();
                recycler_viewInf.setVisibility(View.VISIBLE);
                recycler_viewWallet.setVisibility(View.GONE);

                //button_up_Matches.setTextColor(R.color.colorPrimary);
                button_up_Matches.setTextColor(getResources().getColor(R.color.colorPrimary, getResources().newTheme()));
                button_History.setTextColor(getResources().getColor(R.color.login_text_color, getResources().newTheme()));
               // button_History.setTextColor(R.color.login_text_color);
            }
        });

        button_History.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                lists.clear();
                getInfWalletlist();

                recycler_viewInf.setVisibility(View.GONE);
                recycler_viewWallet.setVisibility(View.VISIBLE);

                button_up_Matches.setTextColor(getResources().getColor(R.color.login_text_color, getResources().newTheme()));
                button_History.setTextColor(getResources().getColor(R.color.colorPrimary, getResources().newTheme()));
            }
        });


        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // finish();
                Intent intent = new Intent(getApplication(), MyAccountActivity.class);
               /* if (bundle != null) {
                    intent.putExtras(bundle);
                }*/
                startActivity(intent);
                overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
            }
        });

        refer_Code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 /*ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("",referCode);
        clipboard.setPrimaryClip(clip);*/
              /*  Intent i=new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT,""+referCode);
                startActivity(Intent.createChooser(i,"Share via"));*/

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,referCode);
                sendIntent.setType("text/plain");
                Intent.createChooser(sendIntent,"Share via");
                startActivity(sendIntent);
             //   Toast.makeText(InfluencerActivity.this, "Referral code copied to clickboard!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getReferCode() {
        getWebRequestHelper().getMyInfUpcomingDetails(this);
    }
    private void getInfWalletlist(){
        getWebRequestHelper().getMyInfWalletDetails(this);
    }
    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getReferCode();
                getInfWalletlist();
            }
        });
    }

    public void onPageSelected() {
        updateViewVisibitity(ll_no_record_foundInf, View.GONE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppBaseActivity.REQUEST_CASHOUT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                String message = data.getStringExtra("message");
                if (message != null && !message.trim().isEmpty()) {
                    showCustomToast(message);
                }
            }
        }
    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {
        super.loggedInUserUpdate(userModel);
        setupUserDetail();
    }

    private void setupUserDetail() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                if (getApplication() != null) {
                    ((AppBaseActivity) InfluencerActivity.this).loadImage(this, iv_profile_pic, pb_image, imageUrl,
                            R.drawable.no_image, 300);
                }
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibitity(pb_image, View.INVISIBLE);
            }
        }
    }
    private void callNextApi() {
        if (isFinishing()) return;
        if (this.currentPage == 0) {
            this.currentPage = 1;
            this.totalPages = 1000;
            callApi();
            return;
        }

        if (this.totalPages > this.currentPage) {
            this.currentPage = this.currentPage + 1;
            callApi();
        }
    }

    private void callApi() {
        //setLoadingNextData(true);
        TransactionRequestModel requestModel = new TransactionRequestModel();
        requestModel.page = currentPage;
        requestModel.limit = 15;
        requestModel.gameid = MyApplication.getInstance().getGemeType();
        getWebRequestHelper().getInfUpcomingList(requestModel, this);
    }



    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        switch (webRequest.getWebRequestId()) {
            case ID_USER_INFLUENCER_ID:
                handleMatchListResponse(webRequest);
                break;
            case ID_UPCOMING_INF_CODE:
            handleInfluencerDetailResponse(webRequest);
            break;

            case USER_INFLUENCER_DETAILS:
                handleInfluencerWalletDetailResponse(webRequest);
            break;
        }
    }

    /*private void handleInfluencerWalletDetailResponse(WebRequest webRequest) {
    }*/

    private void handleInfluencerDetailResponse(WebRequest webRequest) {
        InfluencerModel responseModel = webRequest.getResponsePojo(InfluencerModel.class);
        if (responseModel != null && !responseModel.isError()) {
            String data = responseModel.getData();
            if (data == null) return;
            refer_Code.setText(data);

            Log.e("refer_Code",""+data);
            referCode = data;
        }
    }

    private void handleInfluencerWalletDetailResponse(WebRequest webRequest) {
        InfluencerWalletModel responsePojo = webRequest.getResponsePojo(InfluencerWalletModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<WalletlistModel> data = responsePojo.getData();
                lists.clear();
                if (data != null && data.size() > 0) {
                    lists.addAll(data);
                }
                if (isFinishing()) return;
                adapters.notifyDataSetChanged();
                updateNoDataView();
                MyApplication.getInstance().startTimer();
            }
        } else {
            if (isFinishing()) return;
            lists.clear();
            updateNoDataView();
        }
    }


    private void handleMatchListResponse(WebRequest webRequest) {
        InfluencerMatchlistModel responseModel = webRequest.getResponsePojo(InfluencerMatchlistModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            InfluencerMatchlistModel.DataBean modelData = responseModel.getData();
            List<TransactionInfModel> data = modelData.getMatchDetails();
            if (data != null && data.size() == 0) {
                totalPages = currentPage;
            }
            if (currentPage == 1) {
                updateData(data);
            } else {
                addDataToList(data);
            }
            updateNoDataView();
        } else {
            if (isFinishing()) return;
//            setLoadingNextData(false);
            totalPages = 0;
            updateNoDataView();
        }

    }

    public void updateData(List<TransactionInfModel> levelList) {
        this.list.clear();
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
//            adapter.updateData(list);
            adapter.notifyDataSetChanged();
        }
    }

    public void addDataToList(List<TransactionInfModel> levelList) {
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }



    private void updateNoDataView() {
        if (list.size() > 0) {
             updateViewVisibitity(ll_no_record_foundInf, View.GONE);
            ll_no_record_foundInf.setVisibility(View.GONE);

            Log.e("Call_111111111","call11111111");

        } else {

            Log.e("Call_222222222","call2222222222");
            tv_no_record_foundInf.setText("Not yet contests joined by Others");
            updateViewVisibitity(ll_no_record_foundInf, View.VISIBLE);
        }
    }


    @Override
    public void onToolbarItemClick(View view) {

    }
}
