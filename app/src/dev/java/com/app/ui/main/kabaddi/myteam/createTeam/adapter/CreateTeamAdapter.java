package com.app.ui.main.kabaddi.myteam.createTeam.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.pickeleven.R;
import com.utilities.DeviceScreenUtil;

public class CreateTeamAdapter extends AppBaseRecycleAdapter {

    private Context context;

    public CreateTeamAdapter(Context context) {
        this.context = context;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_players));
    }

    @Override
    public int getDataCount() {
        return 11;
    }


    private class ViewHolder extends BaseViewHolder {
        private RelativeLayout rl_layout;
        private ImageView iv_image;
        private TextView tv_text;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_layout = itemView.findViewById(R.id.rl_layout);
            iv_image = itemView.findViewById(R.id.iv_image);
            tv_text = itemView.findViewById(R.id.tv_text);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rl_layout.getLayoutParams();
            int width = DeviceScreenUtil.getInstance().getWidth();
            float item = (width - DeviceScreenUtil.getInstance().convertDpToPixel(18)) / 11.0f;
            params.width = Math.round(item);
            params.height = Math.round(params.width / 1.60f);
            params.setMarginEnd(-1);
            rl_layout.setLayoutParams(params);
        }

        @Override
        public String setData(int position) {

            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
