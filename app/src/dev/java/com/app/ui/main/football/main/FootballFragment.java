package com.app.ui.main.football.main;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.app.appbase.AppBaseFragment;
import com.app.appbase.ViewPagerAdapter;
import com.app.preferences.UserPrefs;
import com.app.ui.main.football.main.live.LiveFragment;
import com.app.ui.main.football.main.pastresult.PastResultFragment;
import com.app.ui.main.football.main.upcoming.UpcomingFragment;
import com.app.ui.main.slider.SliderFragment;
import com.pickeleven.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.utilities.DeviceScreenUtil;


public class FootballFragment extends AppBaseFragment {

    CoordinatorLayout coordinatorLayout;
    AppBarLayout app_bar_layout;
    TabLayout home_tabs;
    ViewPager viewpager_match;
    ViewPagerAdapter viewPagerAdapter;
    UserPrefs userPrefs;
    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.fragment_cricket;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        home_tabs = getView().findViewById(R.id.home_tabs);
        viewpager_match = getView().findViewById(R.id.viewpager_match);
        coordinatorLayout = getView().findViewById(R.id.coordinatorLayout);
        app_bar_layout = getView().findViewById(R.id.app_bar_layout);

        setupUi();
        addSlider();
        setupViewPager();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupUi() {
        coordinatorLayout.post(new Runnable() {
            @Override
            public void run() {
//                int width = DeviceScreenUtil.getInstance().getWidth();
//                int actualWidht = width - DeviceScreenUtil.getInstance().convertDpToPixel(16f);
//                int height = Math.round(actualWidht / 2.02f);
                int width = app_bar_layout.getWidth();
                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) app_bar_layout.getLayoutParams();
                layoutParams.height = Math.round(width / 2.22f) + DeviceScreenUtil.getInstance().convertDpToPixel(16.0f);
                app_bar_layout.setLayoutParams(layoutParams);
            }
        });

    }

    private void addSlider() {
        SliderFragment sliderFragment = new SliderFragment();
        sliderFragment.setMainContainer(app_bar_layout);
        FragmentTransaction transaction = getChildFm().beginTransaction();
        transaction.add(R.id.container, sliderFragment);
        transaction.commit();
    }

    public void setupViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(getChildFm());
        viewPagerAdapter.addFragment(new UpcomingFragment(), "Fixtures");
        viewPagerAdapter.addFragment(new LiveFragment(), "Live");
        viewPagerAdapter.addFragment(new PastResultFragment(), "Results");

        viewpager_match.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPagerAdapter.getItem(position).onPageSelected();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewpager_match.setAdapter(viewPagerAdapter);
        home_tabs.setupWithViewPager(viewpager_match);

    }
}
