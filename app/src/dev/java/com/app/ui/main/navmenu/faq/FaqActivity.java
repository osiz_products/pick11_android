package com.app.ui.main.navmenu.faq;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.preferences.UserPrefs;
import com.app.ui.main.navmenu.support.SupportActivity;
import com.pickeleven.R;
import com.rest.WebServices;

public class FaqActivity extends AppBaseActivity {
    private LinearLayout ll_faq;
    private LinearLayout ll_about_us;
    private LinearLayout ll_support;
    private LinearLayout ll_how_to_play;
    private LinearLayout ll_fantasy_point;
    private LinearLayout ll_term_condition;
    private LinearLayout ll_privacy_policy;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_faq;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        userPrefs = new UserPrefs(this);
        ll_faq = findViewById(R.id.ll_faq);
        ll_about_us = findViewById(R.id.ll_about_us);
        ll_support = findViewById(R.id.ll_support);
        ll_how_to_play = findViewById(R.id.ll_how_to_play);
        ll_fantasy_point = findViewById(R.id.ll_fantasy_point);
        ll_term_condition = findViewById(R.id.ll_term_condition);
        ll_privacy_policy = findViewById(R.id.ll_privacy_policy);

        ll_faq.setOnClickListener(this);
        ll_about_us.setOnClickListener(this);
        ll_support.setOnClickListener(this);
        ll_how_to_play.setOnClickListener(this);
        ll_fantasy_point.setOnClickListener(this);
        ll_term_condition.setOnClickListener(this);
        ll_privacy_policy.setOnClickListener(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.ll_faq:
                String URL = String.format(WebServices.FaqUrl(), "faqs");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "FAQs");
                goToWebViewActivity(bundle);
                break;

            case R.id.ll_about_us:
                URL = String.format(WebServices.FaqUrl(), "aboutus");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "About Us");
                goToWebViewActivity(bundle);
                break;

            case R.id.ll_support:
//                bundle.putString(DATA, "suprt");
//                bundle.putString(DATA2, "SUPPORT");
//                goToWebViewActivity(bundle);
                goToSupportActivity(bundle);

                Log.e("CallFor_bundle",""+bundle);

                break;

            case R.id.ll_how_to_play:
                URL = String.format(WebServices.FaqUrl(), "hwtoply");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "How To Play");
                goToWebViewActivity(bundle);
                break;

            case R.id.ll_fantasy_point:
                URL = String.format(WebServices.FaqUrl(), "Pick11PointSystem/mobile");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "Points");
                goToWebViewActivity(bundle);
                break;

            case R.id.ll_term_condition:
                URL = String.format(WebServices.FaqUrl(), "termscon");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "Terms & Conditions");
                goToWebViewActivity(bundle);
                break;

            case R.id.ll_privacy_policy:
                URL = String.format(WebServices.FaqUrl(), "prcypoly");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "Privacy Policy");
                goToWebViewActivity(bundle);
                break;
        }
    }

    private void goToSupportActivity(Bundle bundle) {
        Intent intent = new Intent(this, SupportActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
