package com.app.ui.main.cricket.myMatches.result.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.MatchModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;

import java.util.List;

public class MyResultAdapter extends AppBaseRecycleAdapter {

    private List<MatchModel> list;
    private Context context;
    private boolean loadMore;

    public MyResultAdapter(Context context) {
        this.context = context;
    }


    public void updateData (List<MatchModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public boolean setLoadMore (boolean loadMore) {
        if (list == null) return false;
        this.loadMore = loadMore;
        if (loadMore) {
            notifyItemInserted(list.size());
        } else {
            notifyItemRemoved(list.size());
        }
        return true;
    }
    @Override
    public int getViewType (int position) {
        if (list == null) {
            return VIEW_TYPE_DATA;
        } else if (loadMore && position == list.size()) {
            return VIEW_TYPE_LOAD_MORE;
        }
        return VIEW_TYPE_DATA;
    }

    @Override
    public BaseViewHolder getViewHolder (ViewGroup parent, int viewType) {
        if (VIEW_TYPE_DATA == viewType) {
            return new ViewHolder(inflateLayout(R.layout.item_fixtures));
        }
        return new LoadMoreViewHolder(inflateLayout(R.layout.adapter_item_load_more));
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return null;
    }

    @Override
    public int getDataCount () {
        return list == null ? 0 : (loadMore ? list.size() + 1 : list.size());
    }

    public MatchModel getItem(int position) {
        return list.get(position);
    }


    private class ViewHolder extends BaseViewHolder {

        private ImageView iv_image_first;
        private ProgressBar pb_image_first;
        private TextView tv_team_one;
        private ImageView iv_image_second;
        private ProgressBar pb_image_second;
        private TextView tv_team_two;
        private TextView tv_match_name;
        private TextView tv_match_type;
        private TextView tv_match_start_time;
        private TextView tv_series_name;
        private TextView tv_vs;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_image_first = itemView.findViewById(R.id.iv_image_first);
            pb_image_first = itemView.findViewById(R.id.pb_image_first);
            tv_team_one = itemView.findViewById(R.id.tv_team_one);
            iv_image_second = itemView.findViewById(R.id.iv_image_second);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            tv_team_two = itemView.findViewById(R.id.tv_team_two);
            tv_match_name = itemView.findViewById(R.id.tv_match_name);
            tv_match_type = itemView.findViewById(R.id.tv_match_type);
            tv_match_start_time = itemView.findViewById(R.id.tv_match_start_time);
            tv_series_name = itemView.findViewById(R.id.tv_series_name);
            tv_vs = itemView.findViewById(R.id.tv_vs);
            tv_match_start_time.setPadding(15, 5, 15, 5);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            MatchModel matchModel = list.get(position);
            if (matchModel == null) return null;

            if (!matchModel.getSeriesname().isEmpty()) {
                updateViewVisibitity(tv_series_name, View.VISIBLE);
                tv_series_name.setText(matchModel.getSeriesname());
            } else {
                updateViewVisibitity(tv_series_name, View.GONE);
            }

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_first, pb_image_first, matchModel.getTeam1logo(),
                    R.drawable.dummy_logo, 200);

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_second, pb_image_second, matchModel.getTeam2logo(),
                    R.drawable.dummy_logo, 200);

            tv_match_name.setText(matchModel.getMatchname());
            tv_team_one.setText(matchModel.getTeam1());
            tv_team_two.setText(matchModel.getTeam2());
            tv_match_type.setText(matchModel.capitalize(matchModel.getMtype()));
            tv_vs.setText(matchModel.capitalize(matchModel.getFormattedDate(1)));


              if(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate())!=null){

                  if (matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()).equals("COMPLETED")){

                      //tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
                      tv_match_start_time.setText("Completed");
                  }else {
                      tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
                  }
              }


          //  tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
            if (matchModel.isUnderReview() || matchModel.isCanceled()) {
              //  tv_match_start_time.setBackgroundResource(R.drawable.bg_null);
              //  tv_match_start_time.setTextColor(getContext().getResources().getColor(matchModel.getTimerColor()));
                tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_red));
            } else {
               // tv_match_start_time.setBackgroundResource(R.drawable.bg_green);
                tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
            }
            return null;
        }
    }
}
