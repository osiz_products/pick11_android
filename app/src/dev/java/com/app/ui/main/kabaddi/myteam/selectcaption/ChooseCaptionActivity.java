package com.app.ui.main.kabaddi.myteam.selectcaption;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.TeamModel;
import com.app.model.webrequestmodel.CreateTeamRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.myteam.selectcaption.adapter.ChooseCaptionAdapter;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class ChooseCaptionActivity extends AppBaseActivity implements MatchTimerListener {

    TextView tv_team_one;
    LinearLayout ll_timer;
    ImageView iv_clock;
    TextView tv_timer_time;
    RecyclerView recycler_view;
    ChooseCaptionAdapter adapter;
    TextView tv_save;
    UserPrefs userPrefs;

    public boolean isEdit() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return getIntent().getExtras().getBoolean(IS_EDIT, false);
        return false;
    }

    private MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }


    public List<PlayerModel> getSelectedPlayer() {
        String string = getIntent().getExtras().getString(DATA);
        if (isValidString(string))
            return new Gson().fromJson(string, new TypeToken<List<PlayerModel>>() {
            }.getType());
        return null;
    }

    public TeamModel getTeamModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA1);
            if (isValidString(string))
                return new Gson().fromJson(string, TeamModel.class);
            else
                return null;
        }
        return null;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_choose_caption;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_team_one = findViewById(R.id.tv_team_one);
        ll_timer = findViewById(R.id.ll_timer);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);

        tv_save = findViewById(R.id.tv_save);
        tv_save.setOnClickListener(this);
        initializeRecyclerView();
        setupUi();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save:
                saveTeam();
                break;
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new ChooseCaptionAdapter(getSelectedPlayer());
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (v.getId()) {
                    case R.id.tv_captain:
                        adapter.updateCaption(position);
                        setupUi();
                        break;

                    case R.id.tv_vice_captain:
                        adapter.updateVcCaption(position);
                        setupUi();
                        break;
                }

            }
        });

        for (PlayerModel playerModel : getSelectedPlayer()) {
            if (playerModel.getIscap() == 1) {
                adapter.setCaption(playerModel.getPid());
            } else if (playerModel.getIsvcap() == 1) {
                adapter.setVcCaption(playerModel.getPid());
            }
        }

    }

    private void setupUi() {
        if (adapter != null) {
            if (adapter.getCaption() == null || adapter.getVcCaption() == null) {
                tv_save.setActivated(false);
            } else {
                tv_save.setActivated(true);
            }
        } else {
            tv_save.setActivated(false);
        }
    }

    private void saveTeam() {
        if (getMatchModel() != null && getUserModel() != null) {
            if (adapter.getCaption() == null) {
                showCustomToast("please select captaion");
                return;
            }

            if (adapter.getVcCaption() == null) {
                showCustomToast("please select vise captaion");
                return;
            }

            CreateTeamRequestModel requestModel = new CreateTeamRequestModel();
            requestModel.gameid = MyApplication.getInstance().getGemeType();
            requestModel.matchid = getMatchModel().getMatchid();
            List<String> list = new ArrayList<>();
            for (PlayerModel playerModel : getSelectedPlayer()) {
                String id = String.valueOf(playerModel.getPid());
                if (id.equals(adapter.getCaption()))
                    requestModel.iscap = id;
                if (id.equals(adapter.getVcCaption()))
                    requestModel.isvcap = id;

                if (isEdit())
                    requestModel.userteamid = String.valueOf(getTeamModel().getId());

                list.add(id);
            }
            requestModel.userteamplayers = list;
            displayProgressBar(false, "Wait...");
            if (isEdit())
                getWebRequestHelper().updateTeam(requestModel, this);
            else
                getWebRequestHelper().createTeam(requestModel, this);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_CREATE_TEAM:
                handleCreateResponse(webRequest);
                break;

            case ID_UPDATE_PLAYERS:
                handleCreateResponse(webRequest);
                break;
        }
    }

    private void handleCreateResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            Bundle bundle = new Bundle();
            Intent intent = new Intent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void handleUpdatePlayersResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            Bundle bundle = new Bundle();
            Intent intent = new Intent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }


    @Override
    public void onMatchTimeUpdate() {
        String remainTimeText = getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate());
        setHeaderTitle(remainTimeText);
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
            tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
            iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
        }
    }
}
