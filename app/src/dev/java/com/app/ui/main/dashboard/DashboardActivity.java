package com.app.ui.main.dashboard;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseFragment;
import com.app.fcm.AppNotificationModel;
import com.app.model.AppSettingsModel;
import com.app.model.CommonSetting;
import com.app.model.CountryModel;
import com.app.model.GameTypeModel;
import com.app.model.StateModel;
import com.app.model.UserModel;
import com.app.model.webresponsemodel.AppSettingsResponseModel;
import com.app.model.webresponsemodel.CountryResponseModel;
import com.app.model.webresponsemodel.GameTypeResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.ReferResponseModel;
import com.app.model.webresponsemodel.StateResponseModel;
import com.app.model.webresponsemodel.WalletResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.main.CricketFragment;
import com.app.ui.main.dashboard.adapter.GameTypeAdapter;
import com.app.ui.main.dashboard.myMatches.MyMatchesActivity;
import com.app.ui.main.football.main.FootballFragment;
import com.app.ui.main.kabaddi.main.KabaddiFragment;
import com.app.ui.main.navmenu.NavMenuFragment;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.livescore.NewLiveScoreActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.ui.main.notification.NotificationActivity;
import com.app.ui.main.slider.SliderFragment;
import com.app.ui.splash.MenuActivity;
import com.appupdater.AppUpdateChecker;
import com.appupdater.AppUpdatemodal;
import com.appupdater.DialogAppUpdater;
import com.base.BaseFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.pickeleven.R;
import com.fcm.NotificationModal;
import com.fcm.NotificationPrefs;
import com.fcm.PushNotificationHelper;
import com.medy.retrofitwrapper.WebRequest;
import com.permissions.PermissionHelperNew;
import com.rest.WebRequestHelper;
import com.rest.WebServices;
import com.utilities.ItemClickSupport;

import java.io.File;
import java.util.List;

public class DashboardActivity extends AppBaseActivity implements ToolBarFragment.ToolbarFragmentInterFace,
        PushNotificationHelper.PushNotificationHelperListener {

    NavMenuFragment navMenuFragment;
    RecyclerView recycler_view;
    GameTypeAdapter gameTypeAdapter;
    private PushNotificationHelper pushNotificationHelper;
    AppUpdatemodal appUpdatemodal;
    DialogAppUpdater dialogAppUpdater;
    Dialog alertDialogProgressBar;
    boolean fromPermission = false;
    LinearLayout bottom_more,bottom_matches,bottom_home,bottom_live;
    AppBarLayout app_bar_layout;
    UserPrefs userPrefs;
    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.BaseAppThemeDark);
        } else {
            setTheme(R.style.BaseAppTheme);
        }
        return R.layout.activity_dashboard;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return R.id.container;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);

        alertDialogProgressBar = new Dialog(this, R.style.DialogWait);
        alertDialogProgressBar.setCancelable(false);
        alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.dialog_wait);

        pushNotificationHelper = ((MyApplication) getApplication()).getPushNotificationHelper();
        Fragment fragmentById = getFm().findFragmentById(R.id.fragment_navigation_drawer);
        if (fragmentById != null && fragmentById instanceof AppBaseFragment) {
            navMenuFragment = (NavMenuFragment) fragmentById;
            navMenuFragment.initializeComponent();
        }
       /* Intent intent = new Intent(getApplication(), MenuActivity.class);
        startActivity(intent);
        if (getApplication() == null) return;*/

        if(userPrefs!=null){

            if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }

        }else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        initializeRecyclerView();
        getCountry();
        addSlider();
    }

    private void addSlider() {

        app_bar_layout = findViewById(R.id.app_bar_layout);
        SliderFragment sliderFragment = new SliderFragment();
        sliderFragment.setMainContainer(app_bar_layout);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.containers, sliderFragment);
        transaction.commit();
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        gameTypeAdapter = new GameTypeAdapter(this, MyApplication.getInstance().getGameTypeModels());
        recycler_view.setLayoutManager(getHorizentalLinearLayoutManager());
        recycler_view.setAdapter(gameTypeAdapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                GameTypeModel gameTypeModel = MyApplication.getInstance().getGameTypeModels().get(position);
                if (gameTypeModel != null) {
                    gameTypeAdapter.setIsPosition(position);
                    MyApplication.getInstance().setGemeType(gameTypeModel);
                    if (gameTypeModel.isCricket()) {
                        addCricketFragment();
                    } else if (gameTypeModel.isKabaddi()) {
                        addKabaddiFragment();
                    } else {
                        addFootballFragment();
                    }
                }
            }
        });
        if (gameTypeAdapter != null)
            gameTypeAdapter.setIsPosition(Integer.valueOf(MyApplication.getInstance().getGemeType()));

        bottom_more = findViewById(R.id.bottom_more);
        bottom_live = findViewById(R.id.bottom_live);
        bottom_matches = findViewById(R.id.bottom_matches);
        bottom_home = findViewById(R.id.bottom_home);

        bottom_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (navMenuFragment != null) {
                    navMenuFragment.handleDrawer();
                }*/

                Intent intent = new Intent(getApplication(), MenuActivity.class);
                startActivity(intent);
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
                if (getApplication() == null) return;
            }
        });

        bottom_matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  closeDrawer();
                Intent intent = new Intent(getApplication(), MyMatchesActivity.class);
                startActivity(intent);
                if (getApplication() == null) return;
                //getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
            }
        });

        bottom_matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  closeDrawer();
                Intent intent = new Intent(getApplication(), MyMatchesActivity.class);
                startActivity(intent);
                if (getApplication() == null) return;
                //getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
            }
        });


        /*bottom_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  closeDrawer();
                Intent intent = new Intent(getApplication(), DashboardActivity.class);
                startActivity(intent);
                if (getApplication() == null) return;
                //getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
            }
        });*/


        bottom_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), NewLiveScoreActivity.class);
                startActivity(intent);
                if (getApplication() == null) return;
            }
        });
    }

    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.iv_menu:
                if (navMenuFragment != null) {
                    navMenuFragment.handleDrawer();
                }
                break;

            case R.id.iv_wallet:
                goToMyAccountActivity(null);
                break;

            case R.id.rl_notification:
                /*Bundle bundle = new Bundle();
                bundle.putBoolean(NOT_BACK_BTN, false);
                goToNotificationActivity(bundle);*/
                goToMyAccountActivity(null);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (navMenuFragment != null && navMenuFragment.closeDrawer()) {
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    /*public boolean closeDrawer() {
        if (getActivity() == null) return false;
        if (drawerLayout.isDrawerOpen(getActivity().findViewById(R.id.fragment_navigation_drawer))) {
            drawerLayout.closeDrawers();
            return true;
        }
        return false;
    }*/

    private void getCountry() {
        getWebRequestHelper().getCountry(this);
        getWebRequestHelper().getGameType(this);
        getWebRequestHelper().getStatusUrl(this);
        getWebRequestHelper().getWalletDetailUrl(this);
        getWebRequestHelper().getReferCodeUrl(this);
        getWebRequestHelper().getPlayerType(MyApplication.getInstance().getGemeType(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_COUNTRY:
                handleCountryResponse(webRequest);
                break;

            case ID_GAME_TYPE:
                handleGameTypeResponse(webRequest);
                break;

            case ID_GET_STATE:
                handleGetStateResponse(webRequest);
                break;

            case ID_GET_USER_BALANCE:
                handleUserBalanceResponse(webRequest);
                break;

            case ID_PLAYER_TYPE:
                handlePlayerTypeResponse(webRequest);
                break;

            case ID_REFER_CODE:
                handleReferCodeResponse(webRequest);
                break;
            case ID_APP_SETTINGS:
                handleAppSettingResponse(webRequest);
                break;
        }
    }

    private void handleAppSettingResponse(WebRequest webRequest) {
        AppSettingsResponseModel responseModel = webRequest.getResponsePojo(AppSettingsResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            AppSettingsModel data = responseModel.getData();
            if (data != null) {
                int notify_count = data.getUnread_count();
                setNotificationCount(notify_count);
                MyApplication.getInstance().printLog("Message Count: ", String.valueOf(notify_count));
                AppSettingsModel.ConfigModel config = data.getConfig();
                if (config == null) return;
                CommonSetting common = config.getCommon();
                if (common == null) return;

                UserModel userModel = getUserModel();
                userModel.setSetting(common);
                updateUserInPrefs();
            }
        }
    }

    private void handleReferCodeResponse(WebRequest webRequest) {
        ReferResponseModel responseModel = webRequest.getResponsePojo(ReferResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            UserModel data = responseModel.getData();
            if (data == null || getUserModel() == null) return;
            UserModel userModel = getUserModel();
            userModel.setRefbns(data.getRefbns());
            userModel.setRefbnsfrnd(data.getRefbnsfrnd());
            userModel.setMessage(data.getMessage());
            getUserPrefs().updateLoggedInUser(userModel);
        }
    }

    private void handleCountryResponse(WebRequest webRequest) {
        CountryResponseModel responseModel = webRequest.getResponsePojo(CountryResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<CountryModel> data = responseModel.getData();
            getUserPrefs().updateCountryList(data);
            Log.e("GetCountryList",""+data);

        }
    }

    private void handleGetStateResponse(WebRequest webRequest) {
        StateResponseModel responseModel = webRequest.getResponsePojo(StateResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<StateModel> data = responseModel.getData();
            getUserPrefs().updateStateList(data);
            Log.e("GetStateList",""+data);
        }
    }

    private void handleGameTypeResponse(WebRequest webRequest) {
        GameTypeResponseModel responseModel = webRequest.getResponsePojo(GameTypeResponseModel.class);
        if (responseModel != null) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<GameTypeModel> data = responseModel.getData();
                MyApplication.getInstance().getGameTypeModels().clear();
                if (data != null && data.size() > 0) {
                    if (getUserModel() != null) {
                        MyApplication.getInstance().getGameTypeModels().addAll(data);
                        getUserModel().setGameType(data);
                        if (gameTypeAdapter != null) {
                            gameTypeAdapter.setIsPosition((Integer.valueOf(MyApplication.getInstance().getGemeType())) - 1);
                            gameTypeAdapter.notifyDataSetChanged();
                        }
                        MyApplication.getInstance().setGemeType(data.get(0));
                        addCricketFragment();
                    }
                }
            }
        }
    }

    private void addCricketFragment() {
        CricketFragment fragment = new CricketFragment();
        changeFragment(fragment, false, true, 0, true);
    }

    private void addFootballFragment() {
        FootballFragment fragment = new FootballFragment();
        changeFragment(fragment, false, true, 0, true);
    }

    private void addKabaddiFragment() {
        KabaddiFragment fragment = new KabaddiFragment();
        changeFragment(fragment, false, true, 0, true);
    }

    private void handleUserBalanceResponse(WebRequest webRequest) {
        WalletResponseModel responseModel = webRequest.getResponsePojo(WalletResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            getUserModel().setWalletModel(responseModel.getData());
        }
    }

    private void handlePlayerTypeResponse(WebRequest webRequest) {
        PlayerTypeResponseModel responsePojo = webRequest.getResponsePojo(PlayerTypeResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            PlayerTypeResponseModel.DataBean data = responsePojo.getData();
            MyApplication.getInstance().setPlayerTypeModels(data);
        } else {
            if (isFinishing()) return;
//            showErrorMsg(responsePojo.getMsg());
        }
    }

    private void goToMyAccountActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToNotificationActivity(Bundle bundle) {
        Intent intent = new Intent(this, NotificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (pushNotificationHelper != null) {
            pushNotificationHelper.removeNotificationHelperListener(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pushNotificationHelper != null) {
            pushNotificationHelper.addNotificationHelperListener(this);
        }
        getNotificationCount();
        if (gameTypeAdapter != null) {
            gameTypeAdapter.setIsPosition((Integer.valueOf(MyApplication.getInstance().getGemeType())) - 1);
        }
        if (fromPermission) {
            fromPermission = false;
            return;
        }
        getVersionCode();
        checkForAppUpdate();
    }

    private void getVersionCode() {

       /* try {
            PackageInfo pInfo = getApplication().getPackageManager().getPackageInfo(getApplication().getPackageName(), 0);
            String version = pInfo.versionName;
            Log.e("GET_applicationVersion",""+version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.getInstance().stopTimer();
        if (pushNotificationHelper != null) {
            pushNotificationHelper.removeNotificationHelperListener(this);
        }
    }

    @Override
    public void onPushNotificationReceived(NotificationModal notificationModal) {
        AppNotificationModel appNotificationModel = (AppNotificationModel) notificationModal;
        if (appNotificationModel != null) {
            getNotificationCount();
        }
    }

    private void getNotificationCount() {
        WebRequestHelper webRequestHelper = getWebRequestHelper();
        if (webRequestHelper == null) return;
        webRequestHelper.getAppSettings(NotificationPrefs.getInstance(this).getNotificationToken(), "", this);
    }


    private void checkForAppUpdate() {

        Log.e("CallRespons","checkForAppUpdate");
        AppUpdateChecker appUpdateChecker = new AppUpdateChecker(this, new AppUpdateChecker.onAppUpdateAvaliable() {
            @Override
            public void appUpdateAvaliable(AppUpdatemodal appUpdatemodal) {
                printLog("", appUpdatemodal.toString());

                Log.e("Check_App_version",""+appUpdatemodal.toString());

                if (!isFinishing()) {
                    if (dialogAppUpdater != null && dialogAppUpdater.isShowing()) return;
                    if (alertDialogProgressBar != null && alertDialogProgressBar.isShowing())
                        return;
                    showUpdateDialog(appUpdatemodal);
                }
            }
        });
        appUpdateChecker.checkForUpdate(WebServices.PlayStoreVersionUrl());
    }

    private void showUpdateDialog(AppUpdatemodal appUpdatemodals) {
        try {
            this.appUpdatemodal = appUpdatemodals;
            if (appUpdatemodal == null) return;
            dialogAppUpdater = new DialogAppUpdater(DashboardActivity.this, appUpdatemodal);
            dialogAppUpdater.setDialogAppUpdaterListener(new DialogAppUpdater.DialogAppUpdaterListener() {
                @Override
                public void onUpdateClick() {
                    if (PermissionHelperNew.needStoragePermission(DashboardActivity.this, new PermissionHelperNew.OnSpecificPermissionGranted() {
                        @Override
                        public void onPermissionGranted(boolean isGranted, boolean withNeverAsk, String permission, int requestCode) {
                            if (isGranted) {
                                downloadAPK(appUpdatemodal.getAppurl());
                            }
                        }
                    })) {
                        return;
                    }
                    downloadAPK(appUpdatemodal.getAppurl());
                }
            });
            dialogAppUpdater.show();
        } catch (Exception e) {

        }
    }


    private void downloadAPK(String url) {

        Log.e("CallDownLoad_File","GET_New_APK");

        if (dialogAppUpdater != null && dialogAppUpdater.isShowing()) {
            dialogAppUpdater.dismiss();
        }
        String fileName = getResources().getString(R.string.app_name) + ".apk";
        final Uri mDestinationUri = Uri.withAppendedPath(
                Uri.fromFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)),
                fileName);


        //Delete update file if exists
        File file = new File(mDestinationUri.getPath());
        if (file.exists())
            file.delete();
        printLog("downloadAPk", " file:" + file.getAbsolutePath());
        //set downloadmanager
        final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        //request.setDescription(getResources().getString(R.string.notification_description));
        request.setTitle(getResources().getString(R.string.app_name));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(mDestinationUri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);
        printLog("downloadAPk", " downloadId:" + downloadId);

        displayUpdateProgressBar("Updating...");

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean downloading = true;
                while (downloading) {


                    Log.e("callDownload","File");

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);

                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    final double dl_progress = (((float) bytes_downloaded / bytes_total)) * 100;
                    String message = ((int) dl_progress) + "% Downloading...";
                    if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_SUCCESSFUL) ||
                            (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                                    == DownloadManager.STATUS_FAILED)) {
                        downloading = false;
                    } else if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_PENDING)) {
                        message = "Updating...";
                    } else if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_PAUSED)) {
                        message = ((int) dl_progress) + "% Downloading Pause";
                    }
                    printLog("downloadAPk", message);
                    cursor.close();

                    final String finalMessage = message;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            displayUpdateProgressBar(finalMessage);
                        }
                    });

                    if (!downloading) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                openAppInstaller(downloadId);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private void openAppInstaller(long downloadId) {

        printLog("downloadAPk", " openAppInstaller downloadId:" + downloadId);
        dismissUpdateProgressBar();
        if (dialogAppUpdater != null && dialogAppUpdater.isShowing()) {
            dialogAppUpdater.dismiss();
        }
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uriForDownloadedFile = manager.getUriForDownloadedFile(downloadId);
        String mimeType = manager.getMimeTypeForDownloadedFile(downloadId);
        printLog("downloadAPk", " uriForDownloadedFile:" + uriForDownloadedFile);
        printLog("downloadAPk", " mimeType:" + mimeType);
        if (uriForDownloadedFile == null) return;

        Intent downloadIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            downloadIntent = new Intent(Intent.ACTION_VIEW);
            downloadIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            downloadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            downloadIntent.setDataAndType(uriForDownloadedFile, mimeType);

            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(downloadIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, uriForDownloadedFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

        } else {
            downloadIntent = new Intent(Intent.ACTION_VIEW);
            downloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            downloadIntent.setDataAndType(uriForDownloadedFile, mimeType);
        }

        startActivity(downloadIntent);
        getUserPrefs().clearLoggedInUser();
        finish();
    }

    public void dismissUpdateProgressBar() {
        if (alertDialogProgressBar != null && alertDialogProgressBar.isShowing()) {
            alertDialogProgressBar.dismiss();
        }
    }

    public void displayUpdateProgressBar(String loaderMsg) {
        if (alertDialogProgressBar != null) {
            TextView tv_loader_msg = alertDialogProgressBar.findViewById(R.id.tv_loader_msg);
            if (loaderMsg != null && !loaderMsg.trim().isEmpty()) {
                tv_loader_msg.setText(loaderMsg);
            } else {
                tv_loader_msg.setVisibility(View.GONE);
            }

            try {
                alertDialogProgressBar.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                if (!alertDialogProgressBar.isShowing())
                    alertDialogProgressBar.show();

            } catch (Exception ignore) {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        fromPermission = true;
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelperNew.onSpecificRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }
}
