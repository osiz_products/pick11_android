package com.app.ui.main.navmenu.scratch;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anupkumarpanwar.scratchview.ScratchView;
import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.ScratchModel;
import com.bumptech.glide.Glide;
import com.pickeleven.R;

import java.util.List;

public class MyScratchAdapter extends AppBaseRecycleAdapter {

    private List<ScratchModel> list;
    public MyScratchAdapter(List<ScratchModel> list) {
        this.list = list;
        Log.e("GetList_Data",""+list);
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_scratch_card));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public ScratchModel getItem(int position) {
        return list == null ? null : list.get(position);
    }

    private class ViewHolder extends BaseViewHolder{

        private ImageView img_scratch;
        private ScratchView card_view;
        private TextView txtWinamount;
        private TextView txtgetmsg;
        private TextView txtgetcoupon;
        private TextView txtdatemsg;
        private ProgressBar pb_image_second;
        private LinearLayout selectCard;

        public ViewHolder(View inflateLayout) {
            super(inflateLayout);
            img_scratch = itemView.findViewById(R.id.img_scratch);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            card_view = itemView.findViewById(R.id.card_view);
            txtWinamount = itemView.findViewById(R.id.txtgetAmount);
            txtdatemsg = itemView.findViewById(R.id.txtdatemsg);
            txtgetmsg = itemView.findViewById(R.id.txtgetmsg);
            txtgetcoupon = itemView.findViewById(R.id.txtgetcoupon);
            selectCard = itemView.findViewById(R.id.selectCard);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            ScratchModel scratchModel = list.get(position);
            if (scratchModel == null) return null;

            if (scratchModel.getType().equals("coupon")){

                if (scratchModel.getIs_active().equals("0")) {

                    ((AppBaseActivity) getContext()).loadImage(getContext(),
                            img_scratch, pb_image_second, scratchModel.getCouponcardimage(),
                            R.mipmap.scratch, 250);

                    txtgetmsg.setText("You have won a coupon");
                    txtgetcoupon.setText(scratchModel.getCouponcode());
                    txtdatemsg.setText("Valid till : ");
                    txtWinamount.setText(scratchModel.getValidtill());

                }else {
                                    // Not read
                    Glide.with(getContext())
                            .load(R.mipmap.scratch)
                            .centerCrop()
                            .placeholder(R.mipmap.scratch)
                            .into(img_scratch);

                    txtWinamount.setVisibility(View.GONE);
                    txtgetmsg.setVisibility(View.GONE);
                    txtgetcoupon.setVisibility(View.GONE);
                    txtdatemsg.setVisibility(View.GONE);
                }

            }else {

                txtdatemsg.setVisibility(View.GONE);
                if (scratchModel.getIs_active().equals("1")){
                    Glide.with(getContext())
                            .load(R.mipmap.scratch)
                            .centerCrop()
                            .placeholder(R.mipmap.scratch)
                            .into(img_scratch);

                    if(scratchModel.getScratchcardamount().equals("0")){

                        txtgetmsg.setText("Better luck next time");
                        txtgetcoupon.setVisibility(View.GONE);
                        txtWinamount.setVisibility(View.GONE);
                        txtdatemsg.setVisibility(View.GONE);

                        Glide.with(getContext())
                                .load(R.drawable.ic_podium)
                                .centerCrop()
                                .placeholder(R.mipmap.scratch)
                                .into(img_scratch);

                    }else {
                        txtgetmsg.setText("You have won");
                        txtgetcoupon.setVisibility(View.GONE);
                        //txtWinamount.setText("You've won ₹"+scratchModel.getScratchcardamount());
                        txtWinamount.setText("₹"+scratchModel.getScratchcardamount());
                    }

                    txtWinamount.setVisibility(View.GONE);
                    selectCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                }else {

                    Glide.with(getContext())
                            .load(R.drawable.ic_box)
                            .centerCrop()
                            .placeholder(R.mipmap.scratch)
                            .into(img_scratch);

                    int newHeight =100;
                    int newwidth = 100;

                    img_scratch.getLayoutParams().height = newHeight;
                    img_scratch.getLayoutParams().width = newwidth;

                    //  txtWinamount.setText("You've won ₹"+scratchModel.getScratchcardamount());

                    if(scratchModel.getScratchcardamount().equals("0")){
                       // txtWinamount.setText("Better luck next time");
                        txtgetmsg.setText("Better luck next time");
                        txtgetcoupon.setVisibility(View.GONE);
                        txtWinamount.setVisibility(View.GONE);
                        txtdatemsg.setVisibility(View.GONE);

                        Glide.with(getContext())
                                .load(R.drawable.ic_podium)
                                .centerCrop()
                                .placeholder(R.mipmap.scratch)
                                .into(img_scratch);

                    }else {
                       // txtWinamount.setText("You've won ₹"+scratchModel.getScratchcardamount());

                        txtgetmsg.setText("You have won");//Congrats!
                        txtgetcoupon.setVisibility(View.GONE);
                        //txtWinamount.setText("You've won ₹"+scratchModel.getScratchcardamount());
                        txtWinamount.setText("₹"+scratchModel.getScratchcardamount());
                    }
                 //   txtWinamount.setVisibility(View.VISIBLE);
                }

            }

            return null;
        }
    }
}
