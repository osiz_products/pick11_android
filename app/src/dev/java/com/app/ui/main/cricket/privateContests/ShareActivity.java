package com.app.ui.main.cricket.privateContests;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestModel;
import com.app.model.MatchModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.pickeleven.R;
import com.google.gson.Gson;

public class ShareActivity extends AppBaseActivity implements MatchTimerListener {

    private TextView tv_series_name;
    private ImageView iv_image_first;
    private ProgressBar pb_image_first;
    private TextView tv_team_one;
    private ImageView iv_image_second;
    private ProgressBar pb_image_second;
    private TextView tv_team_two;
    private TextView tv_match_name;
    private TextView tv_match_type;
    private TextView tv_match_start_time;

    TextView tv_invite_code;
    TextView tv_copy_code;
    TextView tv_share;
    UserPrefs userPrefs;

    private ContestModel getContestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString(DATA);
            if (isValidString(string)) {
                return new Gson().fromJson(string, ContestModel.class);
            }
            return null;
        }
        return null;
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_share;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        userPrefs = new UserPrefs(this);
        tv_series_name = findViewById(R.id.tv_series_name);
        iv_image_first = findViewById(R.id.iv_image_first);
        pb_image_first = findViewById(R.id.pb_image_first);
        tv_team_one = findViewById(R.id.tv_team_one);
        iv_image_second = findViewById(R.id.iv_image_second);
        pb_image_second = findViewById(R.id.pb_image_second);
        tv_team_two = findViewById(R.id.tv_team_two);
        tv_match_name = findViewById(R.id.tv_match_name);
        tv_match_type = findViewById(R.id.tv_match_type);
        tv_match_start_time = findViewById(R.id.tv_match_start_time);

        tv_invite_code = findViewById(R.id.tv_invite_code);
        tv_copy_code = findViewById(R.id.tv_copy_code);
        tv_share = findViewById(R.id.tv_share);
        tv_share.setOnClickListener(this);
        tv_copy_code.setOnClickListener(this);

        setmatchModel();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void setmatchModel() {
        MatchModel matchModel = getMatchModel();
        if (matchModel == null) return;

        if (!matchModel.getSeriesname().isEmpty()) {
            updateViewVisibility(tv_series_name, View.VISIBLE);
            tv_series_name.setText(matchModel.getSeriesname());
        } else {
            updateViewVisibility(tv_series_name, View.GONE);
        }

        loadImage(this,
                iv_image_first, pb_image_first, matchModel.getTeam1logo(),
                R.drawable.dummy_logo, 200);

        loadImage(this,
                iv_image_second, pb_image_second, matchModel.getTeam2logo(),
                R.drawable.dummy_logo, 200);

        tv_match_name.setText(matchModel.getMatchname());
        tv_team_one.setText(matchModel.getTeam1());
        tv_team_two.setText(matchModel.getTeam2());
        tv_match_type.setText(matchModel.capitalize(matchModel.getMtype()));
        tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
        tv_match_start_time.setTextColor(getResources().getColor(matchModel.getTimerColor()));


        if (getContestModel() == null) return;
        tv_invite_code.setText(getContestModel().getCcode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_share:
                shareWithFriends(tv_invite_code.getText().toString().trim());
                break;

            case R.id.tv_copy_code:
                if (getContestModel() != null) {
                    boolean b = copyToClipboard(getContestModel().getCcode());
                    if (b)
                        showCustomToast("Code Copied.");
                }
                break;
        }
    }

    public void shareWithFriends(String referCode) {
        try {
            MatchModel matchModel = getMatchModel();
            if (matchModel == null) return;
            if (getUserModel() == null) return;
            String appName = getResources().getString(R.string.app_name);
            String referMessage = "You've been challenged!\nThink you can beat me? Join my Invite-Only Contest with %s on %s for %s match and prove it!";
            referMessage = String.format(referMessage, tv_invite_code.getText().toString().trim(), appName
                    , matchModel.getTeam1().toUpperCase() + " vs " + matchModel.getTeam2().toUpperCase());

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, referMessage);
            startActivity(Intent.createChooser(i, "Share Via :"));
        } catch (Exception ignore) {

        }
    }

    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            if (getMatchModel().isUnderReview()) {
                tv_match_start_time.setText("Under Review");
                tv_match_start_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
            } else {
                tv_match_start_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
                tv_match_start_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }
}
