package com.app.ui.main.kabaddi.myMatches.live;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseFragment;
import com.app.model.MatchModel;
import com.app.model.webresponsemodel.MatchResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.joinedContests.JoinedContestsActivity;
import com.app.ui.main.kabaddi.myMatches.live.adapter.MyLiveAdapter;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class MyLiveFragment extends AppBaseFragment {

    SwipeRefreshLayout swipeRefresh;
    ProgressBar pb_data;
    RecyclerView recycler_view;
    MyLiveAdapter adapter;
    LinearLayout ll_no_record_found;
    TextView tv_no_record_found;
    CardView cv_view_upcoming_match;
    List<MatchModel> list = new ArrayList<>();
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.fragment_my_live;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        setupSwipeLayout();
        pb_data = getView().findViewById(R.id.pb_data);
        ll_no_record_found = getView().findViewById(R.id.ll_no_record_found);
        tv_no_record_found = getView().findViewById(R.id.tv_no_record_found);
        cv_view_upcoming_match = getView().findViewById(R.id.cv_view_upcoming_match);
        cv_view_upcoming_match.setOnClickListener(this);
        updateViewVisibitity(ll_no_record_found, View.GONE);
        initializeRecyclerView();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = getView().findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLiveMatches();
            }
        });
    }

    @Override
    public void onPageSelected() {
        getLiveMatches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_view_upcoming_match:
                getActivity().onBackPressed();
                break;
        }
    }

    private void initializeRecyclerView() {
        recycler_view = getView().findViewById(R.id.recycler_view);
        adapter = new MyLiveAdapter(list);
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                MatchModel matchModel = adapter.getItem(position);
                switch (v.getId()) {
                    default:
                        MyApplication.getInstance().setSelectedMatch(matchModel);
                        goToJoinedContestsActivity(null);
                        break;
                }

            }
        });
    }

    private void goToJoinedContestsActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), JoinedContestsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    public void getLiveMatches() {
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getMyMatches(MyApplication.getInstance().getGemeType(), LIVE, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        switch (webRequest.getWebRequestId()) {
            case ID_MY_MATCHES:
                handleMatchListResponse(webRequest);
                break;
        }
    }

    private void handleMatchListResponse(WebRequest webRequest) {
        MatchResponseModel responsePojo = webRequest.getResponsePojo(MatchResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<MatchModel> data = responsePojo.getData();
                list.clear();
                if (data != null && data.size() > 0) {
                    list.addAll(data);
                }
                if (isFinishing()) return;
                adapter.notifyDataSetChanged();
                updateNoDataView();
                MyApplication.getInstance().startTimer();
            }
        } else {
            if (isFinishing()) return;
            list.clear();
            adapter.notifyDataSetChanged();
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (list.size() > 0) {
            updateViewVisibitity(ll_no_record_found, View.GONE);
        } else {
            tv_no_record_found.setText("You haven't joined any contests that are live");
            updateViewVisibitity(ll_no_record_found, View.VISIBLE);
        }
    }
}
