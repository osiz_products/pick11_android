package com.app.ui.main.cricket.privateContests;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.InviteCodeRequestModel;
import com.app.model.MatchModel;
import com.app.model.TeamModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.model.webresponsemodel.TeamResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.joinconfirmdialog.ConfirmationJoinPrivateContestDialog;
import com.app.ui.main.cricket.contests.ContestsActivity;
import com.app.ui.main.cricket.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.cricket.myteam.createTeam.CreateTeamActivity;
import com.app.ui.main.navmenu.myaccount.LowBalanceActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;

import java.util.ArrayList;
import java.util.List;

import static com.app.ui.main.cricket.contests.ContestsActivity.REQUEST_CREATE_TEAM;

public class InviteCodeActivity extends AppBaseActivity {

    EditText et_invite_code;
    TextView tv_join_contest;
    List<TeamModel> myTeams = new ArrayList<>();
    private boolean isTeamExits = false;
    UserPrefs userPrefs;

    private MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.activity_invite_code;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        et_invite_code = findViewById(R.id.et_invite_code);
        tv_join_contest = findViewById(R.id.tv_join_contest);
        tv_join_contest.setOnClickListener(this);
        getAllTeam();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_join_contest:
                onSubmit();
                break;
        }
    }

    private void getAllTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false);
        getWebRequestHelper().getAllTeam(matchModel, this);
    }

    private void onSubmit() {
        String invite_code = et_invite_code.getText().toString().trim();
        if (TextUtils.isEmpty(invite_code)) {
            showErrorMsg("Please enter contest code.");
            return;
        }

        InviteCodeRequestModel requestModel = new InviteCodeRequestModel();
        requestModel.ccode = invite_code;
        requestModel.matchid = getMatchModel().getMatchid();

        displayProgressBar(false);
        getWebRequestHelper().checkInviteCode(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_CHECK_INVITE_CODE:
                handleCheckInviteCodeResponse(webRequest);
                break;

            case ID_ALL_TEAM:
                handleAllTeamsResponse(webRequest);
                break;

        }
    }

    private void handleCheckInviteCodeResponse(WebRequest webRequest) {
        InviteCodeRequestModel extraData = webRequest.getExtraData(DATA);
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            if (extraData != null) {
                JoinContestRequestModel requestModel = new JoinContestRequestModel();
                requestModel.atype = PRE_JOIN;
                requestModel.ccode = extraData.ccode;
                requestModel.matchid = extraData.matchid;
                requestModel.fees = "1";
                requestModel.poolcontestid = "1";
                /*Bundle bundle = new Bundle();
                if (isTeamExits) {
                    bundle.putBoolean(DATA, true);
                    bundle.putString(DATA4, new Gson().toJson(requestModel));
                    goToSwitchTeamActivity(bundle);
                } else {
                    bundle.putBoolean(DATA, true);
                    bundle.putString(DATA4, new Gson().toJson(requestModel));
                    goToCreateTeamActivity(null);
                }*/
                openConfirmJoinPrivateContest(requestModel);
            }
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void handleAllTeamsResponse(WebRequest webRequest) {
        TeamResponseModel responseModel = webRequest.getResponsePojo(TeamResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            isTeamExits = true;
            TeamResponseModel.DataBean data = responseModel.getData();
            if (data == null) return;
            List<TeamModel> teams = data.getTeams();
            myTeams.clear();
            if (teams != null && teams.size() > 0) {
                myTeams.addAll(teams);
            }
        } else {
            isTeamExits = false;
            String message = responseModel.getMsg();
//            if (isValidString(message))
//                showErrorMsg(message);
        }
    }

    private void openConfirmJoinPrivateContest(final JoinContestRequestModel model) {
        Bundle bundle = new Bundle();
        bundle.putString(DATA, new Gson().toJson(model));
        final ConfirmationJoinPrivateContestDialog confirmationJoinPrivateContestDialog = ConfirmationJoinPrivateContestDialog.getInstance(bundle);
        confirmationJoinPrivateContestDialog.setConfirmationJoinContestListener(new ConfirmationJoinPrivateContestDialog.ConfirmationJoinContestListener() {
            @Override
            public void onProceed(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                if (preJoinedModel != null) {
                    model.fees = preJoinedModel.getFeesText();
                    model.poolcontestid = preJoinedModel.getPoolcontestid();
                    JoinPrivateContest(model);
                }
                confirmationJoinPrivateContestDialog.dismiss();
            }

            @Override
            public void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                confirmationJoinPrivateContestDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(preJoinedModel));
                goToLowBalanceActivity(bundle);
            }
        });
        confirmationJoinPrivateContestDialog.show(getFm(), confirmationJoinPrivateContestDialog.getClass().getSimpleName());
    }

    private void JoinPrivateContest(JoinContestRequestModel requestModel) {
        Bundle bundle = new Bundle();
//        if (isTeamExits) {
            bundle.putBoolean(DATA, true);
            bundle.putBoolean(DATA5, true);
            bundle.putString(DATA4, new Gson().toJson(requestModel));
            bundle.putString(DATA1, requestModel.poolcontestid);
            goToSwitchTeamActivity(bundle);
//        } else {
//            requestModel.isJoin = true;
//            bundle.putString(DATA, new Gson().toJson(requestModel));
//            goToCreateTeamActivity(bundle);
//        }
    }

    public void goToCreateTeamActivity(Bundle bundle) {
        String gemeType = MyApplication.getInstance().getGemeType();
        Intent intent = null;
        if (gemeType.equalsIgnoreCase("1")) {
            intent = new Intent(this, CreateTeamActivity.class);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            startActivityForResult(intent, REQUEST_CREATE_TEAM);
        }
    }

    private void goToLowBalanceActivity(Bundle bundle) {
        Intent intent = new Intent(this, LowBalanceActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (isFinishing()) return;
        startActivityForResult(intent, ContestsActivity.REQUEST_LOW_BALANCE);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_JOIN_PRIVATE_CONTEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_JOIN_PRIVATE_CONTEST) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra(DATA, data.getStringExtra(DATA));
                setResult(RESULT_OK, intent);
                supportFinishAfterTransition();
            }
        }
    }
}
