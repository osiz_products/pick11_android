package com.app.ui.main.cricket.contests.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.ContestCategoryModel;
import com.app.model.ContestModel;
import com.app.ui.main.cricket.contests.SelectContestsListActivity;
import com.app.ui.main.dashboard.DashboardActivity;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.List;

public class ContestsAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<ContestCategoryModel> list;

    public ContestsAdapter(Context context, List<ContestCategoryModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_contests));
    }

    public ContestModel getJoinedContestModel(ContestModel contestModel) {
        return null;
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tv_contests_type;
        private TextView tv_contests_des;
        private RecyclerView recycler_view;
        private ContestsChildAdapter adapter;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_contests_type = itemView.findViewById(R.id.tv_contests_type);
//            tv_contests_des = itemView.findViewById(R.id.tv_contests_des);
            recycler_view = itemView.findViewById(R.id.recycler_view);

        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            ContestCategoryModel contestCategoryModel = list.get(position);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recycler_view.setLayoutManager(layoutManager);
            adapter = new ContestsChildAdapter(getContext()) {
                @Override
                public ContestModel getJoinedContestModel(ContestModel contestModel) {
                    return ContestsAdapter.this.getJoinedContestModel(contestModel);
                }
            };
            recycler_view.setAdapter(adapter);
            recycler_view.setTag(position);
            adapter.updateData(contestCategoryModel.getContestPools());

            Log.e("list_list",""+contestCategoryModel.getContestPools().size());

            //if (contestCategoryModel.getContestPools().size() )

            ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    performChildItemClick((Integer) recycler_view.getTag(), position, v);
                }
            });

            final String getTitle = String.valueOf(contestCategoryModel.getMatchcontestid());


            if (contestCategoryModel.getContestPools().size() <= 4 ){
                tv_contests_type.setVisibility(View.GONE);
            }else {
                tv_contests_type.setVisibility(View.VISIBLE);
            }

            tv_contests_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Call_Call","adapter");
                    Intent intent = new Intent(context, SelectContestsListActivity.class);
                    intent.putExtra("selectID",getTitle);
                    Log.e("SendMatchID",""+getTitle);
                    context.startActivity(intent);
                }
            });

           // tv_contests_type.setText(contestCategoryModel.getTitle());
//            tv_contests_des.setText(contestCategoryModel.getSubtitle());

            return null;
        }
        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
