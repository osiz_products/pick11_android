package com.app.ui.main.navmenu.livescore.details;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.fullScoreBoard.BeanModel;
import com.app.model.fullScoreBoard.CustomScoreModel;
import com.app.model.fullScoreBoard.FullScoreBoardModel;
import com.app.model.fullScoreBoard.InningsDetails;
import com.app.model.fullScoreBoard.InningsModel;
import com.app.model.fullScoreBoard.MessageModel;
import com.app.model.fullScoreBoard.ScoreMatchModel;
import com.app.model.fullScoreBoard.SeasonModel;
import com.app.model.fullScoreBoard.TeamModel;
import com.app.model.fullScoreBoard.TeamsModel;
import com.app.model.fullScoreBoard.TossModel;
import com.app.model.webresponsemodel.FullScoreBoardResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.main.navmenu.livescore.details.fragment.ScoreFragment;
import com.app.uitilites.wrapingViewPager.WrappingViewPager;
import com.app.uitilites.wrapingViewPager.WrappingViewPagerAdapter;
import com.pickeleven.R;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.medy.retrofitwrapper.WebRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class LiveScoreDetailActivity extends AppBaseActivity {

    private SwipeRefreshLayout swipeRefresh;
    private ImageView iv_image_first;
    private ProgressBar pb_image_first;
    private TextView tv_team_one;
    private TextView tv_team_one_score;
    private ImageView iv_image_second;
    private ProgressBar pb_image_second;
    private TextView tv_team_two;
    private TextView tv_team_two_score;
    private TextView tv_match_type;
    private TextView tv_match_start_time;
    private TextView tv_match_result;
    private TextView tv_no_record_found;

    TabLayout tabs;
    WrappingViewPager view_pager;
    WrappingViewPagerAdapter adapter;

    private TextView tv_match;
    private TextView tv_date;
    private TextView tv_toss;
    private TextView tv_vanue;
    private TextView tv_team_one_name;
    private TextView tv_playing_11_one;
    private TextView tv_bench_one;
    private TextView tv_team_two_name;
    private TextView tv_playing_11_two;
    private TextView tv_bench_two;
    UserPrefs userPrefs;

    public String getLiveDataModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return extras.getString(DATA);
        }
        return null;
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
           getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
           getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_live_score_detail;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_match_type = findViewById(R.id.tv_match_type);
        iv_image_first = findViewById(R.id.iv_image_first);
        pb_image_first = findViewById(R.id.pb_image_first);
        tv_team_one = findViewById(R.id.tv_team_one);
        tv_team_one_score = findViewById(R.id.tv_team_one_score);
        iv_image_second = findViewById(R.id.iv_image_second);
        pb_image_second = findViewById(R.id.pb_image_second);
        tv_team_two = findViewById(R.id.tv_team_two);
        tv_team_two_score = findViewById(R.id.tv_team_two_score);

        tv_match_start_time = findViewById(R.id.tv_match_start_time);
        tv_match_result = findViewById(R.id.tv_match_result);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);

        updateViewVisibility(tv_no_record_found, View.GONE);

        tabs = findViewById(R.id.tabs);
        view_pager = findViewById(R.id.view_pager);
        adapter = new WrappingViewPagerAdapter(getFm());


        tv_match = findViewById(R.id.tv_match);
        tv_date = findViewById(R.id.tv_date);
        tv_toss = findViewById(R.id.tv_toss);
        tv_vanue = findViewById(R.id.tv_vanue);
        tv_team_one_name = findViewById(R.id.tv_team_one_name);
        tv_playing_11_one = findViewById(R.id.tv_playing_11_one);
        tv_bench_one = findViewById(R.id.tv_bench_one);
        tv_team_two_name = findViewById(R.id.tv_team_two_name);
        tv_playing_11_two = findViewById(R.id.tv_playing_11_two);
        tv_bench_two = findViewById(R.id.tv_bench_two);

//        setupSwipeLayout();
        getLiveScore();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLiveScore();
            }
        });
    }

    private void setData(FullScoreBoardModel fullScoreBoardModel) {
        InningsDetails inningsdetail = fullScoreBoardModel.getInningsdetail();
        if (inningsdetail != null) {
            tv_match_type.setText(fullScoreBoardModel.capitalize(fullScoreBoardModel.getType()));
            TeamModel team1 = inningsdetail.getTeam_a();
            TeamModel team2 = inningsdetail.getTeam_b();
            if (team1 != null) {
                tv_team_one_name.setText(team1.getFull_name());
                tv_team_one.setText(team1.getFull_name());
                loadImage(this,
                        iv_image_first, pb_image_first, team1.getLogo(), R.drawable.dummy_logo);
            }
            if (team2 != null) {
                tv_team_two_name.setText(team2.getFull_name());
                tv_team_two.setText(team2.getFull_name());
                loadImage(this,
                        iv_image_second, pb_image_second, team2.getLogo(), R.drawable.dummy_logo);
            }
            InningsDetails.ABean team11 = inningsdetail.getA();
            InningsDetails.BBean team21 = inningsdetail.getB();
            if (team11 != null) {
                String scoreModel = team11.get_$1();
                String string = "<font color=" + getResources().getColor(R.color.colorGray)
                        + ">" + scoreModel + "</font>";
                tv_team_one_score.setText(Html.fromHtml(string), TextView.BufferType.SPANNABLE);
            }
            if (team21 != null) {
                String scoreModel = team21.get_$1();
                /*String string = scoreModel.getInningRun() + " / " + scoreModel.getInningWicket()
                        + "<font color=" + getResources().getColor(R.color.colorGray)
                        + "> (" + scoreModel.getInningOver() + ")</font>";*/
                String string = "<font color=" + getResources().getColor(R.color.colorGray)
                        + ">" + scoreModel + "</font>";
                tv_team_two_score.setText(Html.fromHtml(string), TextView.BufferType.SPANNABLE);

            }
            tv_match_start_time.setText(fullScoreBoardModel.capitalize(fullScoreBoardModel.getStatus()));

            ScoreMatchModel match = fullScoreBoardModel.getMatch();
            if (match != null) {
                MessageModel msgs = match.getMsgs();
                if (msgs != null && msgs.getCompleted() != null) {
                    tv_match_result.setText(msgs.getCompleted());
                }
            }
        }
    }

    public void getLiveScore() {
        if (getLiveDataModel() != null) {
            if (swipeRefresh != null)
                swipeRefresh.setRefreshing(true);
            displayProgressBar(false);
            getWebRequestHelper().getFullScoreBoardUrl(getLiveDataModel(), "fullscore", this);
        } else {
            updateNoDataView(true);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        dismissProgressBar();
        if (swipeRefresh != null && swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        switch (webRequest.getWebRequestId()) {
            case ID_SCORE_BOARD:
                handleLiveScoreResponse(webRequest);
                break;
        }
    }

    private void handleLiveScoreResponse(WebRequest webRequest) {
        FullScoreBoardResponseModel responsePojo = webRequest.getResponsePojo(FullScoreBoardResponseModel.class);
        if (responsePojo == null) return;
        try {
            if (!responsePojo.isError()) {
                FullScoreBoardModel data = responsePojo.getData();
                if (data != null) {

                    setData(data);
                    ScoreMatchModel match = data.getMatch();
                    if (match != null) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("man_of_match", match.getMan_of_match());
                        jsonObject.put("status", match.getStatus());
                        jsonObject.put("status_overview", match.getStatus_overview());
                        SeasonModel season = match.getSeason();
                        String title;
                        if (season != null) {
                            title = (match.getShort_name() + ", " + match.getRelated_name() + ", " + season.getName());
                        } else {
                            title = (match.getShort_name() + ", " + match.getRelated_name());
                        }

                        tv_match.setText(title);

                        jsonObject.put("title", title);
                        ScoreMatchModel.StartDateBean start_date = match.getStart_date();
                        if (start_date != null) {
                            tv_date.setText(start_date.getStr());
                            jsonObject.put("date", start_date.getStr());
                        }
                        TossModel toss = match.getToss();
                        if (toss != null) {
                            tv_toss.setText(toss.getStr());
                            jsonObject.put("toss", toss.getStr());
                        }
                        tv_vanue.setText(match.getVenue());
                        jsonObject.put("venue", match.getVenue());

                        LinkedTreeMap<String, Object> players = match.getPlayers();
                        ScoreMatchModel.TeamBean teams = match.getTeams();
                        if (teams == null) return;

                        TeamsModel teamsA = teams.getA();
                        if (teamsA != null) {
                            InningsDetails inningsdetail = data.getInningsdetail();
                            if (inningsdetail != null) {
                                //                        pick A --------------
                                TeamModel team1 = inningsdetail.getTeam_a();
                                if (team1 != null) {
                                    jsonObject.put("logo", team1.getLogo());
                                    jsonObject.put("short_name", team1.getShort_name());
                                    jsonObject.put("full_name", team1.getFull_name());
                                }

                                InningsDetails.ABean a1 = inningsdetail.getA();
                                jsonObject.put("runWicketOver", a1.get_$1());
                            }
                            InningsModel innings1 = match.getInnings();
                            JSONObject extras = new JSONObject();
                            if (innings1 != null) {
                                BeanModel a_1 = innings1.getA_1();
                                if (a_1 != null) {
                                    extras.put("key", a_1.getKey());
                                    extras.put("runs", a_1.getRuns());
                                    extras.put("run_str", a_1.getRun_str());
                                    extras.put("run_rate", a_1.getRun_rate());
                                    extras.put("balls", a_1.getBalls());
                                    extras.put("dotballs", a_1.getDotballs());
                                    extras.put("fours", a_1.getFours());
                                    extras.put("sixes", a_1.getSixes());
                                    extras.put("wickets", a_1.getWickets());
                                    extras.put("extras", a_1.getExtras());
                                    extras.put("wide", a_1.getWide());
                                    extras.put("noball", a_1.getNoball());
                                    extras.put("legbye", a_1.getLegbye());
                                    extras.put("bye", a_1.getBye());
                                    extras.put("penalty", a_1.getPenalty());
                                    extras.put("overs", a_1.getOvers());
                                }
                            }

                            jsonObject.put("extras", extras);
                            TeamsModel.MatchBean match1 = teamsA.getMatch();
                            JSONArray jsonArray = new JSONArray();

                            for (String s : match1.getPlaying_xi()) {
                                JSONObject playerObjects = new JSONObject();
                                JSONObject bowlingObjects = new JSONObject();
                                LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap<String, Object>) players.get(s);
                                playerObjects.put("key", s);
                                for (Map.Entry<String, Object> objectEntry : treeMap.entrySet()) {
                                    if (objectEntry.getKey().equalsIgnoreCase("seasonal_role")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("fullname")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("name")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("match")) {
                                        LinkedTreeMap<String, Object> matches = (LinkedTreeMap<String, Object>) objectEntry.getValue();
                                        LinkedTreeMap<String, Object> innings = (LinkedTreeMap<String, Object>) matches.get("innings");
                                        LinkedTreeMap<String, Object> t1 = (LinkedTreeMap<String, Object>) innings.get("1");
                                        LinkedTreeMap<String, Object> batting = (LinkedTreeMap<String, Object>) t1.get("batting");
                                        if (batting != null) {
                                            for (Map.Entry<String, Object> stringObjectEntry : batting.entrySet()) {
                                                if (stringObjectEntry.getKey().equalsIgnoreCase("dismissed")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("dots")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("sixes")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("runs")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("balls")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("fours")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("strike_rate")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("out_str")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                }
                                            }

                                        }
                                        LinkedTreeMap<String, Object> bowling = (LinkedTreeMap<String, Object>) t1.get("bowling");
                                        if (bowling != null) {
                                            for (Map.Entry<String, Object> stringObjectEntry : bowling.entrySet()) {
                                                bowlingObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                            }
                                            playerObjects.put("bowling", bowlingObjects);
                                        } else {
                                            playerObjects.put("bowling", bowlingObjects);
                                        }
                                    }

                                }
                                jsonArray.put(playerObjects);
                            }
                            jsonObject.put("player", jsonArray);
                            String string = jsonObject.toString();
                            CustomScoreModel scoreModel = new Gson().fromJson(string, CustomScoreModel.class);
                            printLog("batting Name ", new Gson().toJson(scoreModel) + "\n");
                            if (scoreModel != null) {
                                ScoreFragment scoreFragment = new ScoreFragment();
                                scoreFragment.setCustomScoreModel(scoreModel);
                                if (adapter != null) {
                                    adapter.addFragment(scoreFragment, scoreModel.getFull_name());
                                    adapter.notifyDataSetChanged();
                                }
                                StringBuilder builder = new StringBuilder();
                                for (CustomScoreModel.PlayerBean playerBean : scoreModel.getPlayer()) {
                                    if (builder.length() > 0) {
                                        builder.append(", ").append(playerBean.getFullname());
                                    } else {
                                        builder.append(playerBean.getFullname());
                                    }
                                }
                                tv_playing_11_one.setText(builder.toString());
                            }
                        }

                        //                ----------------


                        TeamsModel teamsB = teams.getB();
                        if (teamsB != null) {
                            InningsDetails inningsdetail = data.getInningsdetail();
                            if (inningsdetail != null) {
                                //                        pick A --------------
                                TeamModel team2 = inningsdetail.getTeam_b();
                                if (team2 != null) {
                                    jsonObject.put("logo", team2.getLogo());
                                    jsonObject.put("short_name", team2.getShort_name());
                                    jsonObject.put("full_name", team2.getFull_name());
                                }

                                InningsDetails.BBean b = inningsdetail.getB();
                                jsonObject.put("runWicketOver", b.get_$1());
                            }

                            InningsModel innings1 = match.getInnings();
                            JSONObject extras = new JSONObject();
                            if (innings1 != null) {
                                BeanModel a_1 = innings1.getA_1();
                                if (a_1 != null) {
                                    extras.put("key", a_1.getKey());
                                    extras.put("runs", a_1.getRuns());
                                    extras.put("run_str", a_1.getRun_str());
                                    extras.put("run_rate", a_1.getRun_rate());
                                    extras.put("balls", a_1.getBalls());
                                    extras.put("dotballs", a_1.getDotballs());
                                    extras.put("fours", a_1.getFours());
                                    extras.put("sixes", a_1.getSixes());
                                    extras.put("wickets", a_1.getWickets());
                                    extras.put("extras", a_1.getExtras());
                                    extras.put("wide", a_1.getWide());
                                    extras.put("noball", a_1.getNoball());
                                    extras.put("legbye", a_1.getLegbye());
                                    extras.put("bye", a_1.getBye());
                                    extras.put("penalty", a_1.getPenalty());
                                    extras.put("overs", a_1.getOvers());
                                }
                            }

                            jsonObject.put("extras", extras);
                            TeamsModel.MatchBean match1 = teamsB.getMatch();
                            JSONArray jsonArray = new JSONArray();

                            for (String s : match1.getPlaying_xi()) {
                                JSONObject playerObjects = new JSONObject();
                                JSONObject bowlingObjects = new JSONObject();
                                LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap<String, Object>) players.get(s);
                                playerObjects.put("key", s);
                                for (Map.Entry<String, Object> objectEntry : treeMap.entrySet()) {
                                    if (objectEntry.getKey().equalsIgnoreCase("seasonal_role")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("fullname")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("name")) {
                                        playerObjects.put(objectEntry.getKey(), objectEntry.getValue());
                                    } else if (objectEntry.getKey().equalsIgnoreCase("match")) {
                                        LinkedTreeMap<String, Object> matches = (LinkedTreeMap<String, Object>) objectEntry.getValue();
                                        LinkedTreeMap<String, Object> innings = (LinkedTreeMap<String, Object>) matches.get("innings");
                                        LinkedTreeMap<String, Object> t1 = (LinkedTreeMap<String, Object>) innings.get("1");
                                        LinkedTreeMap<String, Object> batting = (LinkedTreeMap<String, Object>) t1.get("batting");
                                        if (batting != null) {
                                            for (Map.Entry<String, Object> stringObjectEntry : batting.entrySet()) {
                                                if (stringObjectEntry.getKey().equalsIgnoreCase("dismissed")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("dots")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("sixes")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("runs")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("balls")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("fours")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("strike_rate")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                } else if (stringObjectEntry.getKey().equalsIgnoreCase("out_str")) {
                                                    playerObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                                }
                                            }

                                        }
                                        LinkedTreeMap<String, Object> bowling = (LinkedTreeMap<String, Object>) t1.get("bowling");
                                        if (bowling != null) {
                                            for (Map.Entry<String, Object> stringObjectEntry : bowling.entrySet()) {
                                                bowlingObjects.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                                            }
                                            playerObjects.put("bowling", bowlingObjects);
                                        } else {
                                            playerObjects.put("bowling", bowlingObjects);
                                        }
                                    }

                                }
                                jsonArray.put(playerObjects);
                            }
                            jsonObject.put("player", jsonArray);
                            String string = jsonObject.toString();
                            CustomScoreModel scoreModel = new Gson().fromJson(string, CustomScoreModel.class);
                            printLog("batting Name ", new Gson().toJson(scoreModel) + "\n");
                            if (scoreModel != null) {
                                ScoreFragment scoreFragment = new ScoreFragment();
                                scoreFragment.setCustomScoreModel(scoreModel);
                                if (adapter != null) {
                                    adapter.addFragment(scoreFragment, scoreModel.getFull_name());
                                    adapter.notifyDataSetChanged();
                                }

                                StringBuilder builder = new StringBuilder();
                                for (CustomScoreModel.PlayerBean playerBean : scoreModel.getPlayer()) {
                                    if (builder.length() > 0) {
                                        builder.append(", ").append(playerBean.getFullname());
                                    } else {
                                        builder.append(playerBean.getFullname());
                                    }
                                }
                                tv_playing_11_two.setText(builder.toString());
                            }
                        }

                        view_pager.setAdapter(adapter);
                        tabs.setupWithViewPager(view_pager);

                    }
                    updateNoDataView(false);
                } else {
                    if (isFinishing()) return;
                    updateNoDataView(true);
                }
            } else {
                if (isFinishing()) return;
                updateNoDataView(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateNoDataView(boolean isVisibile) {
        if (!isVisibile) {
            updateViewVisibility(tv_no_record_found, View.GONE);
        } else {
            tv_no_record_found.setText("No Live Score available");
            updateViewVisibility(tv_no_record_found, View.VISIBLE);
        }
    }
}
