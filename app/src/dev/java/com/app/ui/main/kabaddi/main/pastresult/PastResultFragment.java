package com.app.ui.main.kabaddi.main.pastresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseFragment;
import com.app.model.MatchModel;
import com.app.model.webrequestmodel.TransactionRequestModel;
import com.app.model.webresponsemodel.MatchResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.joinedContests.JoinedContestsActivity;
import com.app.ui.main.kabaddi.main.pastresult.adapter.PastResultAdapter;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class PastResultFragment extends AppBaseFragment {

    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    PastResultAdapter adapter;
    TextView tv_no_record_found;
    List<MatchModel> list = new ArrayList<>();
    private int totalPages = 1000;
    private int currentPage = 0;
    boolean loadingNextData = false;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.fragment_past_result;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        setupSwipeLayout();
        tv_no_record_found = getView().findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        initializeRecyclerView();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = getView().findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                currentPage = 0;
                callNextApi();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = getView().findViewById(R.id.recycler_view);
        adapter = new PastResultAdapter(getActivity());
        adapter.updateData(list);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                MatchModel matchModel = adapter.getItem(position);
                if (matchModel == null) return;
                switch (v.getId()) {
                    default:
                        MyApplication.getInstance().setSelectedMatch(matchModel);
                        goToJoinedContestsActivity(null);
                        break;
                }

            }
        });

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = recycler_view.getLayoutManager().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) recycler_view.getLayoutManager()).findLastVisibleItemPosition();
                if (!loadingNextData && totalItemCount <= (lastVisibleItem + 5)) {
                    callNextApi();
                }
            }
        });

    }

    @Override
    public void onPageSelected() {
        callNextApi();
    }

    private void callNextApi() {
        if (isFinishing()) return;
        if (this.currentPage == 0) {
            this.currentPage = 1;
            this.totalPages = 1000;
            callApi();
            return;
        }
        if (this.totalPages > this.currentPage) {
            this.currentPage = this.currentPage + 1;
            callApi();
        }
    }

    private void callApi() {
        setLoadingNextData(true);
        TransactionRequestModel requestModel = new TransactionRequestModel();
        requestModel.page = currentPage;
        requestModel.limit = 10;
        requestModel.gameid = MyApplication.getInstance().getGemeType();
        requestModel.atype = RESULTS;
        getWebRequestHelper().getResultsMatches(requestModel, this);
    }

    public void setLoadingNextData(boolean isLoading) {
        if (!isFinishing()) {
            this.loadingNextData = isLoading;
            if (swipeRefresh.isRefreshing()) {
                swipeRefresh.setRefreshing(isLoading);
            } else {
                if (adapter != null)
                    adapter.setLoadMore(loadingNextData);
            }
        }
    }

    public void updateData(List<MatchModel> levelList) {
        this.list.clear();
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void addDataToList(List<MatchModel> levelList) {
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    private void goToJoinedContestsActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), JoinedContestsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        setLoadingNextData(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_MATCH_FRONT:
                handleMatchListResponse(webRequest);
                break;
        }
    }

    private void handleMatchListResponse(WebRequest webRequest) {
        MatchResponseModel responsePojo = webRequest.getResponsePojo(MatchResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<MatchModel> data = responsePojo.getData();
                if (data != null && data.size() == 0) {
                    totalPages = currentPage;
                }
                if (currentPage == 1) {
                    updateData(data);
                } else {
                    addDataToList(data);
                }
                updateNoDataView();
                MyApplication.getInstance().startTimer();
            }
        } else {
            if (isFinishing()) return;
            totalPages = 0;
            setLoadingNextData(false);
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (list.size() > 0) {
            updateViewVisibitity(tv_no_record_found, View.GONE);
        } else {
//            tv_no_record_found.setText("No past result available");
            tv_no_record_found.setText("Matches coming soon");
            updateViewVisibitity(tv_no_record_found, View.VISIBLE);
        }
    }
}