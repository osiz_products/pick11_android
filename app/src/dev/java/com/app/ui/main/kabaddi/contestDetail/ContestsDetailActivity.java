package com.app.ui.main.kabaddi.contestDetail;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestModel;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.PriceBreakUpModel;
import com.app.model.TeamModel;
import com.app.model.TeamsPointModel;
import com.app.model.webrequestmodel.ContestDetailRequestModel;
import com.app.model.webresponsemodel.ContestDetailResponseModel;
import com.app.model.webresponsemodel.PlayerPreviewKabaddiResponseModel;
import com.app.model.webresponsemodel.PlayerPreviewResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.TeamsPointResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.winnerbreakupdialog.WinnerBreakupDialog;
import com.app.ui.main.kabaddi.contestDetail.adapter.ContestsDetailAdapter;
import com.app.ui.main.kabaddi.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.kabaddi.myteam.playerpreview.TeamPreviewDialog;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.ConnectionDetector;
import com.medy.retrofitwrapper.WebRequest;
import com.rest.WebServices;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import static com.app.ui.main.kabaddi.contests.ContestsActivity.REQUEST_CREATE_TEAM;

public class ContestsDetailActivity extends AppBaseActivity implements MatchTimerListener, ToolBarFragment.ToolbarFragmentInterFace {

    private TextView tv_team_one;
    private ImageView iv_clock;
    private TextView tv_timer_time;
    private SwipeRefreshLayout swipeRefresh;
    private CardView cv_detail;
    private LinearLayout ll_medium;
    private TextView tv_total_winning;
    private LinearLayout ll_winners;
    private TextView tv_winners;
    private TextView tv_join_team;
    private LinearLayout ll_data;
    //    private RelativeLayout rl_price_pool_breakup;
//    private RecyclerView recycler_view_price;
//    private PricePoolBreakupAdapter breakupAdapter;
    private LinearLayout ll_switch_team;
    private TextView tv_joined_team;
    private TextView tv_joined_team_name;
    private TextView tv_switch_team;
    private LinearLayout ll_c;
    private LinearLayout ll_m;
    private TextView tv_m, tv_m_text;
    private LinearLayout ll_join_team;
    private ProgressBar progressBar;
    private TextView tv_left_team;
    private TextView tv_total_team;
    private RelativeLayout ll_view_payer_stats;
    private TextView tv_download_team;
    private RecyclerView recycler_view;
    private ContestsDetailAdapter adapter;
    private TextView tv_no_record_found;
    private LinearLayout ll_points;
    private List<PriceBreakUpModel> priceBreakUpModels = new ArrayList<>();
    private List<TeamsPointModel> listPoints = new ArrayList<>();
    TeamsPointResponseModel.Data data;
    ContestModel contestModel;
    ContestDetailResponseModel.Details details;
    List<TeamsPointModel> myteams = new ArrayList<>();
    UserPrefs userPrefs;
    private String getContestId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return getIntent().getExtras().getString(DATA);
        return "";
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    private PlayerTypeResponseModel.DataBean getPlayerTypeModel() {
        return MyApplication.getInstance().getPlayerTypeModels();
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_contests_detail;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_team_one = findViewById(R.id.tv_team_one);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);
        cv_detail = findViewById(R.id.cv_detail);
        ll_medium = findViewById(R.id.ll_medium);
        tv_total_winning = findViewById(R.id.tv_total_winning);
        ll_winners = findViewById(R.id.ll_winners);
        tv_winners = findViewById(R.id.tv_winners);


        ll_data = findViewById(R.id.ll_data);
        ll_switch_team = findViewById(R.id.ll_switch_team);
        tv_joined_team_name = findViewById(R.id.tv_joined_team_name);
        tv_switch_team = findViewById(R.id.tv_switch_team);
        ll_c = findViewById(R.id.ll_c);
        ll_m = findViewById(R.id.ll_m);
        tv_m = findViewById(R.id.tv_m);
        tv_m_text = findViewById(R.id.tv_m_text);
        ll_join_team = findViewById(R.id.ll_join_team);
        progressBar = findViewById(R.id.progressBar);
        tv_left_team = findViewById(R.id.tv_left_team);
        tv_total_team = findViewById(R.id.tv_total_team);
        tv_join_team = findViewById(R.id.tv_join_team);
        ll_view_payer_stats = findViewById(R.id.ll_view_payer_stats);
        updateViewVisibility(ll_view_payer_stats, View.GONE);
        tv_download_team = findViewById(R.id.tv_download_team);
        recycler_view = findViewById(R.id.recycler_view);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        ll_points = findViewById(R.id.ll_points);

        ll_winners.setOnClickListener(this);
        tv_switch_team.setOnClickListener(this);
        tv_join_team.setOnClickListener(this);
        ll_view_payer_stats.setOnClickListener(this);
        tv_download_team.setOnClickListener(this);
        updateViewVisibility(tv_no_record_found, View.GONE);

//        initializeRecyclerViewPricePool();
        initializeRecyclerView();
        setUpViews();
        setupSwipeLayout();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setUpViews() {
        if (getMatchModel().isFixtureMatch()) {
            updateViewVisibility(iv_clock, View.VISIBLE);
            updateViewVisibility(ll_switch_team, View.VISIBLE);
            updateViewVisibility(ll_join_team, View.VISIBLE);
            updateViewVisibility(ll_points, View.INVISIBLE);
            updateViewVisibility(tv_download_team, View.GONE);
            updateViewVisibility(getToolBarFragment().tv_score, View.GONE);
        } else {
            updateViewVisibility(iv_clock, View.GONE);
            updateViewVisibility(ll_switch_team, View.GONE);
            updateViewVisibility(ll_join_team, View.GONE);
            updateViewVisibility(ll_points, View.VISIBLE);
            updateViewVisibility(tv_download_team, View.VISIBLE);
            updateViewVisibility(getToolBarFragment().tv_score, View.VISIBLE);
            tv_join_team.setBackground(getResources().getDrawable(R.drawable.blank_bg));
            tv_join_team.setTextColor(getResources().getColor(R.color.color_green_dark));
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContestJoinedTeamsAll();
            }
        });
    }

    private void initializeRecyclerView() {

        recycler_view = findViewById(R.id.recycler_view);
        adapter = new ContestsDetailAdapter(this, listPoints, getUserModel().getTeamname());
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        new ItemClickSupport(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TeamsPointModel pointModel = listPoints.get(position);
                switch (v.getId()) {
                    default:
                        if (pointModel.getTeamname().startsWith(getUserModel().getTeamname())) {
                            TeamModel teamModel = new TeamModel();
                            teamModel.setId(pointModel.getUteamid());
                            teamModel.setTeamname(pointModel.getTeamname());
                            getTeamPlayers(teamModel);
                        } else if (!getMatchModel().isFixtureMatch()) {
                            TeamModel teamModel = new TeamModel();
                            teamModel.setId(pointModel.getUteamid());
                            teamModel.setTeamname(pointModel.getTeamname());
                            getTeamPlayers(teamModel);
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.ll_winners: {
                if (contestModel == null) return;
                if (contestModel.getPricePool() != null) {
                    bundle.putString(DATA, new Gson().toJson(contestModel));
                    goToContestWinnerBreakup(bundle);
                }
            }
            break;

            case R.id.tv_switch_team:
                bundle.putString(DATA1, getContestId());
                goToSwitchTeamActivity(bundle);
                break;

            case R.id.tv_join_team:
                if (contestModel != null && details != null) {
                    if (contestModel.isMultiJoin()) {
                        if (getMatchModel().isFixtureMatch()) {
                            int totalTeam = details.getTotalteams();
                            if (totalTeam > 0) {
                                bundle.putBoolean(DATA, true);
                                bundle.putString(DATA2, new Gson().toJson(contestModel));
                                bundle.putString(DATA1, getContestId());
                                goToSwitchTeamActivity(bundle);
                            } else {
                                bundle.putString(DATA2, new Gson().toJson(contestModel));
                                goToCreateTeamActivity(bundle);
                            }
                        }
                    }
                }
                break;

            case R.id.ll_view_payer_stats:
                bundle.putString(DATA2, "Fantasy Score Card");
                goToWebViewActivity(bundle);
                break;

            case R.id.tv_download_team:
                handleViewTeamsClick();
                break;
        }
    }

    private void goToContestWinnerBreakup(Bundle bundle) {
        WinnerBreakupDialog instance = WinnerBreakupDialog.getInstance(bundle);
        instance.show(getFm(), instance.getClass().getSimpleName());
    }

    private void handleViewTeamsClick() {
        MatchModel matchModel = getMatchModel();
        if (matchModel == null) return;
        if (matchModel.isFixtureMatch()) {
            showErrorMsg("Hang on! You'll be able to download teams after the deadline.");
            return;
        } else {
            getContestPdf();
        }
    }

    private void getContestPdf() {
        if (getMatchModel() != null) {
            String url = String.format(WebServices.GetMatchContestPdf(), getMatchModel().getMatchid(), getContestId());
            startFileDownload(url);
        }
    }

    private void getContestDetail() {
        if (!isValidString(getContestId())) return;
        ContestDetailRequestModel requestModel = new ContestDetailRequestModel();
        requestModel.matchid = getMatchModel().getMatchid();
        requestModel.poolcontestid = Long.parseLong(getContestId());
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getContestDetail(requestModel, this);
        getWebRequestHelper().getContestJoinedTeamsAll(requestModel, this);
    }

    private void getContestJoinedTeamsAll() {
        if (!isValidString(getContestId())) return;
        ContestDetailRequestModel requestModel = new ContestDetailRequestModel();
        requestModel.matchid = getMatchModel().getMatchid();
        requestModel.poolcontestid = Long.parseLong(getContestId());
        getWebRequestHelper().getContestJoinedTeamsAll(requestModel, this);
        getWebRequestHelper().getPlayerType(MyApplication.getInstance().getGemeType(), this);
    }

    private void getTeamPlayers(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        if (swipeRefresh != null && swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_CONTEST_DETAIL:
                handleContestsResponse(webRequest);
                break;

            case ID_GET_JOINED_CONTENTS_TEAM_ALL:
                handleJoinedContestTeamAllResponse(webRequest);
                break;

            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;

            case ID_PLAYER_TYPE:
                handlePlayerTypeResponse(webRequest);
                break;
        }
    }

    private void startFileDownload(String serverFileUrl) {
        if (!new ConnectionDetector(this).isConnectingToInternet()) {
            showErrorMsg("Please check your internet connection.");
            return;
        }

        Uri urlUri = Uri.parse(serverFileUrl);
        printLog("Uri", urlUri.toString());
        DownloadManager.Request request = new DownloadManager.Request(urlUri);
        String lastPathSegment = urlUri.getLastPathSegment();
        if (lastPathSegment == null) {
            lastPathSegment = "File";
        }
        request.setDescription("Downloading" + " - " + lastPathSegment + ".pdf");
        request.setTitle(lastPathSegment + ".pdf");
        request.setMimeType("application/pdf");
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }


    private void handleContestsResponse(WebRequest webRequest) {
        ContestDetailResponseModel responseModel = webRequest.getResponsePojo(ContestDetailResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            this.details = responseModel.getDetails();
            contestModel = responseModel.getData();
            contestModel.setPoolcontestid(Long.parseLong(getContestId()));
            setUpData(contestModel);
            priceBreakUpModels.clear();
            if (contestModel.getBrkprize().size() > 0) {//data != null &&
                priceBreakUpModels.addAll(contestModel.getBrkprize());
            } else {
                updateView(contestModel.getBrkprize());
            }
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

        setUpJoinButton();

    }

    private void handlePlayerTypeResponse(WebRequest webRequest) {
        PlayerTypeResponseModel responsePojo = webRequest.getResponsePojo(PlayerTypeResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            PlayerTypeResponseModel.DataBean data = responsePojo.getData();
            MyApplication.getInstance().setPlayerTypeModels(data);
            setUpJoinButton();
        } else {
            if (isFinishing()) return;
//            showErrorMsg(responsePojo.getMsg());
        }
    }

    private void setUpJoinButton() {
        if (contestModel != null) {
            if (contestModel.isMultiJoin()) {

                if (getMatchModel().isFixtureMatch()) {
                    tv_join_team.setBackground(getResources().getDrawable(R.drawable.button_bg));
                    tv_join_team.setTextColor(getResources().getColor(R.color.color_white));
                } else {
                    tv_join_team.setBackground(getResources().getDrawable(R.drawable.blank_bg));
                    tv_join_team.setTextColor(getResources().getColor(R.color.color_green_dark));
                }
            } else {
                tv_join_team.setBackground(getResources().getDrawable(R.drawable.blank_bg));
                tv_join_team.setTextColor(getResources().getColor(R.color.color_green_dark));
            }
            tv_m.setText(contestModel.isMultiJoin() ? "M" : "S");
            String format;
            if (getPlayerTypeModel() != null)
                format = String.format(getResources().getString(R.string.multi_join_text), getPlayerTypeModel().getMaxteam());
            else
                format = getResources().getString(R.string.single_join_text_only);

            tv_m_text.setText(contestModel.isMultiJoin() ? format
                    : getResources().getString(R.string.single_join_text_only));
            if (contestModel.isConfirmWinning())
                updateViewVisibility(ll_c, View.VISIBLE);
            else updateViewVisibility(ll_c, View.GONE);
        }
    }

    private void setUpData(ContestModel data) {
        tv_total_winning.setText(currency_symbol + data.digitToText());
        tv_winners.setText(String.valueOf(data.getWinners()));
        tv_join_team.setText(currency_symbol + data.get_entry_feesText());

        progressBar.setMax(data.getMaxteams());
        progressBar.setProgress(data.getMaxteams() - data.getJoinleft());
        tv_left_team.setText(data.getTeamLeft());
        tv_total_team.setText(data.getTotalTeam());
    }

    private void updateView(List<PriceBreakUpModel> list) {
//        if (list != null && list.size() > 0)
//            updateViewVisibility(rl_price_pool_breakup, View.VISIBLE);
//        else
//            updateViewVisibility(rl_price_pool_breakup, View.GONE);

    }


    private void handleJoinedContestTeamAllResponse(WebRequest webRequest) {
        TeamsPointResponseModel responseModel = webRequest.getResponsePojo(TeamsPointResponseModel.class);
        if (responseModel == null) return;
        List<TeamsPointModel> list = null;
        if (!responseModel.isError()) {
            data = responseModel.getData();
            if (data == null) return;
            listPoints.clear();
            myteams.clear();
            list = data.getList();
            myteams.addAll(data.getMyteams());
            if (myteams != null && myteams.size() > 0)
                list.addAll(0, myteams);
            listPoints.addAll(list);
            adapter.notifyDataSetChanged();
        }

        if (myteams != null && myteams.size() > 0 && getMatchModel().isFixtureMatch()) {
            updateViewVisibility(ll_switch_team, View.VISIBLE);
        } else updateViewVisibility(ll_switch_team, View.GONE);

        if (myteams.size() > 1) {
            tv_joined_team_name.setText("Team " + myteams.size());
        } else {
            tv_joined_team_name.setText("" + (myteams.size() > 0 ? (myteams.get(0).getTeamname()) : ""));
        }
    }

    private void handleTeamPlayersResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> defender = new ArrayList<>();
            List<PlayerModel> raider = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();

            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPname(datum.getPname());
                playerModel.setFullname(datum.getFullname());
                playerModel.setPtypename(datum.getPtypename());
                playerModel.setPtype(datum.getPtype());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setIsplaying(datum.getIsplaying());
                playerModel.setPimg(datum.getPimg());
                playerModel.setPoints(datum.getPoints());
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                    defender.add(playerModel);
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                    raider.add(playerModel);
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                }
            }

            PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean();
            playersBean.setDefender(defender);
            playersBean.setRaider(raider);
            playersBean.setAllrounder(allrounder);

            PlayerPreviewKabaddiResponseModel.DataBean team = new PlayerPreviewKabaddiResponseModel
                    .DataBean();
            team.setId(String.valueOf(teamModel.getId()));
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);
            team.setName(teamModel.getTeamname());
            for (PlayerPreviewModel datum : data) {
                if (datum.getIscap() == 1) {
                    team.setCaptainid(datum.getPid());
                }
                if (datum.getIsvcap() == 1) {
                    team.setVicecaptainid((datum.getPid()));
                }
            }

            TeamPreviewDialog instance = TeamPreviewDialog.getInstance(null);
            instance.setTeam(team);
            instance.show(getFm(), instance.getClass().getSimpleName());


        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }

    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onResume() {
        super.onResume();
        getContestDetail();
        getContestJoinedTeamsAll();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }

    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
            tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
            iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                getContestDetail();
                getContestJoinedTeamsAll();
            }
        }
    }

    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.tv_score:
                String url = String.format(WebServices.FantasyScoreUrl(), "fantscorcrd/"
                        + MyApplication.getInstance().getGemeType() + "/" + getMatchModel().getMatchid());
                Bundle bundle = new Bundle();
                bundle.putString(DATA, url);
                bundle.putString(DATA2, "Fantasy Score Card");
                goToWebViewActivity(bundle);
                break;
        }
    }
}
