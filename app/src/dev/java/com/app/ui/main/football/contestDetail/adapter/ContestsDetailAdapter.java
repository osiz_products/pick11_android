package com.app.ui.main.football.contestDetail.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.MatchModel;
import com.app.model.TeamsPointModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;

import java.util.List;

public class ContestsDetailAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<TeamsPointModel> list;
    private String teamname;

    public ContestsDetailAdapter(Context context, List<TeamsPointModel> list, String teamname) {
        this.context = context;
        this.list = list;
        this.teamname = teamname;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_contests_details));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {
        private LinearLayout ll_layout;
        private ImageView iv_user_image;
        private TextView tv_team_name;
        private LinearLayout ll_points;
        private TextView tv_team_points;
        private LinearLayout ll_champion;
        private TextView tv_win_amount;
        private TextView tv_rank;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            iv_user_image = itemView.findViewById(R.id.iv_user_image);
            tv_team_name = itemView.findViewById(R.id.tv_team_name);
            ll_points = itemView.findViewById(R.id.ll_points);
            tv_team_points = itemView.findViewById(R.id.tv_team_points);
            ll_champion = itemView.findViewById(R.id.ll_champion);
            tv_win_amount = itemView.findViewById(R.id.tv_win_amount);
            tv_rank = itemView.findViewById(R.id.tv_rank);

            updateViewVisibitity(iv_user_image, View.GONE);
            updateViewVisibitity(ll_champion, View.GONE);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            TeamsPointModel pointModel = list.get(position);
            tv_team_name.setText(pointModel.getTeamname());
            tv_team_points.setText(pointModel.getTotalPointstext());
            String currency = getContext().getResources().getString(R.string.currency_symbol);
            tv_win_amount.setText("Won " + currency + pointModel.getWinAmountText());
//            tv_rank.setText("#"+pointModel.getRank());
            if (MyApplication.getInstance().getSelectedMatch().isFixtureMatch()) {
                tv_rank.setText("-");
            } else {
                String rank = "<font color='" + getContext().getResources().getColor(R.color.colorGray)
                        + "'>#</font>" + pointModel.getRank();
                tv_rank.setText(Html.fromHtml(rank), TextView.BufferType.SPANNABLE);
            }
            if (pointModel.getTeamname().startsWith(teamname)) {
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_item_bg2));
            } else {
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_white));
            }

            if (getMatchModel().isFixtureMatch()) {
                updateViewVisibitity(ll_points, View.INVISIBLE);
                updateViewVisibitity(ll_champion, View.GONE);
            } else {
                updateViewVisibitity(ll_points, View.VISIBLE);
                if (pointModel.getWinbal() > 0)
                    updateViewVisibitity(ll_champion, View.VISIBLE);
                else
                    updateViewVisibitity(ll_champion, View.GONE);
            }
            return currency;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }
}
