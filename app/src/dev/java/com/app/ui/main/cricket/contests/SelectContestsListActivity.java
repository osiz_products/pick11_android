package com.app.ui.main.cricket.contests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestCategoryModel;
import com.app.model.ContestModel;
import com.app.model.MatchModel;
import com.app.model.WalletModel;
import com.app.model.webresponsemodel.JoinContestResponseModel;
import com.app.model.webresponsemodel.MatchContestResponseModel;
import com.app.model.webresponsemodel.WalletResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.ContestJoinConfirmationDialog;
import com.app.ui.main.cricket.allContest.AllContestsActivity;
import com.app.ui.main.cricket.contestDetail.ContestsDetailActivity;
import com.app.ui.main.cricket.contests.adapter.SelectContestsAdapter;
import com.app.ui.main.cricket.joinedContests.JoinedContestsActivity;
import com.app.ui.main.cricket.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.cricket.myteam.myTeams.MyTeamsActivity;
import com.app.ui.main.cricket.privateContests.InviteCodeActivity;
import com.app.ui.main.cricket.privateContests.PrivateContestActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.myaccount.AddCashActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.uitilites.RecyclerSectionItemDecoration;
import com.customviews.TypefaceTextView;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.BuildConfig;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class SelectContestsListActivity extends AppBaseActivity implements MatchTimerListener, ToolBarFragment.ToolbarFragmentInterFace {

    public static final int REQUEST_CREATE_TEAM = 103;
    public static final int REQUEST_JOIN_CONTEST = 104;
    public static final int REQUEST_SWITCH_TEAM = 105;
    public static final int REQUEST_LOW_BALANCE = 106;

    SwipeRefreshLayout swipeRefresh;
    TextView tv_team_one;
    LinearLayout ll_timer;
    ImageView iv_clock;
    TextView tv_timer_time;
    LinearLayout ll_coupon_header;
    LinearLayout ll_coupon;
    CardView cv_contest_code;
    CardView cv_create_contest;
    TextView tv_entry_fees;
    TextView tv_contest_size;
    ProgressBar pb_data;
    RecyclerView recycler_view;
    SelectContestsAdapter adapter;
    TextView tv_no_record_found;
    LinearLayout ll_create_team;
    TextView tv_my_teams_total;
    LinearLayout ll_joined_contests;
    TextView tv_joined_contests_total;
    LinearLayout ll_bottom;
    String getMatchID;
    TypefaceTextView tv_score;
    ImageView iv_delete_all;
    ImageView iv_edit_profile;
    RelativeLayout rl_notification;

    private MatchContestResponseModel.Details details;
    List<ContestCategoryModel> contestCategoryModels = new ArrayList<>();
    private List<ContestModel> joinedContestList = new ArrayList();
    UserPrefs userPrefs;
    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_select_contest;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        userPrefs = new UserPrefs(this);
        tv_team_one = findViewById(R.id.tv_team_one);
        ll_timer = findViewById(R.id.ll_timer);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);
        ll_coupon_header = findViewById(R.id.ll_coupon_header);
        ll_coupon = findViewById(R.id.ll_coupon);
        cv_contest_code = findViewById(R.id.cv_contest_code);
        cv_create_contest = findViewById(R.id.cv_create_contest);
        tv_entry_fees = findViewById(R.id.tv_entry_fees);
        tv_contest_size = findViewById(R.id.tv_contest_size);

        tv_score = findViewById(R.id.tv_score);
        iv_delete_all = findViewById(R.id.iv_delete_all);
        iv_edit_profile = findViewById(R.id.iv_edit_profile);
        rl_notification = findViewById(R.id.rl_notification);

        tv_score.setVisibility(View.GONE);
        iv_delete_all.setVisibility(View.GONE);
        iv_edit_profile.setVisibility(View.GONE);
        rl_notification.setVisibility(View.GONE);

        getMatchID = getIntent().getStringExtra("selectID");

        Log.e("getMatchID_getMatchID",""+getMatchID);

        setupSwipeLayout();
        pb_data = findViewById(R.id.pb_data);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        ll_create_team = findViewById(R.id.ll_create_team);
        tv_my_teams_total = findViewById(R.id.tv_my_teams_total);
        ll_joined_contests = findViewById(R.id.ll_joined_contests);
        tv_joined_contests_total = findViewById(R.id.tv_joined_contests_total);
        ll_bottom = findViewById(R.id.ll_bottom);
        updateViewVisibility(ll_joined_contests, View.GONE);
        updateViewVisibility(ll_bottom, View.GONE);
        updateViewVisibility(ll_coupon, View.GONE);

        cv_contest_code.setOnClickListener(this);
        cv_create_contest.setOnClickListener(this);
        tv_entry_fees.setOnClickListener(this);
        tv_contest_size.setOnClickListener(this);
        ll_create_team.setOnClickListener(this);
        ll_joined_contests.setOnClickListener(this);

        initializeRecyclerView();

        String flavor = BuildConfig.FLAVOR;
        if (flavor.equalsIgnoreCase("brdream11Dev")
                ||flavor.equalsIgnoreCase("worldpro11Dev")
                ||flavor.equalsIgnoreCase("star11Dev")) {
            updateViewVisibility(ll_coupon, View.VISIBLE);
        }

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                getJoinedContestList();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new SelectContestsAdapter(this, contestCategoryModels) {
            @Override
            public ContestModel getJoinedContestModel(ContestModel contestModel) {
                int index = joinedContestList.indexOf(contestModel);
                return index == -1 ? null : joinedContestList.get(index);
            }
        };
        RecyclerSectionItemDecoration sectionItemDecoration =
                new RecyclerSectionItemDecoration(this, getResources().getDimensionPixelOffset(R.dimen.dp50), true, getSectionCallback(contestCategoryModels));
        recycler_view.addItemDecoration(sectionItemDecoration);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);

        new ItemClickSupport(recycler_view) {
            @Override
            public void onChildItemClicked(RecyclerView recyclerView, int parentPosition, int childPosition, View v) {
                ContestCategoryModel contestCategoryModel = contestCategoryModels.get(parentPosition);
                if (contestCategoryModel == null) return;
                ContestModel contestModel = contestCategoryModel.getContestPools().get(childPosition);
                if (contestModel == null) return;
                Bundle bundle = new Bundle();
                switch (v.getId()) {
                    case R.id.tv_entry_fees:
                        if (contestModel.getJoinleft() > 0) {
                            if (details != null) {
                                int totalTeam = details.getTotalteams();
                                if (totalTeam > 0) {
                                    bundle.putBoolean(DATA, true);
                                    bundle.putString(DATA2, new Gson().toJson(contestModel));
                                    bundle.putString(DATA1, String.valueOf(contestModel.getId()));
                                    goToSwitchTeamActivity(bundle);
                                } else {
                                    bundle.putString(DATA2, new Gson().toJson(contestModel));
                                    goToCreateTeamActivity(bundle);
                                }
                            }
                        }else {
                            showErrorMsg("This contest is full");
                        }

                        break;
                    default:
                        bundle.putString(DATA, String.valueOf(contestModel.getId()));
                        goToContestsDetailActivity(bundle);
                        break;
                }
            }
        }.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (v.getId()) {
                    default:

                        break;
                }
            }
        });
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<ContestCategoryModel> categoryModels) {
        if (categoryModels == null) return null;
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0 || categoryModels.get(position).getTitle().charAt(0)
                        != categoryModels.get(position - 1).getTitle().charAt(0);
            }

            @Override
            public String getSectionImage(int position) {
                return categoryModels.get(position)
                        .getContestlogo();
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return categoryModels.get(position)
                        .getTitle();
            }

            @Override
            public CharSequence getSectionSubHeader(int position) {
                return categoryModels.get(position)
                        .getSubtitle();
            }
        };
    }

    private void getJoinedContestList() {
        if (getMatchModel() == null) return;
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getJoinedContests(getMatchModel(), this);
    }

    private void getContestMatchList() {
        if (getMatchModel() == null) return;
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getMatchContestListFrontViewMore(getMatchID,getMatchModel(), this);
       // getWebRequestHelper().getMatchContestListFront(getMatchModel(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        swipeRefresh.setRefreshing(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {

            case ID_JOINED_CONTEST:
                handleJoinedContestsResponse(webRequest);
                break;

            case ID_MATCH_CONTEST_FRONT_VIEWMORE:
                handleContestsResponse(webRequest);
                break;

            case ID_GET_USER_BALANCE:
                handleUserBalanceResponse(webRequest);
                break;
        }
    }

    private void handleJoinedContestsResponse(WebRequest webRequest) {
        JoinContestResponseModel responseModel = webRequest.getResponsePojo(JoinContestResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                List<ContestModel> data = responseModel.getData();
                joinedContestList.clear();
                if (data != null && data.size() > 0) {
                    joinedContestList.addAll(data);
                }
            }
        }
        getContestMatchList();
    }

    private void handleContestsResponse(WebRequest webRequest) {
        MatchContestResponseModel responseModel = webRequest.getResponsePojo(MatchContestResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            this.details = responseModel.getDetails();
            List<ContestCategoryModel> data = responseModel.getData();
            contestCategoryModels.clear();
            if (data != null && data.size() > 0) {
                contestCategoryModels.addAll(data);
                updateData();
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void handleUserBalanceResponse(WebRequest webRequest) {
        ContestModel contestModel = webRequest.getExtraData(DATA);
        WalletResponseModel responseModel = webRequest.getResponsePojo(WalletResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            getUserModel().setWalletModel(responseModel.getData());
            WalletModel data = responseModel.getData();
            if (data == null && contestModel == null) return;
            Bundle bundle = new Bundle();
            String message;
            if (data.getWalletbalance() > contestModel.getJoinfee()) {
                message = String.format("Your current balance is ₹%s and joining amount is %s",
                        data.getWalletbalance(), contestModel.getJoinfee());
                bundle.putString(MESSAGE, "Are you sure?");
                bundle.putString(MESSAGE_SEC, message);
                bundle.putString(NEG_BTN, "No, Cancel it!");
                bundle.putString(POS_BTN, "Yes, Join Contest!");
                addContestJoinConfirmationDialog(bundle, contestModel, data);
            } else {
                message = String.format("Your current balance is ₹%s, for joining contest you have to add balance",
                        data.getWalletbalance());
                bundle.putString(MESSAGE, "Are you sure?");
                bundle.putString(MESSAGE_SEC, message);
                bundle.putString(NEG_BTN, "No, Cancel it!");
                bundle.putString(POS_BTN, "Yes, Add Balance!");
                addContestJoinConfirmationDialog(bundle, contestModel, data);
            }
        }
    }

    private void addContestJoinConfirmationDialog(Bundle bundle, final ContestModel contestModel, final WalletModel data) {
        ContestJoinConfirmationDialog dialog = ContestJoinConfirmationDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE)
                    if (data.getWalletbalance() > contestModel.getJoinfee()) {
                        Bundle bundle = new Bundle();
                        bundle.putString(DATA, new Gson().toJson(data));
                        bundle.putString(DATA2, new Gson().toJson(contestModel));
                        bundle.putString(DATA1, String.valueOf(contestModel.getId()));
                        goToSwitchTeamActivity(bundle);
                    } else
                        goToAddCashActivity(null);
                dialog.dismiss();
            }
        });
        dialog.show(getFm(), "Confirm Join");
    }

    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToAddCashActivity(Bundle bundle) {
        Intent intent = new Intent(this, AddCashActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void updateData() {
        if (details == null) return;
        updateViewVisibility(ll_bottom, View.VISIBLE);
        int totalTeam = details.getTotalteams();
        int joinedContests = details.getTotaljc();
        if (totalTeam > 0) {
            if (totalTeam == 1)
                tv_my_teams_total.setText("My Team (" + totalTeam + ")");
            else tv_my_teams_total.setText("My Teams (" + totalTeam + ")");
            updateViewVisibility(ll_joined_contests, View.VISIBLE);
            if (joinedContests > 0)
                tv_joined_contests_total.setText("(" + details.getTotaljc() + ")");
            else
                tv_joined_contests_total.setText("");
        } else {
            tv_my_teams_total.setText("Create Teams");
            updateViewVisibility(ll_joined_contests, View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.cv_contest_code:
                goToInviteCodeActivity(null);
                break;

            case R.id.cv_create_contest:
                bundle.putBoolean(DATA, joinedContestList.size() > 0 ? true : false);
                goToPrivateContestActivity(bundle);
                break;

            case R.id.tv_entry_fees:
                bundle.putBoolean(DATA, true);
                goToAllContestActivity(bundle);
                break;

            case R.id.tv_contest_size:
                bundle.putBoolean(DATA, false);
                goToAllContestActivity(bundle);
                break;

            case R.id.ll_create_team:
                if (details.getTotalteams() > 0) {
                    goToMyTeamsActivity(null);
                } else {
                    goToCreateTeamActivity(null);
                }
                break;

            case R.id.ll_joined_contests:
                goToJoinedContestsActivity(null);
                break;
        }
    }

    private void goToContestsDetailActivity(Bundle bundle) {
        Intent intent = new Intent(this, ContestsDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToMyTeamsActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyTeamsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToPrivateContestActivity(Bundle bundle) {
        Intent intent = new Intent(this, PrivateContestActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    private void goToInviteCodeActivity(Bundle bundle) {
        Intent intent = new Intent(this, InviteCodeActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    private void goToAllContestActivity(Bundle bundle) {
        Intent intent = new Intent(this, AllContestsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToJoinedContestsActivity(Bundle bundle) {
        Intent intent = new Intent(this, JoinedContestsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getJoinedContestList();
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }

    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            if (getMatchModel().isUnderReview()) {
                tv_timer_time.setText("Under Review");
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            } else {
                tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                if (getContestModel() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(DATA, true);
                    bundle.putString(DATA2, new Gson().toJson(getContestModel()));
                    bundle.putString(DATA1, String.valueOf(getContestModel().getId()));
                    goToSwitchTeamActivity(bundle);
                    supportFinishAfterTransition();
                }
            }
        }
    }

    public ContestModel getContestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA2);
            if (isValidString(string))
                return new Gson().fromJson(string, ContestModel.class);
            else
                return null;
        }
        return null;
    }

    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.iv_wallet:
                goToMyAccountActivity(null);
                break;
        }
    }

    private void goToMyAccountActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
