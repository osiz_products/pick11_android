package com.app.ui.main.football.contestDetail.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.PriceBreakUpModel;
import com.pickeleven.R;

import java.util.List;

public class PricePoolBreakupAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<PriceBreakUpModel> list;

    public PricePoolBreakupAdapter(Context context, List<PriceBreakUpModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {

        return new ViewHolder(inflateLayout(R.layout.item_price_pool));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {
        private TextView tv_price;
        private TextView tv_rank;
        private ImageView iv_trophy_icon;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_rank = itemView.findViewById(R.id.tv_rank);
            iv_trophy_icon = itemView.findViewById(R.id.iv_trophy_icon);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            String currency = getContext().getString(R.string.currency_symbol);
            PriceBreakUpModel priceBreakUpModel = list.get(position);

            tv_rank.setText(Html.fromHtml(getRank(position, priceBreakUpModel)), TextView.BufferType.SPANNABLE);
            tv_price.setText(currency + priceBreakUpModel.getAmountText());

            if (position == 0) {
                updateViewVisibitity(iv_trophy_icon, View.VISIBLE);
            } else {
                updateViewVisibitity(iv_trophy_icon, View.GONE);
            }


            return currency;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }

    private String getRank(int position, PriceBreakUpModel priceBreakUpModel) {
        String rank = "";
        if (position == 0 && (priceBreakUpModel.getPmax() - priceBreakUpModel.getPmin()) < 1) {
            rank = "Rank " + priceBreakUpModel.getPmin();
        } else {
            if ((priceBreakUpModel.getPmax() - priceBreakUpModel.getPmin()) < 1) {
                rank = "<font color='" + getContext().getResources().getColor(R.color.colorGray) + "'>#</font>"
                        + priceBreakUpModel.getPmin();
            } else {
                rank = "<font color='" + getContext().getResources().getColor(R.color.colorGray) + "'>#</font>"
                        + priceBreakUpModel.getPmin() + "-" + priceBreakUpModel.getPmax();
            }
        }
        return rank;
    }
}
