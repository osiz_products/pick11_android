package com.app.ui.main.cricket.myMatches;

import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.viewpager.widget.ViewPager;

import com.app.appbase.AppBaseFragment;
import com.app.appbase.ViewPagerAdapter;
import com.app.preferences.UserPrefs;
import com.app.ui.main.cricket.myMatches.live.MyLiveFragment;
import com.app.ui.main.cricket.myMatches.result.MyResultFragment;
import com.app.ui.main.cricket.myMatches.upcoming.MyUpcomingFragment;
import com.pickeleven.R;
import com.google.android.material.tabs.TabLayout;

public class MyMatchesCricketFragment extends AppBaseFragment {

    TabLayout tabs;
    ViewPager view_pager;
    ViewPagerAdapter viewPagerAdapter;
    private int theme;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.fragment_my_matches;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        tabs = getView().findViewById(R.id.tabs);
        view_pager = getView().findViewById(R.id.view_pager);
        setupViewPager();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(getChildFm());
        viewPagerAdapter.addFragment(new MyUpcomingFragment(), "Fixtures");
        viewPagerAdapter.addFragment(new MyLiveFragment(), "Live");
        viewPagerAdapter.addFragment(new MyResultFragment(), "Results");

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPagerAdapter.getItem(position).onPageSelected();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        view_pager.setAdapter(viewPagerAdapter);
        tabs.setupWithViewPager(view_pager);
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getTheme() {
        return theme;
    }
}
