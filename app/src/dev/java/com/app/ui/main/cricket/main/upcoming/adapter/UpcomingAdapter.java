package com.app.ui.main.cricket.main.upcoming.adapter;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.MatchModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static androidx.core.content.ContextCompat.getColor;

public class UpcomingAdapter extends AppBaseRecycleAdapter {

    private List<MatchModel> list;
    private long hours;
    private long minutes;
    private long second;
    private long days;
    private String lineups ="";

    public UpcomingAdapter(List<MatchModel> list) {
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_fixtures));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public MatchModel getItem(int position) {
        return list == null ? null : list.get(position);
    }

    private class ViewHolder extends BaseViewHolder {
        private TextView tv_series_name;
        private ImageView iv_image_first;
        private ProgressBar pb_image_first;
        private TextView tv_team_one;
        private ImageView iv_image_second;
        private ProgressBar pb_image_second;
        private TextView tv_team_two;
        private TextView tv_match_name;
        private TextView tv_match_type;
        private TextView tv_match_start_time;
        private TextView txtLineups;
        private CardView gamelayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_series_name = itemView.findViewById(R.id.tv_series_name);
            iv_image_first = itemView.findViewById(R.id.iv_image_first);
            pb_image_first = itemView.findViewById(R.id.pb_image_first);
            tv_team_one = itemView.findViewById(R.id.tv_team_one);
            iv_image_second = itemView.findViewById(R.id.iv_image_second);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            tv_team_two = itemView.findViewById(R.id.tv_team_two);
            tv_match_name = itemView.findViewById(R.id.tv_match_name);
            tv_match_type = itemView.findViewById(R.id.tv_match_type);
            tv_match_start_time = itemView.findViewById(R.id.tv_match_start_time);
            txtLineups = itemView.findViewById(R.id.txtLineups);
            gamelayout = itemView.findViewById(R.id.gamelayout);
        }
        @Override
        public String setData(int position) {

            if (list == null) return null;
            MatchModel matchModel = list.get(position);
            if (matchModel == null) return null;
            if (!matchModel.getSeriesname().isEmpty()) {
                updateViewVisibitity(tv_series_name, View.VISIBLE);
                tv_series_name.setText(matchModel.getSeriesname());
            } else {
                updateViewVisibitity(tv_series_name, View.GONE);
            }

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_first, pb_image_first, matchModel.getTeam1logo(), R.drawable.dummy_logo);

            ((AppBaseActivity) getContext()).loadImage(getContext(),
            iv_image_second, pb_image_second, matchModel.getTeam2logo(), R.drawable.dummy_logo);
            tv_match_name.setText(matchModel.getMatchname());
            tv_team_one.setText(matchModel.getTeam1());
            tv_team_two.setText(matchModel.getTeam2());
            tv_match_type.setText(matchModel.capitalize(matchModel.getMtype()));
            tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
            tv_match_start_time.setTextColor(getContext().getResources().getColor(R.color.color_timer));

            String MatchEnableStatus = matchModel.getStatus();
            Log.e("MatchEnableStatus",""+MatchEnableStatus);

                /*if (MatchEnableStatus.equals("0")){
                    //   gamelayout.setBackgroundColor(Color.CYAN);
                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_bg2));
                }else if (MatchEnableStatus.equals("1")){

                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_white));
                }*/
            if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);

                if (MatchEnableStatus.equals("0")){
                    //   gamelayout.setBackgroundColor(Color.CYAN);
                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_join));
                }else if (MatchEnableStatus.equals("1")){
                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_black));
                }

            } else {
                getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
                if (MatchEnableStatus.equals("0")){
                    //   gamelayout.setBackgroundColor(Color.CYAN);
                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_bg2));
                }else if (MatchEnableStatus.equals("1")){
                    gamelayout.setBackgroundColor(getColor(getContext(), R.color.color_white));
                }
            }
            //Log.e("GETLineups_Out",""+matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
    long matchRemainTime = matchModel.getMdategmt() - MyApplication.getInstance().getServerdate();
    if (matchRemainTime < 0) {
        return "";
    }
    days = TimeUnit.SECONDS.toDays(matchRemainTime);
    StringBuilder builder = new StringBuilder();

    if (days > 0) {
        builder.append(days);
        builder.append((days > 1) ? " days" : " day");
        builder.append(" ");
    }

    builder.append(getValidTimeText(matchRemainTime - TimeUnit.DAYS.toSeconds(days)));
            if (hours == 0 && minutes <= 30){
                Log.e("GET_TIME2","Hours "+hours+"Minutes "+minutes+"MatchID "+matchModel.getMatchid());
                if (matchModel.getLineups_out()!=null) {
                    if (matchModel.getLineups_out().equals("1")) {
                        Log.e("GETLineupsOut", "" + matchModel.getMatchid());
                        txtLineups.setVisibility(View.VISIBLE);
                    } else {
                        txtLineups.setVisibility(View.GONE);
                    }
                }else {
                    txtLineups.setVisibility(View.GONE);
                }
               // txtLineups.setVisibility(View.VISIBLE);
            }else {
                txtLineups.setVisibility(View.GONE);
            }

    return builder.toString();

        }
    }

    public String getValidTimeText(long value) {
        hours = TimeUnit.SECONDS.toHours(value);
        minutes = TimeUnit.SECONDS.toMinutes(value - TimeUnit.HOURS.toSeconds(hours));
        second = value - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes);
        StringBuilder builder = new StringBuilder();
        if (hours < 0)
            builder.append("-");
        if (hours != 0)
            builder.append(String.format(Locale.US, "%02dh", Math.abs(hours)));

      //  Log.e("GET_Hours",""+hours);
        if (minutes < 0) {
            builder.append(" -");
        } else {
            builder.append(" ");
        }
        if (minutes != 0)
            builder.append(String.format(Locale.US, "%02dm", Math.abs(minutes)));
      //  Log.e("GET_minutes",""+minutes);
        if (second < 0) {
            builder.append(" -");
        } else {
            builder.append(" ");
        }
        if (second != 0)
            builder.append(String.format(Locale.US, "%02ds", Math.abs(second)));
        return builder.toString();
    }

}