package com.app.ui.main.navmenu.referFriend;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webresponsemodel.ReferResponseModel;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.rest.WebServices;

public class ReferFriendActivity extends AppBaseActivity {

    TextView tv_refer_message;
    TextView tv_invite_code;
    ImageView iv_whatsApp;
    ImageView iv_facebook;
    ImageView iv_mail;
    TextView tv_term_and_condition;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_refer_friend;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_refer_message = findViewById(R.id.tv_refer_message);
        tv_invite_code = findViewById(R.id.tv_invite_code);
        iv_whatsApp = findViewById(R.id.iv_whatsApp);
        iv_facebook = findViewById(R.id.iv_facebook);
        iv_mail = findViewById(R.id.iv_mail);
        tv_term_and_condition = findViewById(R.id.tv_term_and_condition);

        tv_invite_code.setOnClickListener(this);
        iv_whatsApp.setOnClickListener(this);
        iv_facebook.setOnClickListener(this);
        iv_mail.setOnClickListener(this);
        tv_term_and_condition.setOnClickListener(this);
        getReferCodeUrl();
        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setReferCode() {
        if (getUserModel() == null) return;
        tv_invite_code.setText(getUserModel().getRefercode());
        Log.e("",""+getUserModel().getRefercode());
        String format = String.format(getResources().getString(R.string.referral_text),
                getUserModel().getReferalBonusText(), getUserModel().getReferalBonusFriendText());
        tv_refer_message.setText(format);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_invite_code:
                UserModel userModel = getUserModel();
                if (userModel != null) {
                    boolean b = copyToClipboard(userModel.getRefercode());
                    if (b)
                        showCustomToast("Code Copied.");
                }
                break;

            case R.id.iv_whatsApp:
            case R.id.iv_facebook:
            case R.id.iv_mail:
                shareWithFriends(tv_invite_code.getText().toString().trim());
                break;

            case R.id.tv_term_and_condition:
                Bundle bundle = new Bundle();
                String URL = String.format(WebServices.FaqUrl(), "termscon");
                bundle.putString(DATA, URL);
                bundle.putString(DATA2, "Terms & Conditions");
                goToWebViewActivity(bundle);
                break;

        }
    }

    public void shareWithFriends(String referCode) {
        try {
            if (getUserModel() == null) return;
            String appName = getResources().getString(R.string.app_name);
            String referMessage = getUserModel().getMessage();
            //String referMessage = getResources().getString(R.string.refer_message);
            /*referMessage = String.format(referMessage, appName, WebServices.GetLiveScoreUrl(),
                    tv_invite_code.getText().toString().trim(), getUserModel().getReferalBonusFriendText());*/
                String getMessage = String.valueOf(Html.fromHtml(referMessage));


            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView.setText(Html.fromHtml("<h2>Title</h2><br><p>Description here</p>", Html.FROM_HTML_MODE_COMPACT));
            } else {
                textView.setText(Html.fromHtml("<h2>Title</h2><br><p>Description here</p>"));
            }*/

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, referMessage);
            //i.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getUserModel().getMessage(),Html.FROM_HTML_MODE_COMPACT));
            startActivity(Intent.createChooser(i, "Share Via :"));
        } catch (Exception ignore) {

        }
    }

    /*private void goToWebViewActivity(Bundle bundle) {
        Intent intent = new Intent(this, WebViewActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }*/

    private void getReferCodeUrl() {
        getWebRequestHelper().getReferCodeUrl(this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_REFER_CODE:
                handleReferCodeResponse(webRequest);
                break;
        }
    }

    private void handleReferCodeResponse(WebRequest webRequest) {
        ReferResponseModel responseModel = webRequest.getResponsePojo(ReferResponseModel.class);
        if (responseModel == null) return;
        UserModel data = responseModel.getData();
        if (data == null || getUserModel() == null) return;
        UserModel userModel = getUserModel();
        userModel.setRefbns(data.getRefbns());
        userModel.setRefbnsfrnd(data.getRefbnsfrnd());
        getUserPrefs().updateLoggedInUser(userModel);
        setReferCode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setReferCode();
    }
}
