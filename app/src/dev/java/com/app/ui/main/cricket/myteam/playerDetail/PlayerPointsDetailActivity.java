package com.app.ui.main.cricket.myteam.playerDetail;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPointsDetailModel;
import com.app.model.PlayerPointsModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.LinkedTreeMap;
import com.medy.retrofitwrapper.WebRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlayerPointsDetailActivity extends AppBaseActivity {

    ImageView iv_player_image;
    TextView tv_player_credits;
    TextView tv_player_total_points;
    List<PlayerPointsModel> list = new ArrayList<>();
    private LinearLayout ll_runs_layout;
    private TextView tv_runs;
    private LinearLayout ll_four;
    private TextView tv_four;
    private LinearLayout ll_six;
    private TextView tv_six;
    private LinearLayout ll_wickets;
    private TextView tv_wickets;
    private LinearLayout ll_maiden_over;
    private TextView tv_maiden_overs;
    private LinearLayout ll_catch;
    private TextView tv_catch;
    private LinearLayout ll_stumped;
    private TextView tv_stumped;
    private LinearLayout ll_run_out;
    private TextView tv_run_out;
    private LinearLayout ll_fifty;
    private TextView tv_fifty;
    private LinearLayout ll_hundred;
    private TextView tv_hundred;
    private LinearLayout ll_four_wicket;
    private TextView tv_four_wicket;
    private LinearLayout ll_five_wicket;
    private TextView tv_five_wicket;
    private LinearLayout ll_duck;
    private TextView tv_duck;
    private LinearLayout ll_playing_point;
    private TextView tv_playing_point;
    private LinearLayout ll_sr;
    private TextView tv_sr;
    private LinearLayout ll_er;
    private TextView tv_er;
    private LinearLayout ll_thrower;
    private TextView tv_thrower;
    private LinearLayout ll_catcher;
    private TextView tv_catcher;
    private LinearLayout ll_total_points;
    private TextView tv_total_points;
    private TextView tv_selected_Na;
    private TextView tv_selected_pr;
    private Handler handler = new Handler();
    private TextView tv_did_not_play;
    UserPrefs userPrefs;

    private PlayerModel getPlayerModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String data = extras.getString(DATA);
            if (isValidString(data))
                return new Gson().fromJson(data, PlayerModel.class);
        }
        return null;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_player_points_detail;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        iv_player_image = findViewById(R.id.iv_player_image);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        tv_player_total_points = findViewById(R.id.tv_player_total_points);
        tv_selected_Na = findViewById(R.id.tv_selected_Na);
        tv_selected_pr = findViewById(R.id.tv_selected_pr);

        ll_runs_layout = findViewById(R.id.ll_runs_layout);
        tv_runs = findViewById(R.id.tv_runs);
        ll_four = findViewById(R.id.ll_four);
        tv_four = findViewById(R.id.tv_four);
        ll_six = findViewById(R.id.ll_six);
        tv_six = findViewById(R.id.tv_six);
        ll_wickets = findViewById(R.id.ll_wickets);
        tv_wickets = findViewById(R.id.tv_wickets);
        ll_maiden_over = findViewById(R.id.ll_maiden_over);
        tv_maiden_overs = findViewById(R.id.tv_maiden_overs);
        ll_catch = findViewById(R.id.ll_catch);
        tv_catch = findViewById(R.id.tv_catch);
        ll_stumped = findViewById(R.id.ll_stumped);
        tv_stumped = findViewById(R.id.tv_stumped);
        ll_run_out = findViewById(R.id.ll_run_out);
        tv_run_out = findViewById(R.id.tv_run_out);
        ll_fifty = findViewById(R.id.ll_fifty);
        tv_fifty = findViewById(R.id.tv_fifty);
        ll_hundred = findViewById(R.id.ll_hundred);
        tv_hundred = findViewById(R.id.tv_hundred);
        ll_four_wicket = findViewById(R.id.ll_four_wicket);
        tv_four_wicket = findViewById(R.id.tv_four_wicket);
        ll_five_wicket = findViewById(R.id.ll_five_wicket);
        tv_five_wicket = findViewById(R.id.tv_five_wicket);
        ll_duck = findViewById(R.id.ll_duck);
        tv_duck = findViewById(R.id.tv_duck);
        ll_playing_point = findViewById(R.id.ll_playing_point);
        tv_playing_point = findViewById(R.id.tv_playing_point);
        ll_sr = findViewById(R.id.ll_sr);
        tv_sr = findViewById(R.id.tv_sr);
        ll_er = findViewById(R.id.ll_er);
        tv_er = findViewById(R.id.tv_er);

        ll_thrower = findViewById(R.id.ll_thrower);
        tv_thrower = findViewById(R.id.tv_thrower);

        ll_catcher = findViewById(R.id.ll_catcher);
        tv_catcher = findViewById(R.id.tv_catcher);

        ll_total_points = findViewById(R.id.ll_total_points);
        tv_total_points = findViewById(R.id.tv_total_points);

        tv_did_not_play = findViewById(R.id.tv_did_not_play);

        updateViewVisibility(ll_runs_layout, View.GONE);
        updateViewVisibility(ll_four, View.GONE);
        updateViewVisibility(ll_six, View.GONE);
        updateViewVisibility(ll_wickets, View.GONE);
        updateViewVisibility(ll_maiden_over, View.GONE);
        updateViewVisibility(ll_catch, View.GONE);
        updateViewVisibility(ll_stumped, View.GONE);
        updateViewVisibility(ll_run_out, View.GONE);
        updateViewVisibility(ll_fifty, View.GONE);
        updateViewVisibility(ll_hundred, View.GONE);
        updateViewVisibility(ll_four_wicket, View.GONE);
        updateViewVisibility(ll_five_wicket, View.GONE);
        updateViewVisibility(ll_duck, View.GONE);
        updateViewVisibility(ll_playing_point, View.GONE);
        updateViewVisibility(ll_sr, View.GONE);
        updateViewVisibility(ll_er, View.GONE);
        updateViewVisibility(ll_thrower, View.GONE);
        updateViewVisibility(ll_catcher, View.GONE);

        updateViewVisibility(tv_did_not_play, View.GONE);

        setupHeader();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            UserPrefs userPrefs = getUserPrefs();
            if (userPrefs.getLoggedInUser() == null) {
                Log.e("GetplayerPercentage","NotValue");
                return;
            }
           // goToDashboardActivity(getIntent().getExtras());
            Log.e("GetplayerPercentage",""+userPrefs.getLoggedInUser());
        }
    };


    private void setupHeader() {
        if (getPlayerModel() == null) return;
        PlayerModel playerModel = getPlayerModel();
//        int points = playerModel.getPoints().isEmpty() ? 0 : Integer.parseInt(playerModel.getPoints());
        float points = Float.parseFloat(playerModel.getPoints());
        String getPlayings = playerModel.getIsplaying();
        //if (!playerModel.isPlaying() && points == 0) {
        if (getPlayings.equals("0")) {
            tv_did_not_play.setText("Not in playing XI");
          //  tv_selected_pr.setText("Selected : "+playerModel.getPlayerPercentage()+"%");
            updateViewVisibility(tv_did_not_play, View.VISIBLE);
        } else {
            getPlayerDetail();
        }
        setHeaderTitle(playerModel.getPname());
        tv_player_credits.setText(playerModel.getCreditText());
        tv_player_total_points.setText(String.valueOf(playerModel.getPts()));
        loadImage(this, iv_player_image, null, playerModel.getPimg(), R.drawable.no_image);
    }


    private void getPlayerDetail() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        if (matchModel == null) return;
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPlayerPointsDetail("", matchModel.getMatchid(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_PLAYER_POINTS_DETAILS:
                handlePlayerPointsResponse(webRequest);
                break;
        }
    }

    private void handlePlayerPointsResponse(WebRequest webRequest) {
        try {
            EmptyResponseModel responsePojo = webRequest.getResponsePojo(EmptyResponseModel.class);
            if (responsePojo == null || responsePojo.isError() || responsePojo.getData() == null)
                return;

            if (getPlayerModel() != null) {
                LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>) responsePojo.getData();
                for (Map.Entry<String, Object> stringObjectEntry : data.entrySet()) {
                    String key = stringObjectEntry.getKey();

                    if (key.equalsIgnoreCase(getPlayerModel().getPid())) {
                        Object value = stringObjectEntry.getValue();
                        LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap<String, Object>) value;
                        JSONObject object = new JSONObject();
                        for (Map.Entry<String, Object> objectEntry : treeMap.entrySet()) {
                            object.put(objectEntry.getKey(), objectEntry.getValue());
                        }

                        String string = object.toString();
                        PlayerPointsDetailModel pointsDetailModel = new Gson().fromJson(string, PlayerPointsDetailModel.class);
                        if (pointsDetailModel != null) {
                            setUpData(pointsDetailModel);
                        }
                    }
                }
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpData(PlayerPointsDetailModel model) {

        tv_selected_Na.setText(model.getPlayername());


        if (model.getRun() != 0) {
            tv_runs.setText(model.getValidFormatText(model.getRun()));
            updateViewVisibility(ll_runs_layout, View.VISIBLE);
        } else {
            updateViewVisibility(ll_runs_layout, View.VISIBLE);
        }

        if (model.getFour() != 0) {
            tv_four.setText(model.getValidFormatText(model.getFour()));
            updateViewVisibility(ll_four, View.VISIBLE);
        } else {
            updateViewVisibility(ll_four, View.VISIBLE);
        }

        if (model.getSix() != 0) {
            tv_six.setText(model.getValidFormatText(model.getSix()));
            updateViewVisibility(ll_six, View.VISIBLE);
        } else {
            updateViewVisibility(ll_six, View.VISIBLE);
        }

        if (model.getWicket() != 0) {
            tv_wickets.setText(model.getValidFormatText(model.getWicket()));
            updateViewVisibility(ll_wickets, View.VISIBLE);
        } else {
            updateViewVisibility(ll_wickets, View.VISIBLE);
        }

        if (model.getMdnover() != 0) {
            tv_maiden_overs.setText(model.getValidFormatText(model.getMdnover()));
            updateViewVisibility(ll_maiden_over, View.VISIBLE);
        } else {
            updateViewVisibility(ll_maiden_over, View.VISIBLE);
        }

        if (model.getCatchX() != 0) {
            tv_catch.setText(model.getValidFormatText(model.getCatchX()));
            updateViewVisibility(ll_catch, View.VISIBLE);
        } else {
            updateViewVisibility(ll_catch, View.VISIBLE);
        }

        if (model.getStumped() != 0) {
            tv_stumped.setText(model.getValidFormatText(model.getStumped()));
            updateViewVisibility(ll_stumped, View.VISIBLE);
        } else {
            updateViewVisibility(ll_stumped, View.VISIBLE);
        }

        if (model.getRunout() != 0) {
            tv_run_out.setText(model.getValidFormatText(model.getRunout()));
            updateViewVisibility(ll_run_out, View.VISIBLE);
        } else {
            updateViewVisibility(ll_run_out, View.VISIBLE);
        }

        if (model.getFiftyBonus() != 0) {
            tv_fifty.setText(model.getValidFormatText(model.getFiftyBonus()));
            updateViewVisibility(ll_fifty, View.VISIBLE);
        } else {
            updateViewVisibility(ll_fifty, View.VISIBLE);
        }

        if (model.getHundredBonus() != 0) {
            tv_hundred.setText(model.getValidFormatText(model.getHundredBonus()));
            updateViewVisibility(ll_hundred, View.VISIBLE);
        } else {
            updateViewVisibility(ll_hundred, View.VISIBLE);
        }

        if (model.getFourwhb() != 0) {
            tv_four_wicket.setText(model.getValidFormatText(model.getFourwhb()));
            updateViewVisibility(ll_four_wicket, View.VISIBLE);
        } else {
            updateViewVisibility(ll_four_wicket, View.VISIBLE);
        }

        if (model.getFivewhb() != 0) {
            tv_five_wicket.setText(model.getValidFormatText(model.getFivewhb()));
            updateViewVisibility(ll_five_wicket, View.VISIBLE);
        } else {
            updateViewVisibility(ll_five_wicket, View.VISIBLE);
        }

        if (model.getDuck() != 0) {
            tv_duck.setText(model.getValidFormatText(model.getDuck()));
            updateViewVisibility(ll_duck, View.VISIBLE);
        } else {
            updateViewVisibility(ll_duck, View.VISIBLE);
        }

        if (model.getPlayeingPoints() != 0) {
            tv_playing_point.setText(model.getValidFormatText(model.getPlayeingPoints()));
            updateViewVisibility(ll_playing_point, View.VISIBLE);
        } else {
            updateViewVisibility(ll_playing_point, View.VISIBLE);
        }

        if (model.getThrower() != 0) {
            tv_thrower.setText(model.getValidFormatText(model.getThrower()));
            updateViewVisibility(ll_thrower, View.VISIBLE);
        } else {
            updateViewVisibility(ll_thrower, View.VISIBLE);
        }

        if (model.getCatcher() != 0) {
            tv_catcher.setText(model.getValidFormatText(model.getCatcher()));
            updateViewVisibility(ll_catcher, View.VISIBLE);
        } else {
            updateViewVisibility(ll_catcher, View.VISIBLE);
        }

        if (model.getTotalEr() != 0) {
            tv_er.setText(model.getValidFormatText(model.getTotalEr()));
            updateViewVisibility(ll_er, View.VISIBLE);
        } else {
            updateViewVisibility(ll_er, View.VISIBLE);
        }

        if (model.getTotalSr() != 0) {
            tv_sr.setText(model.getValidFormatText(model.getTotalSr()));
            updateViewVisibility(ll_sr, View.VISIBLE);
        } else {
            updateViewVisibility(ll_sr, View.VISIBLE);
        }

        tv_total_points.setText(model.getValidFormatText(model.getTotalpoints()));
    }

}
