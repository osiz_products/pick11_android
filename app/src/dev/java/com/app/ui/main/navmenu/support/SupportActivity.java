package com.app.ui.main.navmenu.support;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.BuildConfig;
import com.app.appbase.AppBaseActivity;
import com.app.model.CommonSetting;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.ContactDetails;
import com.app.model.webrequestmodel.Matchhistory;
import com.app.preferences.UserPrefs;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;

public class SupportActivity extends AppBaseActivity {

    TextView tv_invite;
    TextView tv_contact_email;
    TextView tv_contact_mobile;
    CommonSetting setting;
    WebView setWebView;
    String getTitle,getEmail,getContent,getPhone;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_support;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        getContactDetails();

        userPrefs = new UserPrefs(this);
        tv_invite = findViewById(R.id.tv_invite);
        tv_contact_email = findViewById(R.id.tv_contact_email);
        tv_contact_mobile = findViewById(R.id.tv_contact_mobile);
        setWebView = findViewById(R.id.setWebView);
        tv_contact_email.setOnClickListener(this);
        tv_contact_mobile.setOnClickListener(this);

       /* UserModel userModel = getUserModel();
        if (userModel != null) {
            setting = userModel.getSetting();
            if (setting != null) {
                tv_invite.setText("Email: " );
                Log.e("Support_Email",""+getEmail);
                String string = setting.getSupportphone();

                Log.e("Support_Phone",""+getPhone);

                if (!isValidString(string)) {
                    //updateViewVisibility(tv_contact_mobile, View.GONE);
                }
                tv_contact_mobile.setText("Call us on: " + getPhone);

                String flavor = BuildConfig.FLAVOR;
                if (flavor.equalsIgnoreCase("pick11")) {
                   // updateViewVisibility(tv_contact_mobile, View.GONE);
                }
            }
        }*/

        String string = getResources().getString(R.string.support_phone_number);
        if (!isValidString(string)) {
            //updateViewVisibility(tv_contact_mobile, View.GONE);
        }
       // tv_contact_mobile.setText("Call us on: " + string);

       /* String flavor = BuildConfig.FLAVOR;
        if (flavor.equalsIgnoreCase("pick11")) {
           // updateViewVisibility(tv_contact_mobile, View.GONE);
        }*/

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case CONTACT_US_DETAILS:
                handleContactDetailsResponse(webRequest);
                break;
        }
    }

    private void getContactDetails() {
        getWebRequestHelper().getContactusDetails(this);
    }

        private void handleContactDetailsResponse(WebRequest webRequest) {
          //  ContactDetails extraData = webRequest.getExtraData(DATA);
            ContactDetails responseModel = webRequest.getResponsePojo(ContactDetails.class);
          getTitle = responseModel.getData().getTitle();
          getEmail = responseModel.getData().getEmail();
          getContent = responseModel.getData().getContent();
          getPhone = responseModel.getData().getPhone();

            tv_contact_email.setText(getEmail);
            tv_contact_mobile.setText("Call us on: " + getPhone);
            tv_invite.setText("Email: " + getEmail);

           /* setWebView.loadData(getContent, "text/html",
                    "utf-8");*/

            setWebView.getSettings().setLoadsImagesAutomatically(true);
            setWebView.getSettings().setJavaScriptEnabled(true);
            setWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            setWebView.loadData(getContent, "text/html","utf-8");
        }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_contact_email:
                try {
                    //Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                   // Uri data = Uri.parse("mailto:?subject=" + "" + "&body=" + "" + "&to=" + setting == null ? "" : setting.getSupportemail());
                   /* Uri data = Uri.parse("mailto:?subject=" + "" + "&body=" + "" + "&to=" + setting == null ? "" : "support@pick11.com");
                    mailIntent.setData(data);
                    startActivity(Intent.createChooser(mailIntent, "Send mail..."));*/

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts(
                            "mailto",getEmail, null));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(Intent.createChooser(intent, "Send mail..."));

                }catch (Exception e){

                }
                break;

            case R.id.tv_contact_mobile:
               // makeDirectCall(setting == null ? "" : setting.getSupportphone());
                //makeDirectCall(setting == null ? "" : "0123456789");
                try {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + getPhone));
                    startActivity(callIntent);
                } catch (ActivityNotFoundException e) {
                    showCustomToast("No app found for call.");
                }

                break;
        }
    }





}


