package com.app.ui.main.navmenu.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webresponsemodel.ProfileResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;



public class ProfileActivity extends AppBaseActivity implements ToolBarFragment.ToolbarFragmentInterFace {

    ImageView iv_profile_pic;
    ProgressBar pb_image;
    TextView tv_name;
    TextView tv_email;
    TextView tv_team_name;
    TextView tv_dob;
    TextView tv_mobile_number;
    TextView tv_password;
    TextView tv_gender;
    TextView tv_city;
    LinearLayout ll_state;
    TextView tv_state;
    TextView tv_address;

    LinearLayout ll_email;
    LinearLayout ll_team_name;
    LinearLayout ll_dob;
    LinearLayout ll_mobile_number;
    LinearLayout ll_password;
    LinearLayout ll_gender;
    LinearLayout ll_city;
    LinearLayout ll_address;
    TextView tv_change_passwords;
    CardView tv_change_password;
    UserPrefs userPrefs;
    ImageView bagheaderImg;
    ImageView bagheaderImg_black;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_profile;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();

        userPrefs = new UserPrefs(this);
        iv_profile_pic = findViewById(R.id.iv_profile_pic);
        pb_image = findViewById(R.id.pb_image);
        tv_name = findViewById(R.id.tv_name);
        tv_email = findViewById(R.id.tv_email);
        tv_team_name = findViewById(R.id.tv_team_name);
        tv_dob = findViewById(R.id.tv_dob);
        tv_mobile_number = findViewById(R.id.tv_mobile_number);
        tv_password = findViewById(R.id.tv_password);
        tv_gender = findViewById(R.id.tv_gender);
        tv_city = findViewById(R.id.tv_city);
        ll_state = findViewById(R.id.ll_state);
        tv_state = findViewById(R.id.tv_state);
        tv_address = findViewById(R.id.tv_address);
        bagheaderImg = findViewById(R.id.bagheaderImg);
        bagheaderImg_black = findViewById(R.id.bagheaderImg_black);

        ll_email = findViewById(R.id.ll_email);
        ll_team_name = findViewById(R.id.ll_team_name);
        ll_dob = findViewById(R.id.ll_dob);
        ll_mobile_number = findViewById(R.id.ll_mobile_number);
        ll_password = findViewById(R.id.ll_password);
        ll_gender = findViewById(R.id.ll_gender);
        ll_city = findViewById(R.id.ll_city);
        ll_address = findViewById(R.id.ll_address);
        tv_change_password = findViewById(R.id.tv_change_password);

        updateViewVisibility(ll_password, View.GONE);
        tv_change_password.setOnClickListener(this);
        getUserDetail();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            bagheaderImg.setVisibility(View.VISIBLE);
            bagheaderImg_black.setVisibility(View.GONE);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            bagheaderImg_black.setVisibility(View.VISIBLE);
            bagheaderImg.setVisibility(View.GONE);
        }
    }

    private void getUserDetail() {
        getWebRequestHelper().getProfileDetail(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_change_password:
                goToChangePasswordActivity(null);
                break;
        }
    }

    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_profile:
                goToEditProfileActivity(null);
                break;
        }
    }

    public void goToEditProfileActivity(Bundle bundle) {
        Intent intent = new Intent(this, EditProfileActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    public void goToChangePasswordActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupUserDetail();
    }

    private void setupUserDetail() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            tv_name.setText(userModel.getName());
            tv_email.setText(userModel.getEmail());
            tv_team_name.setText(userModel.getTeamname());
            tv_dob.setText(userModel.getFormattedDate(3));
            tv_mobile_number.setText(userModel.getPhone());
            tv_gender.setText(userModel.capitalize(userModel.getGender()));
            tv_city.setText(userModel.getCity());
            tv_state.setText(userModel.getState());
            tv_address.setText(userModel.getAddress());

            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                loadImage(this, iv_profile_pic, pb_image, imageUrl,
                        R.drawable.no_image, 300);
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibility(pb_image, View.INVISIBLE);
            }
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_GET_USER_PROFILE:
                handleUserProfileResponse(webRequest);
                break;
        }
    }

    private void handleUserProfileResponse(WebRequest webRequest) {
        ProfileResponseModel responseModel = webRequest.getResponsePojo(ProfileResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError() && responseModel.getData() != null) {
            if (isFinishing()) return;
            UserModel userModel = responseModel.getData();
            if (userModel == null) return;
            UserModel userModel1 = getUserModel();
            userModel1.setIstnameedit(userModel.getIstnameedit());
            getUserPrefs().updateLoggedInUser(userModel1);

        }
    }
}
