package com.app.ui.main.navmenu.livescore.details.fragment.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.FieldingModel;
import com.pickeleven.R;

import java.util.List;

public class FieldingAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<FieldingModel.ScoresBean> list;

    public FieldingAdapter(Context context, List<FieldingModel.ScoresBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_feilding));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tv_bowler_name;
        private TextView tv_bowled;
        private TextView tv_catch;
        private TextView tv_lbw;
        private TextView tv_stumped;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_bowler_name = itemView.findViewById(R.id.tv_bowler_name);
            tv_bowled = itemView.findViewById(R.id.tv_bowled);
            tv_catch = itemView.findViewById(R.id.tv_catch);
            tv_lbw = itemView.findViewById(R.id.tv_lbw);
            tv_stumped = itemView.findViewById(R.id.tv_stumped);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            FieldingModel.ScoresBean data = list.get(position);
            tv_bowler_name.setText(data.getName());
            tv_bowled.setText(data.getBowled());
            tv_catch.setText(data.getCatchX());
            tv_lbw.setText(data.getLbw());
            tv_stumped.setText(data.getStumped());
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
