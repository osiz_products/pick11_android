package com.app.ui.main.kabaddi.myteam.createTeam.fragment.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.PlayerModel;
import com.pickeleven.R;

import java.util.List;

public class PlayersAdapter extends AppBaseRecycleAdapter implements Filterable {

    private Context context;
    List<PlayerModel> list;

    public PlayersAdapter(Context context, List<PlayerModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_players));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public PlayerModel getItem(int position) {
        return this.list.get(position);
    }

    public boolean isMaxPlayerSelected() {
        return false;
    }

    public boolean isMaxFrom1Team(String teamId) {
        return false;
    }

    public boolean isMaxPlayerType() {
        return false;
    }

    public String getPlayerTypeName(PlayerModel team_id) {
        return "";
    }

    public boolean isMinPlayerFailed() {
        return false;
    }

    public boolean isCreditExceed(float playerCredit) {
        return false;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    private class ViewHolder extends BaseViewHolder {
        private LinearLayout ll_layout;
        private RelativeLayout rl_player_image;
        private ImageView iv_player_image;
        private ProgressBar pb_image;
        private TextView tv_player_name;
        private TextView tv_team_type;
        private TextView tv_player_points;
        private TextView tv_credit;
        private ImageView iv_add_player;
        private LinearLayout ll_playing_11;
        private ImageView iv_playing;
        private TextView tv_playing;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            rl_player_image = itemView.findViewById(R.id.rl_player_image);
            iv_player_image = itemView.findViewById(R.id.iv_player_image);
            pb_image = itemView.findViewById(R.id.pb_image);
            tv_player_name = itemView.findViewById(R.id.tv_player_name);
            tv_team_type = itemView.findViewById(R.id.tv_team_type);
            tv_player_points = itemView.findViewById(R.id.tv_player_points);
            tv_credit = itemView.findViewById(R.id.tv_credit);
            iv_add_player = itemView.findViewById(R.id.iv_add_player);
            ll_playing_11 = itemView.findViewById(R.id.ll_playing_11);
            iv_playing = itemView.findViewById(R.id.iv_playing);
            tv_playing = itemView.findViewById(R.id.tv_playing);
            updateViewVisibitity(ll_playing_11, View.GONE);

        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            rl_player_image.setTag(position);
            rl_player_image.setOnClickListener(this);
            iv_add_player.setTag(position);
            iv_add_player.setOnClickListener(this);
            PlayerModel playerModel = list.get(position);
            if (!playerModel.getPimg().isEmpty()) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_player_image, pb_image,
                        playerModel.getPimg(), R.drawable.no_image, 100);
            } else {
                updateViewVisibitity(pb_image, View.GONE);
                iv_player_image.setImageDrawable(getContext().getResources().getDrawable(R.drawable.no_image));
            }
            tv_player_name.setText(playerModel.getPname());
//            tv_team_type.setText(playerModel.getTeam_type());
            tv_team_type.setText(Html.fromHtml(getPlayerTypeName(playerModel)), TextView.BufferType.SPANNABLE);
            tv_player_points.setText(String.valueOf(playerModel.getPts()));
            tv_credit.setText(String.valueOf(playerModel.getCredit()));

            if (playerModel.isPlayingVisible()) {
                updateViewVisibitity(ll_playing_11, View.VISIBLE);
                if (playerModel.isPlaying()) {
                    tv_playing.setText("Playing");
                    tv_playing.setTextColor(getContext().getResources().getColor(R.color.color_green_dark));
                    ll_playing_11.setActivated(true);
                } else {
                    tv_playing.setText("Not playing");
                    tv_playing.setTextColor(getContext().getResources().getColor(R.color.color_red));
                    ll_playing_11.setActivated(false);
                }
            } else {
                updateViewVisibitity(ll_playing_11, View.GONE);
            }

            if (playerModel.isSelected()) {
                iv_add_player.setActivated(true);
                ll_layout.setActivated(true);
            } else {
                iv_add_player.setActivated(false);
                ll_layout.setActivated(false);
            }

            if (!playerModel.isSelected() && (isMaxPlayerSelected()
                    || isMaxFrom1Team(playerModel.getTeamname())
                    || isMaxPlayerType()
                    || isMinPlayerFailed()
                    || isCreditExceed(playerModel.getCredit()))) {
                itemView.setAlpha(0.45f);
            } else {
                itemView.setAlpha(1.0f);
            }
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
