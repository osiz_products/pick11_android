package com.app.ui.main.navmenu.livescore.details.fragment.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.fullScoreBoard.CustomScoreModel;
import com.pickeleven.R;

import java.util.List;

public class BatsManAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<CustomScoreModel.PlayerBean> list;

    public BatsManAdapter(Context context, List<CustomScoreModel.PlayerBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_batsman));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tv_batsman_name;
        private TextView tv_r;
        private TextView tv_b;
        private TextView tv_four;
        private TextView tv_six;
        private TextView tv_sr;
        private TextView tv_out_str;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_batsman_name = itemView.findViewById(R.id.tv_batsman_name);
            tv_four = itemView.findViewById(R.id.tv_four);
            tv_six = itemView.findViewById(R.id.tv_six);
            tv_b = itemView.findViewById(R.id.tv_b);
            tv_r = itemView.findViewById(R.id.tv_r);
            tv_sr = itemView.findViewById(R.id.tv_sr);
            tv_out_str = itemView.findViewById(R.id.tv_out_str);

            updateViewVisibitity(tv_out_str, View.GONE);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            CustomScoreModel.PlayerBean data = list.get(position);
            tv_batsman_name.setText(data.getFullname());
            tv_r.setText(data.getRuns());
            tv_b.setText(data.getBalls());
            tv_four.setText(data.getFours());
            tv_six.setText(data.getSixes());
            tv_sr.setText(data.getStrike_rate());
            tv_out_str.setText(data.getOut_str());
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
