package com.app.ui.main.football.main.live.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.MatchModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;

import java.util.List;

public class LiveAdapter extends AppBaseRecycleAdapter {

    private List<MatchModel> list;

    public LiveAdapter(List<MatchModel> list) {
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_fixtures));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public MatchModel getItem(int position) {
        return list == null ? null : list.get(position);
    }

    private class ViewHolder extends BaseViewHolder {
        private ImageView iv_image_first;
        private ProgressBar pb_image_first;
        private TextView tv_team_one;
        private ImageView iv_image_second;
        private ProgressBar pb_image_second;
        private TextView tv_team_two;
        private TextView tv_match_name;
        private TextView tv_match_type;
        private TextView tv_match_start_time;
        private TextView tv_series_name;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_image_first = itemView.findViewById(R.id.iv_image_first);
            pb_image_first = itemView.findViewById(R.id.pb_image_first);
            tv_team_one = itemView.findViewById(R.id.tv_team_one);
            iv_image_second = itemView.findViewById(R.id.iv_image_second);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            tv_team_two = itemView.findViewById(R.id.tv_team_two);
            tv_match_name = itemView.findViewById(R.id.tv_match_name);
            tv_match_type = itemView.findViewById(R.id.tv_match_type);
            tv_match_start_time = itemView.findViewById(R.id.tv_match_start_time);
            tv_series_name = itemView.findViewById(R.id.tv_series_name);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            MatchModel matchModel = list.get(position);
            if (matchModel == null) return null;

            if (!matchModel.getSeriesname().isEmpty()) {
                updateViewVisibitity(tv_series_name, View.VISIBLE);
                tv_series_name.setText(matchModel.getSeriesname());
            } else {
                updateViewVisibitity(tv_series_name, View.GONE);
            }

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_first, pb_image_first, matchModel.getTeam1logo(),
                    R.drawable.dummy_logo, 200);

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_second, pb_image_second, matchModel.getTeam2logo(),
                    R.drawable.dummy_logo, 200);

            tv_match_name.setText(matchModel.getMatchname());
            tv_team_one.setText(matchModel.getTeam1());
            tv_team_two.setText(matchModel.getTeam2());
            tv_match_type.setText(matchModel.capitalize(matchModel.getMtype()));
            tv_match_start_time.setText(matchModel.getRemainTimeText(MyApplication.getInstance().getServerdate()));
            tv_match_start_time.setTextColor(getContext().getResources().getColor(matchModel.getTimerColor()));
            return null;
        }
    }
}
