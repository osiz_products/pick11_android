package com.app.ui.main.cricket.privateContests.prizebreakup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.IdBean;
import com.app.model.MatchModel;
import com.app.model.PrivateBreakUpModel;
import com.app.model.TeamModel;
import com.app.model.WinnerBreakUpRankModel;
import com.app.model.WinnerLavelModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.model.webresponsemodel.TeamResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.cricket.privateContests.prizebreakup.adapter.PricePoolBreakUpAdapter;
import com.app.ui.main.cricket.privateContests.prizebreakup.dialog.PrizeBreakDialog;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class PricePoolBreakUpActivity extends AppBaseActivity {

    private TextView tv_contest_size;
    private TextView tv_prize_pool;
    private TextView tv_entry_fees;
    private LinearLayout ll_winners;
    private TextView tv_winners;
    private RecyclerView recycler_view;
    private PricePoolBreakUpAdapter adapter;
    private TextView tv_create_contest;
    private List<WinnerBreakUpRankModel> list = new ArrayList<>();
    private List<WinnerLavelModel> winnersLabels;
    private WinnerLavelModel winnerLavelModel;
    private int selectedPosition = 0;
    private List<TeamModel> myTeams = new ArrayList<>();
    UserPrefs userPrefs;

    public PrivateBreakUpModel getPrivateBreakUpModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString(DATA);
            if (isValidString(string)) {
                return new Gson().fromJson(string, PrivateBreakUpModel.class);
            }
            return null;
        }
        return null;
    }

    public boolean isTeamCreated() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return extras.getBoolean(DATA2);
        return false;
    }

    public PrizePoolRequestModel getPrizePoolRequestModel() {
        return MyApplication.getInstance().getPrizePoolRequestModel();
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    public MatchModel getSelectedMatch() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_price_pool_break_up;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_contest_size = findViewById(R.id.tv_contest_size);
        tv_prize_pool = findViewById(R.id.tv_prize_pool);
        tv_entry_fees = findViewById(R.id.tv_entry_fees);
        ll_winners = findViewById(R.id.ll_winners);
        tv_winners = findViewById(R.id.tv_winners);
        recycler_view = findViewById(R.id.recycler_view);
        tv_create_contest = findViewById(R.id.tv_create_contest);

        ll_winners.setOnClickListener(this);
        tv_create_contest.setOnClickListener(this);

        initializeRecyclerView();
        setUpData();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setUpData() {
        PrivateBreakUpModel privateBreakUpModel = getPrivateBreakUpModel();
        if (privateBreakUpModel != null) {
            PrizePoolRequestModel prizePoolRequestModel = getPrizePoolRequestModel();
            if (prizePoolRequestModel != null) {
                tv_contest_size.setText(prizePoolRequestModel.contestsize);
                tv_prize_pool.setText(currency_symbol + prizePoolRequestModel.tolwinprize);
                tv_entry_fees.setText(currency_symbol + prizePoolRequestModel.entry_fees);
            }
            winnersLabels = privateBreakUpModel.getWinnerslabs();
            if (winnersLabels != null && winnersLabels.size() > 0) {
                winnerLavelModel = winnersLabels.get(selectedPosition);
                if (winnerLavelModel != null) {
                    tv_winners.setText(winnerLavelModel.getWinner() + " Winners");
                    List<WinnerBreakUpRankModel> ranks = winnerLavelModel.getRanks();
                    list.addAll(ranks);
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new PricePoolBreakUpAdapter(this, list);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_winners:
                showDialog();
                break;

            case R.id.tv_create_contest:
                getAllTeam();
                break;
        }
    }


    private void getAllTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false);
        getWebRequestHelper().getAllTeam(matchModel, this);
    }

    private void createContest(boolean isTeamCreated) {
        PrizePoolRequestModel prizePoolRequestModel = getPrizePoolRequestModel();
        MatchModel selectedMatch = getSelectedMatch();
        if (prizePoolRequestModel == null && selectedMatch == null) return;

        JoinContestRequestModel requestModel = new JoinContestRequestModel();
        requestModel.atype = PRE_JOIN;
        requestModel.contestsize = prizePoolRequestModel.contestsize;
        requestModel.ismultiple = prizePoolRequestModel.type;
        requestModel.joinfees = prizePoolRequestModel.entry_fees;
        requestModel.matchid = selectedMatch.getMatchid();
        requestModel.name = prizePoolRequestModel.name;
        IdBean id = winnerLavelModel.get_id();
        requestModel.prizebreakid = id == null ? "" : id.get$oid();
        requestModel.winners = winnerLavelModel.getWinner();
        requestModel.winningprize = prizePoolRequestModel.tolwinprize;

        Bundle bundle = new Bundle();
//        if (isTeamCreated) {
            bundle.putBoolean(DATA, true);
            bundle.putBoolean(DATA5, true);
            bundle.putString(DATA3, new Gson().toJson(requestModel));
            goToSwitchTeamActivity(bundle);
//        } else {
//            bundle.putBoolean(DATA, true);
//            bundle.putString(DATA3, new Gson().toJson(requestModel));
//            goToCreateTeamActivity(null);
//        }
    }

    private void showDialog() {
        PrizeBreakDialog instance = PrizeBreakDialog.getInstance(null);
        instance.setWinnerLavelModel(winnersLabels);
        instance.setSelectedPosition(selectedPosition);
        instance.setPrizeBreakDialogListener(new PrizeBreakDialog.PrizeBreakDialogListener() {
            @Override
            public void onPositionSelect(int position, WinnerLavelModel winnerLavelModels) {
                if (adapter != null) {
                    winnerLavelModel = winnerLavelModels;
                    tv_winners.setText(winnerLavelModels.getWinner() + " Winners");
                    selectedPosition = position;
                    list.clear();
                    list.addAll(winnerLavelModels.getRanks());
                    adapter.notifyDataSetChanged();
                }
            }
        });
        instance.show(getFm(), instance.getClass().getSimpleName());
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_CREATE_PRIVATE_CONTEST:
                handleCustomerPreJoinContestResponse(webRequest);
                break;

            case ID_ALL_TEAM:
                handleAllTeamsResponse(webRequest);
                break;
        }
    }

    private void handleCustomerPreJoinContestResponse(WebRequest webRequest) {
        PreJoinResponseModel responsePojo = webRequest.getResponsePojo(PreJoinResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            PreJoinResponseModel.PreJoinedModel data = responsePojo.getData();
        } else {
            String msg = responsePojo.getMsg();
            if (isValidString(msg))
                showErrorMsg(responsePojo.getMsg());
        }
    }

    private void handleAllTeamsResponse(WebRequest webRequest) {
        TeamResponseModel responseModel = webRequest.getResponsePojo(TeamResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            createContest(true);
            TeamResponseModel.DataBean data = responseModel.getData();
            if (data == null) return;
            List<TeamModel> teams = data.getTeams();
            myTeams.clear();
            if (teams != null && teams.size() > 0) {
                myTeams.addAll(teams);
            }
        } else {
            createContest(false);
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }


    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_CREATE_PRIVATE_CONTEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_PRIVATE_CONTEST) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra(DATA, data.getStringExtra(DATA));
                setResult(RESULT_OK, intent);
                supportFinishAfterTransition();
            }
        }
    }
}
