package com.app.ui.main.cricket.joinedContests;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestModel;
import com.app.model.MatchModel;
import com.app.model.Matchstatus;
import com.app.model.NewScoreBoardModel;
import com.app.model.webresponsemodel.JoinContestResponseModel;
import com.app.model.webresponsemodel.NewScoreBoardResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.contestDetail.ContestsDetailActivity;
import com.app.ui.main.cricket.joinedContests.adapter.JoinedContestsAdapter;
import com.app.ui.main.cricket.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.cricket.privateContests.ShareActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.livescore.details.LiveScoreDetailActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.rest.WebServices;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class JoinedContestsActivity extends AppBaseActivity implements MatchTimerListener, ToolBarFragment.ToolbarFragmentInterFace {

    private TextView tv_team_one;
    private LinearLayout ll_timer;
    private ImageView iv_clock;
    private TextView tv_timer_time;
    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    JoinedContestsAdapter adapter;
    TextView tv_no_record_found;
    private List<ContestModel> list = new ArrayList();
    NestedScrollView view_nested_scroll;
    //Score board
    CardView ll_score_card;
    LinearLayout ll_score_data;
    TextView tv_team1;
    TextView tv_team1_score;
    TextView tv_team2;
    TextView tv_team2_score;
    TextView tv_win;
    RelativeLayout rl_view_full_score;
    UserPrefs userPrefs;
    String getmatchid,getpoolcontestid,gameid;
    String GgetMatchStatus;

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_joined_contests;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        if (getmatchid!=null) {
            getmatchid = getIntent().getExtras().getString("matchid");
        }
        if(getpoolcontestid!=null) {
            getpoolcontestid = getIntent().getExtras().getString("poolcontestid");
        }
        if (gameid!=null) {
            gameid = getIntent().getExtras().getString("gameid");
        }

        userPrefs = new UserPrefs(this);
        tv_team_one = findViewById(R.id.tv_team_one);
        ll_timer = findViewById(R.id.ll_timer);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);
        view_nested_scroll = findViewById(R.id.view_nested_scroll);
        //Score board
        ll_score_card = findViewById(R.id.ll_score_card);
        updateViewVisibility(ll_score_card, View.GONE);
        ll_score_data = findViewById(R.id.ll_score_data);
        tv_team1 = findViewById(R.id.tv_team1);
        tv_team1_score = findViewById(R.id.tv_team1_score);
        tv_team2 = findViewById(R.id.tv_team2);
        tv_team2_score = findViewById(R.id.tv_team2_score);
        tv_win = findViewById(R.id.tv_win);
        rl_view_full_score = findViewById(R.id.rl_view_full_score);
        rl_view_full_score.setOnClickListener(this);

        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);

        initializeRecyclerView();
        setupSwipeLayout();
        getJoinedContestUrl();
        getLiveScoreBoardUrl();
        getMatchStatus();

        if (getMatchModel().isFixtureMatch()) {
            updateViewVisibility(iv_clock, View.VISIBLE);
        } else {
            updateViewVisibility(iv_clock, View.GONE);
        }

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void getMatchStatus() {
        Log.e("callStatus","callStatus==========");

        getWebRequestHelper().getMatchStatus(getMatchModel(), this);
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJoinedContestUrl();
                getLiveScoreBoardUrl();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new JoinedContestsAdapter(this, list);
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);

        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                ContestModel contestModel = list.get(position);
                Bundle bundle = new Bundle();
                switch (v.getId()) {
                    case R.id.iv_share:
                        bundle.putString(DATA, new Gson().toJson(contestModel));
                        goToShareActivity(bundle);
                        break;

                    case R.id.tv_entry_fees:
                        if (contestModel.getJoinleft() > 0) {
                            if (list.size() > 0 && contestModel.getJoinleft() > 0) {
                                bundle.putBoolean(DATA, true);
                                bundle.putString(DATA2, new Gson().toJson(contestModel));
                                bundle.putString(DATA1, String.valueOf(contestModel.getId()));
                                goToSwitchTeamActivity(bundle);
                            }
                        }else {
                            showErrorMsg("Contest already full");
                        }
                        break;

                    default:
                        if (contestModel.isCancel()) return;
                        bundle.putString(DATA, String.valueOf(contestModel.getPoolcontestid()));
                        goToContestsDetailActivity(bundle);
                        break;
                }

            }
        });
    }

    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_view_full_score:
//                Bundle bundle = new Bundle();
//                bundle.putString(DATA, getMatchModel().getMatchid());
//                goToLiveScoreDetailActivity(bundle);
                Bundle bundle = new Bundle();
                String url = String.format(WebServices.WebScoreUrl(), MyApplication.getInstance().getGemeType(), getMatchModel().getMatchid());
                bundle.putString(DATA, url);
                bundle.putString(DATA2, "Full Score Card");
                goToWebViewActivity(bundle);
                break;
        }
    }

    private void goToLiveScoreDetailActivity(Bundle bundle) {
        Intent intent = new Intent(this, LiveScoreDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToContestsDetailActivity(Bundle bundle) {
        Intent intent = new Intent(this, ContestsDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        Log.e("Call_Win_Contest",""+bundle);

        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToShareActivity(Bundle bundle) {
        Intent intent = new Intent(this, ShareActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    private void getJoinedContestUrl() {
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getJoinedContests(getMatchModel(), this);
    }

    private void getLiveScoreBoardUrl() {
        getWebRequestHelper().getScoreBoardUrl(getMatchModel(), "score", this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        swipeRefresh.setRefreshing(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_JOINED_CONTEST:
                handleJoinedContestsResponse(webRequest);
                break;
            case ID_SCORE_BOARD:
                handleScoreBoardResponse(webRequest);
                break;

            case MATCH_STATUS_UPCOMING:
                Log.e("callStatus","callStatus==========2");
                handleMatchStatusResponse(webRequest);
                break;
        }
    }

    private void handleMatchStatusResponse(WebRequest webRequest) {

        Log.e("callStatus","callStatus==========3");
        Matchstatus responseModel = webRequest.getResponsePojo(Matchstatus.class);

        GgetMatchStatus = responseModel.getMatch_status();
                Log.e("GET_MatchStatus",responseModel.getMatch_status());


    }



    private void handleJoinedContestsResponse(WebRequest webRequest) {
        JoinContestResponseModel responseModel = webRequest.getResponsePojo(JoinContestResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<ContestModel> data = responseModel.getData();
            list.clear();
            if (data != null && data.size() > 0) {
                list.addAll(data);
            }
            adapter.notifyDataSetChanged();
            updateView(data);
        } else {
            updateView(null);
        }
    }

    private void handleScoreBoardResponse(WebRequest webRequest) {
        NewScoreBoardResponseModel responseModel = webRequest.getResponsePojo(NewScoreBoardResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                NewScoreBoardModel scoreBoardModel = responseModel.getData();
                if (scoreBoardModel != null) {
                    onScoreBoardUpdate(scoreBoardModel);
                }
            }
        }
    }

    private void updateView(List<ContestModel> data) {
        if (data != null && data.size() > 0)
            updateViewVisibility(tv_no_record_found, View.GONE);
        else {
            tv_no_record_found.setText("Joined contest not found.");
            updateViewVisibility(tv_no_record_found, View.VISIBLE);
        }
    }

    @Override
    public void onToolbarItemClick(View view) {
        goToMyAccountActivity(null);
    }

    public void goToMyAccountActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onResume() {
        super.onResume();
//        getScoreBoardHelper().setMatchModel(getMatchModel());
//        getScoreBoardHelper().startScoreBoardHelper();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
//        getScoreBoardHelper().stopScoreBoardHelper();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }

    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            if (getMatchModel().isUnderReview()) {
                tv_timer_time.setText("Under Review");
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            } else {
                tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            }
        }
    }

    public void onScoreBoardUpdate(NewScoreBoardModel scoreBoardModel) {
        Log.e("ScoreBoard",""+scoreBoardModel);

        if (scoreBoardModel != null && (scoreBoardModel.isMatchCompleted() || scoreBoardModel.isMatchStarted())) {
            updateViewVisibility(ll_score_card, View.VISIBLE);
            NewScoreBoardModel.MsgInfoBean msg_info = scoreBoardModel.getMsg_info();



            if (msg_info != null) {
                tv_win.setText(msg_info.getCompleted());
                updateViewVisibility(tv_win, View.VISIBLE);
            } else {
                updateViewVisibility(tv_win, View.GONE);
            }

            NewScoreBoardModel.InningsdetailBean inningsdetail = scoreBoardModel.getInningsdetail();
            if (inningsdetail != null) {
                NewScoreBoardModel.InningsdetailBean.ABean a = inningsdetail.getA();
                NewScoreBoardModel.InningsdetailBean.BBean b = inningsdetail.getB();
                NewScoreBoardModel.InningsdetailBean.TeamABean team_a = inningsdetail.getTeam_a();
                NewScoreBoardModel.InningsdetailBean.TeamBBean team_b = inningsdetail.getTeam_b();

                if (a != null) {
                    tv_team1_score.setText(a.get_$1());
                } else {
                    updateViewVisibility(tv_team1_score, View.GONE);
                }

                if (team_a != null) {
                    tv_team1.setText(team_a.getShort_name());
                }

                if (b != null) {
                    tv_team2_score.setText(b.get_$1());
                } else {
                    updateViewVisibility(tv_team2_score, View.GONE);
                }
                if (team_b != null) {
                    tv_team2.setText(team_b.getShort_name());
                }
            } else {
                updateViewVisibility(ll_score_card, View.GONE);
            }
        } else {
            updateViewVisibility(ll_score_card, View.GONE);
        }
    }
}