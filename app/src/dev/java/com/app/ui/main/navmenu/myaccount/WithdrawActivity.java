package com.app.ui.main.navmenu.myaccount;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.WalletModel;
import com.app.model.webrequestmodel.WithdrawRequestResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.main.navmenu.verification.VerificationActivity;
import com.bumptech.glide.Glide;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;

public class WithdrawActivity extends AppBaseActivity {

    EditText tv_add_balance;
    TextView tv_winnings_balance, tv_100, tv_200, tv_300, tv_continue;
    LinearLayout ll_continue;
    UserPrefs userPrefs;
    ImageView imageView;

    public WalletModel getWalletModel() {
        return getUserModel().getWalletModel();
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_withdraw;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_winnings_balance = findViewById(R.id.tv_winnings_balance);
        tv_add_balance = findViewById(R.id.tv_add_balance);
        tv_100 = findViewById(R.id.tv_100);
        tv_200 = findViewById(R.id.tv_200);
        tv_300 = findViewById(R.id.tv_300);
        ll_continue = findViewById(R.id.ll_continue);
        tv_continue = findViewById(R.id.tv_continue);
        imageView = findViewById(R.id.imageView);

        tv_100.setOnClickListener(this);
        tv_200.setOnClickListener(this);
        tv_300.setOnClickListener(this);
        tv_continue.setOnClickListener(this);

        if (getWalletModel() != null) {
            tv_winnings_balance.setText(currency_symbol + getWalletModel().getWltwin());
        } else
            tv_winnings_balance.setText(currency_symbol + "0.0");

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

           /* Glide.with(this)
                    .load(R.mipmap.header)
                    .centerCrop()
                    .placeholder(R.mipmap.header)
                    .into(imageView);*/

        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

          /*  Glide.with(this)
                    .load(R.mipmap.header_black)
                    .centerCrop()
                    .placeholder(R.mipmap.header_black)
                    .into(imageView);*/

        }

    }

    @Override
    public void beforeSetContentView() {
        if (getUserModel() != null) {
            UserModel userModel = getUserModel();
            if (!userModel.isWithdrawAvailable()) {
                goToVerificationActivity(null);
                supportFinishAfterTransition();
            }
        }
        super.beforeSetContentView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_100:
                updateValue(100);
                break;

            case R.id.tv_200:
                updateValue(200);
                break;

            case R.id.tv_300:
                updateValue(300);
                break;

            case R.id.tv_continue:
                addBalance();
                break;
        }
    }

    private void updateValue(int value) {
        int totalValue = 0;
        String trim = tv_add_balance.getText().toString().trim();
        if (isValidString(trim)) {
            totalValue = Integer.parseInt(trim);
            tv_add_balance.setText(String.valueOf(totalValue + value));
        } else {
            tv_add_balance.setText(String.valueOf(totalValue + value));
        }
    }

    private void addBalance() {
        String amount = tv_add_balance.getText().toString().trim();
        if (amount.isEmpty()) {
            showErrorMsg("Please enter amount.");
            return;
        }

        long amt = Long.parseLong(amount);
        if (amt < MIN_AMOUNT) {
            showErrorMsg("Amount cannot be less than " + MIN_AMOUNT);
            return;
        }
        if (getWalletModel() != null) {
            double wltwin = getWalletModel().getWltwin();
            if (amt > wltwin) {
                showErrorMsg("Amount cannot be greater than " + getWalletModel().getWltwin());
                return;
            }
        }
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().withdrawRequest(amount, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_WITHDRAW_REQUEST:
                handleWithdrawRequestResponse(webRequest);
                break;
        }
    }

    private void handleWithdrawRequestResponse(WebRequest webRequest) {
        WithdrawRequestResponseModel responseModel = webRequest.getResponsePojo(WithdrawRequestResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            tv_add_balance.setText("");
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void goToVerificationActivity(Bundle bundle) {
        Intent intent = new Intent(this, VerificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
