package com.app.ui.main.kabaddi.myteam.playerDetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.KabaddiPlayerPointsDetailModel;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPointsModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.LinkedTreeMap;
import com.medy.retrofitwrapper.WebRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created on 1/7/19.
 */
public class PlayerPointsDetailActivity extends AppBaseActivity {

    ImageView iv_player_image;
    TextView tv_player_credits;
    TextView tv_player_total_points;
    List<PlayerPointsModel> list = new ArrayList<>();
    private LinearLayout ll_playing_points;
    private TextView tv_playing_points;
    private LinearLayout ll_green_card;
    private TextView tv_green_card;
    private LinearLayout ll_yellow_card;
    private TextView tv_yellow_card;
    private LinearLayout ll_red_card;
    private TextView tv_red_card;
    private LinearLayout ll_touch;
    private TextView tv_touch;
    private LinearLayout ll_red_bonus;
    private TextView tv_red_bonus;
    private LinearLayout ll_unsuccess_raid;
    private TextView tv_unsuccess_raid;
    private LinearLayout ll_success_tackle;
    private TextView tv_success_tackle;
    private LinearLayout ll_super_tackle;
    private TextView tv_super_tackle;
    private LinearLayout ll_push_all_out;
    private TextView tv_push_all_out;
    private LinearLayout ll_get_all_out;
    private TextView tv_get_all_out;

    private LinearLayout ll_total_points;
    private TextView tv_total_points;

    private TextView tv_did_not_play;
    UserPrefs userPrefs;

    private PlayerModel getPlayerModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String data = extras.getString(DATA);
            if (isValidString(data))
                return new Gson().fromJson(data, PlayerModel.class);
        }
        return null;
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_kabaddi_player_points_detail;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        iv_player_image = findViewById(R.id.iv_player_image);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        tv_player_total_points = findViewById(R.id.tv_player_total_points);

        ll_playing_points = findViewById(R.id.ll_playing_points);
        tv_playing_points = findViewById(R.id.tv_playing_points);
        ll_green_card = findViewById(R.id.ll_green_card);
        tv_green_card = findViewById(R.id.tv_green_card);
        ll_yellow_card = findViewById(R.id.ll_yellow_card);
        tv_yellow_card = findViewById(R.id.tv_yellow_card);
        ll_red_card = findViewById(R.id.ll_red_card);
        tv_red_card = findViewById(R.id.tv_red_card);
        ll_touch = findViewById(R.id.ll_touch);
        tv_touch = findViewById(R.id.tv_touch);
        ll_red_bonus = findViewById(R.id.ll_red_bonus);
        tv_red_bonus = findViewById(R.id.tv_red_bonus);
        ll_unsuccess_raid = findViewById(R.id.ll_unsuccess_raid);
        tv_unsuccess_raid = findViewById(R.id.tv_unsuccess_raid);
        ll_success_tackle = findViewById(R.id.ll_success_tackle);
        tv_success_tackle = findViewById(R.id.tv_success_tackle);
        ll_super_tackle = findViewById(R.id.ll_super_tackle);
        tv_super_tackle = findViewById(R.id.tv_super_tackle);
        ll_push_all_out = findViewById(R.id.ll_push_all_out);
        tv_push_all_out = findViewById(R.id.tv_push_all_out);
        ll_get_all_out = findViewById(R.id.ll_get_all_out);
        tv_get_all_out = findViewById(R.id.tv_get_all_out);

        ll_total_points = findViewById(R.id.ll_total_points);
        tv_total_points = findViewById(R.id.tv_total_points);

        tv_did_not_play = findViewById(R.id.tv_did_not_play);

        updateViewVisibility(ll_playing_points, View.GONE);
        updateViewVisibility(ll_green_card, View.GONE);
        updateViewVisibility(ll_yellow_card, View.GONE);
        updateViewVisibility(ll_red_card, View.GONE);
        updateViewVisibility(ll_touch, View.GONE);
        updateViewVisibility(ll_red_bonus, View.GONE);
        updateViewVisibility(ll_unsuccess_raid, View.GONE);
        updateViewVisibility(ll_success_tackle, View.GONE);
        updateViewVisibility(ll_super_tackle, View.GONE);
        updateViewVisibility(ll_push_all_out, View.GONE);
        updateViewVisibility(ll_get_all_out, View.GONE);

        updateViewVisibility(tv_did_not_play, View.GONE);
        setupHeader();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupHeader() {
        if (getPlayerModel() == null) return;
        PlayerModel playerModel = getPlayerModel();
//        int points = playerModel.getPoints().isEmpty() ? 0 : Integer.parseInt(playerModel.getPoints());
        float points = Float.parseFloat(playerModel.getPoints());
        if (!playerModel.isPlaying() && points == 0) {
            tv_did_not_play.setText("Player not played.");
            updateViewVisibility(tv_did_not_play, View.VISIBLE);
        } else {
            getPlayerDetail();
        }
        setHeaderTitle(playerModel.getPname());
        tv_player_credits.setText(playerModel.getCreditText());
        tv_player_total_points.setText(String.valueOf(playerModel.getPts()));
        loadImage(this, iv_player_image, null, playerModel.getPimg(), R.drawable.no_image);
    }


    private void getPlayerDetail() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        if (matchModel == null) return;
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPlayerPointsDetail("/kabaddi", matchModel.getMatchid(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_PLAYER_POINTS_DETAILS:
                handlePlayerPointsResponse(webRequest);
                break;
        }
    }

    private void handlePlayerPointsResponse(WebRequest webRequest) {
        try {
            EmptyResponseModel responsePojo = webRequest.getResponsePojo(EmptyResponseModel.class);
            if (responsePojo == null || responsePojo.isError() || responsePojo.getData() == null)
                return;

            if (getPlayerModel() != null) {
                LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>) responsePojo.getData();
                for (Map.Entry<String, Object> stringObjectEntry : data.entrySet()) {
                    String key = stringObjectEntry.getKey();
                    if (key.equalsIgnoreCase(getPlayerModel().getPid())) {
                        Object value = stringObjectEntry.getValue();
                        LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap<String, Object>) value;
                        JSONObject object = new JSONObject();
                        for (Map.Entry<String, Object> objectEntry : treeMap.entrySet()) {
                            object.put(objectEntry.getKey(), objectEntry.getValue());
                        }

                        String string = object.toString();
                        printLog("Player data", string);
                        KabaddiPlayerPointsDetailModel pointsDetailModel = new Gson().fromJson(string, KabaddiPlayerPointsDetailModel.class);
                        if (pointsDetailModel != null) {
                            setUpData(pointsDetailModel);
                        }
                    }
                }
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpData(KabaddiPlayerPointsDetailModel model) {
        if (model.getPlayeingPoints() != 0) {
            tv_playing_points.setText(model.getValidFormatText(model.getPlayeingPoints()));
            updateViewVisibility(ll_playing_points, View.VISIBLE);
        } else {
            updateViewVisibility(ll_playing_points, View.GONE);
        }

        if (model.getGreencard() != 0) {
            tv_green_card.setText(model.getValidFormatText(model.getGreencard()));
            updateViewVisibility(ll_green_card, View.VISIBLE);
        } else {
            updateViewVisibility(ll_green_card, View.GONE);
        }

        if (model.getYellowcard() != 0) {
            tv_yellow_card.setText(model.getValidFormatText(model.getYellowcard()));
            updateViewVisibility(ll_yellow_card, View.VISIBLE);
        } else {
            updateViewVisibility(ll_yellow_card, View.GONE);
        }

        if (model.getRedcard() != 0) {
            tv_red_card.setText(model.getValidFormatText(model.getRedcard()));
            updateViewVisibility(ll_red_card, View.VISIBLE);
        } else {
            updateViewVisibility(ll_red_card, View.GONE);
        }

        if (model.getTouch() != 0) {
            tv_touch.setText(model.getValidFormatText(model.getTouch()));
            updateViewVisibility(ll_touch, View.VISIBLE);
        } else {
            updateViewVisibility(ll_touch, View.GONE);
        }

        if (model.getRaidbonus() != 0) {
            tv_red_bonus.setText(model.getValidFormatText(model.getRaidbonus()));
            updateViewVisibility(ll_red_bonus, View.VISIBLE);
        } else {
            updateViewVisibility(ll_red_bonus, View.GONE);
        }

        if (model.getUnsuccessraid() != 0) {
            tv_unsuccess_raid.setText(model.getValidFormatText(model.getUnsuccessraid()));
            updateViewVisibility(ll_unsuccess_raid, View.VISIBLE);
        } else {
            updateViewVisibility(ll_unsuccess_raid, View.GONE);
        }

        if (model.getSuccesstackle() != 0) {
            tv_success_tackle.setText(model.getValidFormatText(model.getSuccesstackle()));
            updateViewVisibility(ll_success_tackle, View.VISIBLE);
        } else {
            updateViewVisibility(ll_success_tackle, View.GONE);
        }

        if (model.getSupertackle() != 0) {
            tv_super_tackle.setText(model.getValidFormatText(model.getSupertackle()));
            updateViewVisibility(ll_super_tackle, View.VISIBLE);
        } else {
            updateViewVisibility(ll_super_tackle, View.GONE);
        }

        if (model.getPushallout() != 0) {
            tv_push_all_out.setText(model.getValidFormatText(model.getPushallout()));
            updateViewVisibility(ll_push_all_out, View.VISIBLE);
        } else {
            updateViewVisibility(ll_push_all_out, View.GONE);
        }

        if (model.getGetallout() != 0) {
            tv_get_all_out.setText(model.getValidFormatText(model.getGetallout()));
            updateViewVisibility(ll_get_all_out, View.VISIBLE);
        } else {
            updateViewVisibility(ll_get_all_out, View.GONE);
        }

        tv_total_points.setText(model.getValidFormatText(model.getTotalpoints()));
    }

}
