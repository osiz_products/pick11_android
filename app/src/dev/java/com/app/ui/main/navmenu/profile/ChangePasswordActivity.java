package com.app.ui.main.navmenu.profile;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.UpdatePasswordRequestModel;
import com.app.model.webresponsemodel.UpdatePasswordResponseModel;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;

import java.util.regex.Pattern;


public class ChangePasswordActivity extends AppBaseActivity {

    EditText et_current_password;
    EditText et_new_password;
    EditText et_c_new_password;
    TextView tv_update;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_change_password;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        et_current_password = findViewById(R.id.et_current_password);
        et_new_password = findViewById(R.id.et_new_password);
        et_c_new_password = findViewById(R.id.et_c_new_password);
        tv_update = findViewById(R.id.tv_update);
        tv_update.setOnClickListener(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_update:
                callChangePassword();
                break;

        }
    }


    private void callChangePassword() {
        String currentPassword = et_current_password.getText().toString().trim();
        String newPassword = et_new_password.getText().toString().trim();
        String confirmPassword = et_c_new_password.getText().toString().trim();

        if (currentPassword.isEmpty()) {
            showErrorMsg("Please enter current password.");
            return;
        }

        if (newPassword.isEmpty()) {
            showErrorMsg("Please enter new password.");
            return;
        }

        if (confirmPassword.isEmpty()) {
            showErrorMsg("Please re-enter new password.");
            return;
        }

        if (!Pattern.matches("[^\\s]{6,15}", newPassword)) {
            showErrorMsg("Password should be 6-15 characters long");
            return;
        }

        if (!newPassword.equals(confirmPassword)) {
            showErrorMsg("Password is not match");
            return;
        }

        UserModel userModel = getUserModel();
        if (userModel != null) {
            UpdatePasswordRequestModel requestModel = new UpdatePasswordRequestModel();
            requestModel.oldpassword = currentPassword;
            requestModel.password = newPassword;
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().userUpdatePassword(requestModel, this);

        }

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case ID_UPDATE_PASSWORD:
                handleUpdatePasswordResponse(webRequest);
                break;
        }
    }

    private void handleUpdatePasswordResponse(WebRequest webRequest) {
        UpdatePasswordResponseModel responsePojo = webRequest.getResponsePojo(UpdatePasswordResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            if (isFinishing()) return;
            showCustomToast(responsePojo.getMsg());
            String newPassword = webRequest.getExtraData(PASSWORD);
            UserModel userModel = getUserModel();
            if (userModel != null) {
//                userModel.password = newPassword;
//                updateUserInPrefs();
                supportFinishAfterTransition();
            }
        } else {
            if (isFinishing()) return;
            String message = responsePojo.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }
}
