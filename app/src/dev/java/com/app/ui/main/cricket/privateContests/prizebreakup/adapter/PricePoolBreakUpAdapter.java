package com.app.ui.main.cricket.privateContests.prizebreakup.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.WinnerBreakUpRankModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;

import java.util.List;

public class PricePoolBreakUpAdapter extends AppBaseRecycleAdapter {

    private PrizePoolRequestModel prizePoolRequestModel;
    private List<WinnerBreakUpRankModel> list;
    private Context context;

    public PricePoolBreakUpAdapter(Context context, List<WinnerBreakUpRankModel> list) {
        this.context = context;
        this.list = list;
        prizePoolRequestModel = MyApplication.getInstance().getPrizePoolRequestModel();
    }


    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_private_price_pool_rank));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public WinnerBreakUpRankModel getItem(int position) {
        return list.get(position);
    }


    private class ViewHolder extends BaseViewHolder {

        private TextView tv_rank;
        private TextView tv_percent;
        private TextView tv_price;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_rank = itemView.findViewById(R.id.tv_rank);
            tv_percent = itemView.findViewById(R.id.tv_percent);
            tv_price = itemView.findViewById(R.id.tv_price);
        }

        @Override
        public String setData(int position) {
            if (list == null && prizePoolRequestModel == null) return null;
            String currency_symbol = getContext().getResources().getString(R.string.currency_symbol) + " ";
            WinnerBreakUpRankModel winnerBreakUpRankModel = list.get(position);
            tv_rank.setText(winnerBreakUpRankModel.getRank());
            tv_percent.setText(winnerBreakUpRankModel.getPercentText());
            String price = winnerBreakUpRankModel.getPercentValue(prizePoolRequestModel);
            tv_price.setText(currency_symbol + price);
            return currency_symbol;
        }
    }
}
