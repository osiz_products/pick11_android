package com.app.ui.main.cricket.privateContests.prizebreakup.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.WinnerLavelModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.privateContests.prizebreakup.dialog.adpater.BreakUpAdapter;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.utilities.ItemClickSupport;

import java.util.List;


public class PrizeBreakDialog extends AppBaseDialogFragment {

    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout bottom_sheet;

    TextView tv_team_name;
    ImageView iv_close;
    RecyclerView recycler_view;
    BreakUpAdapter adapter;
    List<WinnerLavelModel> winnersLabels;
    int selectedPosition;
    PrizeBreakDialogListener prizeBreakDialogListener;

    public PrizePoolRequestModel getPrizePoolRequestModel() {
        return MyApplication.getInstance().getPrizePoolRequestModel();
    }


    public static PrizeBreakDialog getInstance(Bundle bundle) {
        PrizeBreakDialog messageDialog = new PrizeBreakDialog();
        if (bundle != null)
            messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setPrizeBreakDialogListener(PrizeBreakDialogListener prizeBreakDialogListener) {
        this.prizeBreakDialogListener = prizeBreakDialogListener;
    }

    public void setWinnerLavelModel(List<WinnerLavelModel> winnersLabels) {
        this.winnersLabels = winnersLabels;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_prize_breakup;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        bottom_sheet = getView().findViewById(R.id.bottom_sheet);
        initializeBottomSheet();

        tv_team_name = getView().findViewById(R.id.tv_team_name);
        iv_close = getView().findViewById(R.id.iv_close);

        iv_close.setOnClickListener(this);

        if (getPrizePoolRequestModel() != null) {
            tv_team_name.setText(getPrizePoolRequestModel().name);
        }

        initializeRecyclerView();
    }

    private void initializeBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    dismiss();
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = getView().findViewById(R.id.recycler_view);
        adapter = new BreakUpAdapter(getContext(), winnersLabels, selectedPosition);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                WinnerLavelModel winnerLavelModel = winnersLabels.get(position);
                if (winnerLavelModel == null) return;
                if (prizeBreakDialogListener != null) {
                    prizeBreakDialogListener.onPositionSelect(position, winnerLavelModel);
                    dismiss();
                }
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
        }
    }

    public interface PrizeBreakDialogListener {
        void onPositionSelect(int position, WinnerLavelModel winnerLavelModel);
    }
}
