package com.app.ui.main.navmenu.myaccount;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.WalletModel;
import com.app.model.webresponsemodel.WalletResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.KYCConfirmationDialog;
import com.app.ui.main.allTransaction.AllTransactionsActivity;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;

public class MyAccountActivity extends AppBaseActivity {

    private TextView tv_your_balance;
    private TextView tv_add_cash;
    private TextView tv_winnings_balance;
    private TextView tv_withdraw;
    private TextView tv_deposited_balance;
    private TextView tv_bonus_balance;
    private CardView tv_all_transaction;
    UserPrefs userPrefs;
    ImageView imageView;
    ImageView imageViews;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_my_account;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_your_balance = findViewById(R.id.tv_your_balance);
        tv_add_cash = findViewById(R.id.tv_add_cash);
        tv_winnings_balance = findViewById(R.id.tv_winnings_balance);
        tv_withdraw = findViewById(R.id.tv_withdraw);
        tv_deposited_balance = findViewById(R.id.tv_deposited_balance);
        tv_bonus_balance = findViewById(R.id.tv_bonus_balance);
        tv_all_transaction = findViewById(R.id.tv_all_transaction);

        imageView = findViewById(R.id.imageView);
        //imageViews = findViewById(R.id.imageViews);

        tv_add_cash.setOnClickListener(this);
        tv_withdraw.setOnClickListener(this);
        tv_all_transaction.setOnClickListener(this);

        getUserWallet();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

           /* imageView.setVisibility(View.VISIBLE);
            imageViews.setVisibility(View.GONE);*/
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

           /* imageViews.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);*/
           }
    }

    private void getUserWallet() {
        displayProgressBar(false);
        getWebRequestHelper().getWalletDetailUrl(this);
    }

    private void setUserWallet() {

        if (getUserModel() != null && getUserModel().getWalletModel() != null) {
            WalletModel walletModel = getUserModel().getWalletModel();
//            Log.e("WalletRes",""+getUserModel().getWalletModel());
            tv_your_balance.setText(currency_symbol + walletModel.getAddedcash());
            tv_deposited_balance.setText(currency_symbol + getValidDecimalFormat(walletModel.getTotalcash()));
            tv_winnings_balance.setText(currency_symbol + getValidDecimalFormat(walletModel.getWltwin()));
            tv_bonus_balance.setText(currency_symbol + getValidDecimalFormat(walletModel.getWltbns()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_cash:
                goToAddCashActivity(null);
                break;

            case R.id.tv_withdraw:
                if (getUserModel() == null) return;
                UserModel userModel = getUserModel();
                if (userModel.isKycCompleted())
                    goToWithdrawActivity(null);
                else
                    addKYCConfirmationDialog();
                break;

            case R.id.tv_all_transaction:
                goToAllTransactionsActivity(null);
                break;
        }
    }

    private void goToAddCashActivity(Bundle bundle) {
        Intent intent = new Intent(this, AddCashActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToWithdrawActivity(Bundle bundle) {
        Intent intent = new Intent(this, WithdrawActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_CASHOUT);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToAllTransactionsActivity(Bundle bundle) {
        Intent intent = new Intent(this, AllTransactionsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void addKYCConfirmationDialog() {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, "Verify KYC first");
        bundle.putString(POS_BTN, "OK");
        KYCConfirmationDialog dialog = KYCConfirmationDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE)
                    dialog.dismiss();

            }
        });
        dialog.show(getFm(), "Confirm");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CASHOUT) {
            if (resultCode == RESULT_OK) {
                getWebRequestHelper().getWalletDetailUrl(this);
            }
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_GET_USER_BALANCE:
                handleUserBalanceResponse(webRequest);
                break;
        }
    }

    private void handleUserBalanceResponse(WebRequest webRequest) {
        WalletResponseModel responseModel = webRequest.getResponsePojo(WalletResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            getUserModel().setWalletModel(responseModel.getData());
            setUserWallet();
        }
    }
}
