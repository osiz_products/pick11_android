package com.app.ui.main.navmenu.influencer;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TransactionInfModel;
import com.pickeleven.R;
import java.util.List;

public class MyUpcomingInfAdapter extends AppBaseRecycleAdapter {
    private List<TransactionInfModel> list;
    private Context context;

    public MyUpcomingInfAdapter(Context context) {
        this.context = context;
    }

    public void updateData (List<TransactionInfModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new MyUpcomingInfAdapter.ViewHolder(inflateLayout(R.layout.item_infoupcoming));
    }

    public TransactionInfModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getDataCount() {
       // return 0;
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {
        private ImageView iv_image_first;
        private ProgressBar pb_image_first;
        private TextView tv_team_one;
        private ImageView iv_image_second;
        private ProgressBar pb_image_second;
        private TextView tv_team_two;
        private TextView tv_match_name;
        private TextView tv_match_type;
        private TextView tv_match_start_time;
        private TextView tv_series_name;
        private TextView tv_vs;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_image_first = itemView.findViewById(R.id.iv_image_first);
            pb_image_first = itemView.findViewById(R.id.pb_image_first);
            tv_team_one = itemView.findViewById(R.id.tv_team_one);
            iv_image_second = itemView.findViewById(R.id.iv_image_second);
            pb_image_second = itemView.findViewById(R.id.pb_image_second);
            tv_team_two = itemView.findViewById(R.id.tv_team_two);
            tv_match_name = itemView.findViewById(R.id.tv_match_name);
            tv_match_type = itemView.findViewById(R.id.tv_match_type);
            tv_vs = itemView.findViewById(R.id.tv_vs);
            tv_match_start_time = itemView.findViewById(R.id.tv_match_start_time);
            tv_series_name = itemView.findViewById(R.id.tv_series_name);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            TransactionInfModel matchModel = list.get(position);
            if (matchModel == null) return null;

            if (!matchModel.getSeriesname().isEmpty()) {
                updateViewVisibitity(tv_series_name, View.VISIBLE);
                tv_series_name.setText(matchModel.getSeriesname());
            } else {
                updateViewVisibitity(tv_series_name, View.GONE);
            }

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_first, pb_image_first, matchModel.getTeam1logo(),
                    R.drawable.dummy_logo, 200);

            ((AppBaseActivity) getContext()).loadImage(getContext(),
                    iv_image_second, pb_image_second, matchModel.getTeam2logo(),
                    R.drawable.dummy_logo, 200);

            tv_match_name.setText(matchModel.getTeam1());
            tv_team_one.setText(matchModel.getTeam1());
            tv_vs.setText(matchModel.getJoinedUserCount()+"Users");
            tv_match_type.setText(matchModel.capitalize(matchModel.getMtype()));
            tv_team_two.setText(matchModel.getTeam2());
            tv_match_start_time.setText("RS. "+matchModel.getExpectedEarnings()+" Expected Earnings");


            return null;
        }
    }

}
