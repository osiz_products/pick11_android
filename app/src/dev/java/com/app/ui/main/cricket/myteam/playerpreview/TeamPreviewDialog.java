package com.app.ui.main.cricket.myteam.playerpreview;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseDialogFragment;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.TeamModel;
import com.app.model.webresponsemodel.PlayerPreviewResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.myteam.playerDetail.PlayerPointsDetailActivity;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;

import java.util.ArrayList;
import java.util.List;


public class TeamPreviewDialog extends AppBaseDialogFragment {

    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout bottom_sheet;

    TextView tv_team_name;
    ImageView iv_edit_team;
    ImageView iv_close;
    PlayerView wicket_keeper;
    PlayerView batsman;
    PlayerView allrounder;
    PlayerView bowler;

    DialogInterface.OnClickListener onClickListener;

    PlayerPreviewResponseModel.DataBean team;
    TeamModel teamModel;

    public void setTeam(PlayerPreviewResponseModel.DataBean team) {
        this.team = team;
    }

    public void setTeamModel(TeamModel teamModel) {
        this.teamModel = teamModel;
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    public static TeamPreviewDialog getInstance(Bundle bundle) {
        TeamPreviewDialog messageDialog = new TeamPreviewDialog();
        if (bundle != null)
            messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_team_preview;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        bottom_sheet = getView().findViewById(R.id.bottom_sheet);
        initializeBottomSheet();
        Log.e("Call_Cricket","Team");

        tv_team_name = getView().findViewById(R.id.tv_team_name);
        iv_edit_team = getView().findViewById(R.id.iv_edit_team);
        updateViewVisibitity(iv_edit_team, View.GONE);
        iv_close = getView().findViewById(R.id.iv_close);
        wicket_keeper = getView().findViewById(R.id.wicket_keeper);
        batsman = getView().findViewById(R.id.batsman);
        allrounder = getView().findViewById(R.id.allrounder);
        bowler = getView().findViewById(R.id.bowler);

        iv_close.setOnClickListener(this);

        if (teamModel != null)
            getTeamPlayers();
        else
            setupView();

    }

    private void getTeamPlayers() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    private void initializeBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    dismiss();
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }


    private void setupView() {
        if (team != null && team.getPlayers() != null) {
            final View viewById = getView().findViewById(R.id.ll_data_lay);
            viewById.post(new Runnable() {
                @Override
                public void run() {
                    float defaultItemInRow = 5.0f;
                    int width = viewById.getWidth();
                    List<PlayerModel> wicketkeeper = team.getPlayers().getWicketkeeper();
                    if (wicketkeeper.size() > defaultItemInRow) {
                        defaultItemInRow = wicketkeeper.size();
                    }
                    List<PlayerModel> batsman = team.getPlayers().getBatsman();
                    if (batsman.size() > defaultItemInRow) {
                        defaultItemInRow = batsman.size();
                    }
                    List<PlayerModel> allrounder = team.getPlayers().getAllrounder();
                    if (allrounder.size() > defaultItemInRow) {
                        defaultItemInRow = allrounder.size();
                    }
                    List<PlayerModel> bowler = team.getPlayers().getBowler();
                    if (bowler.size() > defaultItemInRow) {
                        defaultItemInRow = bowler.size();
                    }
                    int itemSize = Math.round(width / defaultItemInRow);
                    TeamPreviewDialog.this.wicket_keeper.setPerItemSize(itemSize);
                    TeamPreviewDialog.this.batsman.setPerItemSize(itemSize);
                    TeamPreviewDialog.this.allrounder.setPerItemSize(itemSize);
                    TeamPreviewDialog.this.bowler.setPerItemSize(itemSize);

                    TeamPreviewDialog.this.wicket_keeper.setupItems(wicketkeeper.size());
                    TeamPreviewDialog.this.batsman.setupItems(batsman.size());
                    TeamPreviewDialog.this.allrounder.setupItems(allrounder.size());
                    TeamPreviewDialog.this.bowler.setupItems(bowler.size());

                    setupData();

                }
            });

        }
    }

    private void setupData() {
        if (team != null) {
            tv_team_name.setText(team.getName());
            setupWicketKepperData();
            setupBatsmanData();
            setupAllrounderData();
            setupBowlerData();
        }
    }

    private void setupWicketKepperData() {
        List<PlayerModel> wicketkeeper = team.getPlayers().getWicketkeeper();
        List<View> itemViews = TeamPreviewDialog.this.wicket_keeper.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = wicketkeeper.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");


            Log.e("GetplayerPercentage22",""+String.valueOf(playerModel.getPlayerPercentage()));


            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_players_image,
                        null, playerModel.getPimg(), R.drawable.dummy_player_icon);
//                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }

            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }

            view.setTag(playerModel);
            view.setOnClickListener(this);

        }
    }

    private void setupBatsmanData() {
        List<PlayerModel> wicketkeeper = team.getPlayers().getBatsman();
        List<View> itemViews = TeamPreviewDialog.this.batsman.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = wicketkeeper.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");
            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_players_image,
                        null, playerModel.getPimg(), R.drawable.dummy_player_icon);
//                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }


            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }

            view.setTag(playerModel);
            view.setOnClickListener(this);

        }
    }

    private void setupAllrounderData() {
        List<PlayerModel> wicketkeeper = team.getPlayers().getAllrounder();
        List<View> itemViews = TeamPreviewDialog.this.allrounder.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = wicketkeeper.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");
            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_players_image,
                        null, playerModel.getPimg(), R.drawable.dummy_player_icon);
//                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }


            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }

            view.setTag(playerModel);
            view.setOnClickListener(this);

        }
    }

    private void setupBowlerData() {
        List<PlayerModel> wicketkeeper = team.getPlayers().getBowler();
        List<View> itemViews = TeamPreviewDialog.this.bowler.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = wicketkeeper.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");
            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_players_image,
                        null, playerModel.getPimg(), R.drawable.dummy_player_icon);
//                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }


            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }

            view.setTag(playerModel);
            view.setOnClickListener(this);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_player_view:
                if (getMatchModel().isFixtureMatch()){
                    showCustomToast("Player status available after match start.");
                    return;
                }
                PlayerModel playerModel = (PlayerModel) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(playerModel));
                goToPlayerPointDetailActivity(bundle);
                break;
            case R.id.iv_close:
//                if (onClickListener != null) {
//                    onClickListener.onClick(this.getDialog(),
//                            v.getId() == R.id.iv_close ?
//                                    DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE);
//                }
                dismiss();
                break;
        }
    }

    private void goToPlayerPointDetailActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), PlayerPointsDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;
        }
    }

    private void handleTeamPlayersResponse(WebRequest webRequest) {
        PlayerPreviewResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> wicketkeeper = new ArrayList<>();
            List<PlayerModel> bowler = new ArrayList<>();
            List<PlayerModel> batsman = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();

            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setPlayerPercentage(datum.getPlayerPercentage());
                playerModel.setCaptainPercentage(datum.getCaptainPercentage());
                playerModel.setViceCaptainPercentage(datum.getViceCaptainPercentage());

                if (datum.isWicketKeeper()) {
                    playerModel.setPtype("WK");
                    wicketkeeper.add(playerModel);
                } else if (datum.isBatsmen()) {
                    playerModel.setPtype("BAT");
                    batsman.add(playerModel);
                } else if (datum.isAllRounder()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                } else if (datum.isBowler()) {
                    playerModel.setPtype("BOWL");
                    bowler.add(playerModel);
                }

            }

            PlayerPreviewResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewResponseModel.DataBean.PlayersBean();
            playersBean.setWicketkeeper(wicketkeeper);
            playersBean.setBatsman(batsman);
            playersBean.setAllrounder(allrounder);
            playersBean.setBowler(bowler);

            PlayerPreviewResponseModel.DataBean team = new PlayerPreviewResponseModel.DataBean();
            team.setId("0");
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);

            setTeam(team);
            setupView();

        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }
}
