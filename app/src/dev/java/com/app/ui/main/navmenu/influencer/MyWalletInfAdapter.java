package com.app.ui.main.navmenu.influencer;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.WalletlistModel;
import com.pickeleven.R;

import java.text.DecimalFormat;
import java.util.List;

public class MyWalletInfAdapter extends AppBaseRecycleAdapter {
    private List<WalletlistModel> list;
    private Context context;

    public MyWalletInfAdapter(Context context) {
        this.context = context;
    }

    public void updateData (List<WalletlistModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new MyWalletInfAdapter.ViewHolder(inflateLayout(R.layout.item_infowallet_list));
    }

    public WalletlistModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getDataCount() {
        // return 0;

               return list == null ? 0 : list.size();


    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tv_email;
        private TextView tv_match;
        private TextView tv_earned;
        private TextView tv_date;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_email = itemView.findViewById(R.id.tv_email);
            tv_match = itemView.findViewById(R.id.tv_match);
            tv_earned = itemView.findViewById(R.id.tv_earned);
            tv_date = itemView.findViewById(R.id.tv_date);

        }



        @Override
        public String setData(int position) {
            if (list == null) return null;
            WalletlistModel walletlistModel = list.get(position);
            if (walletlistModel == null) return null;

            tv_email.setText(walletlistModel.getEmail());
            tv_match.setText(walletlistModel.getTeam1()+"v/s"+walletlistModel.getTeam2());

            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            String numberAsString = decimalFormat.format(walletlistModel.getComissionfee());
          //  System.out.println(numberAsString);

            tv_earned.setText("Rs. "+numberAsString);
            tv_date.setText(walletlistModel.getFormattedDate(3));


            Log.e("GET_List",""+list.size());
            return numberAsString;
        }
    }

}
