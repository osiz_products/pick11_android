package com.app.ui.main.football.myteam.chooseTeam.adapter;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TeamModel;
import com.pickeleven.R;

import java.util.List;

public class ChooseTeamAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<TeamModel> list;
    private String selectedTeam;

    public ChooseTeamAdapter(Context context, List<TeamModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_choose_team));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }


    public String getSelectedTeam() {
        return selectedTeam;
    }

    public void setSelectedTeam(int position) {
        String selectedTeam = String.valueOf(list.get(position).getId());
        if (this.selectedTeam != null && this.selectedTeam.equals(selectedTeam)) {
            this.selectedTeam = null;
        } else {
            this.selectedTeam = selectedTeam;
        }
        notifyDataSetChanged();
    }

    public boolean isTeamAlreadyJoined(String teamId) {
        return false;
    }

    private class ViewHolder extends BaseViewHolder {

        private LinearLayout ll_layout;
        private CheckBox cb_team_name;
        private TextView tv_all_ready_added;
        private TextView tv_captain;
        private TextView tv_vice_captain;
        private LinearLayout ll_preview;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            cb_team_name = itemView.findViewById(R.id.cb_team_name);
            tv_all_ready_added = itemView.findViewById(R.id.tv_all_ready_added);
            tv_captain = itemView.findViewById(R.id.tv_captain);
            tv_vice_captain = itemView.findViewById(R.id.tv_vice_captain);
            ll_preview = itemView.findViewById(R.id.ll_preview);
            cb_team_name.setClickable(false);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            ll_layout.setTag(position);
            ll_preview.setTag(position);
            ll_preview.setOnClickListener(this);

            TeamModel teamModel = list.get(position);
            cb_team_name.setText(teamModel.getTeamname());
            tv_captain.setText(teamModel.getCap());
            tv_vice_captain.setText(teamModel.getVcap());


            if (isTeamAlreadyJoined(String.valueOf(teamModel.getId()))) {
                ll_layout.setAlpha(0.5f);
                updateViewVisibitity(tv_all_ready_added, View.VISIBLE);
                ll_layout.setActivated(true);
                cb_team_name.setChecked(true);
            } else {
                ll_layout.setAlpha(1.0f);
                updateViewVisibitity(tv_all_ready_added, View.GONE);
                if (selectedTeam == null) {
                    ll_layout.setActivated(false);
                    cb_team_name.setChecked(false);
                } else {
                    if (String.valueOf(teamModel.getId()).equals(selectedTeam)) {
                        ll_layout.setActivated(true);
                        cb_team_name.setChecked(true);
                    } else {
                        ll_layout.setActivated(false);
                        cb_team_name.setChecked(false);
                    }
                }
            }


            /*TeamsPointModel myChooseTeam = getMyChooseTeam(teamModel);
            if (myChooseTeam != null) {
                ll_layout.setOnClickListener(null);
                cb_team_name.setChecked(true);
                ll_layout.setAlpha(0.5f);
                updateViewVisibility(tv_all_ready_added, View.VISIBLE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_item_bg2));
            } else if (getSelectedTeam() != null) {
                cb_team_name.setChecked(true);
                ll_layout.setAlpha(1f);
                updateViewVisibility(tv_all_ready_added, View.GONE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_item_bg2));
            } else {
                ll_layout.setOnClickListener(this);
                cb_team_name.setChecked(false);
                ll_layout.setAlpha(1f);
                updateViewVisibility(tv_all_ready_added, View.GONE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_white));
            }*/

            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
