package com.app.ui.main.navmenu.verification.panCard;

import android.net.Uri;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseFragment;
import com.app.model.PenCardModel;
import com.app.model.StateModel;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.UpdatePenCardRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.PenCardResponseModel;
import com.app.model.webresponsemodel.ProfileResponseModel;
import com.app.model.webresponsemodel.StateResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.state.SelectStateDialog;
import com.pickeleven.R;
import com.google.android.material.textfield.TextInputLayout;
import com.imagePicker.FileInformation;
import com.imagePicker.ProfilePicDialog;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.DatePickerUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class PanCardFragment extends AppBaseFragment {

    private static final String PAN_REGEX = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
    private ImageView iv_pan_image;
    private LinearLayout ll_upload_image;
    private EditText et_user_name;
    private TextInputLayout tl_pancard;
    private EditText et_pan_card_number;
    private EditText et_confirm_pan_card_number;
    private EditText et_dob;
    private LinearLayout ll_state;
    private TextView tv_add_pan_text;
    private TextView tv_state;
    private ImageView iv_drop_down_state;
    private TextView tv_save;
    private TextView tv_verified_text;
    private String imagePath = "";
    UserPrefs userPrefs;
    UserModel userModel;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.fragment_pan_card;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        tv_add_pan_text = getView().findViewById(R.id.tv_add_pan_text);
        iv_pan_image = getView().findViewById(R.id.iv_pan_image);
        ll_upload_image = getView().findViewById(R.id.ll_upload_image);
        et_user_name = getView().findViewById(R.id.et_user_name);
        tl_pancard = getView().findViewById(R.id.tl_pancard);
        et_pan_card_number = getView().findViewById(R.id.et_pan_card_number);
        et_confirm_pan_card_number = getView().findViewById(R.id.et_confirm_pan_card_number);
        et_dob = getView().findViewById(R.id.et_dob);
        ll_state = getView().findViewById(R.id.ll_state);
        updateViewVisibility(ll_state, View.GONE);
        tv_state = getView().findViewById(R.id.tv_state);
        iv_drop_down_state = getView().findViewById(R.id.iv_drop_down_state);
        tv_save = getView().findViewById(R.id.tv_save);
        tv_verified_text = getView().findViewById(R.id.tv_verified_text);
        updateViewVisibility(tv_verified_text, View.GONE);

        ll_upload_image.setOnClickListener(this);
        et_dob.setOnClickListener(this);
        tv_state.setOnClickListener(this);
        iv_drop_down_state.setOnClickListener(this);
        tv_save.setOnClickListener(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onPageSelected() {
        getPanCard();
        getUserDetail();
    }

    private void getUserDetail() {
        getWebRequestHelper().getProfileDetail(this);
    }

    private void getPanCard() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPanCard(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_upload_image:
                showImagePickerDialog();
                break;
            case R.id.et_dob:
                showDatePickerDialog(et_dob);
                break;
            case R.id.tv_state:
            case R.id.iv_drop_down_state:
                selectStateDialog();
                break;
            case R.id.tv_save:
                onSubmit();
                break;
        }
    }

    private void selectStateDialog() {
        final List<StateModel> state = getUserPrefs().getStateList();
        if (state == null) {
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().getStatusUrl(this);
        } else {
            final SelectStateDialog selectTeamDialog = new SelectStateDialog();
            selectTeamDialog.setDataList(state);
            selectTeamDialog.setTitle("Select State");
            selectTeamDialog.setOnItemSelectedListeners(new SelectStateDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectTeamDialog.dismiss();
                    tv_state.setText(String.valueOf(state.get(position).getName()));
                }
            });
            selectTeamDialog.show(getChildFm(), selectTeamDialog.getClass().getSimpleName());
        }
    }

    private void showDatePickerDialog(final EditText textView) {
        DatePickerUtil.showDatePicker(getActivity(), textView, false, new DatePickerUtil.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Calendar calendar) {
                Date date = calendar.getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String sel_date = simpleDateFormat.format(date);
                textView.setTag(calendar);
                textView.setText(sel_date);
            }
        });
    }

    private void showImagePickerDialog() {
        final ProfilePicDialog dialog = ProfilePicDialog.getNewInstance(false);
        dialog.setProfilePicDialogListner(new ProfilePicDialog.ProfilePicDialogListner() {
            @Override
            public void onProfilePicSelected(FileInformation fileInformation) {
                dialog.dismiss();

                String imageName = "profile_pic_" + System.currentTimeMillis();

                String large_file_path = fileInformation.getBitmapPathForUpload(getContext(),
                        FileInformation.IMAGE_SIZE_LARGE, FileInformation.IMAGE_SIZE_LARGE,
                        "large/" + imageName);

                String thumb_file_path = fileInformation.getBitmapPathForUpload(getContext(),
                        FileInformation.IMAGE_SIZE_THUMB, FileInformation.IMAGE_SIZE_THUMB,
                        "thumb/" + imageName);

                iv_pan_image.setTag(R.id.image_path_tag, large_file_path);
                iv_pan_image.setTag(R.id.image_path_thumb_tag, thumb_file_path);
                if (isValidString(thumb_file_path))
                    iv_pan_image.setImageURI(Uri.parse(large_file_path));
            }

            @Override
            public void onProfilePicRemoved() {

            }
        });
        dialog.showDialog(getContext(), getChildFm());
    }

    private void onSubmit() {
        String userName = et_user_name.getText().toString().trim();
        String panNumber = et_pan_card_number.getText().toString().trim();
        String confirmPanNumber = et_confirm_pan_card_number.getText().toString().trim();
        String dob = et_dob.getText().toString().trim();
        String state = tv_state.getText().toString().trim();

        if (userName.isEmpty()) {
            showErrorMsg("Please enter name");
            return;
        }

        if (panNumber.isEmpty()) {
            showErrorMsg("Please enter pan number");
            return;
        }

        if (!Pattern.matches(PAN_REGEX, panNumber)) {
            showErrorMsg("Please enter valid pan number.");
            return;
        }

        if (confirmPanNumber.isEmpty()) {
            showErrorMsg("Please enter confirm pan number");
            return;
        }
        if (!panNumber.equals(confirmPanNumber))
            if (dob.isEmpty()) {
                showErrorMsg("Confirm pan number not match");
                return;
            }
//        if (state.isEmpty()) {
//            showErrorMsg("Please select state");
//            return;
//        }
        String imagePath = (String) iv_pan_image.getTag(R.id.image_path_tag);
        if (imagePath == null) {
            showErrorMsg("Please select pan image");
            return;
        }

        UpdatePenCardRequestModel requestModel = new UpdatePenCardRequestModel();
        requestModel.panname = userName;
        requestModel.pannumber = panNumber;
        requestModel.dob = dob;
        requestModel.panimage = new File(imagePath);
        requestModel.state = state;
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().updatePanCard(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_GET_STATE:
                handleGetStateResponse(webRequest);
                break;

            case ID_UPDATE_PEN_CARD:
                handleUpdatePenCardResponse(webRequest);
                break;

            case ID_GET_PEN_CARD:
                handleGetPenCardResponse(webRequest);
                break;
            case ID_GET_USER_PROFILE:
                handleUserProfileResponse(webRequest);
                break;
        }
    }

    private void handleUserProfileResponse(WebRequest webRequest) {
        ProfileResponseModel responseModel = webRequest.getResponsePojo(ProfileResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError() && responseModel.getData() != null) {
            if (isFinishing()) return;
            userModel = responseModel.getData();
            if (userModel == null) return;
            UserModel userModel1 = getUserModel();
            userModel1.setIsbankdverify(userModel.getIsbankdverify());
            userModel1.setIsemailverify(userModel.getIsemailverify());
            userModel1.setIsphoneverify(userModel.getIsphoneverify());
            userModel1.setIspanverify(userModel.getIspanverify());
            getUserPrefs().updateLoggedInUser(userModel1);

            if (userModel.getIspanverify()!=null){
               // updateView(true);

                if(userModel.getIspanverify().equals("1")){
                    tv_verified_text.setVisibility(View.VISIBLE);
                    Log.e("call","panVerified==1");

                    updateViewVisibility(tl_pancard, View.GONE);
                    updateViewVisibility(tv_add_pan_text, View.GONE);
                    updateViewVisibility(ll_upload_image, View.GONE);
                    updateViewVisibility(tv_save, View.GONE);

                    et_user_name.setFocusable(false);
                    et_pan_card_number.setFocusable(false);
                    et_confirm_pan_card_number.setFocusable(false);
                    et_dob.setFocusable(false);
                    et_dob.setOnClickListener(null);


                }else {
                    tv_verified_text.setVisibility(View.GONE);
                    Log.e("call","panVerified==2");
                }

                Log.e("PenVerified1",""+userModel.getIspanverify());

            }/*else {
                tv_verified_text.setVisibility(View.GONE);
            }*/

        }
    }

    private void handleGetPenCardResponse(WebRequest webRequest) {
        PenCardResponseModel responseModel = webRequest.getResponsePojo(PenCardResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            PenCardModel data = responseModel.getData();
            imagePath = data.getPanimage();
//            iv_pan_image.setTag(R.id.image_path_thumb_tag, data.getPanimage());
            et_user_name.setText(data.getPanname());
            et_pan_card_number.setText(data.getPannumber());
            et_confirm_pan_card_number.setText(data.getPannumber());
            et_dob.setText(data.getFormattedDate(3));
//            tv_state.setText(data.getState());
            ((AppBaseActivity) getActivity()).loadImage(getContext(), iv_pan_image,
                    null, data.getPanimage(), R.drawable.pan_card, 500);
            updateView(true);

            if (data.isPenVerified()) {
                Log.e("PenVerified2",""+data.isPenVerified());
                updateViewVisibility(tv_verified_text, View.VISIBLE);
                Log.e("call","panVerified==3");
            } else {
                updateViewVisibility(tv_verified_text, View.GONE);
                Log.e("call","panVerified==4");
            }
        }
    }

    private void handleUpdatePenCardResponse(WebRequest webRequest) {
        try {
            EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
            if (responseModel != null && !responseModel.isError()) {
                updateView(true);
                getPanCard();
                String msg = responseModel.getMsg();
                if (isValidString(msg))
                    showCustomToast(msg);
            }else {
                String msg = responseModel.getMsg();
                if (isValidString(msg))
                    showErrorMsg(msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateView(boolean isVarified) {
        if (isVarified) {
            ll_upload_image.setFocusable(false);
            ll_upload_image.setOnClickListener(null);
            et_user_name.setFocusable(false);
            et_pan_card_number.setFocusable(false);
            et_confirm_pan_card_number.setFocusable(false);
            et_dob.setFocusable(false);
            et_dob.setOnClickListener(null);
            updateViewVisibility(tl_pancard, View.GONE);
            updateViewVisibility(tv_add_pan_text, View.GONE);
            updateViewVisibility(ll_upload_image, View.GONE);
            updateViewVisibility(tv_save, View.GONE);

        } else {
            ll_upload_image.setFocusable(true);
            ll_upload_image.setOnClickListener(this);
            et_user_name.setFocusable(true);
            et_pan_card_number.setFocusable(true);
            et_confirm_pan_card_number.setFocusable(true);
            et_dob.setFocusable(true);
            et_dob.setOnClickListener(this);
            updateViewVisibility(tl_pancard, View.VISIBLE);
            updateViewVisibility(tv_add_pan_text, View.VISIBLE);
            updateViewVisibility(ll_upload_image, View.VISIBLE);
            updateViewVisibility(tv_save, View.VISIBLE);
        }
    }

    private void handleGetStateResponse(WebRequest webRequest) {
        StateResponseModel responseModel = webRequest.getResponsePojo(StateResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<StateModel> data = responseModel.getData();
            getUserPrefs().updateStateList(data);
            selectStateDialog();
        }
    }
}
