package com.app.ui.main.cricket.myteam.createTeam.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseFragment;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerTypeModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.myteam.createTeam.CreateTeamActivity;
import com.app.ui.main.cricket.myteam.createTeam.fragment.adapter.PlayersAdapter;
import com.app.ui.main.cricket.myteam.playerDetail.PlayerDetailActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.utilities.ItemClickSupport;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlayersFragment extends AppBaseFragment {

    TextView tv_title;
    LinearLayout ll_player;
    ImageView iv_player_arrow;
    LinearLayout ll_points;
    ImageView iv_points_arrow;
    LinearLayout ll_credit;
    ImageView iv_credit_arrow;
    List<PlayerModel> players;
    String title = "";
    RecyclerView recycler_view;
    PlayersAdapter adapter;
    UpdateSelectedPlayer updateSelectedPlayer;
    int type;
    int min;
    int max;

    View currentSortBy = null;
    int currentSortType = 0;//0 mean asyc 1 mean desc

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Collections.sort(players, listSorter);
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (adapter != null && getActivity() != null) {
                adapter.notifyDataSetChanged();
            }
        }
    };

    Comparator listSorter = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            if (currentSortBy != null) {
                PlayerModel player1 = (PlayerModel) o1;
                PlayerModel player2 = (PlayerModel) o2;
                switch (currentSortBy.getId()) {
                    case R.id.iv_player_arrow: {
                        if (currentSortType == 1) {
                            return player2.getPname().compareToIgnoreCase(player1.getPname());
                        } else {
                            return player1.getPname().compareToIgnoreCase(player2.getPname());
                        }
                    }

                    case R.id.iv_points_arrow: {
                        if (currentSortType == 1) {
                            return Double.compare(player2.getPts(), player1.getPts());
                        } else {
                            return Double.compare(player1.getPts(), player2.getPts());
                        }

                    }
                    case R.id.iv_credit_arrow: {
                        if (currentSortType == 1) {
                            return Float.compare(player2.getCredit(), player1.getCredit());
                        } else {
                            return Float.compare(player1.getCredit(), player2.getCredit());
                        }
                    }
                }
            }
            return 0;
        }

    };


    public void setPlayersList(List<PlayerModel> players) {
        this.players = players;
    }

    public void setType(int type) {
        this.type = type;

        Log.e("GETPlayerType",""+type);
    }

    public void setMin(int min) {
        this.min = min;
        Log.e("GETPlayerMin",""+min);
    }

    public void setMax(int max) {
        this.max = max;
        Log.e("GETPlayerMax",""+max);
    }

    public void setUpdateSelectedPlayer(UpdateSelectedPlayer updateSelectedPlayer) {
        this.updateSelectedPlayer = updateSelectedPlayer;
    }

    private PlayerTypeModel getPlayerType(int currentTab) {
        return ((CreateTeamActivity) getActivity()).getPlayerTypes(currentTab);
    }

    public PlayerTypeResponseModel.DataBean getPlayerTypeModels() {
        return MyApplication.getInstance().getPlayerTypeModels();
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_players;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getView() == null) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }

        return getView();
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        tv_title = getView().findViewById(R.id.tv_title);
        ll_player = getView().findViewById(R.id.ll_player);
        iv_player_arrow = getView().findViewById(R.id.iv_player_arrow);
        ll_points = getView().findViewById(R.id.ll_points);
        iv_points_arrow = getView().findViewById(R.id.iv_points_arrow);
        ll_credit = getView().findViewById(R.id.ll_credit);
        iv_credit_arrow = getView().findViewById(R.id.iv_credit_arrow);
        initializeRecyclerView();
        tv_title.setText(fragmentTitle(type));
        Log.e("GETPlayer_Cricket",""+fragmentTitle(type));

        ll_player.setOnClickListener(this);
        ll_points.setOnClickListener(this);
        ll_credit.setOnClickListener(this);

        updateSortArrow();
        if (getActivity()!=null)
            perFormFilter(((CreateTeamActivity)getActivity()).currentSortBy, ((CreateTeamActivity)getActivity()).currentSortType);
    }

    @Override
    public void onPageSelected() {
        super.onPageSelected();
        if (adapter != null && getView() != null) {
            if (adapter != null && getActivity() != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    public String fragmentTitle(int type) {

        Log.e("SetType2",""+type);

        String playerTypeFullName = getPlayerTypeFullName(type);
        if (isValidString(playerTypeFullName)) {
            String title = "Pick %s " + playerTypeFullName;

            Log.e("Title_Check1",""+title);
            Log.e("Title_Check2",""+playerTypeFullName);

            if (min == max) {
                title = String.format(title, max);
            } else {
                title = String.format(title, (min + "-" + max));
            }
            return title;
        }
        return "";

    }

    private String getPlayerTypeFullName(int type) {
        switch (type) {
            case 1:
                return "wicketkeeper";
            case 2:
                return "batsman";
            case 3:
                return "allrounder";
            case 4:
                return "bowler";
            default:
                return "";
        }
    }

    public String getMinPlayerMessage(int min, String playerType) {
        return String.format("Every pick needs at least %s %s", min, playerType);
    }


    private void initializeRecyclerView() {
        recycler_view = getView().findViewById(R.id.recycler_view);
        adapter = new PlayersAdapter(getActivity(), players) {

            @Override
            public Filter getFilter() {
                return filter;
            }

            @Override
            public boolean isMaxPlayerSelected() {
                return updateSelectedPlayer.getSelectedPlayer() >=
                        getPlayerTypeModels().getTmsize();
            }

            @Override
            public boolean isMaxFrom1Team(String teamname) {
                long playerCount = 0;
                if (teamname.equalsIgnoreCase(getMatchModel().getTeam1())) {
                    playerCount = updateSelectedPlayer.getTotalTeam1Players();
                } else {
                    playerCount = updateSelectedPlayer.getTotalTeam2Players();
                }
                return playerCount >= getPlayerTypeModels().getMxfromteam();
            }

            @Override
            public boolean isMaxPlayerType() {
                return updateSelectedPlayer.getPlayerCountByType(type) >= max;
            }

            @Override
            public boolean isMinPlayerFailed() {
                int previousSelectedPlayer = updateSelectedPlayer.getPlayerCountByType(type);
                previousSelectedPlayer++;
                int requiredSelectedPlayer = 0;
                for (int i = 1; i < 5; i++) {
                    int playerCount1 = updateSelectedPlayer.getPlayerCountByType(i);
                    int minPlayer = getPlayerType(i - 1).getMin();
                    if (i != type) {
                        requiredSelectedPlayer += Math.max(playerCount1, minPlayer);
                    }
                }

                int totalMinPlayer = requiredSelectedPlayer;
                if (totalMinPlayer + previousSelectedPlayer > getPlayerTypeModels().getTmsize()) {
                    return true;
                }
                return false;
            }

            @Override
            public boolean isCreditExceed(float playerCredit) {
                float totalCredit = updateSelectedPlayer.getSelectedPlayerTotalPoints()
                        + playerCredit;
                if (totalCredit > getPlayerTypeModels().getCredits()) {
                    return true;
                }
                return false;
            }

            @Override
            public String getPlayerTypeName(PlayerModel playerModel) {
                String teamName = "";
                if (playerModel.getTeam_name().equalsIgnoreCase(getMatchModel().getTeam1())) {
                    return "<font color='" + getResources().getColor(R.color.colorGray) + "'>" + playerModel.getTeamname() + "</font>";// + playerModel.getPtype();
                } else {
                    return "<font color='" + getResources().getColor(R.color.colorGray) + "'>" + playerModel.getTeamname() + "</font>";// + playerModel.getPtype();
                }
            }
        };
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                PlayerModel playerModel = adapter.getItem(position);
                switch (v.getId()) {
                    case R.id.rl_player_image:
                        Bundle bundle = new Bundle();
                        bundle.putString(DATA, new Gson().toJson(playerModel));
                        goToPlayerDetailActivity(bundle);
                        break;
                    default:
                        boolean b = onPlayerSelected(playerModel);
                        if (b) {
                            if (getActivity() != null)
                                ((CreateTeamActivity) getActivity()).refreshOtherFragmentData();
                        }
                        break;
                }
            }
        });
    }

    private void goToPlayerDetailActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), PlayerDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private boolean onPlayerSelected(PlayerModel playerModel) {
        if (playerModel.isSelected()) {
            playerModel.setSelected(false);
            updateSelectedPlayer.updatePlayer(playerModel);
            adapter.notifyDataSetChanged();
            return true;
        } else if (!adapter.isMaxPlayerSelected()) {
            if (!adapter.isMaxFrom1Team(playerModel.getTeamname())) {

                Log.e("getTeam_____name",""+playerModel.getTeamname());

                if (!adapter.isMaxPlayerType()) {
                    if (adapter.isMinPlayerFailed()) {
                        for (int i = 1; i < 5; i++) {
                            int playerCount1 = updateSelectedPlayer.getPlayerCountByType(i);
                            if (playerCount1 < getPlayerType(i - 1).getMin() && i != type) {
                                String minPlayerMessage = getMinPlayerMessage(getPlayerType(i - 1).getMin(),
                                        getPlayerTypeFullName(i));
                                showErrorMsg(minPlayerMessage);
                                adapter.notifyDataSetChanged();
                                return false;
                            }
                        }
                    }

                    if (!adapter.isCreditExceed(playerModel.getCredit())) {
                        playerModel.setSelected(true);
                        updateSelectedPlayer.updatePlayer(playerModel);
                        adapter.notifyDataSetChanged();
                        return true;
                    } else {
                        double leftCredit = getPlayerTypeModels().getCredits() - updateSelectedPlayer.getSelectedPlayerTotalPoints();
                        showErrorMsg(String.format("Only %s credit left", leftCredit));
                    }

                } else {
                    showErrorMsg(String.format("Only %s allowed", max + " " + getPlayerTypeFullName(type)));
                }
            } else {
                showErrorMsg(String.format("Max %s players from 1 pick", getPlayerTypeModels().getMxfromteam()));
                Log.e("getTeam_____name",""+playerModel.getTeamname());


            }
        } else {
            showErrorMsg(String.format("you already selected %s players ", getPlayerTypeModels().getTmsize()));
        }

        return false;
    }

    public interface UpdateSelectedPlayer {
        void updatePlayer(PlayerModel selectedPlayer);

        int getTotalTeam1Players();

        int getTotalTeam2Players();

        int getPlayerCountByType(int type);

        float getSelectedPlayerTotalPoints();

        int getSelectedPlayer();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_player:
                if (currentSortBy != null) {
                    if (currentSortBy.getId() != iv_player_arrow.getId()) {
                        currentSortBy = iv_player_arrow;
                        currentSortType = 0;
                    } else {
                        currentSortType = (currentSortType == 0) ? 1 : 0;
                    }
                } else {
                    currentSortBy = iv_player_arrow;
                    currentSortType = 0;
                }
                updateSortArrow();
                if (getActivity() != null)
                    ((CreateTeamActivity) getActivity()).performFilterOnOtherFragment(0, currentSortType);
                break;

            case R.id.ll_points:
                if (currentSortBy != null) {
                    if (currentSortBy.getId() != iv_points_arrow.getId()) {
                        currentSortBy = iv_points_arrow;
                        currentSortType = 0;
                    } else {
                        currentSortType = (currentSortType == 0) ? 1 : 0;
                    }
                } else {
                    currentSortBy = iv_points_arrow;
                    currentSortType = 0;
                }
                updateSortArrow();
                if (getActivity() != null)
                    ((CreateTeamActivity) getActivity()).performFilterOnOtherFragment(1, currentSortType);
                break;

            case R.id.ll_credit:
                if (currentSortBy != null) {
                    if (currentSortBy.getId() != iv_credit_arrow.getId()) {
                        currentSortBy = iv_credit_arrow;
                        currentSortType = 0;
                    } else {
                        currentSortType = (currentSortType == 0) ? 1 : 0;
                    }
                } else {
                    currentSortBy = iv_credit_arrow;
                    currentSortType = 0;
                }
                updateSortArrow();
                if (getActivity() != null)
                    ((CreateTeamActivity) getActivity()).performFilterOnOtherFragment(2, currentSortType);
                break;
        }
    }

    public void perFormFilter(int currentSortBy, int currentSortType) {
        if (getView() == null || adapter == null || getActivity() == null) return;
        switch (currentSortBy) {
            case 0: {
                this.currentSortBy = iv_player_arrow;
                this.currentSortType = currentSortType;
                updateSortArrow();
            }
            break;
            case 1: {
                this.currentSortBy = iv_points_arrow;
                this.currentSortType = currentSortType;
                updateSortArrow();
            }
            break;
            case 2: {
                this.currentSortBy = iv_credit_arrow;
                this.currentSortType = currentSortType;
                updateSortArrow();
            }
            break;
        }
    }

    private void updateSortArrow() {
        if (currentSortBy == null) {
            updateViewVisibitity(iv_player_arrow, View.INVISIBLE);
            updateViewVisibitity(iv_points_arrow, View.INVISIBLE);
            updateViewVisibitity(iv_credit_arrow, View.INVISIBLE);
        } else {
            switch (currentSortBy.getId()) {
                case R.id.iv_player_arrow:
                    updateViewVisibitity(iv_player_arrow, View.VISIBLE);
                    updateViewVisibitity(iv_points_arrow, View.INVISIBLE);
                    updateViewVisibitity(iv_credit_arrow, View.INVISIBLE);
                    break;
                case R.id.iv_points_arrow:
                    updateViewVisibitity(iv_player_arrow, View.INVISIBLE);
                    updateViewVisibitity(iv_points_arrow, View.VISIBLE);
                    updateViewVisibitity(iv_credit_arrow, View.INVISIBLE);
                    break;
                case R.id.iv_credit_arrow:
                    updateViewVisibitity(iv_player_arrow, View.INVISIBLE);
                    updateViewVisibitity(iv_points_arrow, View.INVISIBLE);
                    updateViewVisibitity(iv_credit_arrow, View.VISIBLE);
                    break;
            }

            if (currentSortType == 0) {
                currentSortBy.setRotation(-90);
            } else {
                currentSortBy.setRotation(90);
            }
            adapter.getFilter().filter(String.valueOf(currentSortBy.getId()));
        }
    }
}
