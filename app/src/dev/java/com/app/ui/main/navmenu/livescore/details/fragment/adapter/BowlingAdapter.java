package com.app.ui.main.navmenu.livescore.details.fragment.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.fullScoreBoard.BowlingModel;
import com.app.model.fullScoreBoard.CustomScoreModel;
import com.pickeleven.R;

import java.util.List;

public class BowlingAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<CustomScoreModel.PlayerBean> list;

    public BowlingAdapter(Context context, List<CustomScoreModel.PlayerBean> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_bowling));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tv_bowler_name;
        private TextView tv_o;
        private TextView tv_m;
        private TextView tv_r;
        private TextView tv_w;
        private TextView tv_ex;
        private TextView tv_econ;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_bowler_name = itemView.findViewById(R.id.tv_bowler_name);
            tv_o = itemView.findViewById(R.id.tv_o);
            tv_m = itemView.findViewById(R.id.tv_m);
            tv_r = itemView.findViewById(R.id.tv_r);
            tv_w = itemView.findViewById(R.id.tv_w);
            tv_ex = itemView.findViewById(R.id.tv_ex);
            tv_econ = itemView.findViewById(R.id.tv_econ);


        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            CustomScoreModel.PlayerBean data = list.get(position);
            tv_bowler_name.setText(data.getFullname());
            BowlingModel bowling = data.getBowling();
            if (bowling == null) return null;
            tv_o.setText(bowling.getOvers());
            tv_m.setText(bowling.getMaiden_overs());
            tv_r.setText(bowling.getRuns());
            tv_w.setText(bowling.getWickets());
            tv_ex.setText(bowling.getExtras());
            tv_econ.setText(bowling.getEconomy());
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
