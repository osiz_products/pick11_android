package com.app.ui.main.dashboard.myMatches;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.GameTypeModel;
import com.app.model.webrequestmodel.LoginRequestModel;
import com.app.model.webrequestmodel.Matchhistory;
import com.app.model.webresponsemodel.LoginResponse2Model;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.myMatches.MyMatchesCricketFragment;
import com.app.ui.main.cricket.myMatches.adapter.MyGameTypeAdapter;
import com.app.ui.main.football.myMatches.MyMatchesFootBallFragment;
import com.app.ui.main.kabaddi.myMatches.MyMatchesKabaddiFragment;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.ui.main.notification.NotificationActivity;
import com.base.BaseFragment;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

public class MyMatchesActivity extends AppBaseActivity implements ToolBarFragment.ToolbarFragmentInterFace {

    RecyclerView recycler_view;
    MyGameTypeAdapter gameTypeAdapter;
    TextView txtContests;
    TextView txtSeries;
    TextView txtMatches;
    TextView txtWins;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.BaseAppThemeDark);
        } else {
            setTheme(R.style.BaseAppTheme);
        }
        return R.layout.activity_my_matches;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return R.id.container_my_matches;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        initializeRecyclerView();
        getMyMatchCount();
        userPrefs = new UserPrefs(this);

        txtContests = findViewById(R.id.txtContests);
        txtMatches = findViewById(R.id.txtMatches);
        txtWins = findViewById(R.id.txtWins);
        txtSeries = findViewById(R.id.txtSeries);

        if(userPrefs!=null){

            if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){

                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }

        }else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case MY_MATCHCOUNT_REQUEST:
                handleMatchResponse(webRequest);
                break;

        }
    }

    private void getMyMatchCount() {
        getWebRequestHelper().getMyMatchCount(this);

    }

    private void handleMatchResponse(WebRequest webRequest) {
        Matchhistory extraData = webRequest.getExtraData(DATA);
        Matchhistory responseModel = webRequest.<Matchhistory>getResponsePojo(Matchhistory.class);
/*        Log.e("TotalJoinedContest",""+String.valueOf(responseModel.getTotalJoinedContest()));
        Log.e("TotalJoinedMatches",""+String.valueOf(responseModel.getTotalJoinedMatches()));
        Log.e("TotalJoinedSeries",""+String.valueOf(responseModel.getTotalJoinedSeries()));
        Log.e("TotalWins",""+String.valueOf(responseModel.getTotalWins()));*/

        txtContests.setText(String.valueOf(responseModel.getTotalJoinedContest()));
        txtMatches.setText(String.valueOf(responseModel.getTotalJoinedMatches()));
        txtSeries.setText(String.valueOf(responseModel.getTotalJoinedSeries()));
        txtWins.setText(String.valueOf(responseModel.getTotalWins()));
    }

    private void initializeRecyclerView() {

        recycler_view = findViewById(R.id.recycler_view);
        gameTypeAdapter = new MyGameTypeAdapter(this, MyApplication.getInstance().getGameTypeModels());
        recycler_view.setLayoutManager(getHorizentalLinearLayoutManager());
        recycler_view.setAdapter(gameTypeAdapter);
        recycler_view.setNestedScrollingEnabled(true);

        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                GameTypeModel gameTypeModel = MyApplication.getInstance().getGameTypeModels().get(position);
                if (gameTypeModel != null) {
                    gameTypeAdapter.setIsPosition(position);
                    MyApplication.getInstance().setGemeType(gameTypeModel);
                    if (gameTypeModel.isCricket()) {
                        addCricketFragment();
                    } else if (gameTypeModel.isKabaddi()) {
                        addKabaddiFragment();
                    } else {
                        addFootballFragment();
                    }
                }
            }
        });

        if (gameTypeAdapter != null) {
            Integer integer = Integer.valueOf(MyApplication.getInstance().getGemeType());
            gameTypeAdapter.setIsPosition(integer - 1);
            GameTypeModel gameTypeModel = MyApplication.getInstance().getGameTypeModels().get(integer - 1);
            if (gameTypeModel.isCricket()) {
                addCricketFragment();
            } else if (gameTypeModel.isKabaddi()) {
                addKabaddiFragment();
            } else {
                addFootballFragment();
            }
        }
    }


    private void addCricketFragment() {
        MyMatchesCricketFragment fragment = new MyMatchesCricketFragment();
        changeFragment(fragment, false, true, 0, true);
    }

    private void addFootballFragment() {
        MyMatchesFootBallFragment fragment = new MyMatchesFootBallFragment();
        changeFragment(fragment, false, true, 0, true);
    }

    private void addKabaddiFragment() {
        MyMatchesKabaddiFragment fragment = new MyMatchesKabaddiFragment();
        changeFragment(fragment, false, true, 0, true);
    }


    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.iv_wallet:
                goToMyAccountActivity(null);
                break;

            case R.id.iv_notification:
                Bundle bundle = new Bundle();
                bundle.putBoolean(NOT_BACK_BTN, false);
                goToNotificationActivity(bundle);
                break;
        }
    }

    private void goToMyAccountActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToNotificationActivity(Bundle bundle) {
        Intent intent = new Intent(this, NotificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
