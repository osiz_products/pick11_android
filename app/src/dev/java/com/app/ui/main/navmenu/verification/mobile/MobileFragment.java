package com.app.ui.main.navmenu.verification.mobile;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseFragment;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;

public class MobileFragment extends AppBaseFragment {

    private LinearLayout ll_call;
    private ImageView iv_call;
    private TextView tv_mobile_number_text;
    private TextView tv_mobile_number;
    private TextView tv_Verified_mobile;
    private LinearLayout ll_email;
    private ImageView iv_mail;
    private TextView tv_email_text;
    private TextView tv_email;
    private TextView tv_Verified_email;
    private TextView tv_click_email;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.fragment_mobile;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        ll_call = getView().findViewById(R.id.ll_call);
        iv_call = getView().findViewById(R.id.iv_call);
        tv_mobile_number_text = getView().findViewById(R.id.tv_mobile_number_text);
        tv_mobile_number = getView().findViewById(R.id.tv_mobile_number);
        tv_Verified_mobile = getView().findViewById(R.id.tv_Verified_mobile);
        ll_email = getView().findViewById(R.id.ll_email);
        iv_mail = getView().findViewById(R.id.iv_mail);
        tv_email_text = getView().findViewById(R.id.tv_email_text);
        tv_email = getView().findViewById(R.id.tv_email);
        tv_Verified_email = getView().findViewById(R.id.tv_Verified_email);
        tv_click_email = getView().findViewById(R.id.tv_click_email);

        if (getUserModel() == null) return;
        tv_mobile_number.setText(getUserModel().getPhone());
        tv_email.setText(getUserModel().getEmail());
        if (getUserModel().isPhoneVerify()) {
            ll_call.setActivated(true);
            updateViewVisibility(tv_Verified_mobile, View.VISIBLE);
        } else {
            ll_call.setActivated(false);
            updateViewVisibility(tv_Verified_mobile, View.GONE);
        }

        if (getUserModel().isEmailVerify()) {
            ll_email.setActivated(true);
            updateViewVisibility(tv_click_email, View.GONE);
            updateViewVisibility(tv_Verified_email, View.VISIBLE);
        } else {
            ll_email.setActivated(false);
            updateViewVisibility(tv_click_email, View.VISIBLE);
            updateViewVisibility(tv_Verified_email, View.GONE);
        }

        tv_click_email.setOnClickListener(this);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_click_email:
                sendVerifyEmail();
                break;
        }
    }

    private void sendVerifyEmail() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().sendVerifyEmail(this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_SEND_VERIFY_EMAIL:
                handleSendEmailResponse(webRequest);
                break;
        }
    }

    private void handleSendEmailResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }
}
