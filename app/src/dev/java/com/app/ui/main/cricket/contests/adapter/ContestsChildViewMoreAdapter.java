package com.app.ui.main.cricket.contests.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.ContestModel;
import com.pickeleven.BuildConfig;
import com.pickeleven.R;
import com.utilities.DeviceScreenUtil;

import java.util.List;

public class ContestsChildViewMoreAdapter  extends AppBaseRecycleAdapter {

    private Context context;
    List<ContestModel> list;
    ContestsChildViewMoreAdapter adapter = null;
    int num = 1;

    public ContestsChildViewMoreAdapter(Context context) {
        this.context = context;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_contests_child));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();

       /* if(num*2 > list.size()){
            return list.size();
        }else{
            return num*2;
        }*/
    }

    public void updateData(List<ContestModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ContestModel getJoinedContestModel(ContestModel contestModel) {
        return null;
    }


    private class ViewHolder extends BaseViewHolder {

        private CardView cv_card;
        private RelativeLayout ll_layout;
        private TextView tv_total_winning;
        private TextView tv_entry_fees;
        private ProgressBar progressBar;
        private TextView tv_left_team;
        private TextView tv_total_team;
        private TextView tv_percent;
        private TextView tv_c;
        private TextView tv_m;
        private TextView txtViewMore;

        public ViewHolder(View itemView) {
            super(itemView);
            cv_card = itemView.findViewById(R.id.cv_card);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            tv_total_winning = itemView.findViewById(R.id.tv_total_winning);
            tv_entry_fees = itemView.findViewById(R.id.tv_entry_fees);
            progressBar = itemView.findViewById(R.id.progressBar);
            tv_left_team = itemView.findViewById(R.id.tv_left_team);
            tv_total_team = itemView.findViewById(R.id.tv_total_team);
            tv_percent = itemView.findViewById(R.id.tv_percent);
            tv_c = itemView.findViewById(R.id.tv_c);
            tv_m = itemView.findViewById(R.id.tv_m);
            txtViewMore = itemView.findViewById(R.id.txtViewMore);

            int width = DeviceScreenUtil.getInstance().getWidth();
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) cv_card.getLayoutParams();
            layoutParams.width = width;
            cv_card.setLayoutParams(layoutParams);
        }

        @Override
        public String setData(int position) {

            if (list == null) return null;
            String currency = context.getString(R.string.currency_symbol) + " ";
            ContestModel contestModel = list.get(position);
            tv_entry_fees.setTag(position);

            ContestModel joinedContestModel = getJoinedContestModel(contestModel);
            if (joinedContestModel != null) {
                if (!joinedContestModel.isMultiJoin()) {
                    tv_entry_fees.setOnClickListener(null);
                    tv_entry_fees.setBackground(context.getResources().getDrawable(R.drawable.blank_bg));
                    tv_entry_fees.setTextColor(context.getResources().getColor(R.color.color_green_dark));
                } else {
                    tv_entry_fees.setOnClickListener(this);
                    tv_entry_fees.setBackground(context.getResources().getDrawable(R.drawable.button_bg));
                    tv_entry_fees.setTextColor(context.getResources().getColor(R.color.color_white));
                }
            } else if (contestModel.isContestFull()) {
                tv_entry_fees.setOnClickListener(null);
                tv_entry_fees.setBackground(context.getResources().getDrawable(R.drawable.blank_bg));
                tv_entry_fees.setTextColor(context.getResources().getColor(R.color.color_green_dark));
            } else {
                tv_entry_fees.setOnClickListener(this);
                tv_entry_fees.setBackground(context.getResources().getDrawable(R.drawable.button_bg));
                tv_entry_fees.setTextColor(context.getResources().getColor(R.color.color_white));
            }

            tv_total_winning.setText(currency + contestModel.digitToText());
            String flavor = BuildConfig.FLAVOR;
            if (flavor.equalsIgnoreCase("worldpro11Dev")) {
                tv_entry_fees.setText("Join " + currency + contestModel.get_entry_feesText());
            } else {
                tv_entry_fees.setText(currency + contestModel.get_entry_feesText());
            }

            progressBar.setMax(contestModel.getMaxteams());
            progressBar.setProgress(contestModel.getMaxteams() - contestModel.getJoinleft());

            tv_left_team.setText(contestModel.getTeamLeft());
            tv_total_team.setText(contestModel.getTotalTeam());
            tv_percent.setText(String.valueOf(contestModel.getWinners()));



          /*  if((adapter.num)*10 < list.size())
                adapter.num = adapter.num +1;*/

            if (contestModel.isConfirmWinning())
                updateViewVisibitity(tv_c, View.VISIBLE);
            else
                updateViewVisibitity(tv_c, View.GONE);

            tv_m.setText(contestModel.isMultiJoin() ? "M" : "S");
            return currency;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
