package com.app.ui.main.navmenu.support;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.webrequestmodel.ContactDetails;
import com.app.preferences.UserPrefs;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;
public class ContactUs extends AppBaseActivity {

    Button buttonContact;
    ImageView backpage;
    UserPrefs userPrefs;
    String getEmail;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_contact;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        getContactDetails();

        userPrefs = new UserPrefs(this);
        buttonContact = findViewById(R.id.buttonContact);
        backpage = findViewById(R.id.backpage);

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(ContactUs.this, SupportActivity.class);
                if (bundle != null) {
                    intent.putExtras(bundle);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);*/

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts(
                        "mailto",getEmail, null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, "Send mail..."));

            }
        });

        backpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case CONTACT_US_DETAILS:
                handleContactDetailsResponse(webRequest);
                break;
        }
    }

    private void getContactDetails() {
        getWebRequestHelper().getContactusDetails(this);
    }

    private void handleContactDetailsResponse(WebRequest webRequest) {
        //  ContactDetails extraData = webRequest.getExtraData(DATA);
        ContactDetails responseModel = webRequest.getResponsePojo(ContactDetails.class);
        getEmail = responseModel.getData().getEmail();
    }


}
