package com.app.ui.main.football.allContest;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestCategoryModel;
import com.app.model.ContestModel;
import com.app.model.MatchModel;
import com.app.model.webresponsemodel.JoinContestResponseModel;
import com.app.model.webresponsemodel.MatchContestResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.main.football.allContest.adapter.AllContestsAdapter;
import com.app.ui.main.football.contestDetail.ContestsDetailActivity;
import com.app.ui.main.football.myteam.chooseTeam.ChooseTeamActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.app.ui.main.football.contests.ContestsActivity.REQUEST_CREATE_TEAM;

@RequiresApi(api = Build.VERSION_CODES.N)
public class AllContestsActivity extends AppBaseActivity implements MatchTimerListener, ToolBarFragment.ToolbarFragmentInterFace {

    private TextView tv_team_one;
    private ImageView iv_clock;
    private TextView tv_timer_time;
    private TextView tv_total_contests;
    private LinearLayout ll_filter_price_pool;
    private ImageView iv_price_pool_arrow;
    private LinearLayout ll_filter_spots;
    private ImageView iv_filter_spots_arrow;
    private LinearLayout ll_filter_winners;
    private ImageView iv_filter_winners_arrow;
    private LinearLayout ll_filter_entry;
    private ImageView iv_filter_entry_arrow;
    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    AllContestsAdapter adapter;
    TextView tv_no_record_found;
    private MatchContestResponseModel.Details details;
    private List<ContestModel> joinedContestList = new ArrayList();
    List<ContestModel> contestList = new ArrayList<>();
    boolean isPricePool;
    boolean isSpots;
    boolean isWinning;
    boolean isEntry;
    UserPrefs userPrefs;

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    private boolean isEntrySort() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return extras.getBoolean(DATA);

        return false;
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_all_contests;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_team_one = findViewById(R.id.tv_team_one);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);
        tv_total_contests = findViewById(R.id.tv_total_contests);
        updateViewVisibility(tv_total_contests, View.GONE);
        ll_filter_price_pool = findViewById(R.id.ll_filter_price_pool);
        iv_price_pool_arrow = findViewById(R.id.iv_price_pool_arrow);
        ll_filter_spots = findViewById(R.id.ll_filter_spots);
        iv_filter_spots_arrow = findViewById(R.id.iv_filter_spots_arrow);
        ll_filter_winners = findViewById(R.id.ll_filter_winners);
        iv_filter_winners_arrow = findViewById(R.id.iv_filter_winners_arrow);
        ll_filter_entry = findViewById(R.id.ll_filter_entry);
        iv_filter_entry_arrow = findViewById(R.id.iv_filter_entry_arrow);

        setupSwipeLayout();
        recycler_view = findViewById(R.id.recycler_view);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);

        ll_filter_price_pool.setOnClickListener(this);
        ll_filter_spots.setOnClickListener(this);
        ll_filter_winners.setOnClickListener(this);
        ll_filter_entry.setOnClickListener(this);

        initializeRecyclerView();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJoinedContestList();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new AllContestsAdapter(this, contestList) {
            @Override
            public ContestModel getJoinedContestModel(ContestModel contestModel) {
                int index = joinedContestList.indexOf(contestModel);
                return index == -1 ? null : joinedContestList.get(index);
            }
        };
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        new ItemClickSupport(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                ContestModel contestModel = contestList.get(position);
                if (contestModel == null) return;
                ContestModel joinedContestModel = adapter.getJoinedContestModel(contestModel);
                Bundle bundle = new Bundle();
                switch (v.getId()) {
                    case R.id.tv_entry_fees:
                        if (details != null) {
                            int totalTeam = details.getTotalteams();
                            if (totalTeam > 0) {
                                bundle.putBoolean(DATA, true);
                                bundle.putString(DATA2, new Gson().toJson(contestModel));
                                bundle.putString(DATA1, String.valueOf(contestModel.getId()));
                                goToSwitchTeamActivity(bundle);
                            } else {
                                bundle.putString(DATA2, new Gson().toJson(contestModel));
                                goToCreateTeamActivity(bundle);
                            }
                        }

                        break;
                    default:
                        bundle.putString(DATA, String.valueOf(contestModel.getId()));
                        goToContestsDetailActivity(bundle);
                        break;
                }
            }
        });
    }

    private void goToSwitchTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToContestsDetailActivity(Bundle bundle) {
        Intent intent = new Intent(this, ContestsDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void getJoinedContestList() {
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getJoinedContests(getMatchModel(), this);
    }

    private void getContestMatchList() {
        swipeRefresh.setRefreshing(true);
        getWebRequestHelper().getMatchContestListFront(getMatchModel(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        swipeRefresh.setRefreshing(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_MATCH_CONTEST_FRONT:
                handleContestsResponse(webRequest);
                break;

            case ID_JOINED_CONTEST:
                handleJoinedContestsResponse(webRequest);
                break;
        }
    }


    private void handleContestsResponse(WebRequest webRequest) {
        MatchContestResponseModel responseModel = webRequest.getResponsePojo(MatchContestResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            this.details = responseModel.getDetails();
            List<ContestCategoryModel> data = responseModel.getData();
            contestList.clear();
            if (data != null && data.size() > 0) {
                for (ContestCategoryModel datum : data) {
                    contestList.addAll(datum.getContestPools());
                }
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

        if (contestList.size() > 0) {
            updateViewVisibility(tv_total_contests, View.VISIBLE);
            tv_total_contests.setText(String.valueOf(contestList.size() + " contests available"));
        } else updateViewVisibility(tv_total_contests, View.GONE);

        if (isEntrySort()) {
            ll_filter_entry.performClick();
        } else {
            ll_filter_spots.performClick();
        }

    }

    private void handleJoinedContestsResponse(WebRequest webRequest) {
        JoinContestResponseModel responseModel = webRequest.getResponsePojo(JoinContestResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                List<ContestModel> data = responseModel.getData();
                joinedContestList.clear();
                if (data != null && data.size() > 0) {
                    joinedContestList.addAll(data);
                }
            }
        }
        getContestMatchList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                if (getContestModel() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(DATA, true);
                    bundle.putString(DATA2, new Gson().toJson(getContestModel()));
                    bundle.putString(DATA1, String.valueOf(getContestModel().getId()));
                    goToSwitchTeamActivity(bundle);
                    supportFinishAfterTransition();
                }
            }
        }
    }

    public ContestModel getContestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA2);
            if (isValidString(string))
                return new Gson().fromJson(string, ContestModel.class);
            else
                return null;
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        isPricePool = false;
        isSpots = false;
        isWinning = false;
        isEntry = false;
        getJoinedContestList();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }

    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            if (getMatchModel().isUnderReview()) {
                tv_timer_time.setText("Under Review");
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            } else {
                tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_filter_price_pool:
                updateSortArrow(v);
                if (!isPricePool) {
                    isPricePool = true;
                    Collections.sort(contestList, new ContestModel.OrderTotalwining(true));
                    iv_price_pool_arrow.setRotation(90);
                } else {
                    isPricePool = false;
                    Collections.sort(contestList, new ContestModel.OrderTotalwining(false));
                    iv_price_pool_arrow.setRotation(-90);
                }
                break;

            case R.id.ll_filter_spots:
                updateSortArrow(v);
                if (!isSpots) {
                    isSpots = true;
                    Collections.sort(contestList, new ContestModel.OrderMaxteams(true));
                    iv_filter_spots_arrow.setRotation(90);
                } else {
                    isSpots = false;
                    Collections.sort(contestList, new ContestModel.OrderMaxteams(false));
                    iv_filter_spots_arrow.setRotation(-90);
                }
                break;

            case R.id.ll_filter_winners:
                updateSortArrow(v);
                if (!isWinning) {
                    isWinning = true;
                    Collections.sort(contestList, new ContestModel.OrderByMaxWinners(true));
                    iv_filter_winners_arrow.setRotation(90);
                } else {
                    isWinning = false;
                    Collections.sort(contestList, new ContestModel.OrderByMaxWinners(false));
                    iv_filter_winners_arrow.setRotation(-90);
                }
                break;

            case R.id.ll_filter_entry:
                updateSortArrow(v);
                if (!isEntry) {
                    isEntry = true;
                    Collections.sort(contestList, new ContestModel.OrderByFee(true));
                    iv_filter_entry_arrow.setRotation(90);
                } else {
                    isEntry = false;
                    Collections.sort(contestList, new ContestModel.OrderByFee(false));
                    iv_filter_entry_arrow.setRotation(-90);
                }
                break;
        }
        adapter.notifyDataSetChanged();
    }

    private void updateSortArrow(View v) {
        switch (v.getId()) {
            case R.id.ll_filter_price_pool:
                updateViewVisibility(iv_price_pool_arrow, View.VISIBLE);
                updateViewVisibility(iv_filter_spots_arrow, View.GONE);
                updateViewVisibility(iv_filter_winners_arrow, View.GONE);
                updateViewVisibility(iv_filter_entry_arrow, View.GONE);
                break;

            case R.id.ll_filter_spots:
                updateViewVisibility(iv_price_pool_arrow, View.GONE);
                updateViewVisibility(iv_filter_spots_arrow, View.VISIBLE);
                updateViewVisibility(iv_filter_winners_arrow, View.GONE);
                updateViewVisibility(iv_filter_entry_arrow, View.GONE);
                break;

            case R.id.ll_filter_winners:
                updateViewVisibility(iv_price_pool_arrow, View.GONE);
                updateViewVisibility(iv_filter_spots_arrow, View.GONE);
                updateViewVisibility(iv_filter_winners_arrow, View.VISIBLE);
                updateViewVisibility(iv_filter_entry_arrow, View.GONE);
                break;

            case R.id.ll_filter_entry:
                updateViewVisibility(iv_price_pool_arrow, View.GONE);
                updateViewVisibility(iv_filter_spots_arrow, View.GONE);
                updateViewVisibility(iv_filter_winners_arrow, View.GONE);
                updateViewVisibility(iv_filter_entry_arrow, View.VISIBLE);
                break;
        }
    }

    @Override
    public void onToolbarItemClick(View view) {
        switch (view.getId()) {
            case R.id.iv_wallet:
                goToMyAccountActivity(null);
                break;
        }
    }

    private void goToMyAccountActivity(Bundle bundle) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
