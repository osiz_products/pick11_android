package com.app.ui.main.cricket.myteam.chooseTeam.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TeamModel;
import com.app.model.TeamsPointModel;
import com.app.preferences.UserPrefs;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.pickeleven.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class ChooseTeamAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<TeamModel> list;
    private String selectedTeam;
    private String ids;

    UserPrefs userPrefs;

    private ArrayList<String> selectedid;
    private ArrayList<String> selectedidarray;
    private HashSet<String> hashSetselected;
    private HashSet<String> hashSetselectedarray;
    private ArrayList<Integer> teamid;

    JSONArray array;
    JSONArray jsonArray;

    int tag = 0;



    public ChooseTeamAdapter(Context context, List<TeamModel> list) {

        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        userPrefs = new UserPrefs(getContext());
        teamid = new ArrayList<>();
        selectedid=new ArrayList<>();
        selectedidarray=new ArrayList<>();
        hashSetselected =new HashSet<>();
        hashSetselectedarray =new HashSet<>();
        return new ViewHolder(inflateLayout(R.layout.item_choose_team));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public String getSelectedTeam() {
        return selectedTeam;
    }

    public void setSelectedTeam(int position) {
        String selectedTeam = String.valueOf(list.get(position).getId());
        if (this.selectedTeam != null && this.selectedTeam.equals(selectedTeam)) {
            this.selectedTeam = null;
        } else {
            this.selectedTeam = selectedTeam;
        }
        notifyDataSetChanged();
    }

    public void notifyTest() {

        notifyDataSetChanged();
    }

    public boolean isTeamAlreadyJoined(String teamId) {
        return false;
    }

    public void setDataTest(String ids) {
        selectedTeam = ids;
        Log.e("get_nEW_IDS",""+ids);
    }

    private class ViewHolder extends BaseViewHolder {

        private LinearLayout ll_layout;
        private CheckBox cb_team_name;
        private TextView tv_all_ready_added;
        private TextView tv_captain;
        private TextView cv_team_name;
        private TextView tv_vice_captain;
        private LinearLayout ll_preview;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            cb_team_name = itemView.findViewById(R.id.cb_team_name);
            cv_team_name = itemView.findViewById(R.id.cb_team_name);
            tv_all_ready_added = itemView.findViewById(R.id.tv_all_ready_added);
            tv_captain = itemView.findViewById(R.id.tv_captain);
            tv_vice_captain = itemView.findViewById(R.id.tv_vice_captain);
            ll_preview = itemView.findViewById(R.id.ll_preview);
//            cb_team_name.setClickable(false);
        }

        @Override
        public String setData(final int position) {
            if (list == null) return null;
            ll_layout.setTag(position);
            ll_preview.setTag(position);
            cb_team_name.setTag(position);
            ll_preview.setOnClickListener(this);
            cb_team_name.setOnClickListener(this);

            final TeamModel teamModel = list.get(position);
            cv_team_name.setText(teamModel.getTeamname());
//            cb_team_name.setText(teamModel.getTeamname());
            tv_captain.setText(teamModel.getCap());
            tv_vice_captain.setText(teamModel.getVcap());




          /*  cb_team_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                    TeamModel data=list.get(position);
                    if (ischecked) {
                        data.setChecked(true);
                        //hashSetselected.add(String.valueOf(list.get(position).getId()));
                        ll_layout.setActivated(true);
                        cb_team_name.setChecked(true);
                        selectedid.remove(""+teamModel.getId());
                        selectedid.add(""+teamModel.getId());
                    } else {
                        data.setChecked(false);
                      //  hashSetselected.remove(String.valueOf(list.get(position).getId()));
                        ll_layout.setActivated(false);
                        cb_team_name.setChecked(false);
                        selectedid.remove(""+teamModel.getId());
                    }

                    Log.e("Selected_productarray", "" + selectedid.toString());
*//*
                    selectedid.clear();
                    selectedid.addAll(hashSetselected);
                    array = new JSONArray(new ArrayList<String>());
                    Log.e("Selected", "" + hashSetselected);
                    Log.e("Selectedsize", "" + selectedid.size());


                    for (int i = 0; i < selectedid.size(); i++) {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("uteamid", selectedid.get(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        array.put(obj);
                    }
                    selectedTeam = String.valueOf(array);

                    Log.e("Selected_product", "" + selectedTeam);*//*
                }
            });*/
/*
            if (userPrefs.getselectall() == 1) {
//                checkselectall();
               // cb_team_name.setChecked(true);
                if (cb_team_name.isChecked()) {
                    try {
                        array = new JSONArray(new ArrayList<String>());
                        for (int i = 0; i <= list.size(); i++) {
                            hashSetselected.add(String.valueOf(list.get(i).getId()));
                            Log.e("Hashset", "" + hashSetselected);

                            JSONObject obj = new JSONObject();
                            try {
                                selectedid.clear();
                                selectedid.addAll(hashSetselected);
                                obj.put("uteamid", list.get(i).getId());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            array.put(obj);
                        }
                    }
                    catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    selectedTeam = String.valueOf(array);

                    Log.e("Selected_productarray", "" + selectedTeam);


                }

            }else {
               // cb_team_name.setChecked(false);

            }*/


            if (isTeamAlreadyJoined(String.valueOf(teamModel.getId()))) {
              //  ll_layout.setAlpha(0.5f);
                updateViewVisibitity(tv_all_ready_added, View.VISIBLE);
                Log.i("id", String.valueOf(teamModel.getId()));
                ll_layout.setActivated(true);
                cb_team_name.setChecked(true);
            }
            else{
                updateViewVisibitity(tv_all_ready_added,View.GONE);
              /*  ll_layout.setActivated(false);
                cb_team_name.setChecked(false);*/
            }
            /*TeamsPointModel myChooseTeam = getMyChooseTeam(teamModel);
            if (myChooseTeam != null) {
                ll_layout.setOnClickListener(null);
                cb_team_name.setChecked(true);
                ll_layout.setAlpha(0.5f);
                updateViewVisibility(tv_all_ready_added, View.VISIBLE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_item_bg2));
            } else if (getSelectedTeam() != null) {
                cb_team_name.setChecked(true);
                ll_layout.setAlpha(1f);
                updateViewVisibility(tv_all_ready_added, View.GONE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_item_bg2));
            } else {
                ll_layout.setOnClickListener(this);
                cb_team_name.setChecked(false);
                ll_layout.setAlpha(1f);
                updateViewVisibility(tv_all_ready_added, View.GONE);
                ll_layout.setBackgroundColor(context.getResources().getColor(R.color.color_white));
            }*/

            if(teamModel.isChecked()){
                cb_team_name.setChecked(true);
                ll_layout.setActivated(true);

            }else {
                cb_team_name.setChecked(false);
                ll_layout.setActivated(false);
            }

            if (isTeamAlreadyJoined(String.valueOf(teamModel.getId()))) {
          //      ll_layout.setAlpha(0.5f);
               /* updateViewVisibitity(tv_all_ready_added, View.VISIBLE);
                Log.i("id", String.valueOf(teamModel.getId()));
                ll_layout.setActivated(true);
                cb_team_name.setChecked(true);*/
            }

            //Log.e("Selected_productarray", "" + selectedid.toString());
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }

    }

    private void checkselectall() {
    }
}
