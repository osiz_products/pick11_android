package com.app.ui.main.allTransaction;

import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.TransactionModel;
import com.app.model.webrequestmodel.TransactionRequestModel;
import com.app.model.webresponsemodel.TransactionResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.allTransaction.adapter.AllTransactionsAdapter;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;

import java.util.ArrayList;
import java.util.List;

public class AllTransactionsActivity extends AppBaseActivity {

    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    AllTransactionsAdapter adapter;
    TextView tv_no_record_found;
    List<TransactionModel> list = new ArrayList<>();
    private int totalPages = 1000;
    private int currentPage = 0;
    boolean loadingNextData = false;
    UserPrefs userPrefs;


    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_all_transactions;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        setupSwipeLayout();
        recycler_view = findViewById(R.id.recycler_view);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);

        initializeRecyclerView();
        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                currentPage = 0;
                callNextApi();
            }
        });
    }

    private void initializeRecyclerView() {

        recycler_view = findViewById(R.id.recycler_view);
        adapter = new AllTransactionsAdapter(this);
        adapter.updateData(list);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = recycler_view.getLayoutManager().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) recycler_view.getLayoutManager()).findLastVisibleItemPosition();
                if (!loadingNextData && totalItemCount <= (lastVisibleItem + 5)) {
                    callNextApi();
                }
            }
        });

        callNextApi();
    }

    private void callNextApi() {
        if (isFinishing()) return;
        if (this.currentPage == 0) {
            this.currentPage = 1;
            this.totalPages = 1000;
            callApi();
            return;
        }
        if (this.totalPages > this.currentPage) {
            this.currentPage = this.currentPage + 1;
            callApi();
        }
    }

    private void callApi() {

        setLoadingNextData(true);
        TransactionRequestModel requestModel = new TransactionRequestModel();
        requestModel.page = currentPage;
        requestModel.limit = 15;
        requestModel.gameid = MyApplication.getInstance().getGemeType();
        getWebRequestHelper().getTransactions(requestModel, this);
    }

    public void setLoadingNextData(boolean isLoading) {
        if (!isFinishing()) {
            this.loadingNextData = isLoading;
            if (swipeRefresh.isRefreshing()) {
                swipeRefresh.setRefreshing(isLoading);
            } else {
                if (adapter != null)
                    adapter.setLoadMore(loadingNextData);
            }
        }
    }

    public void updateData(List<TransactionModel> levelList) {
        this.list.clear();
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void addDataToList(List<TransactionModel> levelList) {
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        setLoadingNextData(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_MY_TRANSACTION:
                handleContestsResponse(webRequest);
                break;
        }
    }

    private void handleContestsResponse(WebRequest webRequest) {
        TransactionResponseModel responseModel = webRequest.getResponsePojo(TransactionResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            TransactionResponseModel.DataBean modelData = responseModel.getData();
            List<TransactionModel> data = modelData.getList();
            if (data != null && data.size() == 0) {
                totalPages = currentPage;
            }
            if (currentPage == 1) {
                updateData(data);
            } else {
                addDataToList(data);
            }
        } else {
            if (isFinishing()) return;
            setLoadingNextData(false);
            totalPages = 0;
        }

    }

    private void updateView(List<TransactionModel> list) {
        if (list != null && list.size() > 0)
            updateViewVisibility(tv_no_record_found, View.GONE);
        else
            updateViewVisibility(tv_no_record_found, View.VISIBLE);

    }

}
