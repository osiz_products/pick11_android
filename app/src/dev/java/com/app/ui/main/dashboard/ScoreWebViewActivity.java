package com.app.ui.main.dashboard;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.pickeleven.R;

public class ScoreWebViewActivity extends AppBaseActivity {

    WebView wv_web_view;
    UserPrefs userPrefs;

    private String getUrl() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getString(DATA);
        }
        return "";
    }

    private String getTitles() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getString(DATA2);
        }
        return "";
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.activity_web_view;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        wv_web_view = findViewById(R.id.wv_web_view);
        wv_web_view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        wv_web_view.setBackgroundColor(0x00000000);
        wv_web_view.getSettings().setJavaScriptEnabled(true);
        wv_web_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= 21) {
            wv_web_view.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        //FOR WEBPAGE SLOW UI
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv_web_view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            wv_web_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        WebSettings webSettings = wv_web_view.getSettings();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            webSettings.setDefaultTextEncodingName("utf-8");
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setDomStorageEnabled(true);
        }

        webSettings.setJavaScriptEnabled(true);
        setHeaderTitle(getTitles() == null ? "" : getTitles());
        getViewData();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void getViewData() {
        MyApplication.getInstance().printLog("Url:  ", getUrl());
        wv_web_view.loadUrl(getUrl());
        Log.e("GetWeb_Url",""+getUrl());
    }
}

