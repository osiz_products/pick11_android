package com.app.ui.main.navmenu.myaccount;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.webresponsemodel.PaymentResponseModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.paytmpayment.PaytmPaymentDialog;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * @since 1/10/18
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class LowBalanceActivity extends AppBaseActivity implements MatchTimerListener {

    LinearLayout ll_contest_detail_lay;
    TextView tv_team_one;
    ImageView iv_clock;
    TextView tv_timer_time;

    TextView tv_remain_balance;
    TextView tv_entry;
    ImageView iv_info;
    TextView tv_cash_bonus;
    TextView tv_heading;


    EditText et_amount;
    TextView tv_amount1;
    TextView tv_amount2;
    TextView tv_amount3;
    TextView tv_proceed;

    UserPrefs userPrefs;

    PreJoinResponseModel.PreJoinedModel preJoinedModel;

    public PreJoinResponseModel.PreJoinedModel getPreJoinedModel() {
        return preJoinedModel;
    }

    public PreJoinResponseModel.PreJoinedModel getContestDetail() {
        Bundle extras = getIntent().getExtras();
        String data = (extras == null ? null : extras.getString(DATA));
        if (isValidString(data)) {
            try {
                return new Gson().fromJson(data, PreJoinResponseModel.PreJoinedModel.class);
            } catch (JsonSyntaxException e) {

            }
        }
        return null;
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String data = s.toString().trim();
            if (isValidString(data)) {
                if (data.equals(currency_symbol.trim())) {
                    et_amount.setText("");
                    et_amount.setSelection(0);
                } else if (!data.startsWith(currency_symbol.trim())) {
                    data = currency_symbol + data;
                    et_amount.setText(data);
                    et_amount.setSelection(data.length());
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().addMatchTimerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }


    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_low_balance;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        preJoinedModel = getContestDetail();
        userPrefs = new UserPrefs(this);
        ll_contest_detail_lay = findViewById(R.id.ll_contest_detail_lay);
        tv_team_one = findViewById(R.id.tv_team_one);
        iv_clock = findViewById(R.id.iv_clock);
        tv_timer_time = findViewById(R.id.tv_timer_time);
        iv_info = findViewById(R.id.iv_info);
        tv_remain_balance = findViewById(R.id.tv_remain_balance);
        tv_entry = findViewById(R.id.tv_entry);
        tv_cash_bonus = findViewById(R.id.tv_cash_bonus);


        tv_heading = findViewById(R.id.tv_heading);

        et_amount = findViewById(R.id.et_amount);
        et_amount.removeTextChangedListener(textWatcher);
        et_amount.addTextChangedListener(textWatcher);
        tv_amount1 = findViewById(R.id.tv_amount1);
        tv_amount2 = findViewById(R.id.tv_amount2);
        tv_amount3 = findViewById(R.id.tv_amount3);
        tv_proceed = findViewById(R.id.tv_proceed);
        tv_amount1.setOnClickListener(this);
        tv_amount2.setOnClickListener(this);
        tv_amount3.setOnClickListener(this);
        tv_proceed.setOnClickListener(this);

        tv_amount1.setTag("100");
        tv_amount2.setTag("200");
        tv_amount3.setTag("500");


        if (preJoinedModel != null) {
            updateViewVisibility(ll_contest_detail_lay, View.VISIBLE);
            tv_remain_balance.setText(currency_symbol + preJoinedModel.getRemainBalancetext());
            tv_entry.setText(currency_symbol + preJoinedModel.getEnteryFeestext());
            tv_cash_bonus.setText("- " + currency_symbol + preJoinedModel.getUsedBonusText());

            tv_heading.setText("Add cash to your account to join this contest");


        } else {
            updateViewVisibility(ll_contest_detail_lay, View.GONE);
            tv_heading.setText("ADD CASH TO YOUR ACCOUNT");
        }

        tv_amount1.setText(currency_symbol + String.valueOf(tv_amount1.getTag()));
        tv_amount2.setText(currency_symbol + String.valueOf(tv_amount2.getTag()));
        tv_amount3.setText(currency_symbol + String.valueOf(tv_amount3.getTag()));

        if (preJoinedModel != null) {
            et_amount.setText(String.valueOf(preJoinedModel.getFeesText()));
        } else {
            et_amount.setText(String.valueOf(tv_amount1.getTag()));
        }
        setMatchData();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_amount1:
                et_amount.setText(String.valueOf(tv_amount1.getTag()));
                break;
            case R.id.tv_amount2:
                et_amount.setText(String.valueOf(tv_amount2.getTag()));
                break;
            case R.id.tv_amount3:
                et_amount.setText(String.valueOf(tv_amount3.getTag()));
                break;
            case R.id.tv_proceed:
                callDepositAmount();
                break;

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    private void callDepositAmount() {
        String amount = et_amount.getText().toString().trim();
        if (amount.startsWith(currency_symbol) && amount.length() > 1) {
            amount = amount.substring(1).trim();
        }

        if (!isValidString(amount)) {
            showErrorMsg("Please enter amount");
            return;
        }

        if (!checkValidAmount(amount)) {
            showErrorMsg("Please enter valid amount");
            return;
        }
        float amt = Float.parseFloat(amount);

        final PaytmPaymentDialog paytmPaymentDialog = new PaytmPaymentDialog();
        paytmPaymentDialog.setPaymentSuccessListener(new PaytmPaymentDialog.PaymentSuccessListener() {
            @Override
            public void onPaymentSuccess(PaymentResponseModel responseModel) {
                paytmPaymentDialog.dismiss();
                if (responseModel != null && getUserModel() != null) {
                    if (responseModel.getData() != null)
                        if (responseModel.getData().getWallet() != null) {
                            getUserModel().setWalletModel(responseModel.getData().getWallet());
                            updateUserInPrefs();
                            showCustomToast(responseModel.getMsg());
                            setResult(RESULT_OK);
                            supportFinishAfterTransition();
                        }
                }
            }
        });
        paytmPaymentDialog.setWalletRechargeAmount(String.valueOf(amt));
        paytmPaymentDialog.show(getFm(), PaytmPaymentDialog.class.getSimpleName());
    }

    private boolean checkValidAmount(String amount) {
        if (isValidString(amount)) {
            try {
                double amt = Double.parseDouble(amount);
                return amt > 0;
            } catch (NumberFormatException e) {

            }
        }

        return false;
    }


    @Override
    public void onMatchTimeUpdate() {
        setMatchData();
    }

    private void setMatchData() {
        if (getMatchModel() != null) {
            tv_team_one.setText(getMatchModel().getTeam1() + " vs " + getMatchModel().getTeam2());
            if (getMatchModel().isUnderReview()) {
                tv_timer_time.setText("Under Review");
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            } else {
                tv_timer_time.setText(getMatchModel().getRemainTimeText(MyApplication.getInstance().getServerdate()));
                tv_timer_time.setTextColor(getResources().getColor(getMatchModel().getTimerColor()));
                iv_clock.setColorFilter(ContextCompat.getColor(this, getMatchModel().getTimerColor()));
            }
        }
    }
}
