package com.app.ui.main.kabaddi.myteam.playerpreview;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.TeamModel;
import com.app.model.webresponsemodel.PlayerPreviewKabaddiResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.myteam.playerDetail.PlayerPointsDetailActivity;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class TeamPreviewDialog extends AppBaseDialogFragment {

    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout bottom_sheet;

    TextView tv_team_name;
    ImageView iv_edit_team;
    ImageView iv_close;
    PlayerView defender;
    PlayerView raider;
    PlayerView allrounder;

    DialogInterface.OnClickListener onClickListener;

    PlayerPreviewKabaddiResponseModel.DataBean team;
    TeamModel teamModel;

    public void setTeam(PlayerPreviewKabaddiResponseModel.DataBean team) {
        this.team = team;
    }

    public void setTeamModel(TeamModel teamModel) {
        this.teamModel = teamModel;
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    public static TeamPreviewDialog getInstance(Bundle bundle) {
        TeamPreviewDialog messageDialog = new TeamPreviewDialog();
        if (bundle != null)
            messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_team_preview_kabaddi;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        bottom_sheet = getView().findViewById(R.id.bottom_sheet);
        initializeBottomSheet();

        tv_team_name = getView().findViewById(R.id.tv_team_name);
        iv_edit_team = getView().findViewById(R.id.iv_edit_team);
        updateViewVisibitity(iv_edit_team, View.GONE);
        iv_close = getView().findViewById(R.id.iv_close);
        defender = getView().findViewById(R.id.defender);
        raider = getView().findViewById(R.id.raider);
        allrounder = getView().findViewById(R.id.allrounder);

        iv_close.setOnClickListener(this);

        if (teamModel != null)
            getTeamPlayers();
        else
            setupView();

    }

    private void getTeamPlayers() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    private void initializeBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    dismiss();
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }


    private void setupView() {
        if (team != null && team.getPlayers() != null) {
            final View viewById = getView().findViewById(R.id.ll_data_lay);
            viewById.post(new Runnable() {
                @Override
                public void run() {
                    float defaultItemInRow = 5.0f;
                    int width = viewById.getWidth();
                    List<PlayerModel> defender = team.getPlayers().getDefender();
                    if (defender.size() > defaultItemInRow) {
                        defaultItemInRow = defender.size();
                    }
                    List<PlayerModel> raider = team.getPlayers().getRaider();
                    if (raider.size() > defaultItemInRow) {
                        defaultItemInRow = raider.size();
                    }
                    List<PlayerModel> allrounder = team.getPlayers().getAllrounder();
                    if (allrounder.size() > defaultItemInRow) {
                        defaultItemInRow = allrounder.size();
                    }
                    int itemSize = Math.round(width / defaultItemInRow);
                    TeamPreviewDialog.this.defender.setPerItemSize(itemSize);
                    TeamPreviewDialog.this.raider.setPerItemSize(itemSize);
                    TeamPreviewDialog.this.allrounder.setPerItemSize(itemSize);

                    TeamPreviewDialog.this.defender.setupItems(defender.size());
                    TeamPreviewDialog.this.raider.setupItems(raider.size());
                    TeamPreviewDialog.this.allrounder.setupItems(allrounder.size());

                    setupData();

                }
            });


        }

    }

    private void setupData() {
        if (team != null) {
            tv_team_name.setText(team.getName());
            setupDefenderData();
            setupRaiderData();
            setupAllrounderData();
        }
    }

    private void setupDefenderData() {
        List<PlayerModel> defender = team.getPlayers().getDefender();
        List<View> itemViews = TeamPreviewDialog.this.defender.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);
            iv_players_image.setOnClickListener(this);

            PlayerModel playerModel = defender.get(i);
            iv_players_image.setTag(playerModel);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");

            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
//                ((AppBaseActivity) getContext()).loadImage(getContext(), iv_players_image,
//                        null, playerModel.getPimg(), R.drawable.dummy_player_icon);
                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }

            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }
            iv_players_image.setTag(playerModel);
            iv_players_image.setOnClickListener(this);
            view.setTag(playerModel);
            view.setOnClickListener(this);
        }
    }

    private void setupRaiderData() {
        List<PlayerModel> raider = team.getPlayers().getRaider();
        List<View> itemViews = TeamPreviewDialog.this.raider.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = raider.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");
            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }


            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }
            iv_players_image.setTag(playerModel);
            iv_players_image.setOnClickListener(this);
            view.setTag(playerModel);
            view.setOnClickListener(this);
        }
    }

    private void setupAllrounderData() {
        List<PlayerModel> wicketkeeper = team.getPlayers().getAllrounder();
        List<View> itemViews = TeamPreviewDialog.this.allrounder.getItemViews();
        for (int i = 0; i < itemViews.size(); i++) {
            View view = itemViews.get(i);
            ImageView iv_players_image = view.findViewById(R.id.iv_players_image);
            TextView tv_players_name = view.findViewById(R.id.tv_players_name);
            TextView tv_players_points = view.findViewById(R.id.tv_players_points);
            TextView tv_player_type = view.findViewById(R.id.tv_player_type);

            PlayerModel playerModel = wicketkeeper.get(i);
            tv_players_name.setText(playerModel.getPname());
            if (getMatchModel().isFixtureMatch())
                tv_players_points.setText(playerModel.getCreditText() + " Cr");
            else
                tv_players_points.setText(playerModel.getPointsText() + " Pts");
            if (playerModel.getPid().equals(team.getCaptainid())) {
                tv_player_type.setText("c");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else if (playerModel.getPid().equals(team.getVicecaptainid())) {
                tv_player_type.setText("vc");
                updateViewVisibitity(tv_player_type, View.VISIBLE);
            } else {
                updateViewVisibitity(tv_player_type, View.GONE);
            }

            if (playerModel.getPimg() != null) {
                Picasso.get().load(playerModel.getPimg()).into(iv_players_image);
            } else {
                iv_players_image.setImageResource(R.drawable.dummy_player_icon);
            }


            if (playerModel.getTeamname().equals(team.getTeam1_name())) {
                tv_players_name.setActivated(true);
            } else {
                tv_players_name.setActivated(false);
            }

            iv_players_image.setTag(playerModel);
            iv_players_image.setOnClickListener(this);
            view.setTag(playerModel);
            view.setOnClickListener(this);

        }
    }

    @Override
    public void onClick(View v) {
        /*if (v.getId() == R.id.iv_players_image) {
            PlayerModel playerModel = (PlayerModel) v.getTag();
            if (playerModel != null)
                MyApplication.getInstance().printLog("glf", playerModel.getFullname());
        } else if (onClickListener != null) {
            onClickListener.onClick(this.getDialog(),
                    v.getId() == R.id.iv_close ?
                            DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE);
        } else {
            dismiss();
        }*/

        switch (v.getId()) {
            case R.id.iv_players_image:
            case R.id.rl_player_view:
                if (getMatchModel().isFixtureMatch()) {
                    showCustomToast("Player stats available after match start.");
                    return;
                }
                PlayerModel playerModel = (PlayerModel) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(playerModel));
                goToPlayerPointDetailActivity(bundle);
                break;
            case R.id.iv_close:
//                if (onClickListener != null) {
//                    onClickListener.onClick(this.getDialog(),
//                            v.getId() == R.id.iv_close ?
//                                    DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE);
//                }
                dismiss();
                break;
        }
    }


    private void goToPlayerPointDetailActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), PlayerPointsDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;
        }
    }

    private void handleTeamPlayersResponse(WebRequest webRequest) {
        PlayerPreviewKabaddiResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewKabaddiResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> defender = new ArrayList<>();
            List<PlayerModel> raider = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();

            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                    defender.add(playerModel);
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                    raider.add(playerModel);
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                }
            }

            PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean();
            playersBean.setDefender(defender);
            playersBean.setRaider(raider);
            playersBean.setAllrounder(allrounder);

            PlayerPreviewKabaddiResponseModel.DataBean team = new PlayerPreviewKabaddiResponseModel.DataBean();
            team.setId("0");
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);

            setTeam(team);
            setupView();

        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }
}
