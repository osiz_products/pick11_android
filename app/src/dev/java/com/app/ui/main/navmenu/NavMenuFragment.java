package com.app.ui.main.navmenu;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.multidex.BuildConfig;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseFragment;
import com.app.model.UserModel;
import com.app.ui.dialogs.ConfirmationDialog;
import com.app.ui.main.dashboard.myMatches.MyMatchesActivity;
import com.app.ui.main.navmenu.faq.FaqActivity;
import com.app.ui.main.navmenu.livescore.NewLiveScoreActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.ui.main.navmenu.profile.ProfileActivity;
import com.app.ui.main.navmenu.referFriend.ReferFriendActivity;
import com.app.ui.main.navmenu.support.SupportActivity;
import com.app.ui.main.navmenu.verification.VerificationActivity;
import com.app.ui.main.notification.NotificationActivity;
import com.pickeleven.R;



public class NavMenuFragment extends AppBaseFragment {

    TextView tv_name;
    TextView tv_email;
    TextView tv_phone;
    TextView tv_Team_name;

    ProgressBar pb_image;
    ImageView iv_profile_pic;
    ImageView iv_edit_pic;

    LinearLayout ll_home;
    LinearLayout ll_my_matches;
    LinearLayout ll_my_profile;
    LinearLayout ll_my_account;
    LinearLayout ll_refer_friends;
    LinearLayout ll_verified;
    LinearLayout ll_notification;
    LinearLayout ll_live_score;
    LinearLayout ll_more;
    LinearLayout ll_logout;
    LinearLayout ll_scratch;
    TextView tv_app_version;

    boolean needAnim = true;
    DrawerLayout drawerLayout;
    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            if (needAnim && slideOffset > 0.1) {
                needAnim = false;
            //    ((ViewGroup) getView().findViewById(R.id.ll_data_lay)).startLayoutAnimation();
                ((ViewGroup) getView().findViewById(R.id.ll_data_lay)).startLayoutAnimation();
            }
        }
        @Override
        public void onDrawerOpened(@NonNull View drawerView) {

        }
        @Override
        public void onDrawerClosed(@NonNull View drawerView) {
            needAnim = true;
        }
        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_nav_menu;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        if (getActivity() == null || getView() == null) return;
        drawerLayout = getActivity().findViewById(R.id.drawer_layout);
        if (drawerLayout == null) return;
        drawerLayout.removeDrawerListener(drawerListener);
        drawerLayout.addDrawerListener(drawerListener);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        tv_name = getView().findViewById(R.id.tv_name);
        tv_name.setOnClickListener(this);
        tv_email = getView().findViewById(R.id.tv_email);
        tv_email.setOnClickListener(this);
        tv_phone = getView().findViewById(R.id.tv_phone);
        tv_Team_name = getView().findViewById(R.id.tv_Team_name);

        pb_image = getView().findViewById(R.id.pb_image);
        iv_profile_pic = getView().findViewById(R.id.iv_profile_pic);
        iv_edit_pic = getView().findViewById(R.id.iv_edit_pic);

        ll_home = getView().findViewById(R.id.ll_home);
        ll_my_matches = getView().findViewById(R.id.ll_my_matches);
      //  ll_my_profile = getView().findViewById(R.id.ll_my_profile);
        ll_my_account = getView().findViewById(R.id.ll_my_account);
        ll_refer_friends = getView().findViewById(R.id.ll_refer_friends);
        ll_verified = getView().findViewById(R.id.ll_verified);
        ll_notification = getView().findViewById(R.id.ll_notification);
        ll_live_score = getView().findViewById(R.id.ll_live_score);
        ll_scratch = getView().findViewById(R.id.ll_scratch);
        ll_more = getView().findViewById(R.id.ll_more);
        ll_logout = getView().findViewById(R.id.ll_logout);
        String flavor = BuildConfig.FLAVOR;

        if (flavor.equalsIgnoreCase("pick11Dev")){
            updateViewVisibility(ll_notification, View.GONE);
        }

        ll_home.setOnClickListener(this);
        ll_my_matches.setOnClickListener(this);
       // ll_my_profile.setOnClickListener(this);
        ll_my_account.setOnClickListener(this);
        ll_refer_friends.setOnClickListener(this);
        ll_verified.setOnClickListener(this);
        ll_notification.setOnClickListener(this);
        ll_live_score.setOnClickListener(this);
        ll_more.setOnClickListener(this);
        ll_scratch.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        tv_app_version = getView().findViewById(R.id.tv_app_version);

        iv_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                /*if (bundle != null) {
                    intent.putExtras(bundle);
                }*/
                startActivity(intent);
            }
        });

        getVersionCode();
        setUserData();

    }

    private void setUserData() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            tv_name.setText(userModel.getName());
            tv_email.setText(userModel.getEmail());
            tv_phone.setText((userModel.getPhone()));
            tv_Team_name.setText((userModel.getTeamname()));

            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                ((AppBaseActivity) getActivity()).loadImage(this, iv_profile_pic, pb_image, imageUrl,
                        R.drawable.no_image, 300);
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibitity(pb_image, View.INVISIBLE);
            }
        }
    }

    private void getVersionCode() {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            tv_app_version.setText("Version " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void handleDrawer() {
        if (!closeDrawer()) {
            if (getActivity() == null) return;
            drawerLayout.openDrawer(getActivity().findViewById(R.id.fragment_navigation_drawer));
        }
    }

    public boolean closeDrawer() {
        if (getActivity() == null) return false;
        if (drawerLayout.isDrawerOpen(getActivity().findViewById(R.id.fragment_navigation_drawer))) {
            drawerLayout.closeDrawers();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.ll_home:
                closeDrawer();
                break;

            case R.id.ll_my_matches:
                goToMyMatchesActivity(null);
                break;

          /*  case R.id.ll_my_profile:
                goToProfileActivity(null);
                break;*/

            case R.id.ll_my_account:
                goToMyAccountActivity(null);
                break;

            case R.id.ll_refer_friends:
                goToReferFriendActivity(null);
                break;


            case R.id.ll_more:
                goToFaqActivity(null);
                break;

            case R.id.ll_verified:
                goToVerificationActivity(null);
                break;

            case R.id.ll_notification:
                bundle.putBoolean(NOT_BACK_BTN, false);
                goToNotificationActivity(bundle);
                break;

            case R.id.ll_live_score:
                goToLiveScoreActivity(null);
                break;

            case R.id.ll_logout:
                addConfirmationDialog();
                break;
        }
    }

    public void goToProfileActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToMyAccountActivity(Bundle bundle) {
        closeDrawer();
        if (NavMenuFragment.this == null) return;
        Intent intent = new Intent(getActivity(), MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToReferFriendActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), ReferFriendActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToMyMatchesActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), MyMatchesActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToFaqActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), FaqActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToSupportActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), SupportActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToVerificationActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), VerificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToNotificationActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), NotificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToLiveScoreActivity(Bundle bundle) {
        closeDrawer();
        if (getActivity() == null) return;
        Intent intent = new Intent(getActivity(), NewLiveScoreActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getActivity() == null) return;
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void addConfirmationDialog() {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, "Are you sure want to logout?");
        ConfirmationDialog dialog = ConfirmationDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE)
                    ((AppBaseActivity) getActivity()).callLogout();
                dialog.dismiss();
            }
        });
        dialog.show(getChildFm(), "Confirm Logout");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppBaseActivity.REQUEST_CASHOUT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                String message = data.getStringExtra("message");
                if (message != null && !message.trim().isEmpty()) {
                    showCustomToast(message);
                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getUserPrefs().removeListener(this);
        getUserPrefs().addListener(this);
        if (drawerLayout == null) return;
        setupUserDetail();
    }

    @Override
    public void onPause() {
        super.onPause();
        getUserPrefs().removeListener(this);
    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {
        super.loggedInUserUpdate(userModel);
        setupUserDetail();
    }

    private void setupUserDetail() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            tv_name.setText(userModel.getName());
            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                if (getActivity() != null) {
                    ((AppBaseActivity) getActivity()).loadImage(this, iv_profile_pic, pb_image, imageUrl,
                            R.drawable.no_image, 300);
                }
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibitity(pb_image, View.INVISIBLE);
            }

        }
    }


}
