package com.app.ui.main.kabaddi.myteam.createTeam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.viewpager.widget.ViewPager;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseFragment;
import com.app.appbase.ViewPagerAdapter;
import com.app.customViews.ProgressBarView;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerTypeModel;
import com.app.model.TeamModel;
import com.app.model.webresponsemodel.PlayerPreviewKabaddiResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.PlayersResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.myteam.createTeam.fragment.PlayersFragment;
import com.app.ui.main.kabaddi.myteam.playerpreview.TeamPreviewDialog;
import com.app.ui.main.kabaddi.myteam.selectcaption.ChooseCaptionActivity;
import com.pickeleven.R;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;

import java.util.ArrayList;
import java.util.List;

import static com.app.ui.main.kabaddi.contests.ContestsActivity.REQUEST_CREATE_TEAM;

public class CreateTeamActivity extends AppBaseActivity {
    private TextView tv_max_player_in_a_team;
    private LinearLayout ll_team_detail;
    private TextView tv_selected_player;
    private ImageView iv_team_one;
    private TextView tv_team_one;
    private TextView tv_team1players;
    private TextView tv_team_two;
    private TextView tv_team2players;
    private ImageView iv_team_two;
    private TextView tv_credit_limit;
    private ProgressBarView progressBar_team;
    TabLayout tabs;
    public ViewPager view_pager;
    ViewPagerAdapter adapter;
    LinearLayout ll_bottom;
    TextView tv_team_preview;
    TextView tv_continue;
    UserPrefs userPrefs;

    public float totalSelectedPlayerPoints = 0;
    int totalSelectedDefender = 0;
    int totalSelectedRaider = 0;
    int totalSelectedAllrounder = 0;
    public int totalTeam1Players = 0;
    public int totalTeam2Players = 0;
    private List<PlayerModel> defender = new ArrayList<>();
    private List<PlayerModel> raider = new ArrayList<>();
    private List<PlayerModel> allrounder = new ArrayList<>();

    private PlayerTypeResponseModel.DataBean playerTypeModels;
    List<PlayerTypeModel> playerType;
    List<PlayerModel> selectedPlayer = new ArrayList<>();

    public int currentSortBy = -1;
    public int currentSortType = 0;

    public List<PlayerModel> getSelectedPlayer() {
        return selectedPlayer;
    }

    public PlayerTypeModel getPlayerTypes(int position) {
        return playerType.get(position);
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    public boolean isEdit() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return getIntent().getExtras().getBoolean(IS_EDIT, false);
        return false;
    }

    public PlayersResponseModel getPlayersResponseModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA);
            if (isValidString(string))
                return new Gson().fromJson(string, PlayersResponseModel.class);
            else
                return null;
        }
        return null;
    }

    public TeamModel getTeamModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA1);
            if (isValidString(string))
                return new Gson().fromJson(string, TeamModel.class);
            else
                return null;
        }
        return null;
    }

    public String getDATA2() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getString(DATA2);
        }
        return null;
    }


    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_create_team_kabddi;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tv_max_player_in_a_team = findViewById(R.id.tv_max_player_in_a_team);
        ll_team_detail = findViewById(R.id.ll_team_detail);
        tv_selected_player = findViewById(R.id.tv_selected_player);
        iv_team_one = findViewById(R.id.iv_team_one);
        tv_team_one = findViewById(R.id.tv_team_one);
        tv_team1players = findViewById(R.id.tv_team1players);
        tv_team_two = findViewById(R.id.tv_team_two);
        tv_team2players = findViewById(R.id.tv_team2players);
        iv_team_two = findViewById(R.id.iv_team_two);
        tv_credit_limit = findViewById(R.id.tv_credit_limit);

        progressBar_team = findViewById(R.id.progressBar_team);
        progressBar_team.initialize();


        tabs = findViewById(R.id.tabs);
        view_pager = findViewById(R.id.view_pager);
        adapter = new ViewPagerAdapter(getFm());

        ll_bottom = findViewById(R.id.ll_bottom);
        tv_team_preview = findViewById(R.id.tv_team_preview);
        tv_continue = findViewById(R.id.tv_continue);
        tv_team_preview.setOnClickListener(this);
        tv_continue.setOnClickListener(this);


        getPlayersTypes();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                if (adapter == null) return;
                adapter.getItem(position).onPageSelected();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_team_preview:
                teamPreviewKabddi();
                break;

            case R.id.tv_continue:
                if (getSelectedPlayer().size() >= playerTypeModels.getTmsize()) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(IS_EDIT, isEdit());
                    bundle.putString(DATA, new Gson().toJson(selectedPlayer));
                    bundle.putString(DATA1, new Gson().toJson(getTeamModel()));
                    goToChooseCaptionActivity(bundle);
                } else {
                    int count = playerTypeModels.getTmsize() - getSelectedPlayer().size();
                    String format = String.format("Pick %s more players to complete your pick", count);
                    showErrorMsg(format);
                }
                break;
        }
    }

    private void goToChooseCaptionActivity(Bundle bundle) {
        Intent intent = new Intent(this, ChooseCaptionActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_CREATE_TEAM);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void teamPreviewKabddi() {
        defender.clear();
        raider.clear();
        allrounder.clear();
        for (PlayerModel playerModel : getSelectedPlayer()) {
            if (playerModel.isDefender()) {
                defender.add(playerModel);
            } else if (playerModel.isRaider()) {
                raider.add(playerModel);
            } else if (playerModel.isAllRounderKabaddi()) {
                allrounder.add(playerModel);
            }
        }

        PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean();
        playersBean.setDefender(defender);
        playersBean.setRaider(raider);
        playersBean.setAllrounder(allrounder);

        PlayerPreviewKabaddiResponseModel.DataBean team = new PlayerPreviewKabaddiResponseModel.DataBean();
        team.setId("0");
        team.setTeam1_name(getMatchModel().getTeam1());
        team.setTeam2_name(getMatchModel().getTeam2());
        team.setPlayers(playersBean);

        TeamPreviewDialog instance = TeamPreviewDialog.getInstance(null);
        instance.setTeam(team);
        instance.show(getFm(), instance.getClass().getSimpleName());

    }

    private void setHeaderData() {
        MatchModel matchModel = getMatchModel();
        if (!matchModel.getTeam1logo().isEmpty()) {
            loadImage(this, iv_team_one, null, matchModel.getTeam1logo(),
                    R.drawable.dummy_logo, 100);
        } else {
            iv_team_one.setImageDrawable(getResources().getDrawable(R.drawable.dummy_logo));
        }
        tv_team_one.setText(matchModel.getTeam1());
        tv_team1players.setText(String.valueOf(totalTeam1Players));

        if (!matchModel.getTeam2logo().isEmpty()) {
            loadImage(this, iv_team_two, null, matchModel.getTeam2logo(),
                    R.drawable.dummy_logo, 100);
        } else {
            iv_team_two.setImageDrawable(getResources().getDrawable(R.drawable.dummy_logo));
        }
        tv_team_two.setText(matchModel.getTeam2());
        tv_team2players.setText(String.valueOf(totalTeam2Players));
        tv_credit_limit.setText(String.valueOf(playerTypeModels.getCredits()));
        String max_from_one_team = String.format(getResources().getString(R.string.max_7_players_from_a_team), playerTypeModels.getMxfromteam());
        tv_max_player_in_a_team.setText(max_from_one_team);
        tv_selected_player.setText(selectedPlayer.size() + "/" + playerTypeModels.getTmsize());
    }

    private void getPlayersTypes() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPlayerType(MyApplication.getInstance().getGemeType(), this);
    }

    private void getPlayersList() {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPlayersList(getMatchModel().getMatchid(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case ID_PLAYER_TYPE:
                handlePlayerTypeResponse(webRequest);
                break;

            case ID_PLAYERS:
                handlePlayersResponse(webRequest);
                break;
        }
    }

    private void handlePlayerTypeResponse(WebRequest webRequest) {
        PlayerTypeResponseModel responsePojo = webRequest.getResponsePojo(PlayerTypeResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            PlayerTypeResponseModel.DataBean data = responsePojo.getData();
            MyApplication.getInstance().setPlayerTypeModels(data);
            playerTypeModels = data;
            playerType = data.getList();
            setHeaderData();
            getPlayersList();

        } else {
            if (isFinishing()) return;
            showErrorMsg(responsePojo.getMsg());
        }
    }

    private void handlePlayersResponse(WebRequest webRequest) {
        PlayersResponseModel responsePojo = webRequest.getResponsePojo(PlayersResponseModel.class);
        if (responsePojo == null && playerType == null) return;
        if (!responsePojo.isError()) {
            PlayersResponseModel playersResponseModel = getPlayersResponseModel();
            if (playersResponseModel != null) {
                List<PlayerModel> data = responsePojo.getData();
                for (PlayerModel datum : playersResponseModel.getData()) {
                    int index = data.indexOf(datum);
                    if (index != -1) {
                        data.get(index).setSelected(true);
                    }
                }
                selectedPlayer.clear();
                selectedPlayer.addAll(playersResponseModel.getData());
            }
            for (int i = 0; i < playerType.size(); i++) {
                List<PlayerModel> palyers = new ArrayList<>();
                for (PlayerModel playerModel : responsePojo.getData()) {
                    if (playerType.get(i).getName().equals(playerModel.getPtype())) {
                        palyers.add(playerModel);
                    }
                }
                PlayersFragment playersFragment = new PlayersFragment();
                playersFragment.setPlayersList(palyers);
                playersFragment.setType((int) palyers.get(0).getPlayertype());
                playersFragment.setMin(playerType.get(i).getMin());
                playersFragment.setMax(playerType.get(i).getMax());
                playersFragment.setUpdateSelectedPlayer(updateSelectedPlayer);
                adapter.addFragment(playersFragment, playerType.get(i).getName() + " (0)");
                adapter.notifyDataSetChanged();
            }
            view_pager.setAdapter(adapter);
            tabs.setupWithViewPager(view_pager);
            calculateData();
        } else {
            if (isFinishing()) return;
            showErrorMsg(responsePojo.getMsg());
        }
    }

    PlayersFragment.UpdateSelectedPlayer updateSelectedPlayer = new PlayersFragment.UpdateSelectedPlayer() {
        @Override
        public void updatePlayer(PlayerModel playerModel) {
            if (selectedPlayer.contains(playerModel)) {
                selectedPlayer.remove(playerModel);
            } else {
                selectedPlayer.add(playerModel);
            }
            calculateData();
        }

        @Override
        public int getTotalTeam1Players() {
            return totalTeam1Players;
        }

        @Override
        public int getTotalTeam2Players() {
            return totalTeam2Players;
        }

        @Override
        public int getPlayerCountByType(int type) {
            return getPlayerCount(type);
        }

        @Override
        public float getSelectedPlayerTotalPoints() {
            return totalSelectedPlayerPoints;
        }

        @Override
        public int getSelectedPlayer() {
            return getTotalSelectedPlayers();
        }

    };

    public int getPlayerCount(long type) {
        if (type == 5) {
            return totalSelectedDefender;
        } else if (type == 6) {
            return totalSelectedRaider;
        } else if (type == 7) {
            return totalSelectedAllrounder;
        }
        return 0;
    }

    private void calculateData() {
        tv_selected_player.setText(String.valueOf(selectedPlayer.size()) + "/" + playerTypeModels.getTmsize());
        progressBar_team.changeColor(selectedPlayer.size());
        progressBar_team.setMaxPlayer(selectedPlayer.size());

        totalSelectedPlayerPoints = 0;
        totalSelectedDefender = 0;
        totalSelectedRaider = 0;
        totalSelectedAllrounder = 0;
        totalTeam1Players = 0;
        totalTeam2Players = 0;

        for (PlayerModel playerModel : selectedPlayer) {
            if (playerModel.isSelected() && playerModel.isDefender()) {//Defender
                totalSelectedDefender++;
                totalSelectedPlayerPoints += playerModel.getCredit();
                if (playerModel.getTeamname().equalsIgnoreCase(getMatchModel().getTeam1())) {
                    totalTeam1Players++;
                } else {
                    totalTeam2Players++;
                }
            } else if (playerModel.isSelected() && playerModel.isRaider()) {//Raider
                totalSelectedRaider++;
                totalSelectedPlayerPoints += playerModel.getCredit();
                if (playerModel.getTeamname().equalsIgnoreCase(getMatchModel().getTeam1())) {
                    totalTeam1Players++;
                } else {
                    totalTeam2Players++;
                }
            } else if (playerModel.isSelected() && playerModel.isAllRounderKabaddi()) {//AllRounder
                totalSelectedAllrounder++;
                totalSelectedPlayerPoints += playerModel.getCredit();
                if (playerModel.getTeamname().equalsIgnoreCase(getMatchModel().getTeam1())) {
                    totalTeam1Players++;
                } else {
                    totalTeam2Players++;
                }
            }

        }
        updateTeamPlayer();
        TabLayout.Tab tabWicket = tabs.getTabAt(0);
        tabWicket.setText("DEF (" + totalSelectedDefender + ")");

        TabLayout.Tab tabBatsman = tabs.getTabAt(1);
        tabBatsman.setText("RAID (" + totalSelectedRaider + ")");

        TabLayout.Tab tabAllRounder = tabs.getTabAt(2);
        tabAllRounder.setText("AR (" + totalSelectedAllrounder + ")");
    }

    private int getTotalSelectedPlayers() {
        return totalSelectedAllrounder + totalSelectedDefender + totalSelectedRaider;
    }

    private void updateTeamPlayer() {
        tv_team1players.setText(String.valueOf(totalTeam1Players));
        tv_team2players.setText(String.valueOf(totalTeam2Players));
        tv_credit_limit.setText(String.valueOf(playerTypeModels.getCredits() - totalSelectedPlayerPoints));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = new Bundle();
                bundle.putString(DATA2, getDATA2());
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                supportFinishAfterTransition();
            }
        }
    }

    public void refreshOtherFragmentData() {
        int currentItem = view_pager.getCurrentItem();
        for (int i = 0; i < (MyApplication.getInstance().getPlayerTypeModels().getList().size()); i++) {
            if (currentItem != i) {
                adapter.getItem(i).onPageSelected();
            }
        }
    }

    public void performFilterOnOtherFragment(int currentSortBy, int currentSortType) {
        this.currentSortBy = currentSortBy;
        this.currentSortType = currentSortType;
        int currentItem = view_pager.getCurrentItem();
        for (int i = 0; i < (MyApplication.getInstance().getPlayerTypeModels().getList().size()); i++) {
            if (currentItem != i) {
                AppBaseFragment item = adapter.getItem(i);
                ((PlayersFragment) item).perFormFilter(currentSortBy, currentSortType);
            }
        }
    }
}
