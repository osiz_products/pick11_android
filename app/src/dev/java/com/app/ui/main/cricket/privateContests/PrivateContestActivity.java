package com.app.ui.main.cricket.privateContests;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.AppSettingsModel;
import com.app.model.PrivateBreakUpModel;
import com.app.model.PrivateContestModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.model.webresponsemodel.AppSettingsResponseModel;
import com.app.model.webresponsemodel.PrivateBreakUpResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.contests.ContestsActivity;
import com.app.ui.main.cricket.privateContests.prizebreakup.PricePoolBreakUpActivity;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;

import java.util.Locale;

public class PrivateContestActivity extends AppBaseActivity {

    private int dialogGravity = Gravity.BOTTOM;
    private EditText et_contast_name;
    private EditText et_price_pool;
    private EditText et_contest_size;
    private Switch sw_allow_friends;
    private TextView tv_entry_fees;
    private TextView tv_choose_winning_breakup;
    private PrivateContestModel contestModel;
    private String entryFeesTeam = "";
    private Handler handler = new Handler();
    UserPrefs userPrefs;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (contestModel == null) return;
            String price_pool = et_price_pool.getText().toString().trim();
            String contest_size = et_contest_size.getText().toString().trim();
            float totalPrizePool = 0;
            float contestSize = 0;
            if (price_pool.length() > 0) {
                totalPrizePool = Integer.parseInt(price_pool);
            }
            if (contest_size.length() > 0) {
                contestSize = Integer.parseInt(contest_size);
            }

            if (showErrorMessage(totalPrizePool, contestSize)) {
                tv_choose_winning_breakup.setOnClickListener(null);
                tv_choose_winning_breakup.setAlpha(1.0f);
                return;
            }
            tv_choose_winning_breakup.setOnClickListener(PrivateContestActivity.this);
            tv_choose_winning_breakup.setAlpha(1.0f);
            PrivateContestModel.WinprizeBean winprize = contestModel.getWinprize();
            PrivateContestModel.CnstsizeBean cnstsize = contestModel.getCnstsize();

            if (totalPrizePool >= winprize.getMin() && contestSize >= cnstsize.getMin()) {
                float perTeamPrice = (totalPrizePool / contestSize);
                float entryFees = perTeamPrice + ((perTeamPrice * contestModel.getAdminchrg()) / 100);
                double ceil = Math.ceil(entryFees);
                float min_entry_fees = contestModel.getMin_entry_fees();
                if (ceil < min_entry_fees) {
                    entryFeesTeam = "0";
                    tv_entry_fees.setText(getResources().getString(R.string.currency_symbol) + "  0");
                    showErrorMsg("Entry fee cannot be less then " + currency_symbol + contestModel.getMin_entry_fees(), dialogGravity);
                    return;
                }
                entryFeesTeam = getValidText(ceil);
                tv_entry_fees.setText(getResources().getString(R.string.currency_symbol) + "  " + entryFeesTeam);
            }
        }
    };

    private boolean showErrorMessage(float totalPrizePool, float contestSize) {
        if (contestModel == null) return true;
        PrivateContestModel.WinprizeBean winprize = contestModel.getWinprize();
        PrivateContestModel.CnstsizeBean cnstsize = contestModel.getCnstsize();
        if (winprize != null && totalPrizePool >= winprize.getMin()) {
            if (totalPrizePool > winprize.getMax() || totalPrizePool < winprize.getMin()) {
                showErrorMsg("Please enter total prize pool between " + winprize.getMin() + "  and " + winprize.getMax(), dialogGravity);
                return true;
            }
        }
        if (cnstsize != null && contestSize >= cnstsize.getMin()) {
            if (contestSize > cnstsize.getMax() || contestSize < cnstsize.getMin()) {
                showErrorMsg("Please enter contest size between " + cnstsize.getMin() + "  and " + cnstsize.getMax(), dialogGravity);
                return true;
            }
        }
        return false;
    }

    public boolean isTeamCreated() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return extras.getBoolean(DATA);
        return false;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_private_contest;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        et_contast_name = findViewById(R.id.et_contast_name);
        et_price_pool = findViewById(R.id.et_price_pool);
        et_contest_size = findViewById(R.id.et_contest_size);
        sw_allow_friends = findViewById(R.id.sw_allow_friends);
        tv_entry_fees = findViewById(R.id.tv_entry_fees);
        tv_choose_winning_breakup = findViewById(R.id.tv_choose_winning_breakup);
        tv_choose_winning_breakup.setOnClickListener(null);
        tv_choose_winning_breakup.setAlpha(1.0f);

        et_price_pool.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (s.length() > 0) {
                    if (contestModel == null) return;
                    handler.removeCallbacks(runnable);
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                handler.removeCallbacks(runnable);
            }
        });
        et_contest_size.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (s.length() > 0) {
                    if (contestModel == null) return;
                    handler.removeCallbacks(runnable);
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                handler.removeCallbacks(runnable);
            }
        });


        getAppSettings();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    public String getValidText(double value) {
        String s = getValidDecimalFormat(value);
        return s.replaceAll("\\.00", "");
    }

    public String getValidDecimalFormat(double value) {
        return String.format(Locale.ENGLISH, "%.2f", value);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_choose_winning_breakup:
                onSubmit();
                break;
        }
    }

    private void onSubmit() {
        String contast_name = et_contast_name.getText().toString().trim();
        String price_pool = et_price_pool.getText().toString().trim();
        String contest_size = et_contest_size.getText().toString().trim();
        if (contestModel != null) {
            PrivateContestModel.WinprizeBean winprize = contestModel.getWinprize();
            if (winprize != null) {
                if (Integer.parseInt(price_pool) > winprize.getMax() || Integer.parseInt(price_pool) < winprize.getMin()) {
                    showErrorMsg("Please enter total prize pool between " + winprize.getMin() + "  and " + winprize.getMax(), dialogGravity);
                    return;
                }
            }

            PrivateContestModel.CnstsizeBean cnstsize = contestModel.getCnstsize();
            if (cnstsize != null) {
                if (Integer.parseInt(contest_size) > cnstsize.getMax() || Integer.parseInt(contest_size) < cnstsize.getMin()) {
                    showErrorMsg("Please enter contest size between " + cnstsize.getMin() + "  and " + cnstsize.getMax(), dialogGravity);
                    return;
                }
            }

            if (Integer.parseInt(entryFeesTeam) < contestModel.getMin_entry_fees()) {
                showErrorMsg("Entry fee cannot be less then " + currency_symbol + contestModel.getMin_entry_fees(), dialogGravity);
                return;
            }


            PrizePoolRequestModel requestModel = new PrizePoolRequestModel();
            requestModel.name = contast_name;
            requestModel.tolwinprize = price_pool;
            requestModel.entry_fees = entryFeesTeam;
            requestModel.contestsize = contest_size;
            requestModel.type = sw_allow_friends.isChecked() ? "1" : "0";
            displayProgressBar(false);
            getWebRequestHelper().getPrizePoolBreakUp(requestModel, this);
        }

    }

    private void getAppSettings() {
        displayProgressBar(false);
        getWebRequestHelper().getAppSettings(NotificationPrefs.getInstance(this).getNotificationToken(), "pvtcontest", this);
    }

    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {
        super.onWebRequestPreResponse(webRequest);

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_APP_SETTINGS:
                handleAppSettingResponse(webRequest);
                break;

            case ID_PRIZE_POOL_BREAKUP:
                handlePricePoolBreakUpResponse(webRequest);
                break;
        }
    }

    private void handlePricePoolBreakUpResponse(WebRequest webRequest) {
        PrizePoolRequestModel requestModel = webRequest.getExtraData(DATA);
        PrivateBreakUpResponseModel responseModel = webRequest.getResponsePojo(PrivateBreakUpResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            PrivateBreakUpModel data = responseModel.getData();
            if (data != null && requestModel != null) {
                MyApplication.getInstance().setPrizePoolRequestModel(requestModel);
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(data));
                bundle.putBoolean(DATA2, isTeamCreated());
                goToPricePoolBreakActivity(bundle);
            }
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void handleAppSettingResponse(WebRequest webRequest) {
        AppSettingsResponseModel responseModel = webRequest.getResponsePojo(AppSettingsResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            AppSettingsModel data = responseModel.getData();
            if (data != null) {
                contestModel = data.getPvtcontest();
                if (contestModel != null) {
                    PrivateContestModel.WinprizeBean winprize = contestModel.getWinprize();
                    if (winprize != null) {
                        et_price_pool.setHint("min " + winprize.getMin() + " & max " + winprize.getMax());
                    }

                    PrivateContestModel.CnstsizeBean cnstsize = contestModel.getCnstsize();
                    if (cnstsize != null) {
                        et_contest_size.setHint("min " + cnstsize.getMin() + " & max " + cnstsize.getMax());
                    }
                }
            }
        }

    }


    private void goToPricePoolBreakActivity(Bundle bundle) {
        Intent intent = new Intent(this, PricePoolBreakUpActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_CREATE_PRIVATE_CONTEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ContestsActivity.REQUEST_CREATE_PRIVATE_CONTEST) {
            if (resultCode == RESULT_OK) {
                supportFinishAfterTransition();
            }
        }
    }
}
