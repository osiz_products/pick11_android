package com.app.ui.main.cricket.myteam.chooseTeam.adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TeamModel;
import com.app.model.TeamsPointModel;
import com.app.preferences.UserPrefs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.pickeleven.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ChooseCricketAdapter extends RecyclerView.Adapter<ChooseCricketAdapter.ViewHolder> {

    private List<TeamModel> filterList;
    private Context context;
    private List<Long> namelist;
    private HashSet<Long> hashSet;
    private String selectedTeam;
    private TeamModel filterM;
    private PassList passList;
    private UserPrefs shared_preference;
    int getPosition;




    public ChooseCricketAdapter(List<TeamModel> filterModelList, Context ctx,PassList passLists) {
        filterList = filterModelList;
        context = ctx;
        this.passList=passLists;
    }

    public String getSelectedTeam() {
        return selectedTeam;
    }


    public void setSelectedTeam(int position) {
        String selectedTeam = String.valueOf(filterList.get(position).getId());
        if (this.selectedTeam != null && this.selectedTeam.equals(selectedTeam)) {
            this.selectedTeam = null;
        } else {
            this.selectedTeam = selectedTeam;
        }
//        notifyDataSetChanged();
    }

    public boolean isTeamAlreadyJoined(String teamId) {
        return false;
    }

    public boolean isTeamJoined(String teamId) {
        return false;
    }


    @Override
    public ChooseCricketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_choose_team, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        filterM = filterList.get(position);
        namelist=new ArrayList<>();
        getPosition=position;
        hashSet=new HashSet<>();
        shared_preference = new UserPrefs(context);
        Log.e("position", String.valueOf(position));

        holder.cb_team_name.setText(filterM.getTeamname());
        holder.tv_captain.setText(filterM.getCap());
        holder.tv_vice_captain.setText(filterM.getVcap());

        holder.ll_layout.setTag(position);
        holder.ll_preview.setTag(position);
//      /  holder.ll_preview.setOnClickListener(this);



        if (holder.cb_team_name.isChecked())
            holder.cb_team_name.setChecked(true);
        else
            holder.cb_team_name.setChecked(false);


        if(shared_preference.getintKeyValuePrefs("selectall")==1){
            holder.cb_team_name.setChecked(true);

            for(int i=0;i<filterList.size();i++){
                if(holder.cb_team_name.isChecked()){
                    namelist.add(filterList.get(i).getId());
                    holder.ll_layout.setActivated(true);
                    holder.ll_layout.setAlpha(0.5f);
                    Log.e("seletecname", String.valueOf(filterList.get(i).getId()));
                }
            }

        }else {
            holder.cb_team_name.setChecked(false);
            if(!namelist.isEmpty()){
                for(int i=0;i<filterList.size();i++){
                    if(holder.cb_team_name.isChecked()){
                        namelist.add(filterList.get(i).getId());
                        holder.ll_layout.setActivated(false);
                        holder.ll_layout.setAlpha(1f);
                        Log.e("seletecname", String.valueOf(filterList.get(i).getId()));
                    }
                }

            }
//            namelist.clear();
//            namelist.addAll(hashSet);
            passList.passNameList(namelist);

            Log.e("totalsize", String.valueOf(namelist.size()));
        }



        holder.cb_team_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked) {
                    namelist.add(filterList.get(position).getId());
                    passList.passNameList(namelist);
                    holder.ll_layout.setActivated(true);
                    holder.ll_layout.setAlpha(0.5f);
                    filterM.setChecked(isChecked);
                    Log.e("booleanvalue", String.valueOf(filterM.isChecked()));
                    Log.e("booleanvalue", String.valueOf(namelist.size()));

                } else {
                    filterM.setChecked(isChecked);
                    Log.e("booleanvalue", String.valueOf(filterM.isChecked()));
                    namelist.remove(filterList.get(position).getId());
                    holder.ll_layout.setActivated(false);
                    holder.ll_layout.setAlpha(1f);
                    passList.passNameList(namelist);
                }
            }

        });

            if (isTeamAlreadyJoined(String.valueOf(filterList.get(position).getId()))) {
                holder.ll_layout.setAlpha(0.5f);
                updateViewVisibitity(holder.tv_all_ready_added, View.VISIBLE);
                holder.ll_layout.setActivated(true);
                holder.cb_team_name.setChecked(true);
            }else {

                updateViewVisibitity(holder.tv_all_ready_added, View.GONE);
            }

//        if (isTeamAlreadyJoined(String.valueOf(filterM.getId()))) {
//            holder.ll_layout.setAlpha(0.5f);
//            updateViewVisibitity(holder.tv_all_ready_added, View.VISIBLE);
//            holder.ll_layout.setActivated(true);
//            holder.cb_team_name.setChecked(true);
//        }else {
//            updateViewVisibitity(holder.tv_all_ready_added, View.GONE);
//        }
//
//        if (isTeamJoined(String.valueOf(filterM.getId()))){
//            Log.e("SellectAllTeam",""+filterM.getId());
//        }


    }


    public interface PassList{
        public void passNameList(List<Long> namelist);

    }



    @Override
    public int getItemCount() {

        return filterList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        private LinearLayout ll_layout;
        private CheckBox cb_team_name;
        private TextView tv_all_ready_added;
        private TextView tv_captain;
        private TextView tv_vice_captain;
        private LinearLayout ll_preview;
        private String[] data;


        public ViewHolder(View view) {
            super(view);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            cb_team_name = itemView.findViewById(R.id.cb_team_name);
            tv_all_ready_added = itemView.findViewById(R.id.tv_all_ready_added);
            tv_captain = itemView.findViewById(R.id.tv_captain);
            tv_vice_captain = itemView.findViewById(R.id.tv_vice_captain);
            ll_preview = itemView.findViewById(R.id.ll_preview);
        }
    }

    public void updateViewVisibitity (View myView, int visibility) {
        if (myView.getVisibility() != visibility) {
            myView.setVisibility(visibility);
        }

    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        if (holder.cb_team_name != null) {
            holder.cb_team_name.setOnCheckedChangeListener(null);
        }
        super.onViewRecycled(holder);
    }


}