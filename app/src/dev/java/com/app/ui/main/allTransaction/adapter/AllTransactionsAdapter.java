package com.app.ui.main.allTransaction.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TransactionModel;
import com.pickeleven.R;

import java.util.List;

public class AllTransactionsAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private boolean loadMore;
    private List<TransactionModel> list;

    public AllTransactionsAdapter(Context context) {
        this.context = context;
    }

    public void updateData (List<TransactionModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public boolean setLoadMore (boolean loadMore) {
        if (list == null) return false;
        this.loadMore = loadMore;
        if (loadMore) {
            notifyItemInserted(list.size());
        } else {
            notifyItemRemoved(list.size());
        }
        return true;
    }
    @Override
    public int getViewType (int position) {
        if (list == null) {
            return VIEW_TYPE_DATA;
        } else if (loadMore && position == list.size()) {
            return VIEW_TYPE_LOAD_MORE;
        }
        return VIEW_TYPE_DATA;
    }

    @Override
    public BaseViewHolder getViewHolder (ViewGroup parent, int viewType) {
        if (VIEW_TYPE_DATA == viewType) {
            return new ViewHolder(inflateLayout(R.layout.item_all_transactoins));
        }
        return new LoadMoreViewHolder(inflateLayout(R.layout.adapter_item_load_more));
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return null;
    }

    @Override
    public int getDataCount () {
        return list == null ? 0 : (loadMore ? list.size() + 1 : list.size());
    }


    private class ViewHolder extends BaseViewHolder {

        private RelativeLayout ll_layout;
        private TextView tv_date;
        private TextView tv_credit;
        private TextView tv_debit;
        private TextView tv_win;
        private TextView tv_title;
        private ImageView imagegreen,imagered;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_credit = itemView.findViewById(R.id.tv_credit);
            tv_debit = itemView.findViewById(R.id.tv_debit);
            tv_win = itemView.findViewById(R.id.tv_win);
            tv_title = itemView.findViewById(R.id.tv_title);
            imagegreen = itemView.findViewById(R.id.imagegreen);
            imagered = itemView.findViewById(R.id.imagered);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            String currency_symbol = getContext().getResources().getString(R.string.currency_symbol);
            TransactionModel transactionModel = list.get(position);
            tv_date.setText("Date "+transactionModel.getFormattedDate(3));
//            tv_credit.setText(transactionModel.getCreditAmountText());
            tv_win.setText(transactionModel.getDes());
            if (transactionModel.isCredit()) {
                imagegreen.setVisibility(View.VISIBLE);
                imagered.setVisibility(View.GONE);
                tv_debit.setText(currency_symbol + transactionModel.getCreditAmountText());
                tv_title.setText("Credit Amount");
                tv_win.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
                tv_debit.setTextColor(getContext().getResources().getColor(R.color.colorPrimary_dark));
            } else {
                imagegreen.setVisibility(View.GONE);
                imagered.setVisibility(View.VISIBLE);
                tv_debit.setText(currency_symbol + transactionModel.getDeditAmountText());
                tv_title.setText("Debit Amount");
                tv_win.setTextColor(getContext().getResources().getColor(R.color.color_red));
                tv_debit.setTextColor(getContext().getResources().getColor(R.color.colorPrimary_dark));
            }

            return currency_symbol;
        }
    }
}
