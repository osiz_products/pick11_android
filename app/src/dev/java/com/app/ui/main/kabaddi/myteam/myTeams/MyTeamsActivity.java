package com.app.ui.main.kabaddi.myteam.myTeams;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.TeamModel;
import com.app.model.webresponsemodel.PlayerPreviewKabaddiResponseModel;
import com.app.model.webresponsemodel.PlayerPreviewResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.PlayersResponseModel;
import com.app.model.webresponsemodel.TeamResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.contests.ContestsActivity;
import com.app.ui.main.kabaddi.myteam.myTeams.adapter.MyTeamsAdapter;
import com.app.ui.main.kabaddi.myteam.playerpreview.TeamPreviewDialog;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class MyTeamsActivity extends AppBaseActivity {

    TextView tv_create_team;
    RecyclerView recycler_view;
    MyTeamsAdapter adapter;
    TextView tv_no_record_found;
    List<TeamModel> myTeams = new ArrayList<>();
    UserPrefs userPrefs;

    private PlayerTypeResponseModel.DataBean getPlayerTypeModel() {
        return MyApplication.getInstance().getPlayerTypeModels();
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.activity_my_teams;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        recycler_view = findViewById(R.id.recycler_view);
        tv_create_team = findViewById(R.id.tv_create_team);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        tv_create_team.setOnClickListener(this);
        updateViewVisibility(tv_create_team, View.GONE);
        initializeRecyclerView();

        getAllTeam();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_create_team:
                goToCreateTeamActivity(null);
                break;
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new MyTeamsAdapter(this, myTeams);
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TeamModel teamModel = myTeams.get(position);
                switch (v.getId()) {
                    case R.id.ll_edit:
                        editPlayers(teamModel);
                        break;
                    case R.id.ll_preview:
                        getTeamPlayers(teamModel);
                        break;
                    case R.id.ll_clone:
                        if (tv_create_team.getVisibility() == View.GONE) {
                            showErrorMsg("No more clone available");
                            return;
                        }
                        cloneTeam(teamModel);
                        break;
                }

            }
        });
    }

    private void editPlayers(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().editPlayers(teamModel, this);
    }

    private void cloneTeam(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().cloneTeam(teamModel, this);
    }

    private void getTeamPlayers(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    private void getAllTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getAllTeam(matchModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_ALL_TEAM:
                handleAllTeamsResponse(webRequest);
                break;

            case ID_EDIT_PLAYERS:
                handleEditPlayersResponse(webRequest);
                break;

            case ID_CLONE_TEAM:
                handleCloneTeamResponse(webRequest);
                break;

            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;
        }
    }

    private void handleAllTeamsResponse(WebRequest webRequest) {
        TeamResponseModel responseModel = webRequest.getResponsePojo(TeamResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            TeamResponseModel.DataBean data = responseModel.getData();
            if (data == null) return;
            List<TeamModel> teams = data.getTeams();
            myTeams.clear();
            if (teams != null && teams.size() > 0) {
                myTeams.addAll(teams);
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
            updateButton();
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void updateButton() {
        tv_create_team.setText("Create Team (" + (myTeams.size() + 1) + ")");
        if (getPlayerTypeModel() != null && myTeams.size() < getPlayerTypeModel().getMaxteam()) {
            updateViewVisibility(tv_create_team, View.VISIBLE);
        } else {
            updateViewVisibility(tv_create_team, View.GONE);
        }
    }

    private void handleEditPlayersResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> modelList = new ArrayList<>();
            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setSelected(true);
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                }
                modelList.add(playerModel);
            }

            PlayersResponseModel responseModel1 = new PlayersResponseModel();
            responseModel1.setData(modelList);
            Bundle bundle = new Bundle();
            bundle.putBoolean(IS_EDIT, true);
            bundle.putString(DATA, new Gson().toJson(responseModel1));
            bundle.putString(DATA1, new Gson().toJson(teamModel));
            goToCreateTeamActivity(bundle);
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }

    private void handleCloneTeamResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> modelList = new ArrayList<>();
            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setSelected(true);
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                }

                modelList.add(playerModel);
            }

            PlayersResponseModel responseModel1 = new PlayersResponseModel();
            responseModel1.setData(modelList);
            Bundle bundle = new Bundle();
            bundle.putBoolean(IS_EDIT, false);
            bundle.putString(DATA, new Gson().toJson(responseModel1));
            goToCreateTeamActivity(bundle);
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }

    private void handleTeamPlayersResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewKabaddiResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewKabaddiResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> defender = new ArrayList<>();
            List<PlayerModel> raider = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();

            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                    defender.add(playerModel);
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                    raider.add(playerModel);
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                }

            }


            PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean();
            playersBean.setDefender(defender);
            playersBean.setRaider(raider);
            playersBean.setAllrounder(allrounder);

            PlayerPreviewKabaddiResponseModel.DataBean team = new PlayerPreviewKabaddiResponseModel.DataBean();
            team.setId("0");
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);
            for (PlayerPreviewModel datum : data) {
                if (datum.getIscap() == 1) {
                    team.setCaptainid(datum.getPid());
                }
                if (datum.getIsvcap() == 1) {
                    team.setVicecaptainid(datum.getPid());
                }
            }

            TeamPreviewDialog instance = TeamPreviewDialog.getInstance(null);
            instance.setTeam(team);
            instance.show(getFm(), instance.getClass().getSimpleName());


        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ContestsActivity.REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                getAllTeam();
            }
        }
    }
}
