package com.app.ui.main.football.main.upcoming;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseFragment;
import com.app.model.MatchModel;
import com.app.model.webresponsemodel.MatchResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MatchTimerListener;
import com.app.ui.MyApplication;
import com.app.ui.main.football.contests.ContestsActivity;
import com.app.ui.main.football.main.upcoming.adapter.UpcomingAdapter;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UpcomingFragment extends AppBaseFragment implements MatchTimerListener {

    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    UpcomingAdapter adapter;
    TextView tv_no_record_found;
    private List<MatchModel> list = new ArrayList<>();
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.fragment_upcoming;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        setupSwipeLayout();
        tv_no_record_found = getView().findViewById(R.id.tv_no_record_found);
        updateViewVisibitity(tv_no_record_found, View.GONE);
        initializeRecyclerView();

        onPageSelected();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void setupSwipeLayout() {
        swipeRefresh = getView().findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                getUpcomingMatches();
            }
        });
    }


    @Override
    public void onPageSelected() {
        getUpcomingMatches();
    }

    private void initializeRecyclerView() {
        recycler_view = getView().findViewById(R.id.recycler_view);
        adapter = new UpcomingAdapter(list);
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                try {
                    MatchModel matchModel = adapter.getItem(position);
                    if (matchModel == null) return;
                    switch (v.getId()) {
                        default:
                            MyApplication.getInstance().setSelectedMatch(matchModel);
                            Bundle bundle = new Bundle();
                            bundle.putString(DATA, matchModel.getMatchid());
                            bundle.putString(DATA1, UpcomingFragment.this.getClass().getSimpleName());
                            goToContestsActivity(bundle);
                            break;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public void getUpcomingMatches() {
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {*/
                if (getWebRequestHelper() == null) return;
                swipeRefresh.setRefreshing(true);
                getWebRequestHelper().getListMatchFront(MyApplication.getInstance().getGemeType(), FIXTURES, UpcomingFragment.this);
       /*     }
        },100);*/
    }



    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (swipeRefresh != null && swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        switch (webRequest.getWebRequestId()) {
            case ID_MATCH_FRONT:
                handleMatchListResponse(webRequest);
                break;
        }
    }

    private void handleMatchListResponse(WebRequest webRequest) {
        MatchResponseModel responsePojo = webRequest.getResponsePojo(MatchResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<MatchModel> data = responsePojo.getData();
                list.clear();
                MyApplication.getInstance().setServerdate(responsePojo.getServerdate());
                if (data != null && data.size() > 0) {
                    list.addAll(data);
                }
                if (isFinishing()) return;
                adapter.notifyDataSetChanged();
                updateNoDataView();
                MyApplication.getInstance().startTimer();
            }
        } else {
            if (isFinishing()) return;
            list.clear();
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (list.size() > 0) {
            updateViewVisibitity(tv_no_record_found, View.GONE);
        } else {
//            tv_no_record_found.setText("No Upcoming match available");
            tv_no_record_found.setText("Matches coming soon");
            updateViewVisibitity(tv_no_record_found, View.VISIBLE);
        }
    }

    private void goToContestsActivity(Bundle bundle) {
        Intent intent = new Intent(getActivity(), ContestsActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onMatchTimeUpdate() {
        if (isFinishing()) return;
        if (adapter != null)
           // adapter.notifyItemChanged(i);
        for (int i=0; i<adapter.getDataCount();i++) {
            adapter.notifyItemChanged(i);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().addMatchTimerListener(this);
//        getUpcomingMatches();

    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().removeMatchTimerListener(this);
    }
}
