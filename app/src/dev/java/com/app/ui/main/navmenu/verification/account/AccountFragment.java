package com.app.ui.main.navmenu.verification.account;

import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseFragment;
import com.app.model.BankModel;
import com.app.model.IfscModel;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.BankRequestModel;
import com.app.model.webresponsemodel.BankResponseModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.IfscResponseModel;
import com.app.model.webresponsemodel.ProfileResponseModel;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;
import com.imagePicker.FileInformation;
import com.imagePicker.ProfilePicDialog;
import com.medy.retrofitwrapper.WebRequest;

import java.io.File;

public class AccountFragment extends AppBaseFragment {

    //    private static final String IFSC_REGEX = "^[A-Z]{4}[0-9]{7}$";
    private CardView cv_pending;
    private CardView cv_submit;
    private ImageView iv_bank_image,view,viewclose;
    private TextView tv_image_size_message;
    private LinearLayout ll_upload_image,ll_upload_images;
    private EditText et_user_name;
    private EditText et_acc_number;
    private EditText et_ifsc;
    private EditText et_bank_name;
    private EditText et_state;
    private TextView tv_save;
    private TextView tv_verified_text;
    private TextView et_ifscView;
    UserPrefs userPrefs;
    UserModel userModel;
    String userName;
    String bankUpdate = "0";

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String string = s.toString();
            if (string.length() >= 11) {
                hideKeyboard();

                userName = et_user_name.getText().toString().trim();
                chackIfscCode(string,userName);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return R.layout.fragment_account;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        cv_pending = getView().findViewById(R.id.cv_pending);
        cv_submit = getView().findViewById(R.id.cv_submit);
        iv_bank_image = getView().findViewById(R.id.iv_bank_image);
        tv_image_size_message = getView().findViewById(R.id.tv_image_size_message);
        ll_upload_image = getView().findViewById(R.id.ll_upload_image);
        ll_upload_images = getView().findViewById(R.id.ll_upload_images);
        et_user_name = getView().findViewById(R.id.et_user_name);
        et_acc_number = getView().findViewById(R.id.et_acc_number);
        et_ifsc = getView().findViewById(R.id.et_ifsc);
        et_bank_name = getView().findViewById(R.id.et_bank_name);
        et_state = getView().findViewById(R.id.et_state);
        tv_save = getView().findViewById(R.id.tv_save);
        view = getView().findViewById(R.id.view);
        viewclose = getView().findViewById(R.id.viewclose);
        tv_verified_text = getView().findViewById(R.id.tv_verified_text);
        et_ifscView = getView().findViewById(R.id.et_ifscView);
        tv_save.setOnClickListener(this);
        ll_upload_image.setOnClickListener(this);
        updateViewVisibility(tv_verified_text, View.GONE);
        updateViewVisibility(tv_image_size_message, View.GONE);
        et_bank_name.setFocusable(false);
        et_state.setFocusable(false);
        setView();
        et_ifsc.addTextChangedListener(textWatcher);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_bank_image.setVisibility(View.VISIBLE);
                viewclose.setVisibility(View.VISIBLE);
                view.setVisibility(View.GONE);
            }
        });

        viewclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_bank_image.setVisibility(View.GONE);
                viewclose.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }
        });

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    @Override
    public void onPageSelected() {
        getBankDetails();
        getUserDetail();
    }

    private void chackIfscCode(String string,String userName) {
        displayProgressBar(false);
            getWebRequestHelper().checkIfscCode(string, userName,this);
    }

    private void getBankDetails() {
        getWebRequestHelper().getBankDetail(this);
    }

    private void getUserDetail() {
        getWebRequestHelper().getProfileDetail(this);
    }


    private void setView() {
        if (getUserModel() == null) return;
        UserModel userModel = getUserModel();
        if (userModel.isPhoneVerify() && userModel.isEmailVerify() && userModel.isPanVerify()) {
            updateViewVisibility(cv_pending, View.GONE);
            updateViewVisibility(cv_submit, View.VISIBLE);
        } else {
            updateViewVisibility(cv_pending, View.VISIBLE);
            updateViewVisibility(cv_submit, View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save:
                submit();
                break;
            case R.id.ll_upload_image:
                showImagePickerDialog();
                break;
        }
    }

    private void showImagePickerDialog() {
        final ProfilePicDialog dialog = ProfilePicDialog.getNewInstance(false);
        dialog.setProfilePicDialogListner(new ProfilePicDialog.ProfilePicDialogListner() {
            @Override
            public void onProfilePicSelected(FileInformation fileInformation) {
                dialog.dismiss();

                String imageName = "profile_pic_" + System.currentTimeMillis();

                String large_file_path = fileInformation.getBitmapPathForUpload(getContext(),
                        FileInformation.IMAGE_SIZE_LARGE, FileInformation.IMAGE_SIZE_LARGE,
                        "large/" + imageName);

                String thumb_file_path = fileInformation.getBitmapPathForUpload(getContext(),
                        FileInformation.IMAGE_SIZE_THUMB, FileInformation.IMAGE_SIZE_THUMB,
                        "thumb/" + imageName);

                iv_bank_image.setTag(R.id.image_path_tag, large_file_path);
                iv_bank_image.setTag(R.id.image_path_thumb_tag, thumb_file_path);
                if (isValidString(thumb_file_path))
                    iv_bank_image.setImageURI(Uri.parse(large_file_path));
            }

            @Override
            public void onProfilePicRemoved() {

            }
        });
        dialog.showDialog(getContext(), getChildFm());
    }

    private void submit() {
        String userName = et_user_name.getText().toString().trim();
        String account_no = et_acc_number.getText().toString().trim();
        String ifsc = et_ifsc.getText().toString().trim();
        String bank_name = et_bank_name.getText().toString().trim();
        String state = et_state.getText().toString().trim();

        if (userName.isEmpty()) {
            showErrorMsg("Please enter user name.");
            return;
        }
        if (account_no.isEmpty()) {
            showErrorMsg("Please enter account number.");
            return;
        }
        if (account_no.length() < 9) {
            showErrorMsg("Please enter valid account number.");
            return;
        }
        if (ifsc.isEmpty()) {
            showErrorMsg("Please enter ifsc code.");
            return;
        }
        /*if (!Pattern.matches(IFSC_REGEX, ifsc)) {
            showErrorMsg("Please enter valid ifsc code.");
            return;
        }*/

        if (bank_name.isEmpty()) {
            showErrorMsg("Please enter bank name.");
            return;
        }

        String imagePath = (String) iv_bank_image.getTag(R.id.image_path_tag);
        if (imagePath == null) {
            showErrorMsg("Please select bank/cheque image");
            return;
        }

        BankRequestModel requestModel = new BankRequestModel();
        requestModel.acholdername = userName;
        requestModel.acno = account_no;
        requestModel.ifsccode = ifsc;
        requestModel.bankname = bank_name;
        requestModel.state = state;
        requestModel.image = new File(imagePath);
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().updateBankDetail(requestModel, this);

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_CHECK_IFSC:
                handleIfscCodeResponse(webRequest);
                break;

            case ID_BANK_UPDATE:
                handleUpdateBankResponse(webRequest);
                break;

            case ID_GET_BANK_DETAIL:
                handleBankDetailResponse(webRequest);
                break;

            case ID_GET_USER_PROFILE:
                handleUserProfileResponse(webRequest);
                break;
        }
    }

    private void handleUserProfileResponse(WebRequest webRequest) {
        ProfileResponseModel responseModel = webRequest.getResponsePojo(ProfileResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError() && responseModel.getData() != null) {
            if (isFinishing()) return;
            userModel = responseModel.getData();
            if (userModel == null) return;
            UserModel userModel1 = getUserModel();
            userModel1.setIsbankdverify(userModel.getIsbankdverify());
            userModel1.setIsemailverify(userModel.getIsemailverify());
            userModel1.setIsphoneverify(userModel.getIsphoneverify());
            userModel1.setIspanverify(userModel.getIspanverify());
            getUserPrefs().updateLoggedInUser(userModel1);

            /*if (userModel.getIspanverify()!=null){

                et_acc_number.setFocusable(false);
                et_ifsc.setFocusable(false);
                et_user_name.setFocusable(false);
                updateViewVisibility(tv_save, View.GONE);
                updateViewVisibility(ll_upload_image, View.GONE);
                updateViewVisibility(ll_upload_images, View.VISIBLE);


                tv_verified_text.setVisibility(View.VISIBLE);
            }else {
                tv_verified_text.setVisibility(View.GONE);
            }*/

        }
    }

    private void handleIfscCodeResponse(WebRequest webRequest) {
        IfscResponseModel responseModel = webRequest.getResponsePojo(IfscResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {

            try {

            IfscModel data = responseModel.getData();
            et_ifsc.clearFocus();
            et_ifsc.removeTextChangedListener(textWatcher);
            et_ifsc.setText(data.getIFSC());
            et_bank_name.setText(data.getBANK());
            et_state.setText(data.getSTATE());
            et_ifsc.addTextChangedListener(textWatcher);

            }catch (Exception e){
                Log.e("ErrorException",""+e);
                String msg = responseModel.getMsg();
                if (isValidString(msg))
                    showErrorMsg(msg);
            }

        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    private void handleBankDetailResponse(WebRequest webRequest) {
        BankResponseModel responseModel = webRequest.getResponsePojo(BankResponseModel.class);
        if (responseModel == null) return;

        if (!responseModel.isError()) {
            BankModel data = responseModel.getData();
            if (data == null) return;

            Log.e("GetBankDetails",""+data.getAcno());
            et_acc_number.setText(data.getAcno());
            //et_ifsc.setText(data.getIfsccode());
            et_ifsc.setVisibility(View.GONE);
            et_ifscView.setVisibility(View.VISIBLE);
            et_ifscView.setText(data.getIfsccode());
            et_bank_name.setText(data.getBankname());
            et_state.setText(data.getState());
            et_user_name.setText(data.getAcholdername());
             bankUpdate = data.getIsverified();

             Log.e("bankUpdate_bankUpdate",""+bankUpdate);

            ((AppBaseActivity) getActivity()).loadImage(getContext(), iv_bank_image, null, data.getImage(), R.drawable.cheque_image);
            if (data.isBankVerified()) {
                Log.e("dfgdfgdfgfdg1",""+data.isBankVerified());

                bankUpdate = "1";

                updateViewVisibility(tv_verified_text, View.VISIBLE);
            } else {
                bankUpdate = "0";
                Log.e("dfgdfgdfgfdg2",""+data.isBankVerified());
                updateViewVisibility(tv_verified_text, View.GONE);
            }
            updateUi();
        } else {
            String msg = responseModel.getMsg();
//            if (isValidString(msg))
//                showErrorMsg(msg);
        }
    }

    private void updateUi() {
        et_acc_number.setFocusable(false);
        et_ifsc.setFocusable(false);
        et_user_name.setFocusable(false);
        updateViewVisibility(tv_save, View.GONE);
        updateViewVisibility(ll_upload_image, View.GONE);
        updateViewVisibility(ll_upload_images, View.VISIBLE);
    }

    private void handleUpdateBankResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);
            getBankDetails();
            updateUi();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }
}
