package com.app.ui.main.navmenu.scratch;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anupkumarpanwar.scratchview.ScratchView;
import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.ScratchModel;
import com.app.model.TransactionInfModel;
import com.app.model.webrequestmodel.InfluencerMatchlistModel;
import com.app.model.webresponsemodel.ScratchResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.main.upcoming.UpcomingFragment;
import com.app.ui.main.kabaddi.main.live.adapter.LiveAdapter;
import com.bumptech.glide.Glide;
import com.medy.retrofitwrapper.WebRequest;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class ScratchCard extends AppBaseActivity {

ImageView backpage;
ImageView img_giphy;
TextView textBalance;
TextView txtgetmsg;
TextView txtgetcoupon;
TextView txtdatemsg;
private ScratchView card_view;
MyScratchAdapter adapter;
SwipeRefreshLayout swipeRefresh;
RecyclerView recycler_view;
LinearLayout ll_no_record_found;
TextView tv_no_record_found;
PopupWindow popupWindow;
LinearLayout linearLayout1;
UserPrefs userPrefs;
List<ScratchModel> list = new ArrayList<>();
    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_scratch_card;
    }
    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        getScratchCardDetails();
       // setupSwipeLayout();
        initializeRecyclerView();
        backpage = findViewById(R.id.backpage);
       backpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view_scr);
        linearLayout1 = findViewById(R.id.linearLayout1);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        adapter = new MyScratchAdapter(list);
      //  recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setLayoutManager(new GridLayoutManager(this, 2));
        recycler_view.setAdapter(adapter);

        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                try {
                    final ScratchModel scratchModel = adapter.getItem(position);
                    if (scratchModel == null) return;
                    switch (v.getId()) {
                        default:
                            MyApplication.getInstance().setSelectedScratch(scratchModel);
                            Bundle bundle = new Bundle();
                            bundle.putString(DATA, scratchModel.getContestid());
                            bundle.putString(DATA, scratchModel.getIs_active());
                            bundle.putString(DATA, scratchModel.getType());
                            bundle.putString(DATA, scratchModel.getValidtill());
                            bundle.putString(DATA, scratchModel.getCouponcardimage());
                            bundle.putString(DATA1, ScratchCard.this.getClass().getSimpleName());

                            LayoutInflater layoutInflater = (LayoutInflater) ScratchCard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View customView = layoutInflater.inflate(R.layout.scratchpopup,null);

                            final ImageView backimage = (ImageView) customView.findViewById(R.id.backimage);

                            Glide.with(customView)
                                    .load(R.drawable.ic_box)
                                    .centerCrop()
                                    .placeholder(R.mipmap.scratch)
                                    .into(backimage);

                            card_view = (ScratchView) customView.findViewById(R.id.card_view);
                            img_giphy = customView.findViewById(R.id.img_giphy);
                            textBalance = customView.findViewById(R.id.textBalance);
                            txtgetcoupon = customView.findViewById(R.id.txtgetcoupon);
                            txtdatemsg = customView.findViewById(R.id.txtdatemsg);
                            txtgetmsg = customView.findViewById(R.id.txtgetmsg);
                            LinearLayout layout = customView.findViewById(R.id.layout);

                            if(scratchModel.getType().equals("coupon")){

                                if(scratchModel.getIs_active().equals("0")){

                                    Log.e("ActiveStatus",""+scratchModel.getIs_active());

                                    Glide.with(customView)
                                            .load(scratchModel.getCouponcardimage())
                                            .centerCrop()
                                            .placeholder(R.drawable.ic_box)
                                            .into(backimage);

                                    card_view.setVisibility(View.GONE);

                                    txtgetmsg.setText("You have won a coupon");
                                    txtgetcoupon.setText(scratchModel.getCouponcode());
                                    textBalance.setText(scratchModel.getValidtill());


                                }else {

                                    Log.e("Call","Coupon_Code");



                                    card_view.setRevealListener(new ScratchView.IRevealListener() {
                                        @Override
                                        public void onRevealed(ScratchView scratchView) {
                                            // Toast.makeText(getApplicationContext(), "Reveled", Toast.LENGTH_LONG).show();;
                                            img_giphy.setVisibility(View.VISIBLE);
                                           // textBalance.setVisibility(View.GONE);

                                            txtgetmsg.setText("You have won a coupon");
                                            txtgetcoupon.setText(scratchModel.getCouponcode());
                                            textBalance.setText(scratchModel.getValidtill());

                                            Glide.with(customView)
                                                    .load(scratchModel.getCouponcardimage())
                                                    .centerCrop()
                                                    .placeholder(R.drawable.ic_box)
                                                    .into(backimage);

                                            /// call webservices//
                                            getScratchCardRequest(scratchModel.getMatchid(),scratchModel.getPoolcontestid(),scratchModel.getType());
                                            //////////
                                            Glide.with(getApplication())
                                                    .load(R.drawable.giphy)
                                                    .placeholder(R.drawable.giphy)
                                                    .into(img_giphy);
                                        }
                                        @Override
                                        public void onRevealPercentChangedListener(ScratchView scratchView, float percent) {
                                            if (percent>=50) {
                                                Log.d("Reveal Percentage", "onRevealPercentChangedListener: " + String.valueOf(percent));
                                            }
                                        }
                                    });

                                }

                            }else {

                                if(scratchModel.getScratchcardamount().equals("0")){
                                    //textBalance.setText("Better luck next time");
                                    txtgetmsg.setText("Better luck next time");
                                    txtgetcoupon.setVisibility(View.GONE);
                                    txtdatemsg.setVisibility(View.GONE);
                                    textBalance.setVisibility(View.GONE);

                                    Glide.with(customView)
                                            .load(R.drawable.ic_podium)
                                            .centerCrop()
                                            .placeholder(R.mipmap.scratch)
                                            .into(backimage);

                                }else {
                                    txtgetmsg.setText("You've won");
                                    textBalance.setText("₹"+scratchModel.getScratchcardamount());
                                    txtgetcoupon.setVisibility(View.GONE);
                                    txtdatemsg.setVisibility(View.GONE);


                                }

                                if(scratchModel.getIs_active().equals("0")){

                                   /* Glide.with(customView)
                                            .load(R.drawable.ic_box)
                                            .centerCrop()
                                            .placeholder(R.mipmap.scratch)
                                            .into(backimage);*/
                                    card_view.setVisibility(View.GONE);

                                }else {
                                    card_view.setRevealListener(new ScratchView.IRevealListener() {
                                        @Override
                                        public void onRevealed(ScratchView scratchView) {
                                            // Toast.makeText(getApplicationContext(), "Reveled", Toast.LENGTH_LONG).show();;

                                            if(scratchModel.getScratchcardamount().equals("0")){
                                                //textBalance.setText("Better luck next time");
                                                txtgetmsg.setText("Better luck next time");
                                                txtgetcoupon.setVisibility(View.GONE);
                                                txtdatemsg.setVisibility(View.GONE);
                                                textBalance.setVisibility(View.GONE);

                                                Glide.with(customView)
                                                        .load(R.drawable.ic_podium)
                                                        .centerCrop()
                                                        .placeholder(R.mipmap.scratch)
                                                        .into(backimage);

                                            }else {

                                                img_giphy.setVisibility(View.VISIBLE);
                                                textBalance.setVisibility(View.VISIBLE);
                                            }

                                            /// call webservices//
                                            getScratchCardRequest(scratchModel.getMatchid(),scratchModel.getPoolcontestid(),scratchModel.getType());
                                            //////////
                                            Glide.with(getApplication())
                                                    .load(R.drawable.giphy)
                                                    .placeholder(R.drawable.giphy)
                                                    .into(img_giphy);
                                        }
                                        @Override
                                        public void onRevealPercentChangedListener(ScratchView scratchView, float percent) {
                                            if (percent>=50) {
                                                Log.d("Reveal Percentage", "onRevealPercentChangedListener: " + String.valueOf(percent));
                                            }
                                        }
                                    });

                                }
                            }


                            //instantiate popup window
                            popupWindow = new PopupWindow(customView,
                                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //display the popup window
                            popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                            //close the popup window on button click
                            layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    popupWindow.dismiss();
                                    finish();
                                    if (getApplication() == null) return;
                                    Intent intent = new Intent(getApplication(), ScratchCard.class);
                                    startActivity(intent);
                                    if (getApplication() == null) return;
                                    ScratchCard.this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);

                                }
                            });
                            break;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onPageSelected() {
        updateViewVisibitity(ll_no_record_found, View.GONE);
        getScratchCardDetails();
    }

    private void getScratchCardRequest(String matchid, String poolcontestid, String type) {
        getWebRequestHelper().getScratchCardRequest(matchid, poolcontestid,type, this);
        getScratchCardDetails();
    }
    private void getScratchCardDetails() {
        getWebRequestHelper().getScratchCardDetails(this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case USER_SCRATCH_DETAILS:
                handleScratchResponse(webRequest);
                break;

            case USER_SCRATCH_REQUEST:
                handleScratchResponse(webRequest);
                break;
        }
    }

    private void handleScratchResponse(WebRequest webRequest) {
        ScratchResponseModel responsePojo = webRequest.getResponsePojo(ScratchResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            synchronized (MyApplication.getInstance().getLock()) {
                List<ScratchModel> data = responsePojo.getData();
                list.clear();
                if (data != null && data.size() > 0) {
                    list.addAll(data);
                    Log.e("Call_list",""+list);
                }
                if (isFinishing()) return;
                adapter.notifyDataSetChanged();
                updateNoDataView();
                MyApplication.getInstance().startTimer();
            }
        } else {
            if (isFinishing()) return;
            list.clear();
            adapter.notifyDataSetChanged();
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (list.size() > 0) {
           // updateViewVisibitity(ll_no_record_found, View.GONE);
            tv_no_record_found.setVisibility(View.GONE);
        } else {

            tv_no_record_found.setVisibility(View.VISIBLE);
         //   updateViewVisibitity(ll_no_record_found, View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
