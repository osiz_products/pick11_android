package com.app.ui.main.kabaddi.myteam.myTeams.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.TeamModel;
import com.pickeleven.R;

import java.util.List;

public class MyTeamsAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<TeamModel> list;

    public MyTeamsAdapter(Context context, List<TeamModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_my_teams));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }


    private class ViewHolder extends BaseViewHolder {

        private LinearLayout ll_layout;
        private TextView tv_team_name;
        private TextView tv_captain;
        private TextView tv_vice_captain;
        private LinearLayout ll_edit;
        private LinearLayout ll_preview;
        private LinearLayout ll_clone;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            tv_team_name = itemView.findViewById(R.id.tv_team_name);
            tv_captain = itemView.findViewById(R.id.tv_captain);
            tv_vice_captain = itemView.findViewById(R.id.tv_vice_captain);
            ll_edit = itemView.findViewById(R.id.ll_edit);
            ll_preview = itemView.findViewById(R.id.ll_preview);
            ll_clone = itemView.findViewById(R.id.ll_clone);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            ll_edit.setTag(position);
            ll_preview.setTag(position);
            ll_clone.setTag(position);
            ll_edit.setOnClickListener(this);
            ll_preview.setOnClickListener(this);
            ll_clone.setOnClickListener(this);

            TeamModel teamModel = list.get(position);
            tv_team_name.setText(teamModel.getTeamname());
            tv_captain.setText(teamModel.getCap());
            tv_vice_captain.setText(teamModel.getVcap());
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
