package com.app.ui.main.cricket.privateContests.prizebreakup.dialog.adpater;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.ContestModel;
import com.app.model.WinnerLavelModel;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.List;

public class BreakUpAdapter extends AppBaseRecycleAdapter {

    private Context context;
    private List<WinnerLavelModel> list;
    int selectedPosition;

    public BreakUpAdapter(Context context, List<WinnerLavelModel> list, int selectedPosition) {
        this.context = context;
        this.list = list;
        this.selectedPosition = selectedPosition;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_winner_breakup));
    }

    public ContestModel getJoinedContestModel(ContestModel contestModel) {
        return null;
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private LinearLayout ll_layout;
        private TextView tv_winners;
        private TextView tv_recommended;
        private ImageView radio_button;
        private RecyclerView recycler_view;
        private ChildBreakUpAdapter adapter;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            tv_winners = itemView.findViewById(R.id.tv_winners);
            tv_recommended = itemView.findViewById(R.id.tv_recommended);
            radio_button = itemView.findViewById(R.id.radio_button);
            recycler_view = itemView.findViewById(R.id.recycler_view);
            updateViewVisibitity(tv_recommended, View.GONE);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            ll_layout.setTag(position);
            ll_layout.setOnClickListener(this);
            WinnerLavelModel winnerLavelModel = list.get(position);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recycler_view.setLayoutManager(layoutManager);
            adapter = new ChildBreakUpAdapter(getContext()) {
                @Override
                public ContestModel getJoinedContestModel(ContestModel contestModel) {
                    return BreakUpAdapter.this.getJoinedContestModel(contestModel);
                }
            };
            recycler_view.setAdapter(adapter);

            recycler_view.setTag(position);
            adapter.updateData(winnerLavelModel.getRanks());
            ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    performItemClick((Integer) ll_layout.getTag(), v);
                }
            });
            tv_winners.setText(winnerLavelModel.getWinner() + " Winners");
            if (position == 0) {
                updateViewVisibitity(tv_recommended, View.VISIBLE);
            }
            if (position == selectedPosition) {
                ll_layout.setActivated(true);
                radio_button.setActivated(true);
            } else {
                ll_layout.setActivated(false);
                radio_button.setActivated(false);
            }
            return null;
        }

        @Override
        public void onClick(View v) {
            performItemClick((Integer) v.getTag(), v);
        }
    }
}
