package com.app.ui.main.kabaddi.myteam.chooseTeam;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.ContestModel;
import com.app.model.JoinedTeamModel;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPreviewModel;
import com.app.model.TeamModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webrequestmodel.SwitchTeamRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.JoinedTeamResponseModel;
import com.app.model.webresponsemodel.PlayerPreviewKabaddiResponseModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.model.webresponsemodel.TeamResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.SelectTeamDialog;
import com.app.ui.dialogs.joinconfirmdialog.ConfirmationJoinContestDialog;
import com.app.ui.dialogs.selection.DataDialog;
import com.app.ui.main.kabaddi.contests.ContestsActivity;
import com.app.ui.main.kabaddi.myteam.chooseTeam.adapter.ChooseTeamAdapter;
import com.app.ui.main.kabaddi.myteam.playerpreview.TeamPreviewDialog;
import com.app.ui.main.navmenu.myaccount.LowBalanceActivity;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;


public class ChooseTeamActivity extends AppBaseActivity {

    LinearLayout ll_heading;
    RecyclerView recycler_view;
    ChooseTeamAdapter adapter;
    TextView tv_no_record_found;
    LinearLayout ll_join_contest;
    TextView tv_join_contest;
    LinearLayout ll_create_team;
    TextView tv_my_teams_total;
    LinearLayout ll_switch_team;
    TextView tv_switch_team;
    LinearLayout ll_option_team_name;
    TextView tv_switch_team_name;
    ImageView iv_switch_team_name;
    List<TeamModel> myAllTeams = new ArrayList<>();
    String old_team_id = "";
    String alreadyJoinedTeams = "";
    ConfirmationJoinContestDialog confirmationJoinContestDialog;
    UserPrefs userPrefs;

    private PlayerTypeResponseModel.DataBean getPlayerTypeModel() {
        return MyApplication.getInstance().getPlayerTypeModels();
    }


    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    private ContestModel getContestModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = getIntent().getExtras().getString(DATA2);
            if (isValidString(string))
                return new Gson().fromJson(string, ContestModel.class);
        }
        return null;
    }

    private String getMatchContestId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return getIntent().getExtras().getString(DATA1);
        return "";
    }

    private boolean isJoin() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return getIntent().getExtras().getBoolean(DATA, false);
        }
        return false;
    }


    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_choose_team;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        recycler_view = findViewById(R.id.recycler_view);
        ll_heading = findViewById(R.id.ll_heading);
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        ll_option_team_name = findViewById(R.id.ll_option_team_name);
        tv_switch_team_name = findViewById(R.id.tv_switch_team_name);
        iv_switch_team_name = findViewById(R.id.iv_switch_team_name);
        ll_join_contest = findViewById(R.id.ll_join_contest);
        tv_join_contest = findViewById(R.id.tv_join_contest);
        ll_create_team = findViewById(R.id.ll_create_team);
        updateViewVisibility(ll_create_team, View.GONE);
        tv_my_teams_total = findViewById(R.id.tv_my_teams_total);
        ll_switch_team = findViewById(R.id.ll_switch_team);
        tv_switch_team = findViewById(R.id.tv_switch_team);

        ll_option_team_name.setOnClickListener(this);
        tv_join_contest.setOnClickListener(this);
        ll_create_team.setOnClickListener(this);
        ll_switch_team.setOnClickListener(this);

        initializeRecyclerView();

        getJoinedTeam();

        if (isJoin()) {
            updateViewVisibility(ll_join_contest, View.VISIBLE);
            updateViewVisibility(ll_heading, View.GONE);
            updateViewVisibility(ll_switch_team, View.GONE);
        } else {
            updateViewVisibility(ll_join_contest, View.GONE);
            updateViewVisibility(ll_heading, View.VISIBLE);
            updateViewVisibility(ll_switch_team, View.VISIBLE);
        }

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new ChooseTeamAdapter(this, myAllTeams) {
            @Override
            public boolean isTeamAlreadyJoined(String teamId) {
                return alreadyJoinedTeams.contains(teamId);
            }
        };
        recycler_view.setLayoutManager(getFullHeightLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TeamModel teamModel = myAllTeams.get(position);
                switch (v.getId()) {
                    case R.id.ll_preview:
                        getTeamPlayers(teamModel);
                        break;

                    default:
                        if (!adapter.isTeamAlreadyJoined(String.valueOf(teamModel.getId()))) {
                            adapter.setSelectedTeam(position);
                            setupUi();
                        }
                        break;
                }
            }
        });
    }

    private void setupUi() {
        if (adapter != null) {
            if (isValidString(adapter.getSelectedTeam())) {
                tv_switch_team.setActivated(true);
            } else {
                tv_switch_team.setActivated(false);
            }
        } else {
            tv_switch_team.setActivated(false);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_option_team_name:
                showTeamsDialog();
                break;

            case R.id.tv_join_contest:
                String ids = adapter.getSelectedTeam();
                if (TextUtils.isEmpty(ids)) {
                    showErrorMsg("Please select Team for join");
                    return;
                }
                preJoinContest();
                break;

            case R.id.ll_create_team:
                goToCreateTeamActivity(null);
                break;

            case R.id.ll_switch_team:
                String id = adapter.getSelectedTeam();
                if (TextUtils.isEmpty(id)) {
                    showErrorMsg("Please select Team for switch");
                    return;
                }
                callSwitchTeam();
                break;
        }
    }


    private void showTeamsDialog() {
        if (alreadyJoinedTeams.split(",").length > 1) {
            hideKeyboard();
            final List<TeamModel> alreadySelectedTeam = new ArrayList<>();
            for (int i = 0; i < myAllTeams.size(); i++) {

                if (adapter.isTeamAlreadyJoined(String.valueOf(myAllTeams.get(i).getId()))) {
                    alreadySelectedTeam.add(myAllTeams.get(i));
                }

            }
            final SelectTeamDialog selectStateDialog = new SelectTeamDialog();
            selectStateDialog.setDataList(alreadySelectedTeam);
            selectStateDialog.setOnItemSelectedListeners(new DataDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectStateDialog.dismiss();
                    old_team_id = String.valueOf(alreadySelectedTeam.get(position).getId());
                    setHeading();
                }
            });
            selectStateDialog.show(getFm(), selectStateDialog.getClass().getSimpleName());
        }

    }


    private void setHeading() {
        if (alreadyJoinedTeams.split(",").length > 1) {
            updateViewVisibility(iv_switch_team_name, View.VISIBLE);
        } else {
            updateViewVisibility(iv_switch_team_name, View.GONE);
        }

        TeamModel teamModel = new TeamModel();
        if (myAllTeams != null || myAllTeams.size() > 0) {
            for (int i = 0; i < myAllTeams.size(); i++) {
                if (String.valueOf(myAllTeams.get(i).getId()).equalsIgnoreCase(old_team_id)) {
                    teamModel = myAllTeams.get(i);
                }
            }

            if (teamModel != null) {
                tv_switch_team_name.setText(teamModel.getTeamname());
            }

        }


    }

    private void preJoinContest() {
        String matchContestId = getMatchContestId();
        if (TextUtils.isEmpty(matchContestId)) return;
        String new_team_id = adapter.getSelectedTeam();
        if (new_team_id == null) return;
        JoinContestRequestModel requestModel = new JoinContestRequestModel();
        requestModel.atype = PRE_JOIN;
        requestModel.matchid = getMatchModel().getMatchid();
        requestModel.poolcontestid = getMatchContestId();
        requestModel.uteamid = new_team_id;
        requestModel.fees = String.valueOf(getContestModel().getJoinfee());
        openConfirmJoinContest(requestModel);
    }

    private void JoinContest() {
        String matchContestId = getMatchContestId();
        if (TextUtils.isEmpty(matchContestId)) return;
        String new_team_id = adapter.getSelectedTeam();
        if (new_team_id == null) return;
        displayProgressBar(false);
        JoinContestRequestModel requestModel = new JoinContestRequestModel();
        requestModel.atype = JOIN;
        requestModel.matchid = getMatchModel().getMatchid();
        requestModel.poolcontestid = getMatchContestId();
        requestModel.uteamid = new_team_id;
        requestModel.fees = String.valueOf(getContestModel().getJoinfee());
        getWebRequestHelper().joinContest(requestModel, this);
    }

    private void callSwitchTeam() {
        if (getMatchModel() != null) {
            String matchContestId = getMatchContestId();
            if (TextUtils.isEmpty(matchContestId)) return;
            String new_team_id = adapter.getSelectedTeam();
            if (new_team_id == null) return;
            displayProgressBar(false);
            SwitchTeamRequestModel requestModel = new SwitchTeamRequestModel();
            requestModel.matchid = getMatchModel().getMatchid();
            requestModel.poolcontestid = getMatchContestId();
            requestModel.uteamid = old_team_id;
            requestModel.switchteamid = new_team_id;
            getWebRequestHelper().switchTeam(requestModel, this);
        }
    }


    /*private void goToCreateTeamActivity(Bundle bundle) {
        Intent intent = new Intent(this, CreateTeamActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, ContestsActivity.REQUEST_CREATE_TEAM);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }*/

    private void getJoinedTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getJoinedTeam(matchModel, getMatchContestId(), this);
    }

    private void getAllTeam() {
        MatchModel matchModel = MyApplication.getInstance().getSelectedMatch();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getAllTeam(matchModel, this);
    }

    private void getTeamPlayers(TeamModel teamModel) {
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getTeamPlayers(teamModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_JOINED_TEAM:
                handleJoinedTeamResponse(webRequest);
                break;

            case ID_ALL_TEAM:
                handleAllTeamResponse(webRequest);
                break;

            case ID_TEAM_PLAYERS:
                handleTeamPlayersResponse(webRequest);
                break;

            case ID_SWITCH_TEAM:
                handleSwitchTeamResponse(webRequest);
                break;

            case ID_JOIN_CONTEST:
                handleJoinContestResponse(webRequest);
                break;
        }
    }

    private void handleJoinedTeamResponse(WebRequest webRequest) {
        StringBuilder builder = new StringBuilder();
        builder.append("");
        JoinedTeamResponseModel responseModel = webRequest.getResponsePojo(JoinedTeamResponseModel.class);
        if (responseModel != null) {
            if (!responseModel.isError()) {
                List<JoinedTeamModel> data = responseModel.getData();
                if (data == null) return;
                for (JoinedTeamModel datum : data) {
                    if (builder.length() > 0)
                        builder.append(",").append(datum.getUteamid());
                    else
                        builder.append(datum.getUteamid());
                }
            }
        }
        alreadyJoinedTeams = builder.toString();
        old_team_id = alreadyJoinedTeams.split(",")[0];
        getAllTeam();
    }

    private void handleSwitchTeamResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);

            onBackPressed();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }


    private void handleAllTeamResponse(WebRequest webRequest) {
        TeamResponseModel responseModel = webRequest.getResponsePojo(TeamResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            TeamResponseModel.DataBean data = responseModel.getData();
            if (data == null) return;
            List<TeamModel> teams = data.getTeams();
            myAllTeams.clear();
            if (teams != null && teams.size() > 0) {
                myAllTeams.addAll(teams);
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
            updateButton();
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
        setHeading();
    }

    private void updateButton() {
        if (getPlayerTypeModel() != null && myAllTeams.size() < getPlayerTypeModel().getMaxteam()) {
            updateViewVisibility(ll_create_team, View.VISIBLE);
        } else {
            updateViewVisibility(ll_create_team, View.GONE);
        }
    }


    private void handleTeamPlayersResponse(WebRequest webRequest) {
        TeamModel teamModel = webRequest.getExtraData(DATA);
        PlayerPreviewKabaddiResponseModel responseModel = webRequest.getResponsePojo(PlayerPreviewKabaddiResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            List<PlayerPreviewModel> data = responseModel.getData();
            if (data == null) return;
            List<PlayerModel> defender = new ArrayList<>();
            List<PlayerModel> raider = new ArrayList<>();
            List<PlayerModel> allrounder = new ArrayList<>();

            PlayerModel playerModel;
            for (PlayerPreviewModel datum : data) {
                playerModel = new PlayerModel();
                playerModel.setPid(datum.getPid());
                playerModel.setTeamname(datum.getTeamname());
                playerModel.setPts(datum.getPts());
                playerModel.setCredit(datum.getCredit());
                playerModel.setIscap(datum.getIscap());
                playerModel.setIsvcap(datum.getIsvcap());
                playerModel.setPlayertype(datum.getPlayertype());
                playerModel.setPimg(datum.getPimg());
                playerModel.setFullname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setPname(datum.getPname());
                playerModel.setTeam_id(String.valueOf(teamModel.getId()));
                playerModel.setTeam_name(String.valueOf(teamModel.getTeamname()));
                if (datum.isDefender()) {
                    playerModel.setPtype("DEF");
                    defender.add(playerModel);
                } else if (datum.isRaider()) {
                    playerModel.setPtype("RAID");
                    raider.add(playerModel);
                } else if (datum.isAllRounderK()) {
                    playerModel.setPtype("AR");
                    allrounder.add(playerModel);
                }
            }

            PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean playersBean = new PlayerPreviewKabaddiResponseModel.DataBean.PlayersBean();
            playersBean.setDefender(defender);
            playersBean.setRaider(raider);
            playersBean.setAllrounder(allrounder);

            PlayerPreviewKabaddiResponseModel.DataBean team = new PlayerPreviewKabaddiResponseModel.DataBean();
            team.setId("0");
            team.setTeam1_name(getMatchModel().getTeam1());
            team.setTeam2_name(getMatchModel().getTeam2());
            team.setPlayers(playersBean);

            TeamPreviewDialog instance = TeamPreviewDialog.getInstance(null);
            instance.setTeam(team);
            instance.show(getFm(), instance.getClass().getSimpleName());


        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }

    }


    private void goToLowBalanceActivity(Bundle bundle) {
        Intent intent = new Intent(this, LowBalanceActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (isFinishing()) return;
        startActivityForResult(intent, ContestsActivity.REQUEST_LOW_BALANCE);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    private void openConfirmJoinContest(JoinContestRequestModel model) {
        String matchContestId =getMatchContestId();
        if (!isValidString(matchContestId)) return;
        Bundle bundle = new Bundle();
        bundle.putString(DATA, new Gson().toJson(model));
        confirmationJoinContestDialog = ConfirmationJoinContestDialog.getInstance(bundle);
        confirmationJoinContestDialog.setConfirmationJoinContestListener(new ConfirmationJoinContestDialog.ConfirmationJoinContestListener() {
            @Override
            public void onProceed() {
                JoinContest();
            }

            @Override
            public void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel) {
                confirmationJoinContestDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(preJoinedModel));
                goToLowBalanceActivity(bundle);
            }
        });
        confirmationJoinContestDialog.show(getFm(), confirmationJoinContestDialog.getClass().getSimpleName());

    }

    private void handleJoinContestResponse(WebRequest webRequest) {
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);

            onBackPressed();
        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ContestsActivity.REQUEST_CREATE_TEAM) {
            if (resultCode == RESULT_OK) {
                getJoinedTeam();
            }
        } else if (requestCode == ContestsActivity.REQUEST_LOW_BALANCE) {
            if (resultCode == RESULT_OK) {
                preJoinContest();
            }
        }
    }
}
