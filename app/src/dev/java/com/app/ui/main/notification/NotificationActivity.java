package com.app.ui.main.notification;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.appbase.AppBaseActivity;
import com.app.model.IdBean;
import com.app.model.MessageModel;
import com.app.model.webrequestmodel.NotiStatusRequestModel;
import com.app.model.webrequestmodel.TransactionRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.MessageResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.ConfirmationDialog;
import com.app.ui.main.cricket.contestDetail.ContestsDetailActivity;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.main.navmenu.ToolBarFragment;
import com.app.ui.main.notification.adapter.NotificationAdapter;
import com.pickeleven.R;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppBaseActivity implements ToolBarFragment.ToolbarFragmentInterFace {

    SwipeRefreshLayout swipeRefresh;
    RecyclerView recycler_view;
    NotificationAdapter adapter;
    TextView tv_no_record_found;
    List<MessageModel.ListBean> list = new ArrayList<>();
    boolean back_info;
    private int totalPages = 1000;
    private int currentPage = 0;
    boolean loadingNextData = false;
    UserPrefs userPrefs;
    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_notification;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        back_info = getIntent().getExtras().getBoolean(NOT_BACK_BTN);
        setupSwipeLayout();
        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);

        initializeRecyclerView();
        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setupSwipeLayout() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorOrange,
                R.color.colorGreen,
                R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                currentPage = 0;
                callNextApi();
            }
        });
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new NotificationAdapter(this);
        adapter.updateData(list);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                MessageModel.ListBean item = adapter.getItem(position);
                if (item == null) return;
                switch (v.getId()) {
                    case R.id.ll_header:
                        if (!item.isRead()) {
                            updateStatus(item, "statusUpdate");
                        }
                        break;
                    case R.id.iv_delete:
                        addConfirmationDialog(item, "Are you sure want to delete?", false);
                        break;
                }
            }
        });

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = recycler_view.getLayoutManager().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) recycler_view.getLayoutManager()).findLastVisibleItemPosition();
                if (!loadingNextData && totalItemCount <= (lastVisibleItem + 5)) {
                    callNextApi();
                }
            }
        });
        callNextApi();
    }

    @Override
    public void onToolbarItemClick(View view) {
        if (view.getId() == R.id.iv_delete_all) {
            if (list.size() > 0) {
                addConfirmationDialog(null, "Are you sure want to deleteAll?", true);
            } else {
                showErrorMsg("Notification not found for delete.");
            }
        }
    }

    private void addConfirmationDialog(final MessageModel.ListBean item, String message, final boolean deleteAll) {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        ConfirmationDialog dialog = ConfirmationDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if (deleteAll) {
                        displayProgressBar(false);
                        getWebRequestHelper().deleteAllNotification("deleteAll", NotificationActivity.this);
                    } else {
                        updateStatus(item, "delete");
                    }
                }
                dialog.dismiss();

            }
        });
        dialog.show(getFm(), "Confirm Logout");
    }

    private void callNextApi() {
        if (isFinishing()) return;
        if (this.currentPage == 0) {
            this.currentPage = 1;
            this.totalPages = 1000;
            callApi();
            return;
        }
        if (this.totalPages > this.currentPage) {
            this.currentPage = this.currentPage + 1;
            callApi();
        }
    }

    private void callApi() {
        setLoadingNextData(true);
        TransactionRequestModel requestModel = new TransactionRequestModel();
        requestModel.page = currentPage;
        requestModel.limit = 10;
        getWebRequestHelper().getNotification(requestModel, this);
    }

    public void setLoadingNextData(boolean isLoading) {
        if (!isFinishing()) {
            this.loadingNextData = isLoading;
            if (swipeRefresh.isRefreshing()) {
                swipeRefresh.setRefreshing(isLoading);
            } else {
                if (adapter != null)
                    adapter.setLoadMore(loadingNextData);
            }
        }
    }

    public void updateData(List<MessageModel.ListBean> levelList) {
        this.list.clear();
        if (levelList != null) {
            this.list.addAll(levelList);
        }

        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void addDataToList(List<MessageModel.ListBean> levelList) {
        if (levelList != null) {
            this.list.addAll(levelList);
        }
        if (!isFinishing() && adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    private void updateStatus(MessageModel.ListBean item, String type) {
        NotiStatusRequestModel requestModel = new NotiStatusRequestModel();
        requestModel.sendAll = item.getSendAll();
        requestModel.atype = type;//item.getUserid();
        if (item.get_id() != null)
            requestModel.id = item.get_id().get$oid();
        displayProgressBar(false);
        getWebRequestHelper().updateNotiStatus(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        setLoadingNextData(false);
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_NOTIFICATION:
                handleContestsResponse(webRequest);
                break;
            case ID_UPDATE_NOTI_STATUS:
                handleNotiStatusResponse(webRequest);
                break;
            case ID_DELETE_ALL:
                handleDeleteAllResponse(webRequest);
                break;
        }
    }

    private void handleDeleteAllResponse(WebRequest webRequest) {
        EmptyResponseModel responsePojo = webRequest.getResponsePojo(EmptyResponseModel.class);
        if (responsePojo != null) {
            if (!responsePojo.isError()) {
                if (adapter != null) {
                    list.clear();
                    adapter.notifyDataSetChanged();
                    String message = responsePojo.getMessage();
                    if (isValidString(message))
                        showCustomToast(message);
                }
            }
        }
    }

    private void handleContestsResponse(WebRequest webRequest) {
        MessageResponseModel responseModel = webRequest.getResponsePojo(MessageResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            MessageModel data = responseModel.getData();
            if (data == null) return;
            List<MessageModel.ListBean> list = data.getList();
            if (list != null && list.size() == 0) {
                totalPages = currentPage;
            }
            if (currentPage == 1) {
                updateData(list);
            } else {
                addDataToList(list);
            }
        } else {
            if (isFinishing()) return;
            setLoadingNextData(false);
            totalPages = 0;
            updateNoRecord();
        }
        if (adapter != null)
            updateDeleteAllVisibility(adapter.getDataCount());
    }

    private void updateNoRecord() {
        if (list.size() > 0) {
            updateViewVisibility(tv_no_record_found, View.GONE);
        } else {
            tv_no_record_found.setText("No notification found");
            updateViewVisibility(tv_no_record_found, View.VISIBLE);
        }
    }

    private void handleNotiStatusResponse(WebRequest webRequest) {
        NotiStatusRequestModel listBean = webRequest.getExtraData(DATA);
        MessageModel.ListBean bean = new MessageModel.ListBean();
        if (listBean != null) {
            IdBean idBean = new IdBean();
            idBean.set$oid(listBean.id);
            bean.set_id(idBean);

            EmptyResponseModel responsePojo = webRequest.getResponsePojo(EmptyResponseModel.class);
            if (responsePojo != null) {
                if (!responsePojo.isError()) {
                    if (adapter != null) {
                        if (listBean.atype.equalsIgnoreCase("delete")) {
                            int index = list.indexOf(bean);
                            if (index >= 0) {
                                list.remove(index);
                                adapter.notifyItemRemoved(index);
                                adapter.notifyItemRangeChanged(index, list.size());
                                String message = responsePojo.getMessage();
                                if (isValidString(message))
                                    showCustomToast(message);
                            }
                        } else {
                            List<MessageModel.ListBean> list = adapter.getList();
                            if (list != null) {
                                int index = list.indexOf(bean);
                                if (index == -1)
                                    return;
                                MessageModel.ListBean listBean1 = list.get(index);
                                listBean1.setUserid("1");
                                adapter.notifyItemChanged(index);
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (back_info) {
            goToDashboardActivity(getIntent().getExtras());
            return;
        }
    }

    private void goToDashboardActivity(Bundle bundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        startActivity(intent);
        supportFinishAfterTransition();
    }
}
