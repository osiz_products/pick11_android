package com.app.ui.main.kabaddi.myteam.playerDetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.PlayerPointsModel;
import com.app.model.webresponsemodel.PlayerPointsResponse;
import com.app.preferences.UserPrefs;
import com.app.ui.MyApplication;
import com.app.ui.main.kabaddi.myteam.playerDetail.adapter.PlayerPointDetailsAdapter;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 1/7/19.
 */
public class PlayerDetailActivity extends AppBaseActivity {


    RecyclerView recycler_view;
    PlayerPointDetailsAdapter adapter;
    ImageView iv_player_image;
    TextView tv_player_credits;
    TextView tv_player_total_points;
    TextView tv_no_record_found;
    List<PlayerPointsModel> list = new ArrayList<>();
    UserPrefs userPrefs;

    private PlayerModel getPlayerModel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String data = extras.getString(DATA);
            if (isValidString(data))
                return new Gson().fromJson(data, PlayerModel.class);
        }
        return null;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_player_detail;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);

        tv_no_record_found = findViewById(R.id.tv_no_record_found);
        updateViewVisibility(tv_no_record_found, View.GONE);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        iv_player_image = findViewById(R.id.iv_player_image);
        tv_player_credits = findViewById(R.id.tv_player_credits);
        tv_player_total_points = findViewById(R.id.tv_player_total_points);
        initializeRecyclerView();
        if (getPlayerModel() == null) return;
        getJoinedTeam();
        PlayerModel playerModel = getPlayerModel();
        setHeaderTitle(playerModel.getFullname());
        tv_player_credits.setText(playerModel.getCreditText());
        tv_player_total_points.setText(String.valueOf(playerModel.getPts()));
        loadImage(this, iv_player_image, null, playerModel.getPimg(), R.drawable.no_image);

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void initializeRecyclerView() {
        recycler_view = findViewById(R.id.recycler_view);
        adapter = new PlayerPointDetailsAdapter(list);
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
        recycler_view.setNestedScrollingEnabled(true);
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (v.getId()) {

                }

            }
        });
    }

    private void getJoinedTeam() {
        MatchModel selectedMatch = MyApplication.getInstance().getSelectedMatch();
        if (selectedMatch == null || getPlayerModel() == null) return;
        String gameType = selectedMatch.getSeriesid();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().getPlayerDetail("/kabaddi" ,gameType, getPlayerModel(), selectedMatch.getMtype(), this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_PLAYER_DETAIL:
                handlePlayerPointsResponse(webRequest);
                break;
        }
    }

    private void handlePlayerPointsResponse(WebRequest webRequest) {
        PlayerPointsResponse responsePojo = webRequest.getResponsePojo(PlayerPointsResponse.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            List<PlayerPointsModel> data = responsePojo.getData();
            if (data == null) return;
            list.clear();
            if (data.size() > 0) {
                list.addAll(data);
//                int totalCredit = 0;
//                for (PlayerPointsModel playerPointsModel : list) {
//                    totalCredit += Integer.parseInt(playerPointsModel.getTotalpoints());
//                }
//                tv_player_total_points.setText(String.valueOf(totalCredit));
            } else {
                updateView(data);
            }
            if (isFinishing()) return;
            adapter.notifyDataSetChanged();
        } else {
            if (isFinishing()) return;
            updateView(null);
        }
    }

    private void updateView(List<PlayerPointsModel> list) {
        if (list != null && list.size() > 0)
            updateViewVisibility(tv_no_record_found, View.GONE);
        else
            updateViewVisibility(tv_no_record_found, View.VISIBLE);

    }
}
