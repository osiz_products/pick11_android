package com.app.ui.main.cricket.myteam.playerDetail.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.PlayerPointsModel;
import com.pickeleven.R;

import java.util.List;

public class PlayerPointDetailsAdapter extends AppBaseRecycleAdapter {

    private List<PlayerPointsModel> list;
    public PlayerPointDetailsAdapter(List<PlayerPointsModel> list) {
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_player_points));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    private class ViewHolder extends BaseViewHolder {

        private LinearLayout ll_layout;
        private TextView tv_match_name;
        private TextView tv_points;
        private TextView tv_selected_by;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_layout = itemView.findViewById(R.id.ll_layout);
            tv_match_name = itemView.findViewById(R.id.tv_match_name);
            tv_points = itemView.findViewById(R.id.tv_points);
            tv_selected_by = itemView.findViewById(R.id.tv_selected_by);
        }

        @Override
        public String setData(int position) {
            if (list == null) return null;
            String currency_symbol = getContext().getResources().getString(R.string.currency_symbol);
            PlayerPointsModel playerPointsModel = list.get(position);
            tv_match_name.setText(playerPointsModel.getTeam1().toUpperCase() + " Vs " + playerPointsModel.getTeam2().toUpperCase());
            tv_points.setText(playerPointsModel.getPts());
            tv_selected_by.setText(playerPointsModel.getSelectedPercentage() + "%");

            return currency_symbol;
        }
    }
}
