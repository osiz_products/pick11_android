package com.app.ui.main.navmenu.profile;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.StateModel;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.UpdateProfileRequestModel;
import com.app.model.webresponsemodel.StateResponseModel;
import com.app.model.webresponsemodel.UpdateProfileResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.state.SelectStateDialog;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.imagePicker.FileInformation;
import com.imagePicker.ProfilePicDialog;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.DatePickerUtil;
import com.utilities.DeviceUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class EditProfileActivity extends AppBaseActivity {

    ImageView iv_profile_pic;
    ProgressBar pb_image;
    ImageView iv_edit_pic;
    EditText et_name;
    TextView et_email;
    EditText et_team_name;
    TextView et_dob;
    RadioGroup rg_gender;
    TextView et_mobile_number;
    TextView et_password;
    EditText et_city;
    TextView tv_state;
    EditText et_address;

    LinearLayout ll_email;
    LinearLayout ll_team_name;
    LinearLayout ll_dob;
    LinearLayout ll_mobile_number;
    LinearLayout ll_password;
    LinearLayout ll_city;
    LinearLayout ll_state;
    LinearLayout ll_state2;
    LinearLayout ll_address;
    TextView tv_update_profile;
    UserPrefs userPrefs;
    ImageView bagheaderImg;
    ImageView bagheaderImg_black;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_edit_profile;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);

        bagheaderImg = findViewById(R.id.bagheaderImg);
        bagheaderImg_black = findViewById(R.id.bagheaderImg_dark);

        iv_profile_pic = findViewById(R.id.iv_profile_pic);
        pb_image = findViewById(R.id.pb_image);
        iv_edit_pic = findViewById(R.id.iv_edit_pic);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_team_name = findViewById(R.id.et_team_name);
        et_dob = findViewById(R.id.et_dob);
        et_mobile_number = findViewById(R.id.et_mobile_number);
        et_password = findViewById(R.id.et_password);
        rg_gender = findViewById(R.id.rg_gender);
        et_city = findViewById(R.id.et_city);
        tv_state = findViewById(R.id.tv_state);
        et_address = findViewById(R.id.et_address);

        ll_email = findViewById(R.id.ll_email);
        ll_team_name = findViewById(R.id.ll_team_name);
        ll_dob = findViewById(R.id.ll_dob);
        ll_mobile_number = findViewById(R.id.ll_mobile_number);
        ll_password = findViewById(R.id.ll_password);
        updateViewVisibility(ll_password, View.GONE);
        ll_city = findViewById(R.id.ll_city);
        ll_state = findViewById(R.id.ll_state);
        ll_state2 = findViewById(R.id.ll_state2);
        ll_address = findViewById(R.id.ll_address);
        tv_update_profile = findViewById(R.id.tv_update_profile);

        iv_edit_pic.setOnClickListener(this);
        tv_update_profile.setOnClickListener(this);
        et_city.setOnClickListener(this);
        ll_dob.setOnClickListener(this);
        ll_state.setOnClickListener(this);
        ll_state2.setOnClickListener(this);

        updateViewVisibility(ll_email, View.GONE);
//        updateViewVisibility(ll_team_name, View.GONE);
        updateViewVisibility(ll_mobile_number, View.GONE);
        updateViewVisibility(ll_password, View.GONE);

        setupUserDetail();
        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            bagheaderImg.setVisibility(View.VISIBLE);
            bagheaderImg_black.setVisibility(View.GONE);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            bagheaderImg_black.setVisibility(View.VISIBLE);
            bagheaderImg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.iv_edit_pic:
                showImagePickerDialog();
                break;

            case R.id.ll_state:
            case R.id.ll_state2:
              //  selectStateDialog();
                break;

            case R.id.ll_dob:
                showDatePickerDialog(et_dob);
                break;

            case R.id.tv_update_profile:
                onSubmit();
                break;
        }
    }

    private void showDatePickerDialog(final TextView textView) {
        DatePickerUtil.showDatePicker(this, textView, false, new DatePickerUtil.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Calendar calendar) {
                Date date = calendar.getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String sel_date = simpleDateFormat.format(date);
                textView.setTag(calendar);
                textView.setText(sel_date);
            }
        });
    }

    private void selectStateDialog() {
        final List<StateModel> state = getUserPrefs().getStateList();
        if (state == null) {
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().getStatusUrl(this);
        } else {
            final SelectStateDialog selectTeamDialog = new SelectStateDialog();
            selectTeamDialog.setDataList(state);
            selectTeamDialog.setTitle("Select State");
            selectTeamDialog.setOnItemSelectedListeners(new SelectStateDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectTeamDialog.dismiss();
                    tv_state.setText(String.valueOf(state.get(position).getName()));
                }
            });
            selectTeamDialog.show(getFm(), selectTeamDialog.getClass().getSimpleName());
        }
    }

    private void showImagePickerDialog() {
        final ProfilePicDialog dialog = ProfilePicDialog.getNewInstance(false);
        dialog.setProfilePicDialogListner(new ProfilePicDialog.ProfilePicDialogListner() {
            @Override
            public void onProfilePicSelected(FileInformation fileInformation) {
                dialog.dismiss();

                updateViewVisibility(pb_image, View.VISIBLE);

                String imageName = getUserModel().getId() + "_profile_pic_" + System.currentTimeMillis();

                String large_file_path = fileInformation.getBitmapPathForUpload(EditProfileActivity.this,
                        FileInformation.IMAGE_SIZE_LARGE, FileInformation.IMAGE_SIZE_LARGE,
                        "large/" + imageName);

                String thumb_file_path = fileInformation.getBitmapPathForUpload(EditProfileActivity.this,
                        FileInformation.IMAGE_SIZE_THUMB, FileInformation.IMAGE_SIZE_THUMB,
                        "thumb/" + imageName);

                iv_profile_pic.setTag(R.id.image_path_tag, large_file_path);
                iv_profile_pic.setTag(R.id.image_path_thumb_tag, thumb_file_path);
                loadImage(EditProfileActivity.this, iv_profile_pic, pb_image, thumb_file_path,
                        R.drawable.no_image);
            }

            @Override
            public void onProfilePicRemoved() {

            }
        });
        dialog.showDialog(this, getFm());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void setupUserDetail() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            if (userModel.isTeamNameEditable()){
                et_team_name.setFocusable(true);
            }else {
                et_team_name.setFocusable(false);
            }
            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                loadImage(this, iv_profile_pic, pb_image, imageUrl,
                        R.drawable.no_image, 300);
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibility(pb_image, View.INVISIBLE);
            }
            et_name.setText(userModel.getName());
            et_email.setText(userModel.getEmail());
            et_team_name.setText(userModel.getTeamname());
            et_dob.setText(userModel.getFormattedDate(3));
            et_mobile_number.setText(userModel.getPhone());
            if (userModel.getGender().isEmpty()) {
                ((RadioButton) rg_gender.getChildAt(0)).setChecked(false);
                ((RadioButton) rg_gender.getChildAt(1)).setChecked(false);
            } else {
                if (userModel.getGender().equalsIgnoreCase("Male"))
                    ((RadioButton) rg_gender.getChildAt(0)).setChecked(true);
                else
                    ((RadioButton) rg_gender.getChildAt(1)).setChecked(true);
            }
            et_city.setText(userModel.getCity());
            tv_state.setText(userModel.getState());

            Log.e("GETSTAtus",""+userModel.getState());

            et_address.setText(userModel.getAddress());
        }
    }

    private void onSubmit() {

        String name = et_name.getText().toString();
        String teamName = et_team_name.getText().toString();
        String mobile_number = et_mobile_number.getText().toString();
        String dob = et_dob.getText().toString();
        String gender = "";
        if (rg_gender.getCheckedRadioButtonId() != -1) {
            gender = ((RadioButton) findViewById(rg_gender.getCheckedRadioButtonId()))
                    .getText().toString();
        }

        String city = et_city.getText().toString();
        String state = tv_state.getText().toString();
        String address = et_address.getText().toString();

        if (name.isEmpty()) {
            showErrorMsg("Please enter name.");
            return;
        }

        if (teamName.isEmpty()) {
            showErrorMsg("Please enter pick name.");
            return;
        }
        if (mobile_number.isEmpty()) {
            showErrorMsg("Please enter mobile number.");
            return;
        }

        if (gender.isEmpty()) {
            showErrorMsg("Please select gender.");
            return;
        }
        if (city.isEmpty()) {
            showErrorMsg("Please enter city.");
            return;
        }
        /*if (state.isEmpty()) {
            showErrorMsg("Please select state.");
            return;
        }*/

        if (address.isEmpty()) {
            showErrorMsg("Please enter address.");
            return;
        }
        String imagePath = (String) iv_profile_pic.getTag(R.id.image_path_tag);
        if (imagePath == null) {
            imagePath = "";
        }

        UpdateProfileRequestModel requestModel = new UpdateProfileRequestModel();
        requestModel.name = name;
        requestModel.teamname = teamName;
        requestModel.gender = gender.toLowerCase();
        requestModel.dob = dob;
        requestModel.address = address;
        requestModel.state = state;
        requestModel.city = city;
        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        requestModel.profilepic = new File(imagePath);
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().updateProfile(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_UPDATE_PROFILE:
                handleUpdateProfileResponse(webRequest);
                break;

            case ID_GET_STATE:
                handleGetStateResponse(webRequest);
                break;
        }
    }

    private void handleGetStateResponse(WebRequest webRequest) {
        StateResponseModel responseModel = webRequest.getResponsePojo(StateResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<StateModel> data = responseModel.getData();
            getUserPrefs().updateStateList(data);
            selectStateDialog();
        }
    }

    private void handleUpdateProfileResponse(WebRequest webRequest) {
        UpdateProfileResponseModel responseModel = webRequest.getResponsePojo(UpdateProfileResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            UserModel data = responseModel.getData().getData();
            UserModel userModel = getUserModel();
            userModel.setProfilepic(data.getProfilepic());
            userModel.setName(data.getName());
            userModel.setTeamname(data.getTeamname());
            userModel.setDob(data.getDob());
            userModel.setGender(data.capitalize(data.getGender()));
            userModel.setCity(data.getCity());
            userModel.setState(data.getState());
            userModel.setAddress(data.getAddress());
            userModel.setIstnameedit(data.getAddress());
            userModel.setIstnameedit(data.getToken());
            getUserPrefs().updateLoggedInUser(data);
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showCustomToast(msg);

            onBackPressed();

        } else {
            String msg = responseModel.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }
}
