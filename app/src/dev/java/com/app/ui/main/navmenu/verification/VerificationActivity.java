package com.app.ui.main.navmenu.verification;

import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.viewpager.widget.ViewPager;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.ViewPagerAdapter;
import com.app.model.UserModel;
import com.app.model.webresponsemodel.ProfileResponseModel;
import com.app.preferences.UserPrefs;
import com.app.ui.main.navmenu.verification.account.AccountFragment;
import com.app.ui.main.navmenu.verification.mobile.MobileFragment;
import com.app.ui.main.navmenu.verification.panCard.PanCardFragment;
import com.pickeleven.R;
import com.google.android.material.tabs.TabLayout;
import com.medy.retrofitwrapper.WebRequest;

public class VerificationActivity extends AppBaseActivity {

    TabLayout tabs;
    ViewPager view_pager;
    ViewPagerAdapter adapter;
    UserModel userModel;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.activity_verification;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        tabs = findViewById(R.id.tabs);
        view_pager = findViewById(R.id.view_pager);

        setupViewPager();
        getUserDetail();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void getUserDetail() {
        getWebRequestHelper().getProfileDetail(this);
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getFm());
        adapter.addFragment(new MobileFragment(), "Identity");
        adapter.addFragment(new PanCardFragment(), "Pan Card");
        adapter.addFragment(new AccountFragment(), "Account");

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                adapter.getItem(position).onPageSelected();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        view_pager.setAdapter(adapter);
        tabs.setupWithViewPager(view_pager);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_GET_USER_PROFILE:
                handleUserProfileResponse(webRequest);
                break;
        }
    }

    private void handleUserProfileResponse(WebRequest webRequest) {
        ProfileResponseModel responseModel = webRequest.getResponsePojo(ProfileResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError() && responseModel.getData() != null) {
            if (isFinishing()) return;
            userModel = responseModel.getData();
            if (userModel == null) return;
            UserModel userModel1 = getUserModel();
            userModel1.setIsbankdverify(userModel.getIsbankdverify());
            userModel1.setIsemailverify(userModel.getIsemailverify());
            userModel1.setIsphoneverify(userModel.getIsphoneverify());
            userModel1.setIspanverify(userModel.getIspanverify());
            getUserPrefs().updateLoggedInUser(userModel1);
            LinearLayout tabStrip = ((LinearLayout) tabs.getChildAt(0));
            if (!userModel.isEmailVerify() && !userModel.isPhoneVerify()) {
                tabStrip.getChildAt(1).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        if (!userModel.isEmailVerify() && event.getAction() == MotionEvent.ACTION_DOWN)
//                            showErrorMsg("Please email verified fist.");
//                        else if (!userModel.isPhoneVerify() && event.getAction() == MotionEvent.ACTION_DOWN)
//                            showErrorMsg("Please mobile number verified fist.");
                        return true;
                    }
                });
                tabStrip.getChildAt(2).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        if (!userModel.isEmailVerify() && event.getAction() == MotionEvent.ACTION_DOWN)
//                            showErrorMsg("Please email verified fist.");
//                        else if (!userModel.isPhoneVerify() && event.getAction() == MotionEvent.ACTION_DOWN)
//                            showErrorMsg("Please mobile number verified fist.");
                        return true;
                    }
                });


            }
            if (!userModel.isPanVerify()) {
                tabStrip.getChildAt(2).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        if (event.getAction() == MotionEvent.ACTION_DOWN)
//                            showErrorMsg("Please Pan card verified fist.");
                        return true;
                    }
                });
            }
        }
    }
}
