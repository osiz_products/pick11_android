package com.app.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.app.appbase.AppBaseApplication;
import com.app.appbase.AppBaseResponseModel;
import com.app.model.GameTypeModel;
import com.app.model.MatchModel;
import com.app.model.MessageModel;
import com.app.model.ScratchModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.model.webresponsemodel.PlayerTypeResponseModel;
import com.app.ui.login.LoginActivity;
import com.app.ui.splash.SplashActivity;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.smsreceiver.AppSignatureHashHelper;
import com.sociallogin.FacebookLoginHandler;
import com.sociallogin.GplusLoginHandler;

import java.util.ArrayList;
import java.util.List;


public class MyApplication extends AppBaseApplication {
    public Context context;
    FacebookLoginHandler facebookLoginHandler;
    GplusLoginHandler gplusLoginHandler;

    private final Object lock = new Object();
    private long serverdate;
    private GameTypeModel gemeType;
    private Handler handler;
    private Runnable runnable;
    private PlayerTypeResponseModel.DataBean playerTypeModels;
    private List<GameTypeModel> gameTypeModels = new ArrayList<>();
    private List<MatchTimerListener> matchTimerListeners = new ArrayList<>();

    private MatchModel selectedMatch;
    private ScratchModel selectedScratch;
    private PrizePoolRequestModel prizePoolRequestModel;

    public void setPlayerTypeModels(PlayerTypeResponseModel.DataBean playerTypeModels) {
        this.playerTypeModels = playerTypeModels;
    }

    public PlayerTypeResponseModel.DataBean getPlayerTypeModels() {
        return playerTypeModels;
    }

    public String getGemeType() {
        if (gemeType != null)
            return gemeType.getId();
        return "1";
    }

    public void setGemeType(GameTypeModel gemeType) {
        this.gemeType = gemeType;
    }
    public long getServerdate() {
        return serverdate;
    }
    public void setServerdate(long serverdate) {
        this.serverdate = serverdate;
    }

    public MatchModel getSelectedMatch() {
        if (selectedMatch != null)
            return selectedMatch;
        moveToSplashActivity(null);
        return null;
    }

    public void setSelectedMatch(MatchModel selectedMatch) {
        this.selectedMatch = selectedMatch;
    }

    public void setSelectedScratch(ScratchModel selectedScratch) {
        this.selectedScratch = selectedScratch;
    }

    public PrizePoolRequestModel getPrizePoolRequestModel() {
        return prizePoolRequestModel;
    }

    public void setPrizePoolRequestModel(PrizePoolRequestModel prizePoolRequestModel) {
        this.prizePoolRequestModel = prizePoolRequestModel;
    }

    public static MyApplication getInstance() {
        return (MyApplication) instance;
    }

    public FacebookLoginHandler getFacebookLoginHandler() {
        return facebookLoginHandler;
    }

    public GplusLoginHandler getGplusLoginHandler() {
        return gplusLoginHandler;
    }

    public void unAuthorizedResponse(AppBaseResponseModel modelResponse) {
        getUserPrefs().clearLoggedInUser();
        moveToLoginActivity(modelResponse);
    }

    public void moveToLoginActivity(AppBaseResponseModel modelResponse) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        if (modelResponse != null) {
            Bundle bundle = new Bundle();
            bundle.putString("unAuth", new Gson().toJson(modelResponse));
            intent.putExtras(bundle);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void moveToSplashActivity(AppBaseResponseModel modelResponse) {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        if (modelResponse != null) {
            Bundle bundle = new Bundle();
            bundle.putString("unAuth", new Gson().toJson(modelResponse));
            intent.putExtras(bundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (webRequest.getResponseCode() == 401) {
            unAuthorizedResponse(null);
        }
    }

    public void showOtp(String otp) {
        if (otp != null && otp.trim().length() > 0) {
            Toast.makeText(this, otp, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupTimer();
        facebookLoginHandler = new FacebookLoginHandler(this);
        gplusLoginHandler = new GplusLoginHandler(this);
        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);
        // This code requires one time to get Hash keys do comment and share key
        printLog("SMS Hash Key: " , appSignatureHashHelper.getAppSignatures().get(0));
    }

    public Object getLock() {
        return lock;
    }

    public List<GameTypeModel> getGameTypeModels() {
        return gameTypeModels;
    }

    public void addMatchTimerListener(MatchTimerListener matchTimerListener) {
        if (!this.matchTimerListeners.contains(matchTimerListener)) {
            this.matchTimerListeners.add(matchTimerListener);
        }
    }

    public void removeMatchTimerListener(MatchTimerListener matchTimerListener) {
        if (this.matchTimerListeners.contains(matchTimerListener)) {
            this.matchTimerListeners.remove(matchTimerListener);
        }
    }

    private void setupTimer() {
        matchTimerListeners.clear();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
//                if(true) return;
                synchronized (lock) {
                    serverdate++;
                    if (matchTimerListeners != null && matchTimerListeners.size() > 0) {
                        for (MatchTimerListener matchTimerListener : matchTimerListeners) {
                            matchTimerListener.onMatchTimeUpdate();
                        }
                    }
                    handler.postDelayed(runnable, 1000);
                }
            }
        };
    }

    public void startTimer() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000);
    }

    public void stopTimer() {
        handler.removeCallbacks(runnable);
    }

    public void printLog(String tag, String response) {
        if (isDebugBuild() && tag != null && response != null) {
            Log.e(tag, response);
        }
    }


}
