package com.app.ui.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.appbase.AppBaseActivity;
import com.app.model.webrequestmodel.ForgotPasswordRequestModel;
import com.app.model.webrequestmodel.ForgotPasswordVerifyRequestModel;
import com.app.model.webresponsemodel.ForgotPasswordResponseModel;
import com.app.ui.MyApplication;
import com.pickeleven.R;
import com.customviews.OtpView;
import com.fcm.NotificationPrefs;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.DeviceUtil;

import java.util.regex.Pattern;



public class ForgotPasswordVerifyActivity extends AppBaseActivity {

    TextView tv_mobile_number;
    OtpView otp_view;
    TextView tv_resend;

    EditText et_password;
    EditText et_cnfrm_password;
    TextView tv_verify;


    private String getMobileNumber() {
        return getIntent().getExtras().getString(MOBILENO);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_forgot_password_verify;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();

        tv_mobile_number = findViewById(R.id.tv_mobile_number);
        otp_view = findViewById(R.id.otp_view);
        tv_resend = findViewById(R.id.tv_resend);
        et_password = findViewById(R.id.et_password);
        et_cnfrm_password = findViewById(R.id.et_cnfrm_password);
        tv_verify = findViewById(R.id.tv_verify);

        tv_resend.setOnClickListener(this);
        tv_verify.setOnClickListener(this);
        startCounter(OTP_COUNTER);
        tv_mobile_number.setText(getMobileNumber());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_resend:
                callForgotPassword();
                break;
            case R.id.tv_verify:
                callForgotPasswordVerify();
                break;
        }
    }

    public void startCounter(final long millisecond) {
        long remainMin = (millisecond / (60 * 1000));
        long remainSec = (millisecond - (remainMin * 60 * 1000)) / 1000;
        tv_resend.setText(String.format("%02d:%02d", remainMin, remainSec));
        tv_resend.setOnClickListener(null);
        tv_resend.setAlpha(0.5f);

        CountDownTimer downTimer = new CountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (isFinishing()) {
                    return;
                }
                tv_resend.setOnClickListener(null);
                tv_resend.setAlpha(0.5f);
                long remainMin = (millisUntilFinished / (60 * 1000));
                long remainSec = (millisUntilFinished - (remainMin * 60 * 1000)) / 1000;
                tv_resend.setText(String.format("%02d:%02d", remainMin, remainSec));
            }

            @Override
            public void onFinish() {
                if (isFinishing()) {
                    return;
                }
                tv_resend.setText("Resend code");
                tv_resend.setOnClickListener(ForgotPasswordVerifyActivity.this);
                tv_resend.setAlpha(1.0f);
            }
        };
        downTimer.start();
    }

    private void callForgotPassword() {
        String mobileNumber = getMobileNumber();
        if (mobileNumber.isEmpty()) {
            showErrorMsg("Please enter mobile number/email.");
            return;
        }

        ForgotPasswordRequestModel requestModel = new ForgotPasswordRequestModel();
        requestModel.username = getMobileNumber();
        requestModel.atype = FORGOT_PASSWORD;
        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false);
        getWebRequestHelper().UserResendForgotPasswordOtpUrl(requestModel, this);
    }

    private void callForgotPasswordVerify() {
        String otp = otp_view.getOTP();
        if (otp == null || otp.trim().isEmpty() || otp.trim().length() != 6) {
            showErrorMsg("Please enter valid otp.");
            return;
        }
        String password = et_password.getText().toString();
        if (password.isEmpty()) {
            showErrorMsg("Please enter password.");
            return;
        }
        String confirmPassword = et_cnfrm_password.getText().toString();
        if (confirmPassword.isEmpty()) {
            showErrorMsg("Please re-enter password.");
            return;
        }

        if (!Pattern.matches("[^\\s]{6,15}", password)) {
            showErrorMsg("Password should be 6-15 characters long");
            return;
        }

        if (!password.equals(confirmPassword)) {
            showErrorMsg("Password is not match");
            return;
        }

        String mobileNumber = getMobileNumber();
        if (mobileNumber.isEmpty()) {
            showErrorMsg("Please enter mobile number/email.");
            return;
        }


        ForgotPasswordVerifyRequestModel requestModel = new ForgotPasswordVerifyRequestModel();
        requestModel.username = mobileNumber;
        requestModel.otp = otp;
        requestModel.password = password;
        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false);
        getWebRequestHelper().userForgotPasswordVerify(requestModel, this);
    }


    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_FORGOT_PASSWORD:
                handleForgotPasswordResponse(webRequest);
                return;
            case ID_FORGOT_PASSWORD_VERIFY:
                handleForgotPasswordVerifyResponse(webRequest);
                return;
        }
    }

    private void handleForgotPasswordResponse(WebRequest webRequest) {
        ForgotPasswordResponseModel responsePojo = webRequest.getResponsePojo(ForgotPasswordResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            if (isFinishing()) return;
            showCustomToast(responsePojo.getMsg());
            startCounter(OTP_COUNTER);
        } else {
            if (isFinishing()) return;
            String message = responsePojo.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void handleForgotPasswordVerifyResponse(WebRequest webRequest) {
        ForgotPasswordResponseModel responsePojo = webRequest.getResponsePojo(ForgotPasswordResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            if (isFinishing()) return;
            showCustomToast(responsePojo.getMsg());
            Bundle bundle = new Bundle();
            bundle.putString(PASSWORD, (String) webRequest.getExtraData(PASSWORD));
            Intent intent = new Intent();
            intent.putExtras(bundle);

            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else {
            if (isFinishing()) return;
            String message = responsePojo.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        startSMSListener();
    }

    @Override
    public void onOTPReceived(String message) {
        String substring = message.substring(4, 10);
        MyApplication.getInstance().printLog("parseOtp: ", substring);
        if (TextUtils.isDigitsOnly(substring)) {
//            otp_view.setOTP(substring);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getSmsReceiver() != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(getSmsReceiver());
        }
    }

}
