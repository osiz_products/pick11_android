package com.app.ui.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseActivity;
import com.app.model.webrequestmodel.ForgotPasswordRequestModel;
import com.app.model.webresponsemodel.ForgotPasswordResponseModel;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.medy.retrofitwrapper.WebRequest;
import com.permissions.PermissionHelperNew;
import com.utilities.DeviceUtil;


public class ForgotPasswordActivity extends AppBaseActivity implements
        PermissionHelperNew.OnSpecificPermissionGranted {

    public static final int EMAIL_MAX_LENGTH = 50;
    public static final int PHONE_MAX_LENGTH = 15;
    public static final InputFilter.LengthFilter EMAIL_FILTER_LENGTH = new InputFilter.LengthFilter(EMAIL_MAX_LENGTH);
    public static final InputFilter.LengthFilter PHONE_FILTER_LENGTH = new InputFilter.LengthFilter(PHONE_MAX_LENGTH);
    private int lastFilterLength = EMAIL_MAX_LENGTH;
    private ImageView iv_back;
    private TextView tv_header_name;
    EditText et_mobile_number;
    TextView tv_submit;
    private boolean isEmail = false;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {
        /*if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getTheme().applyStyle(R.style.BaseAppTheme, true);
        }*/
        return R.layout.activity_forgot_password;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(this);
        iv_back = findViewById(R.id.iv_back);
        tv_header_name = findViewById(R.id.tv_header_name);
        tv_header_name.setText("Change Password");
        et_mobile_number = findViewById(R.id.et_mobile_number);
        tv_submit = findViewById(R.id.tv_submit);
        iv_back.setOnClickListener(this);
        tv_submit.setOnClickListener(this);

        et_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (!data.isEmpty()) {
                    if (TextUtils.isDigitsOnly(data)) {
                        if (lastFilterLength != PHONE_MAX_LENGTH) {
                            lastFilterLength = PHONE_MAX_LENGTH;
                            isEmail = false;
                            et_mobile_number.setFilters(new InputFilter[]{PHONE_FILTER_LENGTH});
                        }
                    } else {
                        if (lastFilterLength != EMAIL_MAX_LENGTH) {
                            lastFilterLength = EMAIL_MAX_LENGTH;
                            isEmail = true;
                            et_mobile_number.setFilters(new InputFilter[]{EMAIL_FILTER_LENGTH});
                        }
                    }
                } else {
                    if (lastFilterLength != EMAIL_MAX_LENGTH) {
                        lastFilterLength = EMAIL_MAX_LENGTH;
                        isEmail = false;
                        et_mobile_number.setFilters(new InputFilter[]{EMAIL_FILTER_LENGTH});
                    }
                }
            }
        });

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_submit:
                if (getValidate().validEmailOrMobile(et_mobile_number)) {
                    if (isEmail) {
                        callForgotPassword();
                    } else {
                        callForgotPassword();
                    }
                }
                break;
        }
    }

    private void goToForward() {
        if (PermissionHelperNew.needSMSPermission(this, this)) {
            return;
        }
        callForgotPassword();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelperNew.onSpecificRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionGranted(boolean isGranted, boolean withNeverAsk, String permission, int requestCode) {
        if (requestCode == PermissionHelperNew.SMS_PERMISSION_REQUEST_CODE) {
            if (isGranted) {
                callForgotPassword();
            } else {
                if (withNeverAsk) {
                    PermissionHelperNew.showNeverAskAlert(this, true, requestCode);
                } else {
                    PermissionHelperNew.showSpecificDenyAlert(this, permission, requestCode, true);
                }
            }
        }
    }

    private void callForgotPassword() {
        String mobile_number = et_mobile_number.getText().toString().trim();

        ForgotPasswordRequestModel requestModel = new ForgotPasswordRequestModel();
        requestModel.username = mobile_number;
        requestModel.atype = FORGOT_PASSWORD;
        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false);
        getWebRequestHelper().userForgotPassword(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case ID_FORGOT_PASSWORD:
                handleForgotPasswordResponse(webRequest);
                break;
        }
    }

    private void handleForgotPasswordResponse(WebRequest webRequest) {
        ForgotPasswordResponseModel responsePojo = webRequest.getResponsePojo(ForgotPasswordResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
//            showCustomToast(responsePojo.getMsg());
            Bundle bundle = new Bundle();
            bundle.putString(MOBILENO, (String) webRequest.getExtraData(MOBILENO));

            goToForgotPasswordVerifyActivity(bundle);
        } else {
            if (isFinishing()) return;
            String msg = responsePojo.getMsg();
            if (isValidString(msg))
                showErrorMsg(msg);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OTP_VERIFY) {
            if (resultCode == RESULT_OK) {
                supportFinishAfterTransition();
            }
        }
    }

    private void goToForgotPasswordVerifyActivity(Bundle bundle) {
        Intent intent = new Intent(this, ForgotPasswordVerifyActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, REQUEST_OTP_VERIFY);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

}
