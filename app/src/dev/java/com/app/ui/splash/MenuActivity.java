package com.app.ui.splash;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.BuildConfig;
import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.ConfirmationDialog;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.main.navmenu.influencer.InfluencerActivity;
import com.app.ui.main.dashboard.myMatches.MyMatchesActivity;
import com.app.ui.main.navmenu.faq.FaqActivity;
import com.app.ui.main.navmenu.livescore.NewLiveScoreActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.app.ui.main.navmenu.profile.ProfileActivity;
import com.app.ui.main.navmenu.referFriend.ReferFriendActivity;
import com.app.ui.main.navmenu.scratch.ScratchCard;
import com.app.ui.main.navmenu.support.ContactUs;
import com.app.ui.main.navmenu.support.SupportActivity;
import com.app.ui.main.navmenu.verification.VerificationActivity;
import com.app.ui.main.notification.NotificationActivity;
import com.pickeleven.R;

public class MenuActivity extends AppBaseActivity {
    TextView tv_name;
    TextView tv_email;
    TextView tv_phone;
    TextView tv_Team_name;
    TextView txtChangeMode;
    TextView tv_city;

    ProgressBar pb_image;
    ImageView iv_profile_pic;
    ImageView iv_edit_pic;

    LinearLayout ll_home;
    LinearLayout ll_my_matches;
    LinearLayout ll_influencer;
    LinearLayout ll_my_profile;
    LinearLayout ll_my_account;
    LinearLayout ll_refer_friends;
    LinearLayout ll_verified;
    LinearLayout ll_notification;
    LinearLayout ll_live_score;
    LinearLayout ll_more;
    LinearLayout ll_tvChangeTheme;
    LinearLayout ll_contact;
    LinearLayout ll_scratch;
    LinearLayout ll_logout;
    LinearLayout ll_profile_pic_lay;
    TextView tv_app_version;
    String getInfluencer;
    String modeSelect;
    UserPrefs userPrefs;

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.BaseAppThemeDark);
        } else {
            setTheme(R.style.BaseAppTheme);
        }

        return R.layout.fragment_nav_menu;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        userPrefs = new UserPrefs(this);
        ll_profile_pic_lay = MenuActivity.this.findViewById(R.id.ll_profile_pic_lay);

 /*       if (getActivity() == null || getView() == null) return;
        drawerLayout = getActivity().findViewById(R.id.drawer_layout);
        if (drawerLayout == null) return;
        drawerLayout.removeDrawerListener(drawerListener);
        drawerLayout.addDrawerListener(drawerListener);*/

        tv_name = MenuActivity.this.findViewById(R.id.tv_name);
        tv_name.setOnClickListener(this);
        tv_email = MenuActivity.this.findViewById(R.id.tv_email);
        tv_email.setOnClickListener(this);
        tv_phone = MenuActivity.this.findViewById(R.id.tv_phone);
        tv_Team_name = MenuActivity.this.findViewById(R.id.tv_Team_name);
        txtChangeMode = MenuActivity.this.findViewById(R.id.txtChangeMode);
        tv_city = MenuActivity.this.findViewById(R.id.tv_city);

        pb_image = MenuActivity.this.findViewById(R.id.pb_image);
        iv_profile_pic = MenuActivity.this.findViewById(R.id.iv_profile_pic);
        iv_edit_pic = MenuActivity.this.findViewById(R.id.iv_edit_pic);

        ll_home = MenuActivity.this.findViewById(R.id.ll_home);
        ll_my_matches = MenuActivity.this.findViewById(R.id.ll_my_matches);
        ll_influencer = MenuActivity.this.findViewById(R.id.ll_influencer);
        //  ll_my_profile = getView().findViewById(R.id.ll_my_profile);
        ll_my_account = MenuActivity.this.findViewById(R.id.ll_my_account);
        ll_refer_friends = MenuActivity.this.findViewById(R.id.ll_refer_friends);
        ll_verified = MenuActivity.this.findViewById(R.id.ll_verified);
        ll_notification = MenuActivity.this.findViewById(R.id.ll_notification);
        ll_live_score = MenuActivity.this.findViewById(R.id.ll_live_score);
        ll_tvChangeTheme = MenuActivity.this.findViewById(R.id.ll_tvChangeTheme);
        ll_scratch = MenuActivity.this.findViewById(R.id.ll_scratch);

        ll_more = MenuActivity.this.findViewById(R.id.ll_more);
        ll_contact = MenuActivity.this.findViewById(R.id.ll_contact);
        ll_logout = MenuActivity.this.findViewById(R.id.ll_logout);

        String flavor = BuildConfig.FLAVOR;

        if (flavor.equalsIgnoreCase("pick11Dev")){
            updateViewVisibility(ll_notification, View.GONE);
        }

        ll_home.setOnClickListener(this);
        ll_my_matches.setOnClickListener(this);
        ll_influencer.setOnClickListener(this);
        // ll_my_profile.setOnClickListener(this);
        ll_my_account.setOnClickListener(this);
        ll_refer_friends.setOnClickListener(this);
        ll_verified.setOnClickListener(this);
        ll_notification.setOnClickListener(this);
        ll_live_score.setOnClickListener(this);
        ll_tvChangeTheme.setOnClickListener(this);
        ll_scratch.setOnClickListener(this);
        ll_more.setOnClickListener(this);
        ll_contact.setOnClickListener(this);
        ll_logout.setOnClickListener(this);

        tv_app_version = MenuActivity.this.findViewById(R.id.tv_app_version);
        iv_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ProfileActivity.class);
                /*if (bundle != null) {
                    intent.putExtras(bundle);
                }*/
                startActivity(intent);
            }
        });

        getVersionCode();
        setUserData();

        if(userPrefs!=null){
            if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){

                Log.e("SelectMode3",""+userPrefs.getStringKeyValuePrefs("modeSelect"));
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

                txtChangeMode.setText("Change to DarkMode");
                ll_profile_pic_lay.setBackgroundResource(R.mipmap.header);
            }
            else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                Log.e("SelectMode5",""+userPrefs.getStringKeyValuePrefs("modeSelect"));
                txtChangeMode.setText("Change to LightMode");
                ll_profile_pic_lay.setBackgroundResource(R.mipmap.header_black);
            }

        }else {
            Log.e("else","else");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

       /* if(modeSelect.equals("day")){

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }*/

        LinearLayout tvChangeTheme = findViewById(R.id.ll_tvChangeTheme);
        tvChangeTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

                     modeSelect = "day";
                    Log.e("SelectMode1",""+modeSelect);
                    userPrefs.appMode(modeSelect);

                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                     modeSelect = "night";
                    Log.e("SelectMode2",""+modeSelect);
                    userPrefs.appMode(modeSelect);
                }


              //  startActivity(new Intent(MenuActivity.this, DashboardActivity.class));

                Intent intent = new Intent(getApplication(), DashboardActivity.class);
                startActivity(intent);

                finish();
            }
        });
        }

    private void getVersionCode() {

       /* try {
            PackageInfo pInfo = getApplication().getPackageManager().getPackageInfo(getApplication().getPackageName(), 0);
            String version = pInfo.versionName;
            tv_app_version.setText("Version " + version);

            Log.e("GET_applicationVersion",""+version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    private void setUserData() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            tv_name.setText(userModel.getName());
            tv_email.setText(userModel.getEmail());
            tv_phone.setText((userModel.getPhone()));
            tv_Team_name.setText((userModel.getTeamname()));
            tv_city.setText((userModel.getCity()));

            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                ((AppBaseActivity) MenuActivity.this).loadImage(this, iv_profile_pic, pb_image, imageUrl,
                        R.drawable.no_image, 300);
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibitity(pb_image, View.INVISIBLE);
            }
        }
    }

    /*public void updateViewVisibitity (View view, int visibility) {
        if (MenuActivity.this == null) return;
        if (MenuActivity.this instanceof BaseActivity) {
            ((BaseActivity) MenuActivity.this).updateViewVisibility(view, visibility);
        }
    }*/

    public void goToMyAccountActivity(Bundle bundle) {
       // closeDrawer();

        finish();
        if (MenuActivity.this == null) return;
        Intent intent = new Intent(getApplication(), MyAccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToReferFriendActivity(Bundle bundle) {
      //  closeDrawer();

        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), ReferFriendActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToMyMatchesActivity(Bundle bundle) {
       // closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), MyMatchesActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToinfluencer(Bundle bundle) {

        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), InfluencerActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);

    }

    public void goToFaqActivity(Bundle bundle) {
       // closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), FaqActivity.class);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToSupportActivity(Bundle bundle) {
        //closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), SupportActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToVerificationActivity(Bundle bundle) {
        //closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), VerificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToNotificationActivity(Bundle bundle) {
       // closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), NotificationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void goToLiveScoreActivity(Bundle bundle) {
       // closeDrawer();
        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), NewLiveScoreActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToScratchCard(Bundle bundle) {

        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), ScratchCard.class);
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);

    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.ll_home:
              //  closeDrawer();
                overridePendingTransition( R.anim.enter_alpha, R.anim.exit_alpha );
                finish();
                break;

            case R.id.ll_influencer:
                goToinfluencer(null);
                break;

            case R.id.ll_my_matches:
                goToMyMatchesActivity(null);
                break;

          /*  case R.id.ll_my_profile:
                goToProfileActivity(null);
                break;*/

            case R.id.ll_my_account:
                goToMyAccountActivity(null);
                break;

            case R.id.ll_refer_friends:
                goToReferFriendActivity(null);
                break;
            case R.id.ll_more:
                goToFaqActivity(bundle);
              //  goToSupportActivity(bundle);
                break;

            case R.id.ll_contact:
                goToContactActivity(bundle);
                break;

            case R.id.ll_verified:
                goToVerificationActivity(null);
                break;

            case R.id.ll_notification:
                bundle.putBoolean(NOT_BACK_BTN, false);
                goToNotificationActivity(bundle);
                break;

            case R.id.ll_scratch:
                goToScratchCard(bundle);
                break;

            case R.id.ll_live_score:
                goToLiveScoreActivity(null);
                break;

            case R.id.ll_tvChangeTheme:
                goToChangeAppMode();
                break;

            case R.id.ll_logout:
                addConfirmationDialog();
                break;
        }
    }

    private void goToChangeAppMode() {
    }

    private void goToContactActivity(Bundle bundle) {

        finish();
        if (getApplication() == null) return;
        Intent intent = new Intent(getApplication(), ContactUs.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        if (getApplication() == null) return;
        this.overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);

    }


    private void addConfirmationDialog() {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, "Are you sure want to logout?");
        ConfirmationDialog dialog = ConfirmationDialog.getInstance(bundle);
        dialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE)
                    ((AppBaseActivity) MenuActivity.this).callLogout();
                dialog.dismiss();

            }
        });
        dialog.show(getSupportFragmentManager(), "Confirm Logout");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppBaseActivity.REQUEST_CASHOUT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                String message = data.getStringExtra("message");
                if (message != null && !message.trim().isEmpty()) {
                    showCustomToast(message);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserPrefs().removeListener(this);
        getUserPrefs().addListener(this);
       // if (drawerLayout == null) return;
        setupUserDetail();
    }

    @Override
    public void onPause() {
        super.onPause();
        getUserPrefs().removeListener(this);
    }

    @Override
    public void loggedInUserUpdate(UserModel userModel) {
        super.loggedInUserUpdate(userModel);
        setupUserDetail();
    }

    private void setupUserDetail() {
        UserModel userModel = getUserModel();
        if (userModel != null) {
            tv_name.setText(userModel.getName());
            getInfluencer = userModel.getIsInfluencer();

            if (getInfluencer!=null){

                if (getInfluencer.equals("1")){

                    ll_influencer.setVisibility(View.VISIBLE);
                    ll_contact.setVisibility(View.GONE);

                }else {
                    ll_influencer.setVisibility(View.GONE);
                    ll_contact.setVisibility(View.VISIBLE);
                }
            }

            String imageUrl = userModel.getProfilepic();
            if (isValidString(imageUrl)) {
                if (getApplication() != null) {
                    ((AppBaseActivity) MenuActivity.this).loadImage(this, iv_profile_pic, pb_image, imageUrl,
                            R.drawable.no_image, 300);
                }
            } else {
                iv_profile_pic.setImageResource(R.drawable.no_image);
                updateViewVisibitity(pb_image, View.INVISIBLE);
            }

        }
    }
}
