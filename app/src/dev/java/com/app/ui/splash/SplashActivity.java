package com.app.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.app.appbase.AppBaseActivity;
import com.app.fcm.AppNotificationMessagingService;
import com.app.preferences.UserPrefs;
import com.app.ui.login.LoginActivity;
import com.app.ui.main.dashboard.DashboardActivity;
//import com.pickeleven.BuildConfig;
import com.pickeleven.R;
import com.permissions.PermissionHelperNew;

public class SplashActivity extends AppBaseActivity implements PermissionHelperNew.OnSpecificPermissionGranted {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final long SPLASH_TIME_MS = 2000;
    private Handler handler = new Handler();
    //private LinearLayout ll_ifsc_logo;

    private boolean callOnResume = true;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            UserPrefs userPrefs = getUserPrefs();
            if (userPrefs.getLoggedInUser() == null) {
                goToCheckUserActivity(getIntent().getExtras());
                return;
            }
            goToDashboardActivity(getIntent().getExtras());
        }
    };

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_splash_screen;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
       /* ll_ifsc_logo = findViewById(R.id.ll_ifsc_logo);
        updateViewVisibility(ll_ifsc_logo, View.GONE);
        String flavor = BuildConfig.FLAVOR;
        if (flavor.equalsIgnoreCase("pick11Dev")){
            updateViewVisibility(ll_ifsc_logo, View.VISIBLE);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppNotificationMessagingService.generateLatestToken();
        if (callOnResume) {
            goToForward();
        } else {
            callOnResume = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length > 0) {
            callOnResume = false;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelperNew.onSpecificRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionGranted(boolean isGranted, boolean withNeverAsk, String permission, int requestCode) {
        if (requestCode == PermissionHelperNew.STORAGE_PERMISSION_REQUEST_CODE) {
            if (isGranted) {
                goToForward();
            } else {
                if (withNeverAsk) {
                    PermissionHelperNew.showNeverAskAlert(this, true, requestCode);
                } else {
                    PermissionHelperNew.showSpecificDenyAlert(this, permission, requestCode, true);
                }
            }
        }
    }

    private void goToForward() {

        if (isPlayServiceUpdated()) {
            if (PermissionHelperNew.needStoragePermission(this, this)) return;
            handler.postDelayed(runnable, SPLASH_TIME_MS);
        }else {
            if (!isPlayServiceErrorUserResolvable()) {
                showCustomToast("This device is not supported.");
                finish();
            }
        }
    }

    private void goToCheckUserActivity(Bundle bundle) {
        Intent intent = new Intent(this, LoginActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent, null);
        supportFinishAfterTransition();
    }

    private void goToDashboardActivity(Bundle bundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        supportFinishAfterTransition();
    }

}
