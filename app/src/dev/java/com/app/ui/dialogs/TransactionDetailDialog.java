package com.app.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.PaymentModel;
import com.app.preferences.UserPrefs;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.gson.Gson;


public class TransactionDetailDialog extends AppBaseDialogFragment {

    TextView tv_message;
    TextView tv_name;
    TextView tv_transaction_id;
    TextView tv_order_id;
    TextView tv_status;
    TextView tv_payment_mode;
    TextView tv_amount;
    TextView tv_date;
    TextView tv_cancel;
    TextView tv_ok;
    DialogInterface.OnClickListener onClickListener;
    UserPrefs userPrefs;

    public static TransactionDetailDialog getInstance(Bundle bundle) {
        TransactionDetailDialog messageDialog = new TransactionDetailDialog();
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.dialog_transtation_detail;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    private String getMessage() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(MESSAGE);
        }
        return message == null ? "" : message;
    }

    private PaymentModel getPaymentModel() {
        if (getArguments() != null) {
            String data = getArguments().getString(DATA);
            if (isValidString(data))
                return new Gson().fromJson(data, PaymentModel.class);
        }
        return null;
    }

    private String getPositiveBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(POS_BTN);
        }
        return message == null ? "YES" : message;
    }


    private String getNegativeBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(NEG_BTN);
        }
        return message == null ? "CANCEL" : message;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        tv_message = getView().findViewById(R.id.tv_message);
        tv_message.setText(getMessage());

        tv_name = getView().findViewById(R.id.tv_name);
        tv_transaction_id = getView().findViewById(R.id.tv_transaction_id);
        tv_order_id = getView().findViewById(R.id.tv_order_id);
        tv_status = getView().findViewById(R.id.tv_status);
        tv_payment_mode = getView().findViewById(R.id.tv_payment_mode);
        tv_amount = getView().findViewById(R.id.tv_amount);
        tv_date = getView().findViewById(R.id.tv_date);

        tv_cancel = getView().findViewById(R.id.tv_cancel);
        tv_cancel.setText(getNegativeBtnText());
        tv_cancel.setOnClickListener(this);
        updateViewVisibitity(tv_cancel, View.GONE);

        tv_ok = getView().findViewById(R.id.tv_ok);
        tv_ok.setText(getPositiveBtnText());
        tv_ok.setOnClickListener(this);

        setUpdate();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    private void setUpdate() {
        PaymentModel paymentModel = getPaymentModel();
        if (paymentModel == null) return;
        tv_name.setText(getUserModel().getUsername());
        tv_transaction_id.setText(paymentModel.getTXNID());
        tv_order_id.setText(paymentModel.getORDERID());
        tv_status.setText(paymentModel.getSTATUS());
        tv_payment_mode.setText(paymentModel.getPAYMENTMODE());
        tv_amount.setText(paymentModel.getTXNAMOUNT());
        tv_date.setText(paymentModel.getTXNDATE());
    }

    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            onClickListener.onClick(this.getDialog(),
                    v.getId() == R.id.tv_ok ?
                            DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE);
        } else {
            dismiss();
        }
    }
}
