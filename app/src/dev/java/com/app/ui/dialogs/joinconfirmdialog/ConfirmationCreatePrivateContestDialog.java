package com.app.ui.dialogs.joinconfirmdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.MatchModel;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webresponsemodel.PreJoinResponseModel;
import com.app.ui.MyApplication;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.DeviceScreenUtil;


public class ConfirmationCreatePrivateContestDialog extends AppBaseDialogFragment {

    CardView cv_data;
    ImageView iv_close;
    ImageView iv_info;
    TextView tv_remain_balance;
    TextView tv_entry;
    TextView tv_cash_bonus;
    TextView tv_pay;
    TextView tv_join_confirmation_text;
    TextView tv_join_contest;
    RelativeLayout rl_dialog_wait;
    TextView tv_loader_msg;
    PreJoinResponseModel.PreJoinedModel preJoinedModel;

    ConfirmationJoinContestListener confirmationJoinContestListener;

    public static ConfirmationCreatePrivateContestDialog getInstance(Bundle bundle) {
        ConfirmationCreatePrivateContestDialog messageDialog = new ConfirmationCreatePrivateContestDialog();
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public MatchModel getMatchModel() {
        return MyApplication.getInstance().getSelectedMatch();
    }

    public JoinContestRequestModel getCreatePrivateContestRequestModel() {
        Bundle extras = getArguments();
        String data = (extras == null ? null : extras.getString(DATA));
        if (isValidString(data)) {
            try {
                return new Gson().fromJson(data, JoinContestRequestModel.class);
            } catch (JsonSyntaxException e) {

            }
        }
        return null;
    }

    public void setConfirmationJoinContestListener(ConfirmationJoinContestListener confirmationJoinContestListener) {
        this.confirmationJoinContestListener = confirmationJoinContestListener;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_confirm_join_contest;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = DeviceScreenUtil.getInstance().getWidth(0.90f);
        wlmp.dimAmount = 0.8f;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        cv_data = getView().findViewById(R.id.cv_data);
        updateViewVisibitity(cv_data, View.INVISIBLE);
        iv_close = getView().findViewById(R.id.iv_close);
        iv_info = getView().findViewById(R.id.iv_info);
        tv_remain_balance = getView().findViewById(R.id.tv_remain_balance);
        tv_entry = getView().findViewById(R.id.tv_entry);
        tv_cash_bonus = getView().findViewById(R.id.tv_cash_bonus);
        tv_pay = getView().findViewById(R.id.tv_pay);
        tv_join_confirmation_text = getView().findViewById(R.id.tv_join_confirmation_text);
        tv_join_contest = getView().findViewById(R.id.tv_join_contest);
        rl_dialog_wait = getView().findViewById(R.id.rl_dialog_wait);
        tv_loader_msg = getView().findViewById(R.id.tv_loader_msg);
        tv_loader_msg.setText("");
        updateViewVisibitity(rl_dialog_wait, View.VISIBLE);
        iv_close.setOnClickListener(this);
        iv_info.setOnClickListener(this);
        tv_join_contest.setOnClickListener(this);
        callPreJoinContest();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.iv_info:

                break;
            case R.id.tv_join_contest:
                if (confirmationJoinContestListener != null) {
                    confirmationJoinContestListener.onProceed();
                }
                break;
        }
    }

    private void setupData() {
        if (preJoinedModel != null) {
            if (preJoinedModel.getFees() > 0) {
                UserModel userModel = getUserModel();
                if (preJoinedModel.getWallet() != null && userModel != null) {
                    userModel.setWalletModel(preJoinedModel.getWallet());
                    updateUserInPrefs();
                }
                confirmationJoinContestListener.lowBalance(preJoinedModel);
            } else {
                tv_remain_balance.setText("Unutilized Balance + Winnings = " + currency_symbol + preJoinedModel.getRemainBalancetext());
                tv_entry.setText(currency_symbol + preJoinedModel.getEnteryFeestext());
                tv_cash_bonus.setText("- " + currency_symbol + preJoinedModel.getUsedBonusText());
//                tv_pay.setText(currency_symbol + preJoinedModel.getFeesText());
                tv_pay.setText(currency_symbol + preJoinedModel.getPayBalanceWalletText());
                updateViewVisibitity(cv_data, View.VISIBLE);
                updateViewVisibitity(rl_dialog_wait, View.GONE);

                String string = getResources().getString(R.string.join_confirmation_text);
                String format = String.format(string, ""+preJoinedModel.getBonsdeduction(), getResources().getString(R.string.app_name));
                tv_join_confirmation_text.setText(format);
            }
        } else {
            dismiss();
        }
    }

    public interface ConfirmationJoinContestListener {
        void onProceed();

        void lowBalance(PreJoinResponseModel.PreJoinedModel preJoinedModel);
    }

    private void callPreJoinContest() {
        if (getCreatePrivateContestRequestModel() != null) {
            String matchContestId = getCreatePrivateContestRequestModel().matchid;
            if (!isValidString(matchContestId)) {
                dismiss();
                return;
            }
            getWebRequestHelper().createPrivateContest(getCreatePrivateContestRequestModel(), this);
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        super.onWebRequestResponse(webRequest);
        if (webRequest.getResponseCode() == 401) {
            dismiss();
            return;
        }
        switch (webRequest.getWebRequestId()) {
            case ID_CREATE_PRIVATE_CONTEST:
                handleCustomerPreJoinContestResponse(webRequest);
                break;
        }

    }

    private void handleCustomerPreJoinContestResponse(WebRequest webRequest) {
        JoinContestRequestModel model = webRequest.getExtraData(DATA);
        PreJoinResponseModel responsePojo = webRequest.getResponsePojo(PreJoinResponseModel.class);
        if (responsePojo == null) return;
        if (!responsePojo.isError()) {
            if (isValidActivity() && getDialog().isShowing()) {
                PreJoinResponseModel.PreJoinedModel data = responsePojo.getData();
//                data.setEntry_fees(Float.parseFloat(model.joinfees));
                preJoinedModel = data;
                setupData();
            }
        } else {
            if (isValidActivity() && getDialog().isShowing()) {
                showErrorMsg(responsePojo.getMsg());
                dismiss();
            }
        }

    }
}
