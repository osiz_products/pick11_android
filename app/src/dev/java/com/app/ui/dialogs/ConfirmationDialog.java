package com.app.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.appbase.AppBaseDialogFragment;
import com.base.BaseFragment;
import com.pickeleven.R;



public class ConfirmationDialog extends AppBaseDialogFragment {

    TextView tv_message;
    TextView tv_cancel;
    TextView tv_ok;
    DialogInterface.OnClickListener onClickListener;

    public static ConfirmationDialog getInstance(Bundle bundle) {
        ConfirmationDialog messageDialog = new ConfirmationDialog();
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_confirmation;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.dimAmount = 0.8f;
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    private String getMessage() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(MESSAGE);
        }
        return message == null ? "" : message;
    }

    private String getPositiveBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(POS_BTN);
        }
        return message == null ? "YES" : message;
    }


    private String getNegativeBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(NEG_BTN);
        }
        return message == null ? "CANCEL" : message;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        tv_message = getView().findViewById(R.id.tv_message);

        tv_message.setText(getMessage());


        tv_cancel = getView().findViewById(R.id.tv_cancel);
        tv_cancel.setText(getNegativeBtnText());
        tv_cancel.setOnClickListener(this);

        tv_ok = getView().findViewById(R.id.tv_ok);
        tv_ok.setText(getPositiveBtnText());
        tv_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            onClickListener.onClick(this.getDialog(),
                    v.getId() == R.id.tv_ok ?
                            DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE);
        } else {
            dismiss();
        }
    }
}
