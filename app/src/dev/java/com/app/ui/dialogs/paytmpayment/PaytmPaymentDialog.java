package com.app.ui.dialogs.paytmpayment;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.DepositAmountRequestModel;
import com.app.model.webresponsemodel.PaymentResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.main.allTransaction.AllTransactionsActivity;
import com.app.ui.main.navmenu.myaccount.MyAccountActivity;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.rest.WebServices;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class PaytmPaymentDialog extends AppBaseDialogFragment {

    private static final String TAG = PaytmPaymentDialog.class.getSimpleName();
    private String CALLBACK_URL = "";
    private WebView web_view;
    private ImageView iv_back;
    private String walletRechargeAmount = "0";
    private String walletRechargeResponse;
    private PaymentSuccessListener paymentSuccessListener;
    private String promoCode = "";
    private String webUrl = "";

    public void setPaymentSuccessListener(PaymentSuccessListener paymentSuccessListener) {
        this.paymentSuccessListener = paymentSuccessListener;
    }

    public void setWalletRechargeAmount(String walletRechargeAmount) {
        this.walletRechargeAmount = walletRechargeAmount;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_paytm_payment;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return -1;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        web_view = getView().findViewById(R.id.web_view);
        iv_back = getView().findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               getActivity().finish();
            }
        });
        setupWebViewsettings();
        callWalletRecharge();
//        startPayment();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.dimAmount = 0.0f;

//        dialog.setTitle(null);
        setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
    }

    private void setupWebViewsettings() {

        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.getSettings().setSupportMultipleWindows(true);
        web_view.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        web_view.getSettings().setDomStorageEnabled(true);
        web_view.addJavascriptInterface(new ResponseHandleInterface(), ResponseHandleInterface.NAME);

        web_view.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                web_view.loadUrl(webUrl);
                return true;
            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();

                if(url.equals("https://api.pick11.net/public/index.php/CashfreeResponseMobile")){

                    Intent intent = new Intent(getActivity(), MyAccountActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
                    getActivity().finish();

                }else {

                    Log.e("check","check");

                }

                if (url.equals(WebServices.WalletAddBalanceUrl())) {
                    return generateResponseFromString();
                }
                return super.shouldInterceptRequest(view, request);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                if (errorCode == -2) {
                    showErrorMsg("Please check your internet connection.");
                    dismiss();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                MyApplication.getInstance().printLog(TAG, "onPageFinished: " + url +
                        "  CALLBACK_URL =  " + CALLBACK_URL);
                Log.e("call_respons","11"+url);

               /* if(url.equals("https://api.pick11.net/public/index.php/CashfreeResponseMobile")){

                    Intent intent = new Intent(getActivity(), AllTransactionsActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
                    getActivity().finish();

                }else {

                    Log.e("check","check");

                }*/

                if (url.equals(url)) {
                    view.loadUrl("javascript:window." + ResponseHandleInterface.NAME +
                            ".handleResponse1(document.getElementById('callbackurl').value);");

                } else if (url.equals(CALLBACK_URL)) {
                    view.loadUrl("javascript:window." + ResponseHandleInterface.NAME +
                            ".handleResponse(document.getElementsByTagName('body')[0].innerText);");
                }
            }
        });


    }



    private WebResourceResponse generateResponseFromString() {

        InputStream inputStream = new ByteArrayInputStream(walletRechargeResponse.getBytes(Charset.forName("UTF-8")));

        return new WebResourceResponse(
                "text/html",
                "utf-8",
                inputStream);
    }


    private void startPayment() {
        UserModel userModel = getUserModel();
        if (!isValidObject(userModel)) return;
      //  String url = String.format(WebServices.WalletAddBalanceUrl(), walletRechargeAmount, userModel.getId());
        String url = webUrl;
        Log.e(TAG, "startPayment: " + url);
        web_view.loadUrl(webUrl);

        Log.e("CallPaymentURL",""+url);
    }

    public void setPromoCode(String pcode) {
        promoCode = pcode;
    }

    class ResponseHandleInterface {

        public static final String NAME = "RESPONSE_HANDLER";

        public ResponseHandleInterface() {
        }

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void handleResponse(String response) {
            MyApplication.getInstance().printLog(TAG, response);
            if (isValidString(response)) {

                Log.e("SelectRespons",""+response);
                PaymentResponseModel responseModel = new Gson().fromJson(response, PaymentResponseModel.class);
                DepositAmountRequestModel requestModel = new DepositAmountRequestModel();
                webUrl = requestModel.data;

                if (paymentSuccessListener != null)
                    paymentSuccessListener.onPaymentSuccess(responseModel);
            } else {
                dismiss();
            }
        }

        @JavascriptInterface
        public void handleResponse1(String response) {
            CALLBACK_URL = response;
            MyApplication.getInstance().printLog(TAG, "CALLBACK_URL=" + CALLBACK_URL);
        }
    }


    private void callWalletRecharge() {
        DepositAmountRequestModel requestModel = new DepositAmountRequestModel();
        requestModel.amount = walletRechargeAmount;
        requestModel.pcode = promoCode;
        displayProgressBar(false);
        getWebRequestHelper().walletRecharge(requestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        if (webRequest.getResponseCode() == 401 || webRequest.getResponseCode() == 412) return;
        switch (webRequest.getWebRequestId()) {
            case ID_ADD_BALANCE:
                handleWalletRechargeResponse(webRequest);
                break;
        }

    }

    private void handleWalletRechargeResponse(WebRequest webRequest) {
        DepositAmountRequestModel responseModel = webRequest.getResponsePojo(DepositAmountRequestModel.class);
        webUrl = responseModel.data;

        walletRechargeResponse = webRequest.getResponseString();
        if (isValidString(walletRechargeResponse)) {
          //  getcallBackFromHtml();
            Log.e("GetCallWallet",""+walletRechargeResponse);
            startPayment();
        }
    }

    private void getcallBackFromHtml() {
        Document document = Jsoup.parse(walletRechargeResponse);
        if (document != null) {
            Element element = document.getElementById("callbackurl");
            String val = element.val();
            if (isValidString(val))
                CALLBACK_URL = val;
            MyApplication.getInstance().printLog(TAG, "getcallBackFromHtml = " + val);
        }
    }

    public interface PaymentSuccessListener {
        void onPaymentSuccess(PaymentResponseModel responseModel);
    }
}
