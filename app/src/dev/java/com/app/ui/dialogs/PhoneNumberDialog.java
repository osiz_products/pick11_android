package com.app.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseDialogFragment;
import com.base.BaseFragment;
import com.pickeleven.R;



public class PhoneNumberDialog extends AppBaseDialogFragment {

    EditText et_mobile_number;
    TextView tv_ok;
    OnClickListener onClickListener;

    public static PhoneNumberDialog getInstance(Bundle bundle) {
        PhoneNumberDialog messageDialog = new PhoneNumberDialog();
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.dialog_phone_number;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    private String getMessage() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(MESSAGE);
        }
        return message == null ? "" : message;
    }

    private String getPositiveBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(POS_BTN);
        }
        return message == null ? "YES" : message;
    }


    private String getNegativeBtnText() {
        String message = null;
        if (getArguments() != null) {
            message = getArguments().getString(NEG_BTN);
        }
        return message == null ? "CANCEL" : message;
    }


    @Override
    public void initializeComponent() {
        super.initializeComponent();
        et_mobile_number = getView().findViewById(R.id.et_mobile_number);

//        et_mobile_number.setText(getMessage());


//        tv_cancel = getView().findViewById(R.id.tv_cancel);
//        tv_cancel.setText(getNegativeBtnText());
//        tv_cancel.setOnClickListener(this);

        tv_ok = getView().findViewById(R.id.tv_ok);
//        tv_ok.setText(getPositiveBtnText());
        tv_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (((AppBaseActivity) getActivity()).getValidate().validMobileNumber(et_mobile_number)) {
            String mobile_number = et_mobile_number.getText().toString().trim();
            if (onClickListener != null) {
                if (!mobile_number.isEmpty()) {
                    onClickListener.onClick(this.getDialog(),
                            v.getId() == R.id.tv_ok ?
                                    DialogInterface.BUTTON_POSITIVE : DialogInterface.BUTTON_NEGATIVE, mobile_number);
                }else {
                    showErrorMsg("Please enter mobile number.");
                }
            } else {
                dismiss();
            }
        }
    }


    public interface OnClickListener {
        void onClick(DialogInterface dialog, int which, String mobile_number);
    }
}
