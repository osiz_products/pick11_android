package com.app.ui.dialogs.state.adapter;

import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.CountryModel;
import com.base.BaseRecycleAdapter;
import com.pickeleven.R;

import java.util.List;

public class SelectCountryAdapter extends AppBaseRecycleAdapter{
    private List<CountryModel> dataList;

    public SelectCountryAdapter(List<CountryModel> data) {
        isForDesign = false;
        this.dataList = data;
    }

    @Override
    public BaseRecycleAdapter.BaseViewHolder getViewHolder() {
        return new SelectCountryAdapter.ViewHolder(inflateLayout(R.layout.item_data_adapter));
    }

    @Override
    public int getDataCount() {
        return dataList == null ? 0 : dataList.size();
    }


    private class ViewHolder extends BaseRecycleAdapter.BaseViewHolder {
        private TextView tv_item;
        private View ll_view;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_item = itemView.findViewById(R.id.tv_item);
            ll_view = itemView.findViewById(R.id.ll_view);

        }

        @Override
        public String setData(int position) {
            if (dataList == null) return null;
            CountryModel countryModel = dataList.get(position);
            tv_item.setText(countryModel.getName());

            return null;
        }

    }
}
