package com.app.ui.dialogs.winnerbreakupdialog.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseRecycleAdapter;
import com.app.model.PriceBreakUpModel;
import com.pickeleven.R;

import java.util.List;


public class WinnerBreakupAdapter extends AppBaseRecycleAdapter {

    Context context;
    List<PriceBreakUpModel> list;

    public WinnerBreakupAdapter(Context context, List<PriceBreakUpModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return new ViewHolder(inflateLayout(R.layout.item_winner_breakup_adapter));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : list.size();
    }

    public View getView(int position) {
        ViewHolder viewHolder = (ViewHolder) getRecyclerView().findViewHolderForAdapterPosition(position);
        if (viewHolder != null) {
            return viewHolder.itemView;
        }
        return null;

    }

    private class ViewHolder extends BaseViewHolder {

        TextView tv_rank;
        TextView tv_rank_price;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_rank = itemView.findViewById(R.id.tv_rank);
            tv_rank_price = itemView.findViewById(R.id.tv_rank_price);

        }

        @Override
        public String setData(int position) {
            PriceBreakUpModel priceBreakUpModel = list.get(position);

            tv_rank.setText(Html.fromHtml(getRank(position, priceBreakUpModel)), TextView.BufferType.SPANNABLE);
            tv_rank_price.setText(((AppBaseActivity) context).currency_symbol + priceBreakUpModel.getAmountText());


            return null;
        }

        private String getRank(int position, PriceBreakUpModel priceBreakUpModel) {
            String rank = "";
            if ((priceBreakUpModel.getPmax() - priceBreakUpModel.getPmin()) < 1) {
                rank = "Rank " + priceBreakUpModel.getPmin();
            } else {
                rank = "Rank " + priceBreakUpModel.getPmin() + "-" + priceBreakUpModel.getPmax();
//                if ((priceBreakUpModel.getPmax() - priceBreakUpModel.getPmin()) < 1) {
//                    rank = "Rank <font color='" + getContext().getResources().getColor(R.color.colorGray) + "'>#</font>"
//                            + priceBreakUpModel.getPmin();
//                } else {
//                    rank = "Rank <font color='" + getContext().getResources().getColor(R.color.colorGray) + "'>#</font>"
//                            + priceBreakUpModel.getPmin() + "-" + priceBreakUpModel.getPmax();
//                }
            }
            return rank;
        }

    }
}
