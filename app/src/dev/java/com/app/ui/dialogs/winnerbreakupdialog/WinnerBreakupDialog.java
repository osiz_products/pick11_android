package com.app.ui.dialogs.winnerbreakupdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.appbase.AppBaseDialogFragment;
import com.app.model.ContestModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.winnerbreakupdialog.adapter.WinnerBreakupAdapter;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.utilities.DeviceScreenUtil;

public class WinnerBreakupDialog extends AppBaseDialogFragment {
    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout bottom_sheet;

    CardView cv_data;
    LinearLayout ll_data_lay;
    ImageView iv_close;
    TextView tv_price_pool;
    RecyclerView recycler_view;
    TextView tv_bottom_message;

    WinnerBreakupAdapter adapter;
    ContestModel contestModel;
    UserPrefs userPrefs;

    public static WinnerBreakupDialog getInstance(Bundle bundle) {
        WinnerBreakupDialog winnerBreakupDialog = new WinnerBreakupDialog();
        winnerBreakupDialog.setArguments(bundle);
        return winnerBreakupDialog;
    }

    private ContestModel getContestModel() {
        Bundle extras = getArguments();
        if (extras != null) {
            String string = extras.getString(DATA);
            if (isValidString(string))
                return new Gson().fromJson(string, ContestModel.class);
        }

        return null;
    }


    @Override
    public int getLayoutResourceId() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.dialog_winner_breakup;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.dimAmount = 0.8f;

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        bottom_sheet = getView().findViewById(R.id.bottom_sheet);
        ll_data_lay = getView().findViewById(R.id.ll_data_lay);
        cv_data = getView().findViewById(R.id.cv_data);
        updateViewVisibitity(cv_data, View.INVISIBLE);
        iv_close = getView().findViewById(R.id.iv_close);
        tv_price_pool = getView().findViewById(R.id.tv_price_pool);
        recycler_view = getView().findViewById(R.id.recycler_view);
        tv_bottom_message = getView().findViewById(R.id.tv_bottom_message);

        iv_close.setOnClickListener(this);
        if (getContestModel() != null) {
            tv_price_pool.setText(((AppBaseActivity) getActivity()).currency_symbol + getContestModel().digitToText());
            initializeBottomSheet();

            updateBottomMessage();
            initializeRecyclerView();
            updateViewVisibitity(cv_data, View.VISIBLE);
            setupPeakHeightBottomSheet();
        }

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void updateBottomMessage() {
        tv_bottom_message.setText(getString(R.string.price_pool_breackup_message));
    }

    private void initializeBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismiss();
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public void setupPeakHeightBottomSheet() {
        ll_data_lay.post(new Runnable() {
            @Override
            public void run() {
                int defaultPeakHeight = ll_data_lay.getHeight();
                int defaultBottomHeight = tv_bottom_message.getHeight();
                int itemsHeight = 0;
                if (adapter.getDataCount() > 0 && adapter.getView(0) != null) {
                    int item_height = adapter.getView(0).getHeight();
                    itemsHeight = adapter.getDataCount() * item_height;
                }
                int finalHeight = defaultPeakHeight + itemsHeight + defaultBottomHeight;
                int maxHeight = DeviceScreenUtil.getInstance().getHeight(0.60f);
                bottomSheetBehavior.setPeekHeight(Math.min(finalHeight, maxHeight));
            }
        });

    }


    private void initializeRecyclerView() {
        adapter = new WinnerBreakupAdapter(getActivity(), getContestModel().getBrkprize());
        recycler_view.setLayoutManager(getVerticalLinearLayoutManager());
        recycler_view.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
        }
    }
}
