package com.app.ui.dialogs.state;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseDialogFragment;
import com.app.model.CountryModel;
import com.app.preferences.UserPrefs;
import com.app.ui.dialogs.state.adapter.SelectCountryAdapter;
import com.base.BaseFragment;
import com.pickeleven.R;
import com.utilities.ItemClickSupport;

import java.util.List;

public class SelectCountryDialog extends AppBaseDialogFragment {
    TextView tv_title;
    private String title;
    private RecyclerView recycler_view;
    private SelectCountryDialog.OnItemSelectedListener onItemSelectedListeners;
    private List<CountryModel> dataList;
    UserPrefs userPrefs;

    public static SelectCountryDialog getInstance(Bundle bundle) {
        SelectCountryDialog messageDialog = new SelectCountryDialog();
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnItemSelectedListeners(SelectCountryDialog.OnItemSelectedListener onItemSelectedListeners) {
        this.onItemSelectedListeners = onItemSelectedListeners;
    }

    @Override
    public int getLayoutResourceId() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            getDialog().getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }
        return R.layout.dialog_select_state;
    }

    @Override
    public int getFragmentContainerResourceId(BaseFragment baseFragment) {
        return 0;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        userPrefs = new UserPrefs(getActivity());
        recycler_view = getView().findViewById(R.id.recycler_view);
        tv_title = getView().findViewById(R.id.tv_title);
        if (title != null)
            tv_title.setText(title);
        if (dataList != null && dataList.size() > 0)
            initializeRecyclerView();

        if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private void initializeRecyclerView() {
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.setAdapter(new SelectCountryAdapter(dataList));
        ItemClickSupport.addTo(recycler_view).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                onItemSelectedListeners.onItemSelectedListener(position);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    public void setDataList(List<CountryModel> dataList) {
        this.dataList = dataList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public interface OnItemSelectedListener {
        void onItemSelectedListener(int position);
    }
}
