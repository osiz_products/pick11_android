package com.app.ui.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.LoginRequestModel;
import com.app.model.webrequestmodel.SocialRequestModel;
import com.app.model.webresponsemodel.EmptyResponseModel;
import com.app.model.webresponsemodel.LoginResponse2Model;
import com.app.model.webresponsemodel.LoginResponseModel;
import com.app.model.webresponsemodel.RegisterResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.dialogs.PhoneNumberDialog;
import com.app.ui.forgotpassword.ForgotPasswordActivity;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.register.RegisterActivity;
import com.app.ui.register.RegisterVerifyActivity;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.sociallogin.FacebookLoginHandler;
import com.sociallogin.GplusLoginHandler;
import com.sociallogin.SocialData;
import com.sociallogin.SocialLoginListener;
import com.sociallogin.SocialPermissionErrorDialog;
import com.utilities.DeviceUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class LoginActivity extends AppBaseActivity {

    public static final int EMAIL_MAX_LENGTH = 50;
    public static final int PHONE_MAX_LENGTH = 15;
    public static final InputFilter.LengthFilter EMAIL_FILTER_LENGTH = new InputFilter.LengthFilter(EMAIL_MAX_LENGTH);
    public static final InputFilter.LengthFilter PHONE_FILTER_LENGTH = new InputFilter.LengthFilter(PHONE_MAX_LENGTH);
    private int lastFilterLength = EMAIL_MAX_LENGTH;
    private LinearLayout ll_data_lay;
    private EditText et_user_name;
    private TextView tv_login;
    private TextView tv_forgot_password;
    private TextView tv_signup;
    private LinearLayout cv_facebook;
    private LinearLayout cv_google;
    private boolean isEmail = true;

    SocialLoginListener fbLoginListener = new SocialLoginListener() {
        @Override
        public void socialLoginSuccess(SocialData socialData) {
            callSocialLogin(socialData);
        }

        @Override
        public void fbLoginPermissionError() {
            showFbLoginErrorDialog();
        }
    };

    SocialLoginListener gplusLoginListener = new SocialLoginListener() {
        @Override
        public void socialLoginSuccess(SocialData socialData) {
            callSocialLogin(socialData);
        }

        @Override
        public void fbLoginPermissionError() {

        }
    };

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        socialLogout();

        ll_data_lay = findViewById(R.id.ll_data_lay);
        et_user_name = findViewById(R.id.et_user_name);
        tv_login = findViewById(R.id.tv_login);
        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        tv_signup = findViewById(R.id.tv_signup);
        cv_facebook = findViewById(R.id.cv_facebook);
        cv_google = findViewById(R.id.cv_google);

        tv_login.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        cv_facebook.setOnClickListener(this);
        cv_google.setOnClickListener(this);

        et_user_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (!data.isEmpty()) {
                    if (TextUtils.isDigitsOnly(data))
                        isEmail = false;
                    else isEmail = true;

                    if (TextUtils.isDigitsOnly(data)) {
                        if (lastFilterLength != PHONE_MAX_LENGTH) {
                            lastFilterLength = PHONE_MAX_LENGTH;
                            et_user_name.setFilters(new InputFilter[]{PHONE_FILTER_LENGTH});
                        }
                    } else {
                        if (lastFilterLength != EMAIL_MAX_LENGTH) {
                            lastFilterLength = EMAIL_MAX_LENGTH;
                            et_user_name.setFilters(new InputFilter[]{EMAIL_FILTER_LENGTH});
                        }
                    }
                } else {
                    if (lastFilterLength != EMAIL_MAX_LENGTH) {
                        lastFilterLength = EMAIL_MAX_LENGTH;
                        et_user_name.setFilters(new InputFilter[]{EMAIL_FILTER_LENGTH});
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_login:
                callLoginOtp();
                break;

            case R.id.tv_forgot_password:
                goToForgotPasswordActivity(null);
                break;

            case R.id.tv_signup:
                goToRegisterActivity(null);
                break;

            case R.id.cv_facebook:
              /*  MyApplication.getInstance().getFacebookLoginHandler().setSocialLoginListner(fbLoginListener);
                MyApplication.getInstance().getFacebookLoginHandler().callLoginWithRead(this,
                        Arrays.asList("public_profile", "email"));*/
                break;

            case R.id.cv_google:
               /* MyApplication.getInstance().getGplusLoginHandler().setSocialLoginListner(gplusLoginListener);
                MyApplication.getInstance().getGplusLoginHandler().gPlusSignIn(this);*/
                break;
        }
    }

    private void showFbLoginErrorDialog() {
        SocialPermissionErrorDialog socialPermissionErrorDialog = new SocialPermissionErrorDialog();
        socialPermissionErrorDialog.setOnClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        MyApplication.getInstance().getFacebookLoginHandler().setSocialLoginListner(fbLoginListener);
                        MyApplication.getInstance().getFacebookLoginHandler().callLoginWithRead(LoginActivity.this,
                                Arrays.asList("email"));
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        });
        socialPermissionErrorDialog.show(getFm(),
                socialPermissionErrorDialog.getClass().getSimpleName());
    }

    private void callSocialLogin(SocialData socialData) {
        SocialRequestModel requestModel = new SocialRequestModel();
        requestModel.email = socialData.getEmail();
        requestModel.socialid = socialData.getId();
        requestModel.logintype = socialData.getLoginFrom();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false);
        getWebRequestHelper().sociallogin(requestModel, this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GplusLoginHandler.RC_SIGN_IN) {
            MyApplication.getInstance().getGplusLoginHandler().onActivityResult(data);
        } else if (requestCode == FacebookLoginHandler.fbLoginRequestCode()) {
            MyApplication.getInstance().getFacebookLoginHandler().onActivityResult(requestCode,
                    resultCode, data);
        }
    }

    private void callLoginOtp() {
        if (getValidate().validEmailOrMobile(et_user_name)) {

            String userName = et_user_name.getText().toString().trim();

            LoginRequestModel loginRequestModel = new LoginRequestModel();
            loginRequestModel.username = userName;
            loginRequestModel.device_id = DeviceUtil.getUniqueDeviceId();
            loginRequestModel.devicetype = DEVICE_TYPE;
            loginRequestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();

            if (isEmail) {
                Bundle bundle = new Bundle();
                bundle.putString(DATA, new Gson().toJson(loginRequestModel));
                goToEmailLoginActivity(bundle);
            } else {
                displayProgressBar(false);
                getWebRequestHelper().userLogin(loginRequestModel, this);
            }
        }
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case ID_USER_LOGIN:
                handleLoginResponse(webRequest);
                break;

            case ID_SOCIAL_LOGIN:
                handleSocialLoginResponse(webRequest);
                break;
        }
    }

    private void handleLoginResponse(WebRequest webRequest) {
        LoginRequestModel extraData = webRequest.getExtraData(DATA);
        LoginResponse2Model responseModel = webRequest.getResponsePojo(LoginResponse2Model.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            String data = responseModel.getData();
            Bundle bundle = new Bundle();
            bundle.putString(OTP, "");
            bundle.putString(REGISTER_MODEL, new Gson().toJson(extraData));
            goToMobileLoginActivity(bundle);
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void handleSocialLoginResponse(WebRequest webRequest) {

        SocialRequestModel socialRequestModel = webRequest.getExtraData(DATA);
        EmptyResponseModel responseModel = webRequest.getResponsePojo(EmptyResponseModel.class);

        Log.e("LoginRespons",""+responseModel.getMsg());
        Log.e("LoginRespons",""+responseModel.getData());
        if (responseModel != null) {
            if (responseModel.isError() && responseModel.getCode() == 5) {
                addPhoneNumberDialog(socialRequestModel);
            } else if (!responseModel.isError() && responseModel.getCode() == 6) {
                RegisterResponseModel responseModels = webRequest.getResponsePojo(RegisterResponseModel.class);
                Bundle bundle = new Bundle();
                bundle.putString(OTP, "");
                bundle.putString(SOCIAL_MODEL, new Gson().toJson(socialRequestModel));
                goToRegisterVerifyActivity(bundle);
            } else {
                LoginResponseModel loginResponseModel = webRequest.getResponsePojo(LoginResponseModel.class);
                if (loginResponseModel == null) return;
                if (loginResponseModel.isError() && loginResponseModel.getData() == null) {
                    showErrorMsg(responseModel.getMsg());
                } else {
                    showCustomToast(responseModel.getMsg());
                    if (isFinishing()) return;
                    UserModel userModel = loginResponseModel.getData().getData();
                    userModel.setToken(loginResponseModel.getData().getToken());
                    getUserPrefs().saveLoggedInUser(userModel);
                    goToDashboardActivity(null);
                }
            }
        } else {
            if (responseModel == null) return;
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void goToDashboardActivity(Bundle bundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToRegisterVerifyActivity(Bundle bundle) {
        Intent intent = new Intent(this, RegisterVerifyActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }


    private void addPhoneNumberDialog(final SocialRequestModel socialRequestModel) {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, "Are you sure want to logout?");
        PhoneNumberDialog dialog = PhoneNumberDialog.getInstance(bundle);
        dialog.setOnClickListener(new PhoneNumberDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, String mobile_number) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    socialRequestModel.phone = mobile_number;
                    displayProgressBar(false);
                    getWebRequestHelper().sociallogin(socialRequestModel, LoginActivity.this);
                }

                dialog.dismiss();

            }
        });
        dialog.show(getFm(), "Confirm Logout");
    }

    private void goToEmailLoginActivity(Bundle bundle) {
        Intent intent = new Intent(this, EmailLoginActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToMobileLoginActivity(Bundle bundle) {
        Intent intent = new Intent(this, MobileLoginActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToForgotPasswordActivity(Bundle bundle) {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void goToRegisterActivity(Bundle bundle) {
        Intent intent = new Intent(this, RegisterActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    private void socialLogout() {
        MyApplication.getInstance().getFacebookLoginHandler().callLogout();
        MyApplication.getInstance().getGplusLoginHandler().callLogout();
    }

}
