package com.app.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.LoginRequestModel;
import com.app.model.webresponsemodel.LoginResponseModel;
import com.app.ui.main.dashboard.DashboardActivity;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;

public class EmailLoginActivity extends AppBaseActivity {


    private TextView tv_email;
    private EditText et_password;
    private TextView tv_submit;

    private String getLoginModel() {
        return getIntent().getExtras().getString(DATA);
    }

    private LoginRequestModel getLoginRequestModel() {
        return new Gson().fromJson(getLoginModel(), LoginRequestModel.class);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_email_login;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        tv_email = findViewById(R.id.tv_email);
        et_password = findViewById(R.id.et_password);
        tv_submit = findViewById(R.id.tv_submit);
        tv_submit.setOnClickListener(this);

        setData();
    }

    private void setData() {
        LoginRequestModel loginRequestModel = getLoginRequestModel();
        tv_email.setText(loginRequestModel.username);

        Log.e("isInfluencer",""+loginRequestModel.isInfluencer);
        Log.e("LoginUrl_+++",""+loginRequestModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit:
                callLoginOtp();
                break;
        }
    }

    private void callLoginOtp() {

        String password = et_password.getText().toString().trim();
        if (password.isEmpty()) {
            showErrorMsg("Please enter password.");
            return;
        }
        LoginRequestModel loginRequestModel = getLoginRequestModel();
        loginRequestModel.password = password;
        loginRequestModel.devicetype = DEVICE_TYPE;
        loginRequestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false);
        getWebRequestHelper().userLogin(loginRequestModel, this);

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        switch (webRequest.getWebRequestId()) {
            case ID_USER_LOGIN:
                handleLoginResponse(webRequest);
                Log.e("LoginUrl_+++",""+webRequest);
                break;
        }
    }

    private void handleLoginResponse(WebRequest webRequest) {

        LoginResponseModel responseModel = webRequest.getResponsePojo(LoginResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError() && responseModel.getData() != null) {
            UserModel userModel = responseModel.getData().getData();
            userModel.setToken(responseModel.getData().getToken());

            getUserPrefs().saveLoggedInUser(userModel);
            goToDashboardActivity(null);
            String message = responseModel.getMsg();

            Log.e("MessageLogin",""+message);
            Log.e("MessageLogin",""+userModel);
            Log.e("MessageLogin",""+responseModel);
            Log.e("MessageLogin",""+webRequest.getResponsePojo(LoginResponseModel.class));
            Log.e("MessageLogin",""+responseModel.getData().getData());

            if (isValidString(message))
                showCustomToast(message);
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void goToDashboardActivity(Bundle bundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }
}
