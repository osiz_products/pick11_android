package com.app.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.appbase.AppBaseActivity;
import com.app.model.UserModel;
import com.app.model.webrequestmodel.LoginRequestModel;
import com.app.model.webrequestmodel.ResendOtpRequestModel;
import com.app.model.webresponsemodel.LoginResponseModel;
import com.app.model.webresponsemodel.ResendOtpResponseModel;
import com.app.ui.MyApplication;
import com.app.ui.main.dashboard.DashboardActivity;
import com.pickeleven.R;
import com.customviews.OtpView;
import com.fcm.NotificationPrefs;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.utilities.DeviceUtil;

public class MobileLoginActivity extends AppBaseActivity {

    OtpView otp_view;
    TextView tv_resend;
    TextView tv_verify_mobile;

    private String getOtp() {
        return getIntent().getExtras().getString(OTP);
    }

    private String getLoginModel() {
        return getIntent().getExtras().getString(REGISTER_MODEL);
    }

    private LoginRequestModel getLoginRequestModel() {
        return new Gson().fromJson(getLoginModel(), LoginRequestModel.class);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_mobile_login;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();

        otp_view = findViewById(R.id.otp_view);
        tv_resend = findViewById(R.id.tv_resend);
        tv_verify_mobile = findViewById(R.id.tv_verify_mobile);

        tv_resend.setOnClickListener(this);
        tv_verify_mobile.setOnClickListener(this);
        startCounter(OTP_COUNTER);

        if (isValidString(getOtp())) {
            if (otp_view != null) {
//                otp_view.setOTP(getOtp());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_resend:
                callResendOtp();
                break;

            case R.id.tv_verify_mobile:
                callVerifyOtp();
                break;
        }
    }

    private void callResendOtp() {

        ResendOtpRequestModel requestModel = new ResendOtpRequestModel();
        requestModel.username = getLoginRequestModel().username;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.atype = VERIFICATION;
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().userResendOtp(requestModel, this);
    }

    private void callVerifyOtp() {
        String otp = otp_view.getOTP();
        if (otp == null || otp.trim().isEmpty() || otp.trim().length() != 6) {
            showErrorMsg("Please enter valid otp.");
            return;
        }

        LoginRequestModel loginRequestModel = getLoginRequestModel();
        loginRequestModel.otp = otp;
        loginRequestModel.devicetype = DEVICE_TYPE;
        loginRequestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().userLogin(loginRequestModel, this);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_RESEND_OTP:
                handleResendOtpResponse(webRequest);
                break;
            case ID_USER_LOGIN:
                handleLoginResponse(webRequest);
                break;
        }
    }

    private void handleResendOtpResponse(WebRequest webRequest) {
        ResendOtpResponseModel responsePojo = webRequest.getResponsePojo(ResendOtpResponseModel.class);
        if (responsePojo == null) return;
        if (responsePojo.isError())
            showErrorMsg(responsePojo.getMsg());
        else {
            ResendOtpResponseModel.DataBean data = responsePojo.getData();
//            otp_view.setOTP(data.getOtp());
            startCounter(OTP_COUNTER);
        }
    }

    private void handleLoginResponse(WebRequest webRequest) {
        LoginResponseModel responseModel = webRequest.getResponsePojo(LoginResponseModel.class);
        if (responseModel == null) return;
        if (responseModel.isError() && responseModel.getData() == null) {
            showErrorMsg(responseModel.getMsg());
        } else {
            showCustomToast(responseModel.getMsg());
            if (isFinishing()) return;
            UserModel userModel = responseModel.getData().getData();
            userModel.setToken(responseModel.getData().getToken());
            getUserPrefs().saveLoggedInUser(userModel);
            goToDashboardActivity(null);
        }
    }

    private void goToDashboardActivity(Bundle bundle) {
        Intent intent = new Intent(this, DashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    public void startCounter(final long millisecond) {
        long remainMin = (millisecond / (60 * 1000));
        long remainSec = (millisecond - (remainMin * 60 * 1000)) / 1000;
        tv_resend.setText(String.format("%02d:%02d", remainMin, remainSec));
        tv_resend.setOnClickListener(null);
        tv_resend.setAlpha(0.5f);

        CountDownTimer downTimer = new CountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (isFinishing()) {
                    return;
                }
                tv_resend.setOnClickListener(null);
                tv_resend.setAlpha(0.5f);
                long remainMin = (millisUntilFinished / (60 * 1000));
                long remainSec = (millisUntilFinished - (remainMin * 60 * 1000)) / 1000;
                tv_resend.setText(String.format("%02d:%02d", remainMin, remainSec));
            }

            @Override
            public void onFinish() {
                if (isFinishing()) {
                    return;
                }
                tv_resend.setText("Resend code");
                tv_resend.setOnClickListener(MobileLoginActivity.this);
                tv_resend.setAlpha(1.0f);
            }
        };
        downTimer.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startSMSListener();
    }

    @Override
    public void onOTPReceived(String message) {
        String substring = message.substring(4, 10);
        MyApplication.getInstance().printLog("parseOtp: ", substring);
        if (TextUtils.isDigitsOnly(substring)) {
//            otp_view.setOTP(substring);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getSmsReceiver() != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(getSmsReceiver());
        }
    }

}
