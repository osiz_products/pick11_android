package com.app.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.appbase.AppBaseActivity;
import com.app.model.CountryModel;
import com.app.model.RegisterOtpModel;
import com.app.model.StateModel;
import com.app.model.webrequestmodel.RegisterRequestModel;
import com.app.model.webresponsemodel.CountryResponseModel;
import com.app.model.webresponsemodel.RegisterResponseModel;
import com.app.model.webresponsemodel.StateResponseModel;
import com.app.ui.dialogs.state.SelectCountryDialog;
import com.app.ui.dialogs.state.SelectStateDialog;
import com.pickeleven.BuildConfig;
import com.pickeleven.R;
import com.fcm.NotificationPrefs;
import com.google.gson.Gson;
import com.imagePicker.FileInformation;
import com.imagePicker.ProfilePicDialog;
import com.medy.retrofitwrapper.WebRequest;
import com.permissions.PermissionHelperNew;
import com.rest.WebServices;
import com.utilities.DeviceUtil;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppBaseActivity implements
        PermissionHelperNew.OnSpecificPermissionGranted {

    private RelativeLayout ll_data_lay;
    private ImageView iv_profile_pic;
    private ProgressBar pb_image;
    private EditText et_email_address;
    private EditText et_mobile_number;
    private LinearLayout ll_refer_friends;
    private EditText et_refer_code;
    private EditText et_password;
    private EditText et_state_name;
    private TextView tv_term_and_condition;
    private TextView tv_register;
    private TextView tv_sign;
    TextView tv_country;
    TextView tv_state;
    private LinearLayout ll_country;
    private LinearLayout ll_state;
    String getCountryId;
    String getStateId;

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_register;
    }

    @Override
    public void initializeComponent() {
        super.initializeComponent();
        getCountry();
        ll_data_lay = findViewById(R.id.ll_data_lay);
        iv_profile_pic = findViewById(R.id.iv_profile_pic);
        ll_country = findViewById(R.id.ll_country);
        ll_state = findViewById(R.id.ll_state);
        pb_image = findViewById(R.id.pb_image);
        et_email_address = findViewById(R.id.et_email_address);
        et_mobile_number = findViewById(R.id.et_mobile_number);
        ll_refer_friends = findViewById(R.id.ll_refer_friends);
        et_refer_code = findViewById(R.id.et_refer_code);
        et_password = findViewById(R.id.et_password);
        tv_term_and_condition = findViewById(R.id.tv_term_and_condition);
        tv_register = findViewById(R.id.tv_register);
        tv_sign = findViewById(R.id.tv_sign);
        tv_country = findViewById(R.id.tv_country);
        tv_state = findViewById(R.id.tv_state);

        String flavor = BuildConfig.FLAVOR;
        if (flavor.equalsIgnoreCase("pick11Dev")){
//            updateViewVisibility(ll_refer_friends, View.GONE);
        }

        iv_profile_pic.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        ll_country.setOnClickListener(this);
        ll_state.setOnClickListener(this);
        clickSpan();
    }

    private void getCountry() {
        getWebRequestHelper().getCountry(this);
    }

    private void clickSpan() {
        tv_term_and_condition.setMovementMethod(LinkMovementMethod.getInstance());

        Pattern termsAndConditionsMatcher = Pattern.compile("Terms");
        Matcher m1 = termsAndConditionsMatcher.matcher(tv_term_and_condition.getText().toString());
        if (m1.find()) {
            URLSpan urlSpan = new URLSpan(m1.group(0)) {
                @Override
                public void onClick(View widget) {
                    Bundle bundle = new Bundle();
                    String URL = String.format(WebServices.FaqUrl(), "termscon");
                    bundle.putString(DATA, URL);
                    bundle.putString(DATA2, "Terms & Conditions");
                    goToWebViewActivity(bundle);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(getResources().getColor(R.color.login_text_color));
                    ds.setUnderlineText(false);
                }
            };
            SpannableString string = SpannableString.valueOf(tv_term_and_condition.getText());
            string.setSpan(urlSpan, m1.start(), m1.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        Pattern privacyPolicyMatcher = Pattern.compile("Privacy Policy");
        Matcher m2 = privacyPolicyMatcher.matcher(tv_term_and_condition.getText().toString());
        if (m2.find()) {
            URLSpan urlSpan = new URLSpan(m1.group(0)) {
                @Override
                public void onClick(View widget) {
                    Bundle bundle = new Bundle();
                    String URL = String.format(WebServices.FaqUrl(), "prcypoly");
                    bundle.putString(DATA, URL);
                    bundle.putString(DATA2, "Privacy policy");
                    goToWebViewActivity(bundle);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(getResources().getColor(R.color.login_text_color));
                    ds.setUnderlineText(false);
                }
            };
            SpannableString string = SpannableString.valueOf(tv_term_and_condition.getText());
            string.setSpan(urlSpan, m2.start(), m2.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        tv_sign.setMovementMethod(LinkMovementMethod.getInstance());

        Pattern signInMatcher = Pattern.compile("Sign In");
        Matcher signIn = signInMatcher.matcher(tv_sign.getText().toString());
        if (signIn.find()) {
            URLSpan urlSpan = new URLSpan(signIn.group(0)) {
                @Override
                public void onClick(View widget) {
                    onBackPressed();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(getResources().getColor(R.color.login_text_color));
                    ds.setUnderlineText(false);
                }
            };
            SpannableString string = SpannableString.valueOf(tv_sign.getText());
            string.setSpan(urlSpan, signIn.start(), signIn.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_profile_pic:
                showImagePickerDialog();
                break;

            case R.id.tv_register:
                callRegister();
                break;
            case R.id.ll_country:
              //  selectCountryDialog();
            break;

            case R.id.ll_state:
               // selectStateDialog(getCountryId);
                break;
        }
    }



    private void selectCountryDialog() {
        final List<CountryModel> country = getUserPrefs().getCountryList();
        if (country == null) {
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().getCountry(this);
        } else {
            final SelectCountryDialog selectTeamDialog = new SelectCountryDialog();
            selectTeamDialog.setDataList(country);
            selectTeamDialog.setTitle("Select Country");
            selectTeamDialog.setOnItemSelectedListeners(new SelectCountryDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectTeamDialog.dismiss();
                    tv_country.setText(country.get(position).getName());
                    getCountryId = country.get(position).getId();
                    Log.e("GetSelectCountry",""+country.get(position).getId());
                    Log.e("GetSelectCountry",""+country.get(position).getName());

                    //selectStateDialog(getCountryId);
                     getState();
                }
            });
            selectTeamDialog.show(getFm(), selectTeamDialog.getClass().getSimpleName());
        }
    }

    private void getState() {
        getWebRequestHelper().getStateUrl(getCountryId,this);
        selectStateDialog(getCountryId);
    }

    private void selectStateDialog(String getCountryId) {
Log.e("GetAndSetCountryID",""+getCountryId);
        final List<StateModel> state = getUserPrefs().getStateList();
        if (state == null) {
            displayProgressBar(false, "Wait...");
            getWebRequestHelper().getStateUrl(getCountryId,this);
        } else {
            final SelectStateDialog selectTeamDialog = new SelectStateDialog();
            selectTeamDialog.setDataList(state);
            selectTeamDialog.setTitle("Select State");
            selectTeamDialog.setOnItemSelectedListeners(new SelectStateDialog.OnItemSelectedListener() {
                @Override
                public void onItemSelectedListener(int position) {
                    selectTeamDialog.dismiss();
                    tv_state.setText(String.valueOf(state.get(position).getName()));
                    Log.e("GetSelectState",""+state.get(position).getId());
                    Log.e("GetSelectState",""+state.get(position).getName());
                    getStateId = state.get(position).getId();
                }
            });
            selectTeamDialog.show(getFm(), selectTeamDialog.getClass().getSimpleName());
        }
    }

    private void showImagePickerDialog() {
        final ProfilePicDialog dialog = ProfilePicDialog.getNewInstance(false);
        dialog.setProfilePicDialogListner(new ProfilePicDialog.ProfilePicDialogListner() {
            @Override
            public void onProfilePicSelected(FileInformation fileInformation) {
                dialog.dismiss();

//                updateViewVisibility(pb_image, View.VISIBLE);

                String imageName = "profile_pic_" + System.currentTimeMillis();
                String large_file_path = fileInformation.getBitmapPathForUpload(RegisterActivity.this,
                        FileInformation.IMAGE_SIZE_LARGE, FileInformation.IMAGE_SIZE_LARGE,
                        "large/" + imageName);

                String thumb_file_path = fileInformation.getBitmapPathForUpload(RegisterActivity.this,
                        FileInformation.IMAGE_SIZE_THUMB, FileInformation.IMAGE_SIZE_THUMB,
                        "thumb/" + imageName);

                iv_profile_pic.setTag(R.id.image_path_tag, large_file_path);
                iv_profile_pic.setTag(R.id.image_path_thumb_tag, thumb_file_path);
//                iv_profile_pic.setImageURI(Uri.parse(thumb_file_path));
                loadImage(RegisterActivity.this, iv_profile_pic, pb_image, thumb_file_path,
                        R.drawable.camera_icon);
            }

            @Override
            public void onProfilePicRemoved() {

            }
        });
        dialog.showDialog(this, getFm());
    }

    private void callRegister() {

        String emailAddress = et_email_address.getText().toString().trim();
        String mobileNo = et_mobile_number.getText().toString().trim();
        String referCode = et_refer_code.getText().toString().trim();
        String password = et_password.getText().toString().trim();

        String country = tv_country.getText().toString().trim();
        String state = tv_state.getText().toString().trim();

        if (emailAddress.isEmpty()) {
            showErrorMsg("Please enter email.");
            return;
        }

        if (mobileNo.isEmpty()) {
            showErrorMsg("Please enter mobile number.");
            return;
        }else if(mobileNo.length() < 10){
            showErrorMsg("Mobile No. must be minimum 10 digits");
            return;
        }

        /*if (country.isEmpty()) {
            showErrorMsg("Please select country.");
            return;
        }

        if (state.isEmpty()) {
            showErrorMsg("Please select state.");
            return;
        }*/

        if (password.isEmpty()) {
            showErrorMsg("Please enter password.");
            return;
        }

        if (password.length() < 6) {
            showErrorMsg("Please enter more then 6 char.");
            return;
        }

        String imagePath = (String) iv_profile_pic.getTag(R.id.image_path_tag);
        if (imagePath == null) {
            imagePath = "";
        }
        RegisterRequestModel requestModel = new RegisterRequestModel();
        requestModel.phone = mobileNo;
        requestModel.email = emailAddress;
        requestModel.password = password;
        requestModel.refercode = referCode;

       // requestModel.countryId = getCountryId;
       // requestModel.stateId = getStateId;

        requestModel.device_id = DeviceUtil.getUniqueDeviceId();
        requestModel.devicetype = DEVICE_TYPE;
        requestModel.devicetoken = NotificationPrefs.getInstance(this).getNotificationToken();
        requestModel.profilepic = new File(imagePath);
        displayProgressBar(false, "Wait...");
        getWebRequestHelper().userRegister(requestModel, this);
    }


    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        dismissProgressBar();
        super.onWebRequestResponse(webRequest);
        switch (webRequest.getWebRequestId()) {
            case ID_REGISTER:
                handleRegisterResponse(webRequest);
                break;
            case ID_COUNTRY:
                handleCountryResponse(webRequest);
                break;
            case ID_GET_STATE:
                handleGetStateResponse(webRequest);
                break;

        }
    }

    private void handleGetStateResponse(WebRequest webRequest) {
        StateResponseModel responseModel = webRequest.getResponsePojo(StateResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<StateModel> data = responseModel.getData();
            getUserPrefs().updateStateList(data);
        }
    }

    private void handleCountryResponse(WebRequest webRequest) {
        CountryResponseModel responseModel = webRequest.getResponsePojo(CountryResponseModel.class);
        if (responseModel != null && !responseModel.isError()) {
            List<CountryModel> data = responseModel.getData();
            getUserPrefs().updateCountryList(data);
        }
    }

    private void handleRegisterResponse(WebRequest webRequest) {
        RegisterRequestModel extraData = webRequest.getExtraData(REGISTER_MODEL);
        RegisterResponseModel responseModel = webRequest.getResponsePojo(RegisterResponseModel.class);
        if (responseModel == null) return;
        if (!responseModel.isError()) {
            RegisterOtpModel data = responseModel.getData();
//            if (data == null) return;
            Bundle bundle = new Bundle();
            bundle.putString(OTP, "");
            bundle.putString(REGISTER_MODEL, new Gson().toJson(extraData));
            goToRegisterVerifyActivity(bundle);
        } else {
            String message = responseModel.getMsg();
            if (isValidString(message))
                showErrorMsg(message);
        }
    }

    private void goToRegisterVerifyActivity(Bundle bundle) {
        Intent intent = new Intent(this, RegisterVerifyActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.enter_alpha, R.anim.exit_alpha);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelperNew.onSpecificRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionGranted(boolean isGranted, boolean withNeverAsk, String permission, int requestCode) {
        if (requestCode == PermissionHelperNew.SMS_PERMISSION_REQUEST_CODE) {
            if (isGranted) {
                callRegister();
            } else {
                if (withNeverAsk) {
                    PermissionHelperNew.showNeverAskAlert(this, true, requestCode);
                } else {
                    PermissionHelperNew.showSpecificDenyAlert(this, permission, requestCode, true);
                }
            }
        }
    }
}
