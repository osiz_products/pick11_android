package com.app.uitilites;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.app.appbase.AppBaseActivity;
import com.app.preferences.UserPrefs;
import com.pickeleven.R;


public class RecyclerSectionItemDecoration extends RecyclerView.ItemDecoration {

    private final int headerOffset;
    private final boolean sticky;
    private final SectionCallback sectionCallback;

    private View headerView;
    private ImageView iv_contestLogo;
    private ProgressBar pb_image;
    private TextView tv_contests_type;
    private TextView tv_contests_des;
    private Context context;
    UserPrefs userPrefs;
    public RecyclerSectionItemDecoration(Context context, int headerHeight, boolean sticky, @NonNull SectionCallback sectionCallback) {
        this.context = context;
        headerOffset = headerHeight;
        this.sticky = sticky;
        this.sectionCallback = sectionCallback;

    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect,
                view,
                parent,
                state);

        int pos = parent.getChildAdapterPosition(view);
        if (sectionCallback.isSection(pos)) {
            outRect.top = headerOffset;
        }
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(canvas, parent, state);

        if (headerView == null) {

            userPrefs = new UserPrefs(context);
            headerView = inflateHeaderView(parent);
            iv_contestLogo = headerView.findViewById(R.id.iv_contestLogo);
            pb_image = headerView.findViewById(R.id.pb_image);
            tv_contests_type = headerView.findViewById(R.id.tv_contests_type);
            tv_contests_des = headerView.findViewById(R.id.tv_contests_des);
            fixLayoutSize(headerView, parent);

            if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("day")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            else if(userPrefs.getStringKeyValuePrefs("modeSelect").equals("night")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        CharSequence previousHeader = "";
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            final int position = parent.getChildAdapterPosition(child);

            String image = sectionCallback.getSectionImage(position);
            CharSequence title = sectionCallback.getSectionHeader(position);
            CharSequence subTitle = sectionCallback.getSectionSubHeader(position);
            ((AppBaseActivity) context).loadImage(context, iv_contestLogo, pb_image,
                    image, R.drawable.dummy_logo, 40);
            tv_contests_type.setText(title);


            tv_contests_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            tv_contests_des.setText(subTitle);
            if (!previousHeader.equals(title) || sectionCallback.isSection(position)) {
                drawHeader(canvas,
                        child,
                        headerView);
                previousHeader = title;
            }
        }
    }

    private void moveHeader(Canvas canvas, View currentHeader, View nextHeader) {
        canvas.save();
        canvas.translate(0, nextHeader.getTop() - currentHeader.getHeight());
        currentHeader.draw(canvas);
        canvas.restore();
    }

    private void drawHeader(Canvas canvas, View child, View headerView) {
        canvas.save();
        int max = Math.max(0, child.getTop() - headerView.getHeight());
        if (sticky) {
            canvas.translate(0, max);
        } else {
            canvas.translate(0, max);
        }
        headerView.draw(canvas);
        canvas.restore();
    }



    private View inflateHeaderView(RecyclerView parent) {


        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            parent.getContext().getTheme().applyStyle(R.style.BaseAppThemeDark, true);
        } else {
            parent.getContext().getTheme().applyStyle(R.style.BaseAppTheme, true);
        }

        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_section_header,
                        parent,
                        false);
    }

    /**
     * Measures the tv_contests_type view to make sure its size is greater than 0 and will be drawn
     * https://yoda.entelect.co.za/view/9627/how-to-android-recyclerview-item-decorations
     */
    private void fixLayoutSize(View view, ViewGroup parent) {
        int widthSpec = View.MeasureSpec.makeMeasureSpec(parent.getWidth(),
                View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(),
                View.MeasureSpec.UNSPECIFIED);

        int childWidth = ViewGroup.getChildMeasureSpec(widthSpec,
                parent.getPaddingLeft() + parent.getPaddingRight(),
                view.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec,
                parent.getPaddingTop() + parent.getPaddingBottom(),
                view.getLayoutParams().height);

        view.measure(childWidth,
                childHeight);

        view.layout(0,
                0,
                view.getMeasuredWidth(),
                view.getMeasuredHeight()
//                DeviceScreenUtil.getInstance().convertDpToPixel(40f)
        );
    }

    public interface SectionCallback {
        boolean isSection(int position);

        String getSectionImage(int position);

        CharSequence getSectionHeader(int position);

        CharSequence getSectionSubHeader(int position);
    }

}


