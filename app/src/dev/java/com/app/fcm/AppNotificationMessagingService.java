package com.app.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.app.appbase.AppBaseApplication;
import com.app.ui.MyApplication;
import com.app.ui.main.cricket.contests.ContestsActivity;
import com.app.ui.main.dashboard.DashboardActivity;
import com.app.ui.main.navmenu.scratch.ScratchCard;
import com.bumptech.glide.request.target.Target;
import com.pickeleven.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.fcm.NotificationMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class AppNotificationMessagingService extends NotificationMessagingService<AppNotificationModel>
        implements AppNotificationType {

    public static final String KEY_NOTIFICATION_TITLE = "title";
    public static final String KEY_NOTIFICATION_MESSAGE = "body";
    public static final String KEY_NOTIFICATION_TYPE = "noti_type";
    public static final String KEY_NOTIFICATION_TIME = "noti_time";
    public static final String KEY_JSON_DATA = "json_data";
//    public static final String KEY_IMAGE_LARGE = "noti_large";
    public static final String KEY_IMAGE_LARGE = "logoUrl";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_IMAGE_SMALL = "noti_thumb";

    Bitmap newbitmap= null;

    @Override
    public AppNotificationModel getNotificationModal(RemoteMessage remoteMessage) {
        Log.e("Appnotifi",remoteMessage.getData().toString());
        return new AppNotificationModel(remoteMessage);
    }

    @Override
    public void pushMessageReceived(AppNotificationModel notificationModal) {
        if (notificationModal != null) {
            //if (isAppIsInBackground()) {
            generatePushNotification(notificationModal);
//            } else {
            AppBaseApplication.instance.getPushNotificationHelper().
                    triggerNotificationListener(notificationModal);
//            }
        }
    }

    @Override
    public void generatePushNotification(AppNotificationModel notificationModal) {
//        if (notificationModal.isAdminAlert()) {
        String imgUrl = notificationModal.getImage();
        String bigimgUrl = notificationModal.getImageLarge();
        if (imgUrl != null && imgUrl.trim().length() > 0) {
            new GeneratePictureNotification(notificationModal).execute();
            return;
        }
//        }

        sendNotification(notificationModal, null);


    }


    public class GeneratePictureNotification extends AsyncTask<Void, Void, Bitmap> {
        AppNotificationModel appNotificationModel;


        public GeneratePictureNotification(AppNotificationModel appNotificationModel) {
            this.appNotificationModel = appNotificationModel;

        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String imageUrl = appNotificationModel.getImage();
            String imageUrl1 = appNotificationModel.getImageLarge();



            if(imageUrl1 != null && imageUrl1.trim().length() > 0)
            {
                try {
                    newbitmap= Glide.with(getApplicationContext()).asBitmap().override(300, 100)
                            .load(imageUrl1)  // image url in string
                            .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
                    Log.e("Bitmaap",""+newbitmap);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }



            Log.e("get_Bitmap_Bitmap",""+imageUrl);
            if (imageUrl != null && imageUrl.trim().length() > 0) {
                RequestManager requestManager = Glide.with(MyApplication.getInstance());
                RequestOptions options = new RequestOptions()
                        .override(400)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .priority(Priority.HIGH);

                try {
                    return requestManager.asBitmap().load(imageUrl).
                            apply(options).submit().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }




            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            sendNotification(appNotificationModel, bitmap);
        }
    }

    public static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (context == null) return;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            String name = context.getString(R.string.app_name);
            createNotificationChannel(context, name, name, importance, name);
        }
    }

    private void sendNotification(AppNotificationModel notificationModal, Bitmap image) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String title = notificationModal.getTitle();
        String images = notificationModal.getImage();
        String largeimages = notificationModal.getImageLarge();
        String message = notificationModal.getMessage();
        Bundle bundle = notificationModal.generateBundle();

        Log.e("Image_Message",notificationModal.getImageLarge());

//        Bitmap b=StringToBitMap(notificationModal.getImageLarge());


        Intent intent= null;
        if(message.equals("You've been rewarded!"))
        {
            intent = new Intent(this, ScratchCard.class);
        }
        else if(message.equals("The match contest you have joined is going to start in 30 mins.") || message.equals("The match contest you have joined is going to start in 15 mins.") || message.equals("The match contest you have joined is going to start in 5 mins."))
        {
            intent = new Intent(this, ContestsActivity.class);
        }
        else
        {
            intent = new Intent(this, DashboardActivity.class);
        }


        if (bundle != null) {
            intent.putExtras(bundle);
        }
      /*  intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);*/

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        long notificationId = 0;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if(newbitmap != null)
        {

            NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle()
                    .bigPicture(newbitmap)
                    .setSummaryText(message);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                    getString(R.string.app_name))
                    .setSmallIcon(R.drawable.watermark1)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);

            if (image != null) {
                notificationBuilder.setLargeIcon(image);
                notificationBuilder.setStyle(style);
           /* notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle(notificationBuilder)
                    .bigPicture(image));*/

//                    .bigLargeIcon(null));
            }


            notificationBuilder.setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setContentText(message)
                    //API Level min 16 is required
                    .setStyle(style)
                    /*   .setStyle(new
                               NotificationCompat.BigTextStyle().bigText(message))*/
                    .setFullScreenIntent(pendingIntent,true);

            notificationBuilder.setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = notificationBuilder.build();
            notificationManager.notify((int) notificationId, notification);
        }
        else
        {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                    getString(R.string.app_name))
                    .setSmallIcon(R.drawable.watermark1)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);

            if (image != null) {
                notificationBuilder.setLargeIcon(image);
           /*     notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle(notificationBuilder)
                    .bigPicture(image));*/
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));

//                    .bigLargeIcon(null));
            }


            notificationBuilder.setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setContentText(message)
                    //API Level min 16 is required
                   /*    .setStyle(new
                               NotificationCompat.BigTextStyle().bigText(message))*/
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setFullScreenIntent(pendingIntent,true);

            notificationBuilder.setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = notificationBuilder.build();
            notificationManager.notify((int) notificationId, notification);
        }



    }

}
