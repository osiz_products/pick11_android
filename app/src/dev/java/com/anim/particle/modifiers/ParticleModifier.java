package com.anim.particle.modifiers;


import com.anim.particle.Particle;

public interface ParticleModifier {

    /**
     * modifies the specific value of a particle given the current miliseconds
     *
     * @param particle
     * @param miliseconds
     */
    void apply (Particle particle, long miliseconds);

}
