package com.anim.particle.initializers;

import com.anim.particle.Particle;

import java.util.Random;


public interface ParticleInitializer {

	void initParticle (Particle p, Random r);

}
