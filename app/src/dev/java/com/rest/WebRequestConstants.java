package com.rest;


public interface WebRequestConstants {

    String SMS_SENDER = "APP";
    String MOBILE_CODE = "+91";
    String DEVICE_TYPE = "android";
    String VERIFICATION = "newreg";
    String FORGOT_PASSWORD = "forget";

    String SOCIAL_MODEL = "SocialModel";
    String REGISTER_MODEL = "RegisterModel";
    String OTP = "Otp";
    String FIXTURES = "fixtures";
    String LIVE = "live";
    String RESULTS = "results";

    String MATCHLIST = "matchList";
    String DATA = "DATA";
    String DATA1 = "DATA1";
    String DATA2 = "DATA2";
    String DATA3 = "DATA3";
    String DATA4 = "DATA4";
    String DATA5 = "DATA5";
    String IS_EDIT = "EDIT";
    String PRE_JOIN = "prejoin";
    String JOIN = "join";


    int MAX_AMOUNT = 10000;
    int MIN_AMOUNT = 200;
    String GAME_ID = "gameid";
    String ATYPE = "atype";
    String MATCH_ID = "matchid";
    String POOL_CONTEST_ID = "poolcontestid";
    String TEAMID = "teamid";
    String PLAYER_ID = "pid";
    String GAME_TYPE_ID = "seriesid";
    String MATCH_TYPE_ID = "matchtype";


    String PASSWORD = "password";
    String MOBILENO = "mobileno";
    String USERID = "userid";
    String MESSAGE = "message";
    String MESSAGE_SEC = "message_sec";
    String POS_BTN = "pos_btn";
    String NEG_BTN = "neg_btn";

    String NOT_BACK_BTN = "notification_back";
    //    _______________________________USER__________________________________________
    int ID_USER_LOGIN = 1;
    int ID_REGISTER = 2;
    int ID_VERIFY_OTP = 3;
    int ID_GAME_TYPE = 4;
    int ID_FORGOT_PASSWORD = 5;
    int ID_FORGOT_PASSWORD_VERIFY = 6;
    int ID_RESEND_OTP = 7;
    int ID_COUNTRY = 8;
    int ID_UPDATE_PASSWORD = 9;
    int ID_UPDATE_PROFILE = 10;
    int ID_USER_DETAILS = 11;
    int ID_MATCH_FRONT = 12;
    int ID_MATCH_CONTEST_FRONT = 13;

    int ID_PLAYER_TYPE = 14;
    int ID_PLAYERS = 15;
    int ID_ALL_TEAM = 16;
    int ID_SLIDER_IMAGES = 17;
    int ID_TEAM_PLAYERS = 18;
    int ID_CREATE_TEAM = 19;
    int ID_CONTEST_DETAIL = 20;
    int ID_MY_MATCHES = 21;
    int ID_MY_TRANSACTION = 22;
    int ID_GET_STATE = 22;
    int ID_EDIT_PLAYERS = 23;
    int ID_CLONE_TEAM = 24;
    int ID_UPDATE_PLAYERS = 25;
    int ID_JOIN_CONTEST = 26;
    int ID_JOINED_CONTEST = 27;
    int ID_ADD_BALANCE = 28;
    int ID_UPDATE_PEN_CARD = 29;
    int ID_GET_PEN_CARD = 30;
    int ID_GET_USER_BALANCE = 31;
    int ID_GET_JOINED_CONTENTS_TEAM_ALL = 32;
    int ID_GET_CMS_PAGES = 33;
    int ID_SWITCH_TEAM = 34;
    int ID_JOINED_TEAM = 35;
    int ID_LIVE_SCORE = 36;
    int ID_WALLET_RECHARGE_PAYTM = 37;
    int ID_PRE_JOIN_CONTEST = 38;
    int ID_SEND_VERIFY_EMAIL = 39;
    int ID_FANTASY_SCORE = 40;
    int ID_GET_USER_PROFILE = 41;
    int ID_BANK_UPDATE = 42;
    int ID_GET_BANK_DETAIL = 43;
    int ID_WITHDRAW_REQUEST = 44;
    int ID_NOTIFICATION = 45;
    int ID_SOCIAL_LOGIN = 46;
    int ID_LIVE_SCORE_BOARD = 47;
    int ID_REFER_CODE = 48;
    int ID_PLAYER_DETAIL = 49;
    int ID_PROMO_CODE = 50;
    int ID_UPDATE_NOTI_STATUS = 51;
    int ID_SCORE_BOARD = 52;
    int ID_LIVE_DATA = 53;
    int ID_APP_SETTINGS = 54;
    int ID_PLAYER_POINTS_DETAILS = 55;
    int ID_CHECK_IFSC = 56;
    int ID_DELETE_ALL = 57;
    int ID_LEADERBOARD_COUNT = 58;
    int ID_PRIZE_POOL_BREAKUP = 59;
    int ID_CREATE_PRIVATE_CONTEST = 60;
    int ID_CHECK_INVITE_CODE = 61;
    int ID_UPCOMING_INF_CODE = 62;
    int ID_USER_INFLUENCER_ID = 63;
    int USER_INFLUENCER_DETAILS = 64;
    int USER_SCRATCH_DETAILS = 65;
    int USER_SCRATCH_REQUEST = 66;
    int MY_MATCHCOUNT_REQUEST = 67;
    int CONTACT_US_DETAILS = 68;
    int ID_USER_STATE = 69;
    int ID_USER_STATUS_PAY = 70;
    int MATCH_STATUS_UPCOMING = 71;
    int ID_MATCH_CONTEST_FRONT_VIEWMORE = 72;



}
