package com.rest;


public final class WebServices {
    static {
        System.loadLibrary("rest");
    }

    //    ______________________________USER______________________________

    public static native String GetDomain();

    public static native String GetLiveScoreUrl();

    public static native String UserLoginUrl();

    public static native String PlayStoreVersionUrl();

    public static native String UserRegisterUrl();

    public static native String userVerifyOtp();

    public static native String UserResendOtpUrl();

    public static native String UserForgotPasswordUrl();

    public static native String UserResendForgotPasswordOtpUrl();

    public static native String UserForgotPasswordVerifyUrl();

    public static native String UserChangePasswordUrl();

    public static native String GetCountryUrl();

    public static native String GetGameTypeUrl();

    public static native String GetPayStatusUrl();

    public static native String GetListMatchFrontUrl();

    public static native String GetMatchContestListFrontUrl();

    public static native String GetMatchContestListFrontViewMoreUrl();

    public static native String GetPlayerTypeUrl();

    public static native String GetPlayersUrl();

    public static native String GetAllTeamUrl();

    public static native String GetSliderImagesUrl();

    public static native String GetTeamPlayersUrl();

    public static native String CreateTeamUrl();

    public static native String UpdateTeamUrl();

    public static native String ContestDetailUrl();

    public static native String GetMyMatchesUrl();

    public static native String GetTransactionUrl();

    public static native String UpdateUserProfileUrl();

    public static native String GetStatusUrl();

    public static native String GetJoinContestUrl();

    public static native String GetJoinedContestUrl();

    public static native String AddBalanceUrl();

    public static native String UpdatePenCardUrl();

    public static native String GetPenCardUrl();

    public static native String GetUserBalanceUrl();

    public static native String GetContestJoinedTeamsAll();

    public static native String GetCmsPagesUrl();

    public static native String SwitchTeamUrl();

    public static native String GetJoinedTeamUrl();

    public static native String WalletAddBalanceUrl();

    public static native String LiveScoreUrl();

    public static native String FantasyScoreUrl();

    public static native String SendVerifyEmailUrl();

    public static native String GetProfileDetailUrl();

    public static native String UpdateBankDetailsUrl();

    public static native String GetBankDetailsUrl();

    public static native String WithdrawRequestUrl();

    public static native String GetNotificationUrl();

    public static native String GetMatchContestPdf();

    public static native String GetSocialloginUrl();

    public static native String GetLiveScoreBoardUrl();

    public static native String GetReferCodeUrl();

    public static native String GetPromoCodeUrl();

    public static native String GetUserPayStatusUrl();

    public static native String GetUserPayStatusCheckUrl();

    public static native String GetNotificationUpdateUrl();

    public static native String ScoreBoardUrl();

    public static native String WebScoreUrl();

    public static native String GetLiveDataUrl();

    public static native String FaqUrl();

    public static native String GetPlayerDetailsUrl();

    public static native String GetAppSettingsUrl();

    public static native String GetPlayerPointsDetailsUrl();

    public static native String CheckIFSC();

    public static native String GetLeaderBoardCountUrl();

    public static native String PrivateContestBreakUptUrl();

    public static native String CreatePrivateContestUrl();

    public static native String CheckInviteCode();

    public static native String GetInfluencerMatchesUrl();

    public static native String GetInfluencerListUrl();

    public static native String GetInfluencerWalletDetailsUrl();

    public static native String GetScratchCardDetailsUrl();

    public static native String GetScratchCardRequestUrl();

    public static native String GetgetMyMatchCountUrl();

    public static native String GetContactUSDetailsUrl();

    public static native String GetUpcomingMatchStatusUrl();


}
