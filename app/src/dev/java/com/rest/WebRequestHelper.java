package com.rest;

import android.content.Context;
import android.util.Log;

import com.anupkumarpanwar.scratchview.ScratchView;
import com.app.model.InviteCodeRequestModel;
import com.app.model.MatchModel;
import com.app.model.PlayerModel;
import com.app.model.TeamModel;
import com.app.model.webrequestmodel.BankRequestModel;
import com.app.model.webrequestmodel.ContestDetailRequestModel;
import com.app.model.webrequestmodel.CreateTeamRequestModel;
import com.app.model.webrequestmodel.DepositAmountRequestModel;
import com.app.model.webrequestmodel.ForgotPasswordRequestModel;
import com.app.model.webrequestmodel.ForgotPasswordVerifyRequestModel;
import com.app.model.webrequestmodel.InfluencerMatchlistModel;
import com.app.model.webrequestmodel.JoinContestRequestModel;
import com.app.model.webrequestmodel.LoginRequestModel;
import com.app.model.webrequestmodel.NotiStatusRequestModel;
import com.app.model.webrequestmodel.PrizePoolRequestModel;
import com.app.model.webrequestmodel.RegisterRequestModel;
import com.app.model.webrequestmodel.ResendOtpRequestModel;
import com.app.model.webrequestmodel.SocialRequestModel;
import com.app.model.webrequestmodel.SwitchTeamRequestModel;
import com.app.model.webrequestmodel.TransactionRequestModel;
import com.app.model.webrequestmodel.UpdatePasswordRequestModel;
import com.app.model.webrequestmodel.UpdatePenCardRequestModel;
import com.app.model.webrequestmodel.UpdateProfileRequestModel;
import com.app.model.webrequestmodel.VerifyOtpRequestModel;
import com.app.model.webrequestmodel.InfluencerRequestModel;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebServiceResponseListener;

public class WebRequestHelper implements WebRequestConstants {

    private static final String TAG = WebRequestHelper.class.getSimpleName();
    private Context context;
    public WebRequestHelper(Context context) {
        this.context = context;
    }
    public boolean isValidString(String data) {
        Log.e("GetLoginUserData",""+data);
        return data != null && !data.trim().isEmpty();
    }

    //    _______________________________USER__________________________________________


    public void userLogin(LoginRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_USER_LOGIN,
                WebServices.UserLoginUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userRegister(RegisterRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_REGISTER,
                WebServices.UserRegisterUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(REGISTER_MODEL, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userVerifyOtp(VerifyOtpRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_VERIFY_OTP,
                WebServices.userVerifyOtp(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userResendOtp(ResendOtpRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_RESEND_OTP,
                WebServices.UserResendOtpUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userForgotPassword(ForgotPasswordRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_FORGOT_PASSWORD,
                WebServices.UserForgotPasswordUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(MOBILENO, requestModel.username);
        webRequest.send(context, webServiceResponseListener);
    }

    public void UserResendForgotPasswordOtpUrl(ForgotPasswordRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_FORGOT_PASSWORD,
                WebServices.UserResendForgotPasswordOtpUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(MOBILENO, requestModel.username);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userForgotPasswordVerify(ForgotPasswordVerifyRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_FORGOT_PASSWORD_VERIFY,
                WebServices.UserForgotPasswordVerifyUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(PASSWORD, requestModel.password);
        webRequest.send(context, webServiceResponseListener);
    }

    public void userUpdatePassword(UpdatePasswordRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPDATE_PASSWORD,
                WebServices.UserChangePasswordUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
//      webRequest.addExtra(PASSWORD, requestModel.newpassword);
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_API_userUpdate",""+WebServices.UserChangePasswordUrl());
    }

    public void getCountry(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_COUNTRY,
                WebServices.GetCountryUrl(), WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getStateUrl(String getCountryId,WebServiceResponseListener webServiceResponseListener) {
        Log.e("getCountryID",""+getCountryId);
        WebRequest webRequest = new WebRequest(ID_GET_STATE,
                WebServices.GetStatusUrl(), WebRequest.POST_REQ);
        webRequest.addParam("id", getCountryId);
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_API_getStatusUrl","addParam"+getCountryId);
        Log.e("Call_API_getStatusUrl",""+WebServices.GetStatusUrl());
    }

    public void getStatusUrl(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_STATE,
                WebServices.GetStatusUrl(), WebRequest.POST_REQ);
        webRequest.addParam("iddd", "0");
        webRequest.send(context, webServiceResponseListener);
    }

    public void getSliderImages(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_SLIDER_IMAGES,
                WebServices.GetSliderImagesUrl(), WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_API_getSlider",""+WebServices.GetSliderImagesUrl());
    }

    public void getGameType(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GAME_TYPE,
                WebServices.GetGameTypeUrl(), WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getGameType",""+WebServices.GetGameTypeUrl());
    }


    public void getListMatchFront(String gameId, String type, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MATCH_FRONT,
            WebServices.GetListMatchFrontUrl(), WebRequest.POST_REQ);
            webRequest.addParam(GAME_ID, gameId);
            webRequest.addParam(ATYPE, type);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getListMatch",""+WebServices.GetListMatchFrontUrl());
        Log.e("Call_API_getListMatch",""+GAME_ID+":"+gameId);
        Log.e("Call_API_getListMatch",""+ATYPE+":"+type);
    }

    public void getMatchContestListFront(MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MATCH_CONTEST_FRONT,
                WebServices.GetMatchContestListFrontUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_API_getMatchContes",""+WebServices.GetMatchContestListFrontUrl());
        Log.e("Call_API_getMatchContes",""+MATCH_ID+""+matchModel.getMatchid());
    }

///View More
    public void getMatchContestListFrontViewMore(String getMatchID,MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MATCH_CONTEST_FRONT_VIEWMORE,
                WebServices.GetMatchContestListFrontViewMoreUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.addParam("matchcontestid", getMatchID);
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_API_getMatchContes2",""+WebServices.GetMatchContestListFrontViewMoreUrl());
        Log.e("Call_API_getMatchContes2",""+MATCH_ID+""+matchModel.getMatchid());
    }

    public void getPlayerType(String geme_id, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_PLAYER_TYPE,
                WebServices.GetPlayerTypeUrl(), WebRequest.POST_REQ);
        webRequest.addParam(GAME_ID, geme_id);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getPlayerType",""+WebServices.GetPlayerTypeUrl());
    }

    public void getPlayersList(String match_id, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_PLAYERS,
                WebServices.GetPlayersUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, match_id);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getPlayersList",""+WebServices.GetPlayersUrl());
    }

    public void getJoinedTeam(MatchModel matchModel, String contestId, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_JOINED_TEAM,
                WebServices.GetJoinedTeamUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.addParam(POOL_CONTEST_ID, contestId);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getJoinedTeam",""+WebServices.GetJoinedTeamUrl());
        Log.e("Call_API_getJoinedTeam",""+MATCH_ID+""+matchModel.getMatchid());
        Log.e("Call_API_getJoinedTeam",""+POOL_CONTEST_ID+""+contestId);
    }

    /// Call JoinContest /////////////////////

    public void getAllTeam(MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_ALL_TEAM,
                WebServices.GetAllTeamUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_API_getAllTeam",""+WebServices.GetAllTeamUrl());
        Log.e("Call_API_Request",""+MATCH_ID);
        Log.e("Call_API_Request",""+ID_ALL_TEAM);
        Log.e("Call_API_Request",""+matchModel.getMatchid());
    }

    public void getTeamPlayers(TeamModel teamModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_TEAM_PLAYERS,
                WebServices.GetTeamPlayersUrl(), WebRequest.POST_REQ);
        webRequest.addParam(TEAMID, String.valueOf(teamModel.getId()));
        webRequest.addExtra(DATA, teamModel);
        webRequest.send(context, webServiceResponseListener);
        Log.e("TEAMID",""+TEAMID+String.valueOf(teamModel.getId()));
        Log.e("TEAMID",""+TEAMID+WebServices.GetTeamPlayersUrl());
    }

    public void editPlayers(TeamModel teamModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_EDIT_PLAYERS,
                WebServices.GetTeamPlayersUrl(), WebRequest.POST_REQ);
        webRequest.addParam(TEAMID, String.valueOf(teamModel.getId()));
        webRequest.addExtra(DATA, teamModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void cloneTeam(TeamModel teamModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CLONE_TEAM,
                WebServices.GetTeamPlayersUrl(), WebRequest.POST_REQ);
        webRequest.addParam(TEAMID, String.valueOf(teamModel.getId()));
        webRequest.addExtra(DATA, teamModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void createTeam(CreateTeamRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CREATE_TEAM,
                WebServices.CreateTeamUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_createTeam",""+WebServices.CreateTeamUrl());
    }

    public void updateTeam(CreateTeamRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPDATE_PLAYERS,
                WebServices.UpdateTeamUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_updateTeam",""+WebServices.UpdateTeamUrl());
    }

    public void getContestDetail(ContestDetailRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CONTEST_DETAIL,
                WebServices.ContestDetailUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getContestDetail",""+WebServices.ContestDetailUrl());
    }

    public void getContestJoinedTeamsAll(ContestDetailRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_JOINED_CONTENTS_TEAM_ALL,
                WebServices.GetContestJoinedTeamsAll(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
        Log.e("API_getJoinedTeamsAll",""+WebServices.GetContestJoinedTeamsAll());
    }

    public void getMyMatches(String gameId, String type, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MY_MATCHES,
                WebServices.GetMyMatchesUrl(), WebRequest.POST_REQ);
        webRequest.addParam(GAME_ID, gameId);
        webRequest.addParam(ATYPE, type);
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getMyMatches",""+WebServices.GetMyMatchesUrl());
        Log.e("API_getMyMatches",""+GAME_ID+""+gameId);
        Log.e("API_getMyMatches",""+ATYPE+""+type);
    }

    public void updateProfile(UpdateProfileRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPDATE_PROFILE,
                WebServices.UpdateUserProfileUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void updatePanCard(UpdatePenCardRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPDATE_PEN_CARD,
                WebServices.UpdatePenCardUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getPanCard(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_PEN_CARD,
                WebServices.GetPenCardUrl(), WebRequest.POST_REQ);
        webRequest.addParam(DATA, "123456789");
        webRequest.send(context, webServiceResponseListener);
    }

    public void getWalletDetailUrl(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_USER_BALANCE,
                WebServices.GetUserBalanceUrl(), WebRequest.POST_REQ);
        webRequest.addParam(DATA, "123456789");
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getWalletDetailUrl",""+WebServices.GetUserBalanceUrl());
    }

    public void getJoinedContests(MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_JOINED_CONTEST,
                WebServices.GetJoinedContestUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getJoinedContests",""+WebServices.GetJoinedContestUrl());
        Log.e("API_getJoinedContests",""+MATCH_ID);
        Log.e("API_getJoinedContests",""+matchModel.getMatchid());
    }


    public void getMatchStatus(MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(MATCH_STATUS_UPCOMING,
                WebServices.GetUpcomingMatchStatusUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, matchModel.getMatchid());
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getMatchStatus",""+WebServices.GetUpcomingMatchStatusUrl());
        Log.e("API_getMatchStatus",""+MATCH_ID);
        Log.e("API_getMatchStatus",""+matchModel.getMatchid());
    }





    public void getJoinedContestss(String getmatchid,WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_JOINED_CONTEST,
                WebServices.GetJoinedContestUrl(), WebRequest.POST_REQ);
        webRequest.addParam(MATCH_ID, getmatchid);
        webRequest.send(context, webServiceResponseListener);

        Log.e("API_getJoinedContests",""+WebServices.GetJoinedContestUrl());
        Log.e("API_getJoinedContests",""+MATCH_ID);
        Log.e("API_getJoinedContests",""+getmatchid);
    }

    public void getCmsPages(String data, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.GetCmsPagesUrl(), data);
        WebRequest webRequest = new WebRequest(ID_GET_CMS_PAGES, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
    }

    public void switchTeam(SwitchTeamRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_SWITCH_TEAM,
                WebServices.SwitchTeamUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    ///Send JoinContest Request

    public void preJoinContest(JoinContestRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_PRE_JOIN_CONTEST,
                WebServices.GetJoinContestUrl(),
                WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void joinContest(JoinContestRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_JOIN_CONTEST,
                WebServices.GetJoinContestUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getLiveScore(String match_id, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.LiveScoreUrl(), match_id);
        WebRequest webRequest = new WebRequest(ID_LIVE_SCORE, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getFantasyScore(MatchModel matchModel, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.FantasyScoreUrl(), matchModel.getMatchid());
        WebRequest webRequest = new WebRequest(ID_FANTASY_SCORE, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);

    }

    public void sendVerifyEmail(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_SEND_VERIFY_EMAIL,
                WebServices.SendVerifyEmailUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
    }

    public void getProfileDetail(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_USER_PROFILE,
                WebServices.GetProfileDetailUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
    }

    public void updateBankDetail(BankRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_BANK_UPDATE,
                WebServices.UpdateBankDetailsUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);

    }

    public void getBankDetail(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_GET_BANK_DETAIL,
                WebServices.GetBankDetailsUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);

    }

    public void withdrawRequest(String amount, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_WITHDRAW_REQUEST,
                WebServices.WithdrawRequestUrl(), WebRequest.POST_REQ);
        webRequest.addParam("amount", amount);
        webRequest.send(context, webServiceResponseListener);
        Log.e("API_withdrawRequest",""+webRequest);
    }

    public void sociallogin(SocialRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_SOCIAL_LOGIN,
                WebServices.GetSocialloginUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);

       Log.e("LoginURL",""+WebServices.GetSocialloginUrl());
       Log.e("LoginURL",""+WebServices.GetSocialloginUrl());
    }

    public WebRequest getLiveScoreAll(String value, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.GetLiveScoreBoardUrl(), value);
        WebRequest webRequest = new WebRequest(ID_LIVE_SCORE_BOARD, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
        return webRequest;
    }

    public WebRequest getReferCodeUrl(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_REFER_CODE, WebServices.GetReferCodeUrl(), WebRequest.POST_REQ);
        webRequest.addParam("search", "");
        webRequest.send(context, webServiceResponseListener);
        return webRequest;
    }

    public WebRequest walletRecharge(DepositAmountRequestModel requestModel,
                                     WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_ADD_BALANCE, WebServices.WalletAddBalanceUrl(),
                WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_walletRecharge",""+webRequest);
        Log.e("Call_walletRecharge",""+ID_ADD_BALANCE);
        Log.e("API_walletRecharge",""+WebServices.WalletAddBalanceUrl());
        return webRequest;
    }

    public WebRequest getPromoCodeUrl(String amount, String promoCode,
                                      WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_PROMO_CODE, WebServices.GetPromoCodeUrl(),
                WebRequest.POST_REQ);
        webRequest.addParam("amount", amount);
        webRequest.addParam("pcode", promoCode);
        webRequest.addExtra(DATA, promoCode);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_getPromoCodeUrl",""+webRequest);
        Log.e("Call_getPromoCodeUrl",""+ID_PROMO_CODE);
        Log.e("API_getPromoCodeUrl",""+WebServices.GetPromoCodeUrl());
        return webRequest;
    }


    public WebRequest getUserStatesUrl(String getState, String getUserDateOfbirth,
                                      WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_USER_STATE, WebServices.GetUserPayStatusUrl(),
                WebRequest.POST_REQ);
        webRequest.addParam("state", getState);
        webRequest.addParam("dob", getUserDateOfbirth);
        webRequest.addExtra(DATA, getUserDateOfbirth);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_getPromoCodeUrl",""+webRequest);
        Log.e("Call_getPromoCodeUrl",""+ID_USER_STATE);
        Log.e("API_getPromoCodeUrl",""+WebServices.GetUserPayStatusUrl());
        return webRequest;
    }


    public void getPayStatus(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_USER_STATUS_PAY,
                WebServices.GetPayStatusUrl(), WebRequest.POST_REQ);
        webRequest.addParam("state", "state");
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_UserPayMentStatus",""+WebServices.GetPayStatusUrl());
    }


    public WebRequest getScoreBoardUrl(MatchModel matchModel, String scoreType, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.ScoreBoardUrl(), matchModel.getMatchid(), scoreType);
        WebRequest webRequest = new WebRequest(ID_SCORE_BOARD, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
        return webRequest;
    }

    public void updateNotiStatus(NotiStatusRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPDATE_NOTI_STATUS,
                WebServices.GetNotificationUpdateUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getLiveData(String gameId, String type, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.GetLiveDataUrl(), gameId, type);
        WebRequest webRequest = new WebRequest(ID_LIVE_DATA, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getPlayerDetail(String game, String gameType, PlayerModel playerModel, String mtype,
                                WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.GetPlayerDetailsUrl(), game);
        WebRequest webRequest = new WebRequest(ID_PLAYER_DETAIL, URL, WebRequest.POST_REQ);
        webRequest.addParam(PLAYER_ID, playerModel.getPid());
        webRequest.addParam(GAME_TYPE_ID, gameType);
        webRequest.addParam(MATCH_TYPE_ID, mtype);
        webRequest.addParam(MATCH_ID, playerModel.getMatchid());
        webRequest.send(context, webServiceResponseListener);
    }

   /* public void getAppSettings(String devicetoken, String atype, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_APP_SETTINGS, WebServices.GetAppSettingsUrl(), WebRequest.POST_REQ);
        webRequest.addParam("devicetoken", devicetoken);
        webRequest.addParam("atype", atype);
        webRequest.send(context, webServiceResponseListener);

        Log.e("Call_devicetoken",""+devicetoken);
        Log.e("Call_atype",""+atype);
    }*/

    public void getPlayerPointsDetail(String gameType, String match_id, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.GetPlayerPointsDetailsUrl(), gameType, match_id);
        WebRequest webRequest = new WebRequest(ID_PLAYER_POINTS_DETAILS, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);

    }

    public void getResultsMatches(TransactionRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MATCH_FRONT,
                WebServices.GetListMatchFrontUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getMyMatchesResult(TransactionRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MY_MATCHES,
                WebServices.GetMyMatchesUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getTransactions(TransactionRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_MY_TRANSACTION,
                WebServices.GetTransactionUrl(), WebRequest.POST_REQ);
//        webRequest.addParam(GAME_ID, String.valueOf(userModel.getId()));
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getNotification(TransactionRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_NOTIFICATION,
                WebServices.GetNotificationUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void checkIfscCode(String ifsc,String userName, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CHECK_IFSC,
                WebServices.CheckIFSC(), WebRequest.POST_REQ);
        webRequest.addParam("ifsccode", ifsc);
        webRequest.addParam("acholdername", userName);
        webRequest.send(context, webServiceResponseListener);

    }

    public WebRequest getFullScoreBoardUrl(String match_id, String scoreType, WebServiceResponseListener webServiceResponseListener) {
        String URL = String.format(WebServices.ScoreBoardUrl(), match_id, scoreType);
        WebRequest webRequest = new WebRequest(ID_SCORE_BOARD, URL, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
        return webRequest;
    }

    public void deleteAllNotification(String deleteAll, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_DELETE_ALL,
                WebServices.GetNotificationUpdateUrl(), WebRequest.POST_REQ);
        webRequest.addParam("atype", deleteAll);
        webRequest.send(context, webServiceResponseListener);
    }

    public void getLeaderboardCount(String matchid, String poolcontestid, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_LEADERBOARD_COUNT,
                WebServices.GetLeaderBoardCountUrl(), WebRequest.POST_REQ);
        webRequest.addParam("matchid", matchid);
        webRequest.addParam("poolcontestid", poolcontestid);
        webRequest.send(context, webServiceResponseListener);
    }


    public void getPrizePoolBreakUp(PrizePoolRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_PRIZE_POOL_BREAKUP,
                WebServices.PrivateContestBreakUptUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void createPrivateContest(JoinContestRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CREATE_PRIVATE_CONTEST,
                WebServices.CreatePrivateContestUrl(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }

    public void checkInviteCode(InviteCodeRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_CHECK_INVITE_CODE,
                WebServices.CheckInviteCode(), WebRequest.POST_REQ);
        webRequest.setRequestModel(requestModel);
        webRequest.addExtra(DATA, requestModel);
        webRequest.send(context, webServiceResponseListener);
    }


    public void getMyInfUpcomingDetails(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_UPCOMING_INF_CODE,
                WebServices.GetInfluencerMatchesUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_GetInfluencerUrl",""+WebServices.GetInfluencerMatchesUrl());
    }


    public void getInfUpcomingList(TransactionRequestModel requestModel, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_USER_INFLUENCER_ID,
                WebServices.GetInfluencerListUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.setRequestModel(requestModel);
        webRequest.send(context, webServiceResponseListener);
        Log.e("Call_GetInfluencerUrl",""+WebServices.GetInfluencerListUrl());
    }

    public void getMyInfWalletDetails(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(USER_INFLUENCER_DETAILS,
                WebServices.GetInfluencerWalletDetailsUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
    }

    public void getScratchCardDetails(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(USER_SCRATCH_DETAILS,
                WebServices.GetScratchCardDetailsUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
    }


    public void getMyMatchCount(WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(MY_MATCHCOUNT_REQUEST,
                WebServices.GetgetMyMatchCountUrl(), WebRequest.POST_REQ);
        webRequest.addParam("123", "123");
        webRequest.send(context, webServiceResponseListener);
    }

    public void getAppSettings(String devicetoken, String atype, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(ID_APP_SETTINGS, WebServices.GetAppSettingsUrl(), WebRequest.POST_REQ);
        webRequest.addParam("devicetoken", devicetoken);
        webRequest.addParam("atype", atype);
        webRequest.send(context, webServiceResponseListener);

        Log.e("devicetoken",""+devicetoken);
        Log.e("atype",""+atype);

    }

    public void getScratchCardRequest(String matchid, String poolcontestid, String type, WebServiceResponseListener webServiceResponseListener) {
        WebRequest webRequest = new WebRequest(USER_SCRATCH_REQUEST,WebServices.GetScratchCardRequestUrl(), WebRequest.POST_REQ);
        webRequest.addParam("matchid", matchid);
        webRequest.addParam("poolcontestid",poolcontestid);
        webRequest.addParam("type",type);
        webRequest.send(context,  webServiceResponseListener);
    }

    public void getContactusDetails(WebServiceResponseListener webServiceResponseListener){
        WebRequest webRequest =new WebRequest(CONTACT_US_DETAILS,WebServices.GetContactUSDetailsUrl(),WebRequest.POST_REQ);
        webRequest.addParam("slug","support");
        Log.e("Call_url",""+WebServices.GetContactUSDetailsUrl());
        webRequest.send(context,webServiceResponseListener);
    }


}
