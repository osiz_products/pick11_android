#include <jni.h>
#include <string>
#include "include/main.h"

extern "C" {

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetDomain(
        JNIEnv *env,
        jobject obj) {
    std::string data = getDomain(env);
    return env->NewStringUTF(data.c_str());
    }

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveScoreUrl(
            JNIEnv *env,
            jobject obj) {
        std::string data = getLiveScoreUrl(env);
        return env->NewStringUTF(data.c_str());
        }

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserLoginUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "userlogin";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_PlayStoreVersionUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "appversion/android";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserRegisterUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "userregister";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_userVerifyOtp(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "otpverify";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserResendOtpUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "resendotp";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserForgotPasswordUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "resendotp";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserResendForgotPasswordOtpUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "resendotp";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserForgotPasswordVerifyUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "resetpassword";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserChangePasswordUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/changepassword";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetCountryUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getcountry";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetGameTypeUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getgametype";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPayStatusUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/checkKYCstatus";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetListMatchFrontUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/listmatchesfront";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestListFrontUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/matchcontestlistfront";
    return env->NewStringUTF(data.c_str());
}


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestListFrontViewMoreUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getallmatchcontestlistfront";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerTypeUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getplayertype";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayersUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getmatchteamfront";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetAllTeamUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getuserteam";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetSliderImagesUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "slider";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetTeamPlayersUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getuserteamplayer";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CreateTeamUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/createteamuser";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateTeamUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/updateteamuser";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_ContestDetailUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getcontestdetails";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMyMatchesUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/mymatches";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetTransactionUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/gettransactions";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateUserProfileUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/userprofile";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetStatusUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getstate";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinContestUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/joincontestnew";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinedContestUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getjoinedcontest";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUpcomingMatchStatusUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getMatchStatus";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_AddBalanceUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/addbalance";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdatePenCardUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/updatepancard";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPenCardUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getpancard";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUserBalanceUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getuserbalance";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetContestJoinedTeamsAll(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getcontestjoinedteamsall";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetCmsPagesUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "cmspages/%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_SwitchTeamUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/switchteam";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinedTeamUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getjoinedteam";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetSocialloginUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "sociallogin";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WalletAddBalanceUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getpaymentlink";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_LiveScoreUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getLiveScoreUrl(env)+"match?matchid=%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_FantasyScoreUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getFantasyScoreUrl(env) +"#/%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_SendVerifyEmailUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/sendverifyemail";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetProfileDetailUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getprofile";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateBankDetailsUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/updatebankdetails";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetBankDetailsUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getbankdetails";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WithdrawRequestUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/withdrawreq";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetNotificationUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getnotification";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestPdf(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "leaderboarddownload/%s/%S";//match_id, contest_id
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveScoreBoardUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getLiveScoreUrl(env) + "getmatchesscore/%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetReferCodeUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getrefercode";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPromoCodeUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/promocode/validchk";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUserPayStatusUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/quickcheck";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetNotificationUpdateUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/notificationupdate";
    return env->NewStringUTF(data.c_str());
}


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_ScoreBoardUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getmatchdata?matchid=%s&responsetype=%s";//match_id /scoreType
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WebScoreUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getFantasyScoreUrl(env) + "/#/fullscorecardmb/%s/%s";//gameType /match_id
    return env->NewStringUTF(data.c_str());
}

//http://192.168.1.17/brfantasyfull/public/getmatchdata?matchid=irezim_2019_t20_02&responsetype=live
JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveDataUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "getmatchdata?matchid=%s&responsetype=%s";//match_id /scoreType
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_FaqUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getFantasyScoreUrl(env) +"#/%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerDetailsUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/getplayerpoints%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetAppSettingsUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/appsettings";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerPointsDetailsUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = "https://cron.pick11.net/getPlayerpoints%s?matchid=%s";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CheckIFSC(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/checkifsc";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLeaderBoardCountUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/leaderboardcount";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_PrivateContestBreakUptUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/pvtcnstprzbrk";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CreatePrivateContestUrl(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/createpvtcntst";
    return env->NewStringUTF(data.c_str());
}


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CheckInviteCode(
        JNIEnv *env,
        jobject obj) {
    std::string data = getBaseUrl(env) + "frontapi/pvtcntstcheck";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerMatchesUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getInfluencerById";
    return env->NewStringUTF(data.c_str());
}


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerListUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getUsersByInfluencerId";
    return env->NewStringUTF(data.c_str());
}


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerWalletDetailsUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getUserDetail";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetScratchCardDetailsUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getUsersAvailableScratchCard";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetScratchCardRequestUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getScratchCard";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetgetMyMatchCountUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "frontapi/getMyMatchCount";
    return env->NewStringUTF(data.c_str());
}

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetContactUSDetailsUrl(
        JNIEnv *env,
        jobject obj) {
  std::string data = getBaseUrl(env) + "getcmspage";
    return env->NewStringUTF(data.c_str());
}


}



