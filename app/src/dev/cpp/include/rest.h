#include <jni.h>

#ifndef REST_NATIVE_LIB_H
#define REST_NATIVE_LIB_H

#endif //REST_NATIVE_LIB_H
extern "C" {

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetDomain(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveScoreUrl(
        JNIEnv *env,
        jobject obj);


JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserLoginUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_PlayStoreVersionUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserRegisterUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_userVerifyOtp(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserResendOtpUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserForgotPasswordUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserResendForgotPasswordOtpUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserForgotPasswordVerifyUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UserChangePasswordUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetCountryUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetGameTypeUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPayStatusUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetListMatchFrontUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestListFrontUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestListFrontViewMoreUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerTypeUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayersUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetAllTeamUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetSliderImagesUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetTeamPlayersUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CreateTeamUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateTeamUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_ContestDetailUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMyMatchesUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetTransactionUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateUserProfileUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetStatusUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinContestUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinedContestUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUpcomingMatchStatusUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_AddBalanceUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdatePenCardUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPenCardUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUserBalanceUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetContestJoinedTeamsAll(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetCmsPagesUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_SwitchTeamUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetJoinedTeamUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetSocialloginUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WalletAddBalanceUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_LiveScoreUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_FantasyScoreUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_SendVerifyEmailUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetProfileDetailUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_UpdateBankDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetBankDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WithdrawRequestUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetNotificationUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetMatchContestPdf(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveScoreBoardUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetReferCodeUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPromoCodeUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetUserPayStatusUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetNotificationUpdateUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_ScoreBoardUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WebScoreUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveDataUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_FaqUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_ScoreBoardUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_WebScoreUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLiveDataUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_FaqUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetAppSettingsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetPlayerPointsDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CheckIFSC(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetLeaderBoardCountUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_PrivateContestBreakUptUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_CreatePrivateContestUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerMatchesUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerListUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetInfluencerWalletDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetScratchCardDetailsUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetgetMyMatchCountUrl(
        JNIEnv *env,
        jobject obj);

JNIEXPORT jstring JNICALL Java_com_rest_WebServices_GetContactUSDetailsUrl(
        JNIEnv *env,
        jobject obj);


}

