#include <jni.h>
#include <string>

extern "C" {

std::string getDomain(
        JNIEnv *env) {
   std::string data = "https://api.pick11.net/";

    return data;
}

std::string getBaseUrl(
        JNIEnv *env) {
    std::string data = getDomain(env) + "public/";
    return data;
}

std::string getLiveScoreUrl(
        JNIEnv *env) {
    std::string data = "https://web.pick11.net/";
    return data;
}

std::string getFantasyScoreUrl(
        JNIEnv *env) {
    std::string data = "https://web.pick11.net/";
    return data;
}

}