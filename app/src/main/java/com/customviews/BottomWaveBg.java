package com.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import com.pickeleven.R;



public class BottomWaveBg extends Drawable {

    Paint mPaint;
    Rect rectBound;
    Context context;


    public BottomWaveBg(Context context) {
        this.context = context;
        inIt();
    }


    private void inIt () {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        try {
            TypedArray typedArray = context.obtainStyledAttributes(R.styleable.AppTheme);
            int color = typedArray.getColor(R.styleable.AppTheme_bottom_wave_color, 0xff000000);
            mPaint.setColor(color);
            typedArray.recycle();
        } catch (Resources.NotFoundException ignore) {

        }
    }

    @Override
    public void draw (Canvas canvas) {
        Path p = generatePath();
        canvas.drawPath(p, mPaint);
    }

    @Override
    public void setAlpha (int alpha) {
        if (mPaint != null) {
            mPaint.setAlpha(alpha);
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter (ColorFilter colorFilter) {
        if (mPaint != null) {
            mPaint.setColorFilter(colorFilter);
            invalidateSelf();
        }
    }

    @Override
    public int getOpacity () {
        return PixelFormat.UNKNOWN;
    }

    @Override
    protected void onBoundsChange (Rect bounds) {
        rectBound = new Rect(bounds);
        super.onBoundsChange(rectBound);
    }

    @Override
    public boolean getPadding (Rect padding) {
        // TODO Auto-generated method stub
        return true;
    }

    private Path generatePath () {
        float curveStartFrom = 0.18f;
        float curveEndTo = 0.35f;
        float curveMidPoint = ((curveStartFrom + curveEndTo) * 0.5f);

        float curvedHeight = 0.14f;
        float curvedMidHeight = curvedHeight * 0.3f;

        RectF rectF = new RectF(rectBound);
        Path p = new Path();
        p.moveTo(0, 0);
        p.lineTo(0, rectF.height());

        p.lineTo(rectF.width() * curveStartFrom, rectF.height());
        float firstMidPoint = ((curveStartFrom + curveMidPoint) * 0.5f);

        p.quadTo(rectF.width() * ((curveStartFrom + firstMidPoint) * 0.5f), rectF.height(),
                rectF.width() * firstMidPoint, rectF.height() * (1 - curvedMidHeight));

        float secMidPoint = ((curveMidPoint + curveEndTo) * 0.5f);
        p.quadTo(rectF.width() * curveMidPoint, rectF.height() * (1 - curvedHeight),
                rectF.width() * secMidPoint, rectF.height() * (1 - curvedMidHeight));


        p.quadTo(rectF.width() * ((curveEndTo + secMidPoint) * 0.5f), rectF.height(),
                rectF.width() * curveEndTo, rectF.height());

        p.lineTo(rectF.width(), rectF.height());
        p.lineTo(rectF.width(), 0);
        p.close();
        return p;
    }


}
