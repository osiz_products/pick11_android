package com.appupdater;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.pickeleven.R;
import com.rest.WebServices;

public class DialogAppUpdater extends Dialog implements View.OnClickListener {
    static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=%s&hl=%s";
    private Context context;
    private TextView tv_update, tv_later;
    private TextView tv_message_data,tv_version_data;
    private String msg = "";
    private AppUpdatemodal appUpdatemodal;
    private DialogAppUpdaterListener dialogAppUpdaterListener;

    public void setDialogAppUpdaterListener(DialogAppUpdaterListener dialogAppUpdaterListener) {
        this.dialogAppUpdaterListener = dialogAppUpdaterListener;
    }

    public DialogAppUpdater(Context context, AppUpdatemodal appUpdatemodal) {
        super(context);
        this.context = context;
        this.appUpdatemodal = appUpdatemodal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
       /* if (appUpdatemodal != null) {
            msg = String.format(getContext().getString(R.string.text_app_update),
                    appUpdatemodal.getVersionName());
        }*/
       // Log.e("GET_Features",""+appUpdatemodal.getFeatures());

        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflate.inflate(R.layout.dialog_app_updater, null);
        setContentView(layout);

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;

        setTitle(null);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setOnCancelListener(null);

        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if (msg != null && !msg.trim().isEmpty()) {
                    tv_message_data.setText(Html.fromHtml(msg));
                }
            }
        });

       //  Log.e("callData",""+appUpdatemodal.getFeatures());

        tv_update = (TextView) findViewById(R.id.tv_update);
        tv_later = (TextView) findViewById(R.id.tv_later);
        tv_message_data = (TextView) findViewById(R.id.tv_message_data);
        tv_version_data = (TextView) findViewById(R.id.tv_version_data);

        tv_message_data.setText(appUpdatemodal.getFeatures());
        tv_update.setOnClickListener(this);
        tv_later.setOnClickListener(this);
        tv_version_data.setText("App current version "+appUpdatemodal.getPreviousVersion()+ " update to new version "+appUpdatemodal.getVersion());
        tv_message_data.setText(appUpdatemodal.getFeatures());
        /*if (appUpdatemodal != null) {
            if (appUpdatemodal.isForceUpdate()) {

                tv_later.setVisibility(View.GONE);
            } else {
                tv_later.setVisibility(View.VISIBLE);
            }
        }*/

        checkForUpdate(WebServices.PlayStoreVersionUrl());
    }

    private void checkForUpdate(String playStoreVersionUrl) {
   }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_later) {
            this.dismiss();
        } else if (view.getId() == R.id.tv_update) {
            dialogAppUpdaterListener.onUpdateClick();
            this.dismiss();
        }
    }

    public interface DialogAppUpdaterListener{
        void onUpdateClick();
    }
}
