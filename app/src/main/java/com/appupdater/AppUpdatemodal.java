package com.appupdater;

public class AppUpdatemodal {
    private String versionName;
    private int versionCode = 0;
    private String releaseNotes;
    private String appurl;
    private String forceUpdate;
    private String features;
    private String previousVersion;
    private String version;

    public AppUpdatemodal() {
    }

    public AppUpdatemodal(String latestVersionName) {
        this.versionName = latestVersionName;
    }

    public String getVersionName() {
        return versionName == null ? "" : versionName;
    }

    public Integer getVersionNameInteger() {
        try {
            return new Integer(versionName);
        } catch (Exception e) {

        }
        return null;

    }

    public String getFormatedVersionName() {
        StringBuilder stringBuilder = new StringBuilder();

        for (char cs : versionName.toCharArray()) {
            if (stringBuilder.length() == 0) {
                stringBuilder.append(cs);
            } else {
                stringBuilder.append(".").append(cs);
            }
        }
        return stringBuilder.toString();
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getReleaseNotes() {
        return releaseNotes;
    }

    public void setReleaseNotes(String releaseNotes) {
        this.releaseNotes = releaseNotes;
    }

    public String getAppurl() {
        return appurl;
    }

    public void setAppurl(String appurl) {
        this.appurl = appurl;
    }

    public String getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(String forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public boolean isForceUpdate() {
        return getForceUpdate().equalsIgnoreCase("1");
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (versionName != null) {
            stringBuffer.append("VersionName=" + versionName);
        }
        if (String.valueOf(versionCode) != null) {
            stringBuffer.append("Versioncode=" + String.valueOf(versionCode));
        }
        if (releaseNotes != null) {
            stringBuffer.append("ReleaseNotes=" + releaseNotes);
        }

        return stringBuffer.toString();
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getPreviousVersion() {
        return previousVersion;
    }

    public void setPreviousVersion(String previousVersion) {
        this.previousVersion = previousVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
