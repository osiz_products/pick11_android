package com.appupdater;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.ui.MyApplication;
import com.app.ui.main.dashboard.DashboardActivity;
import com.google.gson.Gson;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebServiceResponseListener;
import com.permissions.PermissionHelperNew;
import com.pickeleven.R;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import static com.appupdater.AppUpdateUtils.printLog;

public class AppUpdateChecker implements WebServiceResponseListener {

    Context context;
    onAppUpdateAvaliable onAppUpdateAvaliable;
    String version;
    AppUpdatemodal appUpdatemodal;
    DialogAppUpdater dialogAppUpdater;
    Dialog alertDialogProgressBar;
    String updateUrl;
    String features;
    String previousVersion;
    String appVersion;

    ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private long fileSize = 0;

    public void setOnAppUpdateAvaliable(AppUpdateChecker.onAppUpdateAvaliable onAppUpdateAvaliable) {
        this.onAppUpdateAvaliable = onAppUpdateAvaliable;
    }

    public AppUpdateChecker(Context context, onAppUpdateAvaliable onAppUpdateAvaliable) {
        this.context = context;
        this.onAppUpdateAvaliable = onAppUpdateAvaliable;
        getVersionCode();
    }

    private void getVersionCode() {

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
            Log.e("GET_applicationVersion",""+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void checkForUpdate(String url) {
        Log.e("Call","AppUpdate");
        String package_name = AppUpdateUtils.getAppPackageName(context);
        getPlaystoreAppVersion(url, package_name, this);

        Log.e("CallGet",""+package_name);
        Log.e("CallGet",""+url);
    }

    private void getPlaystoreAppVersion(String url, String appId, WebServiceResponseListener webServiceResponseListener) {
        url = String.format(url, appId);
        WebRequest webRequest = new WebRequest(1, url, WebRequest.GET_REQ);
        webRequest.send(context, webServiceResponseListener);
    }

    @Override
    public void onWebRequestCall(WebRequest webRequest) {

    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        String response = webRequest.getResponseString();
        Log.e("GET_RESPONS22",""+response);

        AppUpdatemodal latestAppUpdatemodal = parsePlayStoreResponse(response);
        if (latestAppUpdatemodal != null) {
            printLog(latestAppUpdatemodal.toString());
            if (isUpdateAvailable(latestAppUpdatemodal)) {
                if (onAppUpdateAvaliable != null) {
                    onAppUpdateAvaliable.appUpdateAvaliable(latestAppUpdatemodal);
                }
            }
        }
    }

    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {

    }

    private AppUpdatemodal parsePlayStoreResponse(String response) {
        if (response != null && !response.trim().isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                Log.e("GET_RESPONS",""+jsonObject);

                if (jsonObject.getBoolean("error")) {
                    return null;
                }
                updateUrl = jsonObject.getString("downloadLink");
                String versionName = jsonObject.getString("data");
                JSONObject getAppDetails = jsonObject.getJSONObject("data");
                String name = getAppDetails.getString("appname");
                appVersion = getAppDetails.getString("version");
                features = getAppDetails.getString("features");
                previousVersion = getAppDetails.getString("previous_version");
                appUpdatemodal=new AppUpdatemodal();
                appUpdatemodal.setFeatures(features);
                appUpdatemodal.setVersion(appVersion);
                appUpdatemodal.setPreviousVersion(previousVersion);
                Log.e("GET_Features2",""+appUpdatemodal.getFeatures());

                Log.e("GetApplicationName",""+name);
                Log.e("GetApplicationNames",""+appVersion);
                Log.e("GetDownloadLink",""+updateUrl);
                Log.e("features",""+features);

                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    version = pInfo.versionName;
                    Log.e("GET_applicationVersion",""+version);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                Log.e("GetVersion1",""+appVersion);
                Log.e("GetVersion2",""+version);

                if (appVersion!=null){
                    int getMyappverSion = Integer.parseInt(appVersion.replace(".", ""));
                    int getappverSion = Integer.parseInt(version.replace(".", ""));

                    //if (appVersion == version){
                    if(getMyappverSion > getappverSion){

                        showUpdateDialog();
                    }else {
                        //showUpdateDialog();
                    }
                }

                return new Gson().fromJson(versionName, AppUpdatemodal.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void showUpdateDialog() {
        try {
            DialogAppUpdater dialogAppUpdater;
            dialogAppUpdater = new DialogAppUpdater(context,appUpdatemodal );
            dialogAppUpdater.setDialogAppUpdaterListener(new DialogAppUpdater.DialogAppUpdaterListener() {
                @Override
                public void onUpdateClick() {
                    if (PermissionHelperNew.needStoragePermission((Activity) context, new PermissionHelperNew.OnSpecificPermissionGranted() {
                        @Override
                        public void onPermissionGranted(boolean isGranted, boolean withNeverAsk, String permission, int requestCode) {
                            if (isGranted) {
                              //  downloadAPK(appUpdatemodal.getAppurl());
                                downloadAPK(updateUrl);
                             //   callFileDownloads();

                            }
                        }
                    })) {
                        return;
                    }
                   // downloadAPK(appUpdatemodal.getAppurl());
                    downloadAPK(updateUrl);
                  //  callFileDownloads();
                }
            });

            dialogAppUpdater.show();
        } catch (Exception e) {

        }

    }

    // checking how much file is downloaded and updating the filesize
    public int doOperation() {
        //The range of ProgressDialog starts from 0 to 10000
        while (fileSize <= 10000) {
            fileSize++;
            if (fileSize == 1000) {
                return 10;
            } else if (fileSize == 2000) {
                return 20;
            } else if (fileSize == 3000) {
                return 30;
            } else if (fileSize == 4000) {
                return 40; // you can add more else if
            }
         /* else {
                return 100;
            }*/
        }//end of while
        return 100;
    }//end of doOperation

    private void downloadAPK(String url) {
        // creating progress bar dialog
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(false);
        progressBar.setMessage("File downloading ..."); //please wait your file download is processing
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();

        //reset progress bar and filesize status
        progressBarStatus = 0;
        fileSize = 0;

        if (dialogAppUpdater != null && dialogAppUpdater.isShowing()) {
            dialogAppUpdater.dismiss();
        }
        String fileName = context.getResources().getString(R.string.app_name) + ".apk";
        final Uri mDestinationUri = Uri.withAppendedPath(
                Uri.fromFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)),
                fileName);

        //Delete update file if exists
        File file = new File(mDestinationUri.getPath());
        if (file.exists())
            file.delete();
       // printLog("downloadAPk", " file:" + file.getAbsolutePath());
        //set downloadmanager
        final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        //request.setDescription(getResources().getString(R.string.notification_description));
        request.setTitle(context.getResources().getString(R.string.app_name));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(mDestinationUri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);
        //printLog("downloadAPk", " downloadId:" + downloadId);
        displayUpdateProgressBar("Updating...");

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean downloading = true;
                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);

                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    //final double dl_progress = (((float) bytes_downloaded / bytes_total)) * 100;
                    final double dl_progress = (((float) bytes_downloaded / bytes_total)) * 100;

                    //progressBarStatus = doOperation();
                    progressBarStatus = (int) ((((float) bytes_downloaded / bytes_total)) * 100);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // Updating the progress bar
                    progressBarHandler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });

                    String message = ((int) dl_progress) + "% Downloading...";
                    if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_SUCCESSFUL) ||
                            (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                                    == DownloadManager.STATUS_FAILED)) {
                        downloading = false;
                    } else if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_PENDING)) {
                        message = "Updating...";
                    } else if ((cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_PAUSED)) {
                        message = ((int) dl_progress) + "% Downloading Pause";
                    }


                    //  printLog("downloadAPk", message);
                    cursor.close();

                    final String finalMessage = message;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            displayUpdateProgressBar(finalMessage);


                        }
                    });
                    if (!downloading) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                openAppInstaller(downloadId);
                                Log.e("FileDownload____","Install_App");
                            }
                        });
                    }

                }

                // performing operation if file is downloaded,
                if (progressBarStatus >= 100) {
                    // sleeping for 1 second after operation completed
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // close the progress bar dialog
                    progressBar.dismiss();
                }
            }

            private void runOnUiThread(Runnable runnable) {

                Log.e("FileDownload1____","Install_App"+ runnable );
                openAppInstaller(downloadId);

            }
        }).start();

    }


    private void openAppInstaller(long downloadId) {
       // printLog("downloadAPk", " openAppInstaller downloadId:" + downloadId);
        dismissUpdateProgressBar();
        if (dialogAppUpdater != null && dialogAppUpdater.isShowing()) {
            dialogAppUpdater.dismiss();
        }
        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uriForDownloadedFile = manager.getUriForDownloadedFile(downloadId);
        String mimeType = manager.getMimeTypeForDownloadedFile(downloadId);
      //  printLog("downloadAPk", " uriForDownloadedFile:" + uriForDownloadedFile);
     //   printLog("downloadAPk", " mimeType:" + mimeType);
        if (uriForDownloadedFile == null) return;

        Intent downloadIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            downloadIntent = new Intent(Intent.ACTION_VIEW);
            downloadIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            downloadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            downloadIntent.setDataAndType(uriForDownloadedFile, mimeType);

            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(downloadIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uriForDownloadedFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

        } else {
            downloadIntent = new Intent(Intent.ACTION_VIEW);
            downloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            downloadIntent.setDataAndType(uriForDownloadedFile, mimeType);
        }

        context.startActivity(downloadIntent);
       // getUserPrefs().clearLoggedInUser();
      //  finish();
    }

    public void dismissUpdateProgressBar() {
        if (alertDialogProgressBar != null && alertDialogProgressBar.isShowing()) {
            alertDialogProgressBar.dismiss();
        }
    }

    public void displayUpdateProgressBar(String loaderMsg) {
        if (alertDialogProgressBar != null) {
            TextView tv_loader_msg = alertDialogProgressBar.findViewById(R.id.tv_loader_msg);
            if (loaderMsg != null && !loaderMsg.trim().isEmpty()) {
                tv_loader_msg.setText(loaderMsg);
            } else {
                tv_loader_msg.setVisibility(View.GONE);
            }

            try {
                alertDialogProgressBar.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                if (!alertDialogProgressBar.isShowing())
                    alertDialogProgressBar.show();

            } catch (Exception ignore) {

            }
        }
    }


    public interface onAppUpdateAvaliable {
        void appUpdateAvaliable(AppUpdatemodal appUpdatemodal);
    }

    Boolean isUpdateAvailable(AppUpdatemodal latestAppUpdatemodal) {
        if (latestAppUpdatemodal == null) return false;
        AppUpdatemodal installedAppUpdateModal = new AppUpdatemodal();
        installedAppUpdateModal.setVersionCode(AppUpdateUtils.getAppInstalledVersionCode(context));
        installedAppUpdateModal.setVersionName(AppUpdateUtils.getAppInstalledVersionName(context));

        Boolean res = false;

      //  if (latestAppUpdatemodal.getVersionCode() != null && latestAppUpdatemodal.getVersionCode() > 0) {
        if (latestAppUpdatemodal.getVersionCode() > 0) {
            res = latestAppUpdatemodal.getVersionCode() > installedAppUpdateModal.getVersionCode();
        } else {
            Integer latestVersionName = latestAppUpdatemodal.getVersionNameInteger();
            Integer installedVersionName = installedAppUpdateModal.getVersionNameInteger();
            if (latestVersionName != null && installedVersionName != null) {
                res = latestVersionName > installedVersionName;
            }
            MyApplication.getInstance().printLog("version: ", "latestVersionName: "+latestVersionName+"  installedVersionName:  "+installedVersionName);
        }
        MyApplication.getInstance().printLog("version: ", "isUpdate Available: "+res);
        return res;
    }
}
