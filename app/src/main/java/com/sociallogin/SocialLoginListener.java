package com.sociallogin;


public interface SocialLoginListener {


    void socialLoginSuccess(SocialData socialData);

    void fbLoginPermissionError();

}
