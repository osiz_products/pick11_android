package com.utilities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;

import com.pickeleven.R;

import java.util.Calendar;


public class DatePickerUtil {

    private static final long NEXT_DAY = 24 * 60 * 60 * 1000;
    private static final long NEXT_DAY2 = 24 * 60 * 60 * 1000 * 365 * 18;

    /**
     * use for show date picker
     *
     * @param clickedView
     */
    public static void showDatePicker(Context context, final View clickedView,
                                      boolean isCurrentMin, final OnDateSelectedListener onDateSelectedListener) {

        Calendar previousSelectedCal = null;
        Calendar now = Calendar.getInstance();
        if (clickedView.getTag() != null) {
            previousSelectedCal = (Calendar) clickedView.getTag();
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                R.style.DatePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar now = Calendar.getInstance();
                        now.set(Calendar.YEAR, year);
                        now.set(Calendar.MONTH, month);
                        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        now.set(Calendar.HOUR_OF_DAY, 23);
                        now.set(Calendar.MINUTE, 59);
                        now.set(Calendar.SECOND, 59);
                        now.set(Calendar.MILLISECOND, 999);
                        if (onDateSelectedListener != null)
                            onDateSelectedListener.onDateSelected(now);


                    }
                }, previousSelectedCal == null ? now.get(Calendar.YEAR) : previousSelectedCal.get(Calendar.YEAR),
                previousSelectedCal == null ? now.get(Calendar.MONTH) : previousSelectedCal.get(Calendar.MONTH),
                previousSelectedCal == null ? now.get(Calendar.DAY_OF_MONTH) : previousSelectedCal.get(Calendar.DAY_OF_MONTH));


        if (isCurrentMin) {
            Calendar _16NovCal = Calendar.getInstance();
            _16NovCal.set(Calendar.MONTH, 10);
            _16NovCal.set(Calendar.DAY_OF_MONTH, 16);
            _16NovCal.set(Calendar.YEAR, 2018);
            _16NovCal.set(Calendar.HOUR_OF_DAY, 0);
            _16NovCal.set(Calendar.MINUTE, 0);
            _16NovCal.set(Calendar.SECOND, 0);
            _16NovCal.set(Calendar.MILLISECOND, 0);
            if (now.getTimeInMillis() < _16NovCal.getTimeInMillis()) {
                datePickerDialog.getDatePicker().setMinDate(_16NovCal.getTimeInMillis());
            } else {
                datePickerDialog.getDatePicker().setMinDate(now.getTimeInMillis() + NEXT_DAY);
            }

        } else {
            now.set(Calendar.YEAR, now.get(Calendar.YEAR) - 18);
            datePickerDialog.getDatePicker().setMaxDate(now.getTimeInMillis());
        }

        datePickerDialog.show();
    }

    public interface OnDateSelectedListener {
        void onDateSelected(Calendar calendar);
    }
}
